
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th>Message</th>
        <th style="width: 10%">Send to</th>
        <th style="width: 20%">Send Type</th>
        <th style="width: 20%">Send Date</th>
        <th class="table-text table-th">Status</th>
        <th class="table-text table-th">Action</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($notification as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td style="width: 70%">
                <?php $str_length = strlen($list['message']);?>
                @if($str_length > 216)
                    {{substr($list['message'],0,210)}}...
                @else
                    {{$list['message']}}
                @endif
            </td>
             <td>@if($list['send_to']=='all') All @elseif($list['send_to']=='sub_admin') Salon Owner @elseif($list['send_to']=='user') User @else - @endif</td>
            <td>@if($list['service_type']=='both') Both @elseif($list['service_type']=='email') Email @elseif($list['service_type']=='push') Push Notification @else - @endif</td>
            <td>{{$list['created_at']}}</td>
            <td class="table-text table-th" style="padding-top: 14px;">
                @if($list['is_read'] == 0)
                    <label class="label label-danger" style="padding:5px 8px; font-size: 14px;">Pending</label>
                @else
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">Success</label>
                @endif    
            </td>
            <td class="table-text table-th" style="width: 10%">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="View Notification" data-trigger="hover">
                        <button class="btn btn-info res-btn" type="button" data-toggle="modal" data-target="#deatil_modal{{$list['id']}}" style="height: 28px; padding-top: 3px;"><i class="fa fa-eye"></i>
                        </button>
                    </span>
                    
                    <span data-toggle="tooltip" title="Delete Notification" data-trigger="hover">
                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}" style="height: 28px; padding-top: 3px;"><i class="fa fa-trash"></i></button>
                    </span>
                </div>
            </td>
        </tr>

        <!--******************************************DETAIL VIEW MODEL******************************************-->
        
        <div id="deatil_modal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                            <h4 class="modal-title">Notification</h4>
                    </div>
                    <div class="modal-body">
                        <p style="color: #482162">{{$list['message']}}</p>
                    </div>
                </div>
            </div>
        </div>

        <!--******************************************DELETE MODEL******************************************-->
        
        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete Notification</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this Notification ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" onclick="destroy_notification({{$list['id']}})">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $notification])</div>

<script>
    function destroy_notification(id)
    {
        $.ajax({
            url:'notification/'+id,
            type:'delete', 
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'notification';
                window.location.href = new_url;
            }
        });
    }

</script>