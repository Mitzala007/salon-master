<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="message">Send notification message <span class="text-red">*</span></label>
    <div class="col-sm-10">
        {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Message','rows'=>'3', 'id'=>'notification']) !!}
        @if ($errors->has('message'))
            <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Send to <span class="text-red">*</span></label>
    <div class="col-sm-10">
        @foreach (\App\Notification::$send_type as $key => $value)
            <label>
                @if($key == 'all')
                    {!! Form::radio('send_to', $key, null, ['class' => 'flat-red','id'=>'send_to_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="send_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('send_to', $key, null, ['class' => 'flat-red','id'=>'send_to_'.$key]) !!} <span style="margin-right: 10px" id="send_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('send_to'))
            <span class="help-block">
             <strong>{{ $errors->first('send_to') }}</strong>
            </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="type" id="status_lb">Send type<span class="text-red">*</span></label>
    <div class="col-sm-10">
        @foreach (\App\Notification::$service as $key => $value)
            <label>
                @if($key == 'both')
                    {!! Form::radio('service_type', $key, null, ['class' => 'flat-red','id'=>'service_type_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="service_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('service_type', $key, null, ['class' => 'flat-red','id'=>'servce_type_'.$key]) !!} <span style="margin-right: 10px" id="service_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('service_type'))
            <span class="help-block">
             <strong>{{ $errors->first('service_type') }}</strong>
            </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>