@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$menu}}
                <small>Send</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> {{ $menu }} </a></li>
                <li class="active">Send</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
             @include ('admin.error')
            <div class="row">
                <!-- right column -->
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">SEND {{$menu}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['url' => url('admin/notification'), 'class' => 'form-horizontal','files'=>true]) !!}
                            <div class="box-body">
                                @include ('admin.notification.form')
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                    {{--<button class="btn btn-info pull-right send-notification" type="submit" onclick="send_notification()">Send</button>--}}
                                    <button class="btn btn-info pull-right send-notification" type="submit" onclick="send_notification()">Send</button>
                            </div>
                            <!-- /.box-footer -->
                        {!! Form::close() !!}
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

        @include('admin.loader')
        <section class="content">
            <div class="box box-info">
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.notification.table')
                </div>
            </div>

        </section>

    </div>

    <script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
    <script>


        /*FOR SEND NOTIFICATION*/

        {{--$(document).on('click','.send-notification',function(e){--}}
            {{--e.preventDefault();--}}
            {{--$('#load').append('<img style="position: absolute; width:80px; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');--}}
            {{--document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';--}}
            {{--document.getElementById('bodyid').style.opacity=0.5;--}}
        {{--});--}}

//        function send_notification() {
//            var notification = document.getElementById('notification').value;
//            $.ajax({
//                url:'notification',
//                type:'post',
//                data:{'message':notification},
//                success:function(data)
//                {
//                    location.reload();
//                    document.getElementById('bodyid').style.opacity=1;
//
//                }
//            });
//        }

        /*FOR PAGINATION*/

        $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

        })

        function getPagination(page)
        {
            document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
            document.getElementById('bodyid').style.opacity=0.5;
            document.getElementById('load').style.display="block";

            $.ajax({
                url : page
            }).done(function (data) {
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $("#itemlist").empty().html(data);
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }).fail(function () {
                alert('Pagination could not be loaded.');
            });
        }

        
    </script>
    
@endsection
