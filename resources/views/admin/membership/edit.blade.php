@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                Membership
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Edit</a></li>
                <li class="active">Membership</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Membership </h3>
                        </div>
                        {!! Form::model($membership, ['url' => url('admin/membership/'.$membership->id),'class' => 'form-horizontal','method' => 'patch','files'=>true]) !!}
                        <div class="box-body">

                            @include('admin.membership.form')

                        </div>

                        <div class="box-footer">
                            <a href="{{ url('admin/membership') }}" ><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection