<table id="example2" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Edit</th>
        <th>Title</th>
        <th>Amount</th>
        {{--<th>Is Recurring</th>--}}
        {{--<th>Total Recurring Occurance</th>--}}
        {{--<th>Free Trial</th>--}}
        <th>Status</th>
        {{--<th class="table-text table-th">Action</th>--}}
    </tr>
    </thead>
    <tbody id="sortable">
    <?php $count = 0;?>
       @foreach($membership as $item)
           <?php $count++; ?>
           <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $item['id'] }}" />
           <tr class="ui-state-default" id="arrayorder_{{$item['id']}}">
            <td>
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/membership/'.$item['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Membership" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
            <td>{{$item->title}}</td>
            <td>{{$item->amount}}</td>
            {{--<td>@if($item->is_recurring==1) Yes @else No @endif</td>--}}
            {{--<td>{{$item->total_recurring_occurance}}</td>--}}
            {{--<td>@if($item->free_trial==1) Yes @else No @endif</td>--}}
            <td class="table-text table-th">
                @if($item['status'] == 'active')
                    <div class="btn-group-horizontal" id="assign_remove_{{ $item['id'] }}" >
                        <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $item['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_add_{{ $item['id'] }}"  style="display: none"  >
                        <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $item['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                @endif
                @if($item['status'] == 'in-active')
                    <div class="btn-group-horizontal" id="assign_add_{{ $item['id'] }}"   >
                        <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $item['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_remove_{{ $item['id'] }}" style="display: none" >
                        <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $item['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                    </div>
                @endif
            </td>
            {{--<td>--}}
                {{--<div class="btn-group-horizontal">--}}
                    {{--<span data-toggle="tooltip" title="Delete Membership" data-trigger="hover">--}}
                        {{--<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$item['id']}}"><i class="fa fa-trash"></i></button>--}}
                    {{--</span>--}}
                {{--</div>--}}
            {{--</td>--}}
        </tr>
        <div id="myModal{{$item['id']}}" class="fade modal modal-danger" role="dialog">
            {{ Form::open(array('url' => 'admin/membership/'.$item['id'], 'method' => 'delete','style'=>'display:inline')) }}
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Delete Membership</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this Membership ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline">Delete</button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    @endforeach
    </tbody>
</table>
<input type="hidden" name="count" id="count" value="<?php echo $count;?>" />
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $membership])</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var emp_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/membership/assign')}}',
                type: "put",
                data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+emp_id).show();
                    $('#assign_add_'+emp_id).hide();
                }
            });
        });

        $('.unassign').click(function(){
            var emp_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/membership/unassign')}}',
                type: "put",
                data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+emp_id).hide();
                    $('#assign_add_'+emp_id).show();
                }
            });
        });
    });
</script>