{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title"><b>Title</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        @if(isset($membership))
          {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => ' Enter Membership','disabled']) !!}
        @else
          {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => ' Enter Membership']) !!}
        @endif
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="amount"><b>Amount</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        @if(isset($membership))
         {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Enter Amount','disabled']) !!}
        @else
         {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Enter Amount']) !!}
        @endif
        @if ($errors->has('amount'))
            <span class="help-block">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('total_recurring_occurance') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="total_recurring_occurance"><b>Total Recurring Occurance</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        @if(isset($membership))
           {!! Form::text('total_recurring_occurance', null, ['class' => 'form-control', 'placeholder' => ' Enter Total recurring occurance','disabled']) !!}
        @else
          {!! Form::text('total_recurring_occurance', null, ['class' => 'form-control', 'placeholder' => ' Enter Total recurring occurance']) !!}
        @endif
            @if ($errors->has('total_recurring_occurance'))
            <span class="help-block">
                <strong>{{ $errors->first('total_recurring_occurance') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="short_description"><b> Short Description</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder' => 'Short Description','rows'=>'10','cols'=>'80']) !!}
        @if ($errors->has('short_description'))
            <span class="help-block">
                <strong>{{ $errors->first('short_description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="description"><b>Description</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>'10','cols'=>'80', 'id'=>'editor1']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('free_trial') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="free_trial"><b>Free Trial</b><span class="text-red">*</span></label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('free_trial', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('free_trial'))
            <span class="help-block">
                <strong>{{ $errors->first('free_trial') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Membership_plan::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('is_recurring') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="is_recurring">Is Recurring</label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('is_recurring', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('is_recurring'))
            <span class="help-block">
                <strong>{{ $errors->first('is_recurring') }}</strong>
            </span>
        @endif
    </div>
</div>
