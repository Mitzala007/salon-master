<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PDF</title>
    <style>

        body{ margin: 0px auto; width: 700px;}
        .mg-table{
            text-align: center;
            font-size: 13px;
        }
        table td{ padding: 7px}

        .tb-border {
            border: 1px solid #482162;
            border-collapse: collapse;
        }

        .td-border {
            border: 1px solid #482162;
        }

        .column {
            float: left;
            width: 100%;
            padding: 5px;
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: center;
        }

        .rgt{
            text-align: right;
            font-weight: bold;
        }

        #tbl1 tr{ padding: 0px ; margin: 0px}
        #tbl1 tr td{ line-height: 20px}
    </style>
</head>
<body style="font-family: notosans">

<table style="border-bottom: 2px solid #482162; margin-bottom: 2%; width: 700px;" id="tbl1">

    <tr>
        <th colspan="3" style="width: 700px; font-size: 30px; text-align: center">{{$Booking['saloon']['title']}}</th>
    </tr>

    <tr>
        <td colspan="3" style="width: 700px; text-align: center">{{$Booking['saloon']['location']}}</td>
    </tr>
    <tr>
        <td colspan="3" style="width: 700px; text-align: center">{{$Booking['saloon']['saloon_phone']}}<br>{{$Date_time}}<br>{{$Booking['service_expert']}}</td>
    </tr>
    <tr>
        <th colspan="3" style="width: 700px; text-align: center">{{ $Booking['order'] }}</th>
    </tr>
    <tr>
        <td colspan="3" style="width: 700px; text-align: left">Customer: {{ $Booking['users']['name'] }}<br>Booking ID: {{ $Booking['booking_name'] }}</td>
    </tr>

</table>

    <table class="tb-border" style="width: 700px;">
        <tr style="color: #000;">
            <th class="mg-table td-border" style="width:50px; height: 50px;">Sr. No.</th>
            <th class="mg-table td-border" style="width:150px;">Service Name</th>
            <th class="mg-table td-border" style="width:100px;">Time Duration</th>
            <th class="mg-table td-border" style="width:95px;">Price</th>
            <th class="mg-table td-border" style="width:70px;">Discount (%)</th>
            <th class="mg-table td-border" style="width:100px;">Final Price</th>
        </tr>
        <?php $cnt = 1; $tamt = '';?>
        @foreach($Booking['Booking_details'] as $book_detail)
            <tr>
                <td class="mg-table td-border">{{$cnt}}</td>
                <td class="mg-table td-border">{{$book_detail['service_type']['title']}}</td>
                <td class="mg-table td-border">{{$book_detail['service_type']['min_duration']}} Min.</td>
                <td class="mg-table td-border">{{ number_format((float)$book_detail['charges'], 2, '.', '')}}</td>
                <td class="mg-table td-border">{{number_format((float)$book_detail['discount'], 2, '.', '')}}</td>
                <td class="mg-table td-border">{{number_format((float)$book_detail['total_charge'], 2, '.', '')}}</td>
            </tr>
            <?php $cnt++;?>
        @endforeach

        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Sub Total</span></td>
            <td class="mg-table td-border ">{{number_format((float)$Booking['sub_total'], 2, '.', '')}}</td>
        </tr>


            <?php $additional_amount = $Booking['additional_amount']; ?>
            <tr>
                <td colspan="5" class="mg-table td-border rgt"><span>Additional Amount</span></td>
                <td class="mg-table td-border">{{number_format((float)$additional_amount, 2, '.', '')}}</td>
            </tr>

            <?php
            if ($Booking['discount_amount'] == "0.00")
            {
                $discount_amount = number_format((float)$Booking['discount_amount'], 2, '.', '');
            }
            else{
                $discount_amount = "- ".number_format((float)$Booking['discount_amount'], 2, '.', '');
            }
            ?>
            <tr>
                <td colspan="5" class="mg-table td-border rgt"><span>Discount Amount</span></td>
                <td class="mg-table td-border">{{ $discount_amount }}</td>
            </tr>

            <tr>
                <td colspan="5" class="mg-table td-border rgt"><span>Tax: (0.00%)</span></td>
                <td class="mg-table td-border">{{ $Booking['tax'] }}</td>
            </tr>

        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Total Amount</span></td>
            <td class="mg-table td-border">{{number_format((float)$Booking['final_total'], 2, '.', '')}}</td>
        </tr>
</table>


    <table style="width: 700px;">

        <tr>
            <td style="width: 700px;">
                <span style="font-weight: bold">Remarks:</span>
                <span style="font-size: 14px;"> <?php if($Booking['remarks']!=""){ echo $Booking['remarks'];} else { echo "N/A"; } ?></span>
                <br>

                <span style="font-weight: bold">Payment Type:</span>
                <span style="font-size: 14px;"> <?php
                    if($Booking['payment_mode']=="1"){ echo "Cash";}
                    elseif ($Booking['payment_mode']=="2"){ echo "Card/Netbanking";}
                    elseif ($Booking['payment_mode']=="3"){ echo "Gift Card";}
                    elseif ($Booking['payment_mode']=="4"){ echo "N/A";}
                    ?>
                </span>

            </td>
        </tr>
        <tr>
            <td style="text-align: center">Thank you for visiting!!</td>
        </tr>

    </table>


<div class="footer">
    <p>Powerd by: Nail Master.Co LLC</p>
</div>
</body>
</html>