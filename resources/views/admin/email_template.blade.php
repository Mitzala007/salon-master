<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{config('siteVars.title')}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body style="color: #000000; font-size: 16px; font-weight: 400; font-family: 'Exo', sans-serif;margin: 0;">

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td align="center" valign="top"><table cellpadding="0" cellspacing="0" width="700px" style="border:thin;border-color:#FF6C00; border-style:solid;">
                <tr>
                    <td><center>
                            <img src="{{ URL::asset('assets/website/logo_master.png') }}" width="150px">
                        </center>
                        <hr style="border: 0; width: 40%; color: #183D6B; background-color: #FF6C00;	height: 2px;">
                        <div style="padding:10px 10px 10px 20px; background-color:#FF6C00; color:#ffffff; font-size:16px; font-family: 'Exo', sans-serif;"> {{ $title }} </div>
                        <div style="padding:10px 10px 10px 20px;">
                           {!! $content  !!}
                        </div>

                        <div>
                            <hr style="border: 0; width: 100%; color: #FF6C00; background-color: #FF6C00;	height: 4px;">
                            <div style="padding:10px 10px 10px 10px; color: #4D4D4D; text-align: center">Copyright © 2018 - All rights reserved</div>
                        </div>
                    </td>
                </tr>
            </table></td>
    </tr>
</table>

</body>
</html>
