<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>

        {{--<th>Id</th>--}}
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th class="table-text table-th">User Type</th>
        <!--<th>Role</th>-->
        <th class="table-text table-th">Image</th>
        <th>Deleted Date</th>
        <th class="table-text table-th">Recover</th>
        <th class="table-text table-th">Delete</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($user as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{$list['name']}} {{ !empty($list['last_name'])?$list['last_name']:""}} </td>
            <td>{{$list['email']}}</td>
            <td>{{$list['phone']}}</td>
            <td style="padding-top: 16px;" class="table-text table-th">
                <?php $plan = 0; ?>
                @if($list['membership_plan'] == '1' || $list['membership_plan'] == '2')
                    <?php $plan = $list['membership_plan']; ?>
                @elseif($list['admin_membership_plan']== '1' || $list['admin_membership_plan']=='2')
                    <?php $plan = $list['admin_membership_plan'];?>
                @endif

                @if($plan==1)
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        Salon Owner
                    </label>
                @elseif($plan==2)
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        Freelancer
                    </label>
                @else
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        User
                    </label>
                @endif
            </td>

            <td class="table-text">
                <a data-toggle="modal" data-target="#imageModal{{$list['id']}}" style="cursor: pointer;">
                @if($list['image']!="" && file_exists($list['image']))
                    <img src="{{ url($list->image) }}" width="30">
                @else
                    <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                @endif
                </a>
            </td>
            <td>
                {{ $list->deleted_at }}
            </td>

            <td class="table-text">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="User Recover" data-trigger="hover">
                        <button class="btn btn-danger res-btn" type="button" onclick="recover_user({{$list['id']}})"><i class="fa fa-reply-all"></i></button>
                    </span>
                </div>
            </td>

            <td class="table-text">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="Delete User" data-trigger="hover">
                        <button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                    </span>
                </div>
            </td>
        </tr>


        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <div class="modal-body">
                        <p>After delete user will no longer available in our system.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" onclick="destroy_user({{$list['id']}})">Delete</button>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $user])</div>

<script type="text/javascript">
    function recover_user(id)
    {
        $.ajax({
            url: '{{url('admin/deleted-user/recover')}}',
            type: "put",
            data: {'id': id,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                var new_url = 'deleted-user';
                window.location.href = new_url;
            }
        });
    }

    function destroy_user(id)
    {
        $.ajax({
            url:'deleted-user/force-delete/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'deleted-user';
                window.location.href = new_url;
            }
        });
    }

</script>
