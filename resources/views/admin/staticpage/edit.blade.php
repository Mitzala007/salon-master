@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>

                </h1>
            </h1>
            <ol class="breadcrumb">
                <li><a href=""><i class="fa fa-dashboard"></i> {{ $menu }} </a></li>
            </ol>
        </section>

        <section class="content">
            <br>
            @include ('admin.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $menu }} </h3>
                        </div>

                        {!! Form::model($staticpage, ['url' => url('admin/staticpage/' . $staticpage->id), 'method' => 'patch', 'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="box-body">
                            @include ('admin.staticpage.form')
                        </div>
                        <div class="box-footer">
                            @if($staticpage['id'] == 1)<a href="{{ url('api/staticpage/terms_and_condition') }}" target="_blank"><input type="button" class="btn btn-info" value="Preview"></a>@endif
                            @if($staticpage['id'] == 2)<a href="{{ url('api/staticpage/loyalty') }}" target="_blank"><input type="button" class="btn btn-info" value="Preview"></a>@endif
                            @if($staticpage['id'] == 3)<a href="{{ url('api/staticpage/user_manual') }}" target="_blank"><input type="button" class="btn btn-info" value="Preview"></a>@endif
                            @if($staticpage['id'] == 4)<a href="{{ url('api/staticpage/salon_manual') }}" target="_blank"><input type="button" class="btn btn-info" value="Preview"></a>@endif
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

