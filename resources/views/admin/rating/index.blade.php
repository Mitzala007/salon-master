@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" >
        <section class="content-header">
            <h1>
                {{$menu}} &nbsp;
                {{--@for($i=0; $i<$salon_id['rating']; $i++)--}}
                    {{--<i class="fa fa-star" style="color: #daa520;"></i>--}}
                {{--@endfor--}}

                {{--@for($i=0; $i<5-$salon_id['rating']; $i++)--}}
                    {{--<i class="fa fa-star" style="color: #d3d3d3;"></i>--}}
                {{--@endfor--}}
            </h1>

            <ol class="breadcrumb">
                <li><a href="{{ url('admin/service') }}"><i class="fa fa-dashboard"></i> {{$menu}} </a></li>
            </ol>
        </section>

        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none"></div>
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/service'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
{{--                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">--}}
                                {!! Form::select('salon', $salon ,null, ['id'=>'search', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                            <a href="{{ url('admin/rating') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                @include('admin.loader')
                <div class="box-body table-responsive " id="itemlist">
                    @include('admin.rating.table')
                </div>

            </div>

        </section>

    </div>

@endsection


<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>

<!-- ajax status code add custom js -->

<script type="text/javascript">
</script>
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();
        var search = $('#search').val();
        var type = $('#type').val();
        $.ajax({
            url: '{{url('admin/rating')}}',
            type: "get",
            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
//                alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>





