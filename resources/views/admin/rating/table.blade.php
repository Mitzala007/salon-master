<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th style="width: 15%">User</th>
        <th style="width: 15%">Rating</th>
        <th style="width: 60%">Review</th>
        <th class="table-text table-th">Action</th>
    </tr>
    </thead>

    <tbody id="">
    @foreach ($rating as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{$list['User']['name']}}</td>
            <td>
                @for($i=0; $i<$list['rating']; $i++)
                    <i class="fa fa-star" style="color: #daa520;"></i>
                @endfor

                @for($i=0; $i<5-$list['rating']; $i++)
                    <i class="fa fa-star" style="color: #d3d3d3;"></i>
                @endfor
            </td>
            <td>
                <?php $str_length = strlen($list['review']);?>
                @if($str_length > 136)
                    {{substr($list['review'],0,133)}}...
                @else
                    {{$list['review']}}
                @endif
            </td>
            <td class="table-text">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="View Message" data-trigger="hover">
                        <button class="btn btn-info res-btn" type="button" data-toggle="modal" data-target="#deatil_modal{{$list['id']}}" style="height: 28px; padding-top: 3px;"><i class="fa fa-eye"></i>
                        </button>
                    </span>
                </div>
            </td>
        </tr>

        <!--******************************************REVIEWS VIEW MODEL******************************************-->

        <div id="deatil_modal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['User']['name']}}</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            @for($i=0; $i<$list['rating']; $i++)
                                <i class="fa fa-star" style="color: #daa520;"></i>
                            @endfor

                            @for($i=0; $i<5-$list['rating']; $i++)
                                <i class="fa fa-star" style="color: #d3d3d3;"></i>
                            @endfor
                        </p>
                        <p style="color: #482162">{{$list['review']}}</p>
                    </div>
                </div>
            </div>
        </div>

    @endforeach

</table>
@if(!empty($rating))
<div style="text-align:right;float:right;" class="ajaxpagination"> @include('admin.pagination.limit_links', ['paginator' => $rating])</div>
@endif
<script src="{{ URL::asset('assets/dist/js/custom.js')}}"></script>

<script>

    function slideout() {
        setTimeout(function() {
            $("#responce").slideUp("slow", function() {
            });

        }, 3000);
    }

    $("#responce").hide();
    $( function() {
        $( "#sortable" ).sortable({opacity: 0.9, cursor: 'move', update: function() {
            var order = $(this).sortable("serialize") + '&update=update';
            $.get("{{url('admin/service/reorder')}}", order, function(theResponse) {
                $("#responce").html(theResponse);
                $("#responce").slideDown('slow');
                slideout();
            });
        }});
        $( "#sortable" ).disableSelection();
    } );

    function destroy_service(id)
    {
        $.ajax({
            url:'service/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'service';
                window.location.href = new_url;
            }
        });
    }

</script>