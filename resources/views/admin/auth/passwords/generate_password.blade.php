<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nali Master | Login</title>
    <link rel="shortcut icon" href="{{ URL::asset('assets/dist/img/favicon.png') }}"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css') }} ">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/square/blue.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ URL::to('/admin') }}"><b>Admin</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Reset your password</p>
        @include("admin.error")
        <form class="form-horizontal" method="POST" action="{{ url('admin/generate_password') }}" >
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback" style="margin-right: 0px !important;margin-left: 0px !important;">
                <label>Password</label>
                <input type="password" placeholder="Password" id="password" name="password" class="form-control" >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} has-feedback" style="margin-right: 0px !important;margin-left: 0px !important;">
                <label>Confrim Password</label>
                <input type="password" placeholder="Confrim Password" id="password_confirmation" name="password_confirmation" class="form-control" >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-4" style="float: right">
                    <button type="submit" class="btn btn-danger btn-block btn-flat">Send</button>
                </div>
            </div>
            @if(isset($_GET['reset']))
                <input type="hidden" name="token" id="token" value="{{$_GET['reset']}}">
            @endif
        </form>

    </div>
</div>

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js') }}"></script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

</body>
</html>