<table class="table table-bordered table-striped" id="example2">

    <thead>

    <tr>
        @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')
            <th>Salon</th>
        @endif
        <th>User Name</th>
        <th>User Email</th>
        <th style="text-align: left">Login Status</th>
        <th style="text-align: left">Role</th>
        <th class="table-text table-th">Delete</th>
    </tr>

    </thead>

    <tbody id="sortable">

    <?php
    $date_time = \Carbon\Carbon::now();
    $date_today = $date_time->format('Y-m-d');
    $get_booking = \App\Booking::with('Booking_details')->where('saloon_id', $salon_id)->where('status', 'in-progress')->where('date', $date_today)->get();
    $emp_ids = "";
    foreach ($get_booking as $val){
        foreach ($val['booking_details'] as $k => $values){
            $emp_ids .= $values['emp_id'].",";
        }
    }
    $emp_id = array_unique(explode(",",rtrim($emp_ids,",")));
    ?>

    @foreach ($employees as $list)

        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">

            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')
                <td>{{ $list['saloon_master']['title'] }}</td>
            @endif

                <td>{{$list['user']['name']}}</td>
                <td>{{$list['user']['email']}}</td>
                <td>

                    <?php
                    if (in_array($list['user_id'],$emp_id)){
                    ?>
                    <input id="toggle-event_{{$list['id']}}" title="Delete salon" data-trigger="hover" type="checkbox" disabled @if($list['user']['available'] =="present") checked="checked" @endif data-size="small" data-toggle="toggle" data-on="Present" data-off="Absent" data-onstyle="success" data-offstyle="danger">
                    <?php
                    }
                    else{
                    ?>
                    <input id="toggle-event_{{$list['id']}}" type="checkbox"  @if($list['user']['available'] =="present") checked="checked" @endif data-size="small" data-toggle="toggle" data-on="Present" data-off="Absent" data-onstyle="success" data-offstyle="danger">
                    <script>
                        $(function() {
                            $('#toggle-event_{{$list['id']}}').change(function() {
                                //$('#console-event').html('Toggle: ' + $(this).prop('checked'))

                                var typ = "";
                                if($(this).prop('checked')==false){
                                    typ = "absent";
                                }
                                if($(this).prop('checked')==true){
                                    typ = "present";

                                }

                                var mid = '{{$list['id']}}';
                                $.ajax({
                                    url:'{{url('admin/saloon/employee_attendance')}}',
                                    type:'get',
                                    data:{'id':'{{$list['user']['id']}}','type':typ,'salon_id':'{{$list['saloon_master']['id']}}'},
                                    success:function(data)
                                    {
                                        //alert(data);
                                        if (data=="1"){
                                            //$('#myModal1{{$list['id']}}').modal('show');
                                        }
                                    }
                                });
                            })
                        })
                    </script>
                    <?php
                    }
                    ?>



                </td>
                <td style="padding-top: 15px;">
                    <label class="label @if($list['is_salon_owner']=="1") label-warning @else label-success @endif" style="padding:5px 8px; font-size: 14px;">
                        @if($list['is_salon_owner']=="1")
                            Salon Owner
                        @else
                            Employee
                        @endif
                    </label>
                </td>
                <td class="table-text table-th">
                    <div class="btn-group-horizontal">
                            <span data-toggle="tooltip" title="Delete salon" data-trigger="hover">
                                <button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                            </span>
                    </div>
                </td>
        </tr>

        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            {{ Form::open(array('url' => 'admin/saloon/delete_assign_owner/'.$list['id'].'/'.$list['saloon_master']['id'], 'method' => 'delete','style'=>'display:inline')) }}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>

                    <?php
                    if (in_array($list['user_id'],$emp_id)){
                        ?>
                            <div class="modal-body">
                                <p>You can not delete employee in serving mode!</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                            </div>
                        <?php
                    }
                    else{
                        ?>
                            <div class="modal-body">
                                <p>Are you sure you want to delete this User ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
{{--                                <button type="submit" class="btn btn-outline" onclick="destroy_user({{$list['id']}})">Delete</button>--}}
                                <button type="submit" class="btn btn-outline">Delete</button>
                            </div>
                        <?php
                    }
                    ?>


                </div>
            </div>
            {{ Form::close() }}
        </div>


        <div id="myModal1{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">You can not change employee login status while employee in serving mode!</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>

    @endforeach
    </tbody>

</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $employees])</div>

<script>

    function assign_salon_owner() {
        var salon_id = '<?php echo $salon_id;?>';
        var salon_owner = document.getElementById('header_search').value;
        $.ajax({
            url: '{{url('admin/saloon/assign_owner')}}',
            data: { salon_id: salon_id, salon_owner:salon_owner },
            type: "GET",
            success: function (data) {
                {window.location.reload();}
            }
        });
    }

    function destroy_user(id)
    {
        $.ajax({
            url:'{{url('admin/saloon/delete_assign_owner')}}',
            type:'delete',
            data:{'id':id},
            success:function(data)
            {window.location.reload();}
        });
    }
</script>
