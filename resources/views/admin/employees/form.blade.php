{!! Form::hidden('redirects_to', URL::previous()) !!}

@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
    <div class="form-group{{ $errors->has('saloon_id') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label" for="saloon_id">Salon<span class="text-red">*</span></label>
        <div class="col-sm-5">
            {!! Form::select('saloon_id', $saloons, null, ['id'=>'saloon_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
            @if ($errors->has('saloon_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('saloon_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
@endif

<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="first_name_lb">First Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name','id'=>'first_name']) !!}
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
        <strong><span id="first_name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="last_name_lb">Last Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name','id'=>'last_name']) !!}
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
        <strong><span id="last_name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('nick_name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="nick_name" id="nick_name_lb">Nick Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('nick_name', null, ['class' => 'form-control', 'placeholder' => 'Nick Name','id'=>'nick_name']) !!}
        @if ($errors->has('nick_name'))
            <span class="help-block">
                <strong>{{ $errors->first('nick_name') }}</strong>
            </span>
        @endif
        <strong><span id="nick_name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>



<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="email" id="email_lb">Email <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address','id'=>'email','style'=>'text-transform:lowercase']) !!}
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <strong><span id="email_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="phone_lb">Phone <span class="text-red">*</span></label>

    <div class="col-md-2 col-sm-5 phone-code">
         @if(isset($employee) && $employee!="")
            <?php
            $fcode = substr($employee['phone'],0, 1);
            if ($fcode==1){
                $code = 1;
            }
            else{
                $code = substr($employee['phone'],0, 2);
            }
            ?>
        @endif
        <select name="countries" id="countries" class="select2 form-control">
            <option value='1' @if(isset($code) && $code == 1) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag us" data-title="United State">United State</option>
            <option value='91' @if(isset($code) && $code == 91) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag in" data-title="India">India</option>
            <option value='61' @if(isset($code) && $code == 61) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag au" data-title="Australia">Australia</option>
        </select>
    </div>

    <div class="col-md-3 col-sm-5">
        @if(isset($employee) && $employee!="")
            <?php
            $fcode = substr($employee['phone'],0, 1);
            if ($fcode==1){
                $phone = substr($employee['phone'],1);
            }
            else{
                $phone = substr($employee['phone'],2);
            }
            ?>
            {!! Form::text('phone', $phone, ['class' => 'form-control', 'placeholder' => 'Enter Phone','id'=>'phone']) !!}
        @else
            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone','id'=>'phone']) !!}
        @endif

        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
        <strong><span id="phone_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Address<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-sm-5">
        {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) !!}
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="image" id="image_lb">Image <span class="text-red">*</span></label>
    <div class="col-sm-5">
        <div class="">
            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}
        </div>
        <?php
        if (!empty($employee->image) && $employee->image != "") {
        ?>
        <br><img id="DisplayImage" src="{{ url($employee->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';
        } ?>
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
        <strong><span id="image_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Employees::$status as $key => $value)
            <label>
                @if($key == 'active')
                     {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<script type="text/javascript">
    $("#image").fileinput({
        showUpload: false,
        showCaption: false,
        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });
    function AjaxUploadImage(obj,id){
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }

    function imageIsLoaded(e) {
        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');
    };

</script>

<script type="text/javascript">
    function validation()
    {
        var flag = 0;
        if(document.getElementById("first_name").value.split(" ").join("") == "")
        {
            document.getElementById('first_name_msg').innerHTML = 'The first name field is required.';
            document.getElementById("first_name").focus();
            document.getElementById("first_name").style.borderColor ='#dd4b39';
            document.getElementById("first_name_lb").style.color ='#dd4b39';
            document.getElementById('first_name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("first_name").style.borderColor ='#d2d6de';
            document.getElementById("first_name_lb").style.color ='#333';
            document.getElementById('first_name_msg').style.display = 'none';
        }

        if(document.getElementById("last_name").value.split(" ").join("") == "")
        {
            document.getElementById('last_name_msg').innerHTML = 'The last name field is required.';
            document.getElementById("last_name").focus();
            document.getElementById("last_name").style.borderColor ='#dd4b39';
            document.getElementById("last_name_lb").style.color ='#dd4b39';
            document.getElementById('last_name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("last_name").style.borderColor ='#d2d6de';
            document.getElementById("last_name_lb").style.color ='#333';
            document.getElementById('last_name_msg').style.display = 'none';
        }

        if(document.getElementById("nick_name").value.split(" ").join("") == "")
        {
            document.getElementById('nick_name_msg').innerHTML = 'The nick name field is required.';
            document.getElementById("nick_name").focus();
            document.getElementById("nick_name").style.borderColor ='#dd4b39';
            document.getElementById("nick_name_lb").style.color ='#dd4b39';
            document.getElementById('nick_name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("nick_name").style.borderColor ='#d2d6de';
            document.getElementById("nick_name_lb").style.color ='#333';
            document.getElementById('nick_name_msg').style.display = 'none';
        }

        if(document.getElementById("email").value.split(" ").join("") == "")
        {
            document.getElementById('email_msg').innerHTML = 'The email field is required.';
            document.getElementById("email").focus();
            document.getElementById("email").style.borderColor ='#dd4b39';
            document.getElementById("email_lb").style.color ='#dd4b39';
            document.getElementById('email_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var email1 = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            if (!document.getElementById("email").value.match(email1)) {
                document.getElementById('email_msg').innerHTML = 'The email must be a valid email address.';
                document.getElementById("email").style.borderColor ='#dd4b39';
                document.getElementById("email_lb").style.color ='#dd4b39';
                document.getElementById('email_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("email").style.borderColor ='#d2d6de';
                document.getElementById("email_lb").style.color ='#333';
                document.getElementById('email_msg').style.display = 'none';
            }
        }

        if(document.getElementById("phone").value.split(" ").join("") == "")
        {
            document.getElementById('phone_msg').innerHTML = 'The phone field is required.';
            document.getElementById("phone").focus();
            document.getElementById("phone").style.borderColor ='#dd4b39';
            document.getElementById("phone_lb").style.color ='#dd4b39';
            document.getElementById('phone_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("phone").style.borderColor ='#d2d6de';
            document.getElementById("phone_lb").style.color ='#333';
            document.getElementById('phone_msg').style.display = 'none';
        }

        if(document.getElementById("mod").value=="Add")
        {
            if(document.getElementById("image").value.split(" ").join("") == "")
            {
                document.getElementById('image_msg').innerHTML = 'The image field is required.';
                document.getElementById("image").focus();
                document.getElementById('image_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById('image_msg').style.display = 'none';
            }
        }

        var active = 'active';
        var inactive = 'in-active';
        if(document.getElementById("status_"+active).checked == false && document.getElementById("status_"+inactive).checked == false)
        {
            document.getElementById('status_msg').innerHTML = 'The status field is required.';
            document.getElementById("status_val_"+active).style.color ='#dd4b39';
            document.getElementById("status_val_"+inactive).style.color ='#dd4b39';
            document.getElementById("status_lb").style.color ='#dd4b39';
            document.getElementById('status_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("status_val_"+active).style.color ='#333';
            document.getElementById("status_val_"+inactive).style.color ='#333';
            document.getElementById("status_lb").style.color ='#333';
            document.getElementById('status_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>