@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/employees') }}"><i class="fa fa-dashboard"></i> Employee</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box box-info">
                <div class="box-header">

                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')
                        {!! Form::open(['url' => url('admin/service'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
{{--                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">--}}
                                {!! Form::select('salon', $salon ,null, ['id'=>'search', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}

                    @else
                        <div class="col-md-5">
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                                <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                            </div>
                        </div>
                    @endif


                    {{--<div class="col-md-7">--}}
                    {{--<label class="col-sm-2 control-label" for="type_id" id="user_id_lb">Salon Owner <span class="text-red"></span></label>--}}
                    {{--<div class="col-md-9 col-sm-6">--}}
                    {{--{!! Form::text('salon_owner', null, ['class' => 'form-control typeahead', 'id'=>'header_search', 'autocomplete'=>'off', 'placeholder'=>'Search User']) !!}--}}
                    {{--</div>--}}
                    {{--<div class="col-md-1 col-sm-6">--}}
                    {{--<button class="btn btn-info pull-right" type="button" name="assign" onclick="assign_salon_owner()">Assign</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">

                    @include('admin.employees.table')
                </div>

            </div>

        </section>

    </div>

@endsection



<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>

<script>Ladda.bind( 'input[type=submit]' );</script>



<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({
            url : page
        }).done(function (data) {

            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true

            });
            $.getScript(" https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js");
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();
        var search = $('#search').val();
        var type = $('#type').val();
        $.ajax({
            url: '{{url('admin/employee/'.$salon_id.'/list')}}',
            type: "get",
            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                //alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
                $.getScript(" https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js");
            }

        });
    });
</script>

