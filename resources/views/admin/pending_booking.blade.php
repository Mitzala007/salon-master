<style>
    @media (min-width: 768px){
        .modal-dialog {
            width: 725px;
            margin: 30px auto;
        }
    }
</style>
<div id="load2" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped" id="pending_example2">
    <thead>
    <tr>
        <th style="width: 10%;">Booking Name</th>
        <th style="width: 15%;">User</th>
        <th style="width: 15%;">No of persons</th>
        {{--<th style="width: 15%;">Final Total</th>--}}
        <th>Services</th>
        <th style="width: 15%;" class="table-text table-th">Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pending_booking as $serving)
        <tr>
            <td >{{$serving['booking_name']}}</td>
            <td>{{$serving['Users']['name']}}</td>
            <td>{{$serving['no_of_persons']}}</td>
            {{--<td>{{$serving['final_total']}}</td>--}}


            {{--<td style="padding-top: 11px;" class="table-text table-th">--}}
            {{--<label class="label @if($serving['status'] == 'in-progress') label-info @elseif($serving['status'] == 'waiting') label-warning @elseif($serving['status'] == 'cancel') label-danger @elseif($serving['status'] == 'completed') label-success @endif" style="padding:5px 8px; font-size: 14px;">{{ucfirst($serving['status'])}}</label>--}}
            {{--</td>--}}


            <td>
                <?php
                $service = '';

                foreach($serving['Booking_details'] as $details)
                {
                    $service .= $details['service_type']['title'].',';
                }
                $ser_title = rtrim($service,',');
                ?>
                {{$ser_title}}
            </td>

            <td class="table-text" width="10%">
                <?php $status_selected = explode(",",$serving['status']);
                $disabled = '';
                if ($serving['status']=="completed" || $serving['status']=="cancel"){
                    $disabled = "disabled";
                }
                ?>
                {!! Form::select('status', \App\Booking::$status, !empty($status_selected)?$status_selected:null,
                ['class' => 'select2 form-control','id'=>'status_'.$serving->id,'style' => 'width: 100%','onchange'=> 'calltype(this.value,'.$serving->id.','.$serving['saloon']['id'].');',$disabled]) !!}
            </td>
        </tr>


        @include('admin.booking.done_walkin_model')

    @endforeach
    </tbody>
</table>

<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links_2', ['paginator' => $pending_booking])</div>

<script>

    function calltype(val,vid,sid){

        if(val=="completed"){
            $('#myModal_'+vid).modal('show');
        }
        if(val=="in-progress"){
            serve_now(vid,sid);
        }
        if(val=="cancel"){
            $.ajax({
                url: '{{ url('admin/booking') }}/'+val +'/'+ vid,
                error:function(){
                },
                success: function(result){
                    document.getElementById('status_'+vid).disabled=true;
                }
            });
        }
    }

    function serve_now(id,sid) {

        {{--$('#load').append('<img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');--}}
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url: '{{ url('admin/serve_now') }}',
            type:'post',
            data:{'id':id,'salon_id':sid},
            success:function(data)
            {
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){

                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

    function end_now(bid,sid) {

        <!--$('#load').append('<img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');-->
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        var discount_amount = document.getElementById('discount_amount_'+bid).value;
        var remarks = document.getElementById('remarks_'+bid).value;
        var final_total = document.getElementById('final_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var point = document.getElementById('point_'+bid).value;
        var payment_mode  = $("input:radio[name=payment_mode_"+bid+"]:checked").val();

        $.ajax({
            url: '{{ url('admin/end_now') }}',
            type:'post',
            data:{'salon_id':sid,'bid':bid,'discount_amount':discount_amount,'remarks':remarks,'final_total':final_total,'additional_amount':additional_amount,'point':point,'payment_mode':payment_mode},
            success:function(data)
            {
                //alert(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";

                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);

                if (JSONObject["Result"]==1){
                    $('#myModal_'+bid).modal('hide');
                    document.getElementById('status_'+bid).disabled=true;
                    document.getElementById('final_total_td_'+bid).innerHTML = final_total;
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

</script>


<script>

    function reedem_point(bid,point) {
        var available_point = document.getElementById('available_point_'+bid).value;
        if(parseInt(available_point)< parseInt(point)){
            document.getElementById('point_'+bid).value = "";
            document.getElementById('point_alert_'+bid).style.display='block';

        }
        else {
            document.getElementById('point_alert_'+bid).style.display='none';
        }
    }

    function access_redeem(bid)
    {
        if($('#redeem_point_'+bid).prop("checked") == true){
            document.getElementById('point_'+bid).removeAttribute('readonly');
            document.getElementById('point_'+bid).focus();
        }
        else if($('#redeem_point_'+bid).prop("checked") == false){
            document.getElementById('point_'+bid).setAttribute('readonly','readonly');
            document.getElementById('point_'+bid).value="00";
        }
    }

    function access_discount(bid)
    {
        if($('#add_discount_'+bid).prop("checked") == true){
            document.getElementById('by_discount_'+bid).style.display='block';
            document.getElementById('discount_amount_'+bid).focus();
        }
        else if($('#add_discount_'+bid).prop("checked") == false){
            document.getElementById('by_discount_'+bid).style.display='none';
        }
    }

    function access_payment(bid)
    {
        if($('#add_payment_'+bid).prop("checked") == true){
            document.getElementById('by_amount_div_'+bid).style.display='block';
            document.getElementById('additional_amount_'+bid).focus();
        }
        else if($('#add_payment_').prop("checked") == false){
            document.getElementById('by_amount_div_'+bid).style.display='none';
        }
    }

    function count_percentage(bid,val){
        var sub_total = document.getElementById('sub_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var by_price = document.getElementById('discount_amount_'+bid).value;
        if(additional_amount==""){additional_amount = 0;}

        if(parseInt(sub_total)<parseInt(by_price)){
            document.getElementById('discount_alert_'+bid).style.display='block';
            document.getElementById('discount_alert_'+bid).innerHTML='Discount amount must be less than final total';
            document.getElementById('discount_amount_'+bid).value = 0;
            document.getElementById('discount_percentage_'+bid).value = 0;
            document.getElementById('final_total_display_'+bid).innerHTML = sub_total.toFixed(2);
            document.getElementById('final_total_'+bid).value= sub_total.toFixed(2);
            return false;
        }
        else{
            document.getElementById('discount_alert_'+bid).style.display='none';
        }

        if(by_price==""){ by_price = 0;}
        var dis_per = by_price/sub_total*100;
        document.getElementById('discount_percentage_'+bid).value = dis_per.toFixed(2);
        var dis_price = (parseFloat(sub_total)-parseFloat(by_price))+parseFloat(additional_amount);
        document.getElementById('final_total_display_'+bid).innerHTML = dis_price;
        document.getElementById('final_total_'+bid).value= dis_price;
    }

    function count_price(bid,val){

        var sub_total = document.getElementById('sub_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var by_per = document.getElementById('discount_percentage_'+bid).value;
        if(additional_amount==""){additional_amount = 0;}

        if(parseInt(by_per)>100){
            document.getElementById('discount_alert_'+bid).style.display='block';
            document.getElementById('discount_alert_'+bid).innerHTML='You can not enter discount percentage more than 100%';
            document.getElementById('discount_amount_'+bid).value = 0;
            document.getElementById('discount_percentage_'+bid).value = 0;
            document.getElementById('final_total_display_'+bid).innerHTML = sub_total.toFixed(2);
            document.getElementById('final_total_'+bid).value= sub_total.toFixed(2);
            return false;
        }
        else{
            document.getElementById('discount_alert_'+bid).style.display='none';
        }

        if(by_per==""){by_per = 0;}
        var dis_price = sub_total*by_per/100;
        document.getElementById('discount_amount_'+bid).value = dis_price.toFixed(2);
        var by_dis_price = document.getElementById('discount_amount_'+bid).value;
        var aft_dis_price = (parseFloat(sub_total)-parseFloat(by_dis_price))+parseFloat(additional_amount);
        document.getElementById('final_total_display_'+bid).innerHTML = aft_dis_price.toFixed(2);
        document.getElementById('final_total_'+bid).value= aft_dis_price.toFixed(2);
    }

    function call_user(id){

        if(id==0){
            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";
        }
        else {
            $.ajax({
                type: 'POST',
                data: {'id': id},
                url: '{{ url('admin/ajax_get_customer')}}',
                success: function (data) {
                    var myJSON = JSON.stringify(data)
                    var JSONObject = JSON.parse(myJSON);

                    document.getElementById('name').value = JSONObject["name"];
                    document.getElementById('phone').value = JSONObject["phone"];
                    document.getElementById('email').value = JSONObject["email"];

                    document.getElementById('name').disabled="disabled";
                    document.getElementById('email').disabled="disabled";
                    document.getElementById('phone').disabled="disabled";

                    document.getElementById('phone_display').style.display="none";

                    if(JSONObject["phone"]=="" || JSONObject["phone"]==null){
                        document.getElementById('phone_display').style.display="block";
                        document.getElementById('phone').disabled=false;
                    }

                    if(JSONObject["email"]=="" || JSONObject["email"]==null){
                        document.getElementById('email').disabled=false;
                    }
                }
            });
        }
    }

    function get_type(key)
    {
        if(key == 1)
        {
            document.getElementById('shortdis').style.display='block';
            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";
            $('#user_ids').prop('selectedIndex',0);
            document.getElementById('select2-user_ids-container').innerHTML="Please Select";
            document.getElementById('select2-user_ids-container').title="Please Select";
            document.getElementById('phone_display').style.display="none";
        }
        else
        {
            document.getElementById('shortdis').style.display='none';
            document.getElementById('phone_display').style.display="block";
            document.getElementById('shortdis').value='';

            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";


            document.getElementById('name').disabled=false;
            document.getElementById('phone').disabled=false;
            document.getElementById('email').disabled=false;
        }
    }

</script>


