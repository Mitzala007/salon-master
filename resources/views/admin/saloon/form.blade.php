<style>
    input[type="radio"] {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        opacity: 0;
        position: absolute;
        margin: 0;
        z-index: -1;
        width: 0;
        height: 0;
        overflow: hidden;
        left: 0;
        pointer-events: none;
    }
    .label-text1{
        padding-left: 0!important;
    }
    .label-text1 span{
        margin-top: 20px;
    }
    input[type="radio"] + .label-text:before{
        content: '\f10c';
        font-family: "FontAwesome";
        speak: none;
        font-size: 23px;
        font-style: normal;
        font-weight: bold;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        margin-right: 5px;
        color: #D7DCDE;
    }
    input[type="radio"]:checked + .label-text:before{
        content: "\f192";
        color: #1ABC9C;
    }
    input[type="radio"]:disabled + .label-text{
        color: #D7DCDE;
    }
    input[type="radio"]:disabled + .label-text:before{
        content: "\f111";
        color: #D7DCDE;
    }
</style>
{!! Form::hidden('redirects_to', URL::previous()) !!}

@if(isset($id))
    @if(count($salon_employee)>0)
        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
            <label class="col-sm-2 control-label" for="user_id" id="user_id_lb">Owner <span class="text-red">*</span></label>
            <div class="col-md-5 col-sm-6">
                {!! Form::text('user_id',  $saloon_users['email'], ['class' => 'form-control','id'=>'user_id','disabled']) !!}
                @if ($errors->has('user_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('user_id') }}</strong>
                    </span>
                @endif
                <strong><span id="user_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
            </div>
        </div>
        @else
        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
            <label class="col-sm-2 control-label" for="user_id" id="user_id_lb">Owner <span class="text-red">*</span></label>
            <div class="col-md-5 col-sm-6">
                {!! Form::select('user_id', [''=>'Please select']+$saloon_users , null, ['id'=>'user_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                @if ($errors->has('user_id'))
                    <span class="help-block">
                    <strong>{{ $errors->first('user_id') }}</strong>
                </span>
                @endif
                <strong><span id="user_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
            </div>
        </div>
    @endif
@else
    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label" for="user_id" id="user_id_lb">Owner <span class="text-red">*</span></label>
        <div class="col-md-5 col-sm-6">
            {!! Form::select('user_id', [''=>'Please select']+$saloon_users , null, ['id'=>'user_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
            @if ($errors->has('user_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_id') }}</strong>
                </span>
            @endif
            <strong><span id="user_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
        </div>
    </div>
@endif

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="title_lb">Salon Name <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Salon Name','id'=>'title']) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
        @endif
        <strong><span id="title_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="address" id="location_lb">Salon Location <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Search Box','id'=>'pac-input']) !!}
        @if ($errors->has('location'))
            <span class="help-block">
                    <strong>{{ $errors->first('location') }}</strong><br>
                </span>
        @endif
        <strong><span id="location_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
        {{--<div class="form-group{{ $errors->has('latitude') && $errors->has('longitude') ? ' has-error' : '' }} col-sm-6">--}}
        {{--@if ($errors->has('latitude') && $errors->has('longitude'))--}}
        {{--<span class="help-block">--}}
        {{--<strong>Update your location</strong>--}}
        {{--</span>--}}
        {{--@endif--}}
        {{--<strong><span id="lat_lon_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
        {{--</div>--}}
    </div>
</div>

<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="address1"> <span class="text-red">&nbsp;&nbsp;</span></label>
    <div class="col-md-5 col-sm-6">
        <p>Drag Marker to your Location.</p>
        {{--<input id="pac-input" class=" form-control" type="text" placeholder="Search Box" style="width: 150px; height: 30px;">--}}
        <div id="map" style="min-height:300px;" >
        </div>
    </div>
</div>

<div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="timezone">Timezone</label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('timezone', null, ['class' => 'form-control', 'id' => 'timezone', 'placeholder' => 'Timezone','readonly']) !!}
    </div>
</div>


<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="address1" id="address1">Address Line1<span class="text-red"></span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('address1', null, ['class' => 'form-control', 'placeholder' => 'Enter Address line1','id'=>'address_line_1']) !!}
        @if ($errors->has('address1'))
            <span class="help-block">
                    <strong>{{ $errors->first('address1') }}</strong>
                </span>
        @endif
        <strong><span id="address1_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="address2" id="address2">Address Line2<span class="text-red"></span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('address2', null, ['class' => 'form-control', 'placeholder' => 'Enter Address line2','id'=>'address_line_2']) !!}
        @if ($errors->has('address2'))
            <span class="help-block">
                    <strong>{{ $errors->first('address2') }}</strong>
                </span>
        @endif
        <strong><span id="address2_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="description" id="description_lb">Salon Description <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>'10','cols'=>'80', 'id'=>'editor1']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
        @endif
        <strong><span id="description_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('saloon_phone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="saloon_phone_lb">Salon Phone <span class="text-red">*</span></label>
    <div class="col-md-2 col-sm-5 phone-code">
        @if(isset($saloon) && $saloon!="")
            <?php
            $fcode = substr($saloon['saloon_phone'],0, 1);
            if ($fcode==1){
                $code = 1;
            }
            else{
                $code = substr($saloon['saloon_phone'],0, 2);
            }
            ?>
        @endif
        <select name="countries" id="countries" class="select2 form-control">
            <option value='1' @if(isset($code) && $code == 1) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag us" data-title="United State">United State</option>
            <option value='91' @if(isset($code) && $code == 91) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag in" data-title="India">India</option>
            <option value='61' @if(isset($code) && $code == 61) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag au" data-title="Australia">Australia</option>
        </select>
    </div>

    <div class="col-md-3 col-sm-5">
        @if(isset($saloon) && $saloon!="")
            <?php
            $fcode = substr($saloon['saloon_phone'],0, 1);
            if ($fcode==1){
                $phone = substr($saloon['saloon_phone'],1);
            }
            else{
                $phone = substr($saloon['saloon_phone'],2);
            }
            ?>
            {!! Form::text('saloon_phone', $phone, ['class' => 'form-control', 'placeholder' => 'Enter Saloon Phone Number','id'=>'saloon_phone']) !!}
        @else
            {!! Form::text('saloon_phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Saloon Phone Number','id'=>'saloon_phone']) !!}
        @endif

        @if ($errors->has('saloon_phone'))
            <span class="help-block">
                    <strong>{{ $errors->first('saloon_phone') }}</strong>
                </span>
        @endif
        <strong><span id="saloon_phone_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="image" id="image_lb">Salon Logo <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        <div class="">
            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}
        </div>
        <?php
        if (!empty($saloon->image) && $saloon->image != "") {
        ?>
        <br><img id="DisplayImage" src="{{ url($saloon->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';
        } ?>
        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
        <strong><span id="image_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('website_url') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="web_url" id="website_url">Website Url<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('website_url', null, ['class' => 'form-control', 'placeholder' => 'Enter Website Url','id'=>'website_url']) !!}
        @if ($errors->has('website_url'))
            <span class="help-block">
                    <strong>{{ $errors->first('website_url') }}</strong>
                </span>
        @endif
        <strong><span id="title_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        @foreach (\App\saloon::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
                 <strong>{{ $errors->first('status') }}</strong>
                </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('global_discount') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role">Global Discount<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-md-5 col-sm-6 ">
        @foreach (\App\saloon::$global_discount as $key => $value)
            <label class="label-text1">
                {!! Form::radio('global_discount', $key, null, ['class' => '', 'onclick'=>'get_discount('.$key.')']) !!}<span class="label-text" style="margin-right: 26px; font-weight: bold;">{{ $value }}</span>
            </label>
        @endforeach
        @if ($errors->has('global_discount'))
            <span class="help-block">
                 <strong>{{ $errors->first('global_discount') }}</strong>
                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('global_discount') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role"></label>
    <div class="col-md-5 col-sm-6" style="color: #ffa500;">
        NOTE:- Enabling Global Discount means salon is providing discount in at least one service.
    </div>
</div>

<div class="form-group{{ $errors->has('promotional_title') ? ' has-error' : '' }}" id="shortdis" style="@if(isset($saloon) && $saloon->global_discount !=1) display: none; @else display: block; @endif">
    <label class="col-sm-2 control-label" for="promotional_title" id="promotional_title">Promotional Title</label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('promotional_title', null, ['class' => 'form-control', 'placeholder' => 'Promotional title','id'=>'promotional_title']) !!}
        @if ($errors->has('promotional_title'))
            <span class="help-block">
                    <strong>{{ $errors->first('promotional_title') }}</strong>
                </span>
        @endif
    </div>
</div>

<div class="form-group" id="promo_validation" style="@if(isset($saloon) && $saloon->global_discount !=1) display: none; @else display: block; @endif">
    <label class="col-sm-2 control-label" for="promotional_title" id="promotional_title">&nbsp;</label>
    <div class="col-md-5 col-sm-6">
            <span>
                <strong><i>The promotional title may not be greater than 135 characters.</i></strong>
            </span>
    </div>
</div>

<div class="form-group{{ $errors->has('is_promo_display') ? ' has-error' : '' }}" id="is_promo" style="@if(isset($saloon) && $saloon->global_discount !=1) display: none; @else display: block; @endif">
    <label class="col-sm-2 control-label" for="is_promo_display">Promotional Display</label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('is_promo_display', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('is_promo_display'))
            <span class="help-block">
                 <strong>{{ $errors->first('is_promo_display') }}</strong>
                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('is_online') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="is_promo_display">Is Online</label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('is_online', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('is_online'))
            <span class="help-block">
                     <strong>{{ $errors->first('is_online') }}</strong>
                    </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('loyalty_program') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="loyalty_program">Loyalty Program</label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('loyalty_program', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('loyalty_program'))
            <span class="help-block">
                     <strong>{{ $errors->first('loyalty_program') }}</strong>
                    </span>
        @endif
    </div>
</div>

<br>

<input type="hidden" name="latitude" id="latitude" value="<?php
if (!empty($saloon) && $saloon['latitude'] != "") {
    echo $saloon["latitude"];
}
?>">
<input type="hidden" name="longitude" id="longitude" value="<?php
if (!empty($saloon) && $saloon['longitude'] != "") {
    echo $saloon["longitude"];
}
?>">

<script type="text/javascript">

    function AjaxUploadImage(obj,id){
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }
    function imageIsLoaded(e) {
        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');
    };

</script>



<script type="text/javascript">
    var map;
    var markersArray = [];
        <?php
        if (!empty($saloon) && !empty($saloon["latitude"]) && !empty($saloon["longitude"])) {
            echo 'var myLatLng = {lat: ' . $saloon["latitude"] . ', lng: ' . $saloon["longitude"] . '};';
        } else {
            echo 'var myLatLng = {lat: 37.9642529, lng: -91.8318334};';
        }
        ?>
    var latitude;
    var longitude;
    var myLatLng;
    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 14
        });

        var marker = new google.maps.Marker({
            draggable: true,
            position: myLatLng,
            map: map,
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
//        map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            searchBox.set('map', null);

            var places = searchBox.getPlaces();

            var bounds = new google.maps.LatLngBounds();
            var i, place;
            if(places[0] !== undefined) {

                place = places[0];

                $("#latitude").val(place.geometry.location.lat());
                $("#longitude").val(place.geometry.location.lng());

                marker.setPosition( place.geometry.location );
                map.panTo( place.geometry.location );
                $('#pac-input').val(place.formatted_address);
                $('#address_line_1').val(results[2].formatted_address);
                $('#address_line_2').val(results[5].formatted_address);

            }

            var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#pac-input').val(results[0].formatted_address);
                        $('#address_line_1').val(results[2].formatted_address);
                        $('#address_line_2').val(results[5].formatted_address);

                        url = "https://maps.googleapis.com/maps/api/timezone/json?location="+place.geometry.location.lat()+","+place.geometry.location.lng()+"&timestamp="+(Math.round((new Date().getTime())/1000)).toString()+"&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";

                        $.ajax({
                            url:url,
                        })
                            .done(function(response){
                                if(response.status == 'OK'){
                                    $('#timezone').val(response.timeZoneId);
                                }
                            });

                    }
                }
            });

            searchBox.set('map', map);
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            $("#latitude").val(event.latLng.lat());
            $("#longitude").val(event.latLng.lng());

            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        //alert(results[1].address_components[0]['long_name']);
                        $('#pac-input').val(results[0].formatted_address);
                        $('#address_line_1').val(results[2].formatted_address);
                        $('#address_line_2').val(results[5].formatted_address);
//                        alert(results[0].formatted_address);

                        url = "https://maps.googleapis.com/maps/api/timezone/json?location="+event.latLng.lat()+","+event.latLng.lng()+"&timestamp="+(Math.round((new Date().getTime())/1000)).toString()+"&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";

                        $.ajax({
                            url:url,
                        })
                            .done(function(response){
                                if(response.status == 'OK'){
                                    $('#timezone').val(response.timeZoneId);
                                }
                            });

                    }
                }
            });
        });


    }

    function clearMarkers() {
        setMapOnAll(null);
    }


    function placeMarker(location) {
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });

        markersArray.push(marker);
    }

    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap&libraries=places" async defer></script>

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<script type="text/javascript">

    $(document).on("keydown", ":input:not(textarea)", function(event) {
        if (event.key == "Enter") {
            event.preventDefault();
        }
    });
    function validation()
    {
        return event.key != "Enter";

        var flag = 0;

        @if(!isset($id))
        if(document.getElementById("user_id").value.split(" ").join("") == "")
        {
            document.getElementById('user_id_msg').innerHTML = 'The Owner selection field is required.';
            document.getElementById("user_id").focus();
            document.getElementById("user_id").style.borderColor ='#dd4b39';
            document.getElementById("user_id_lb").style.color ='#dd4b39';
            document.getElementById('user_id_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("user_id").style.borderColor ='#d2d6de';
            document.getElementById("user_id_lb").style.color ='#333';
            document.getElementById('user_id_msg').style.display = 'none';
        }
        @endif

        if(document.getElementById("title").value.split(" ").join("") == "")
        {
            document.getElementById('title_msg').innerHTML = 'The title field is required.';
            document.getElementById("title").focus();
            document.getElementById("title").style.borderColor ='#dd4b39';
            document.getElementById("title_lb").style.color ='#dd4b39';
            document.getElementById('title_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("title").style.borderColor ='#d2d6de';
            document.getElementById("title_lb").style.color ='#333';
            document.getElementById('title_msg').style.display = 'none';
        }

        if(document.getElementById("pac-input").value.split(" ").join("") == "")
        {
            document.getElementById('location_msg').innerHTML = 'The location field is required.';
            document.getElementById("pac-input").focus();
            document.getElementById("pac-input").style.borderColor ='#dd4b39';
            document.getElementById("location_lb").style.color ='#dd4b39';
            document.getElementById('location_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("pac-input").style.borderColor ='#d2d6de';
            document.getElementById("location_lb").style.color ='#333';
            document.getElementById('location_msg').style.display = 'none';
        }



        if(document.getElementById("editor1").value.split(" ").join("") == "")
        {
            document.getElementById('description_msg').innerHTML = 'The description field is required.';
            document.getElementById("editor1").focus();
            document.getElementById("editor1").style.borderColor ='#dd4b39';
            document.getElementById("description_lb").style.color ='#dd4b39';
            document.getElementById('description_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("editor1").style.borderColor ='#d2d6de';
            document.getElementById("description_lb").style.color ='#333';
            document.getElementById('description_msg').style.display = 'none';
        }



        if(document.getElementById("saloon_phone").value.split(" ").join("") == "")
        {
            document.getElementById('saloon_phone_msg').innerHTML = 'The saloon phone field is required.';
            document.getElementById("saloon_phone").focus();
            document.getElementById("saloon_phone").style.borderColor ='#dd4b39';
            document.getElementById("saloon_phone_lb").style.color ='#dd4b39';
            document.getElementById('saloon_phone_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var phone1=document.getElementById("saloon_phone").value;
            if(isNaN(phone1))
            {
                document.getElementById('saloon_phone_msg').innerHTML = 'Please enter a valid mobile number.';
                document.getElementById("saloon_phone").focus();
                document.getElementById("saloon_phone").style.borderColor ='#dd4b39';
                document.getElementById("saloon_phone_lb").style.color ='#dd4b39';
                document.getElementById('saloon_phone_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("saloon_phone").style.borderColor ='#d2d6de';
                document.getElementById("saloon_phone_lb").style.color ='#333';
                document.getElementById('saloon_phone_msg').style.display = 'none';
            }
        }



        if(document.getElementById("mod").value=="Add")
        {
            if(document.getElementById("image").value.split(" ").join("") == "")
            {
                document.getElementById('image_msg').innerHTML = 'The image field is required.';
                document.getElementById("image").focus();
                document.getElementById('image_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById('image_msg').style.display = 'none';
            }
        }


        var active = 'active';
        var inactive = 'in-active';
        if(document.getElementById("status_"+active).checked == false && document.getElementById("status_"+inactive).checked == false)
        {
            document.getElementById('status_msg').innerHTML = 'The status field is required.';
            document.getElementById("status_val_"+active).style.color ='#dd4b39';
            document.getElementById("status_val_"+inactive).style.color ='#dd4b39';
            document.getElementById("status_lb").style.color ='#dd4b39';
            document.getElementById('status_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("status_val_"+active).style.color ='#333';
            document.getElementById("status_val_"+inactive).style.color ='#333';
            document.getElementById("status_lb").style.color ='#333';
            document.getElementById('status_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>

@section('jquery')
    <script type="text/javascript">

        $("#start_hour").timepicker({
            showPeriod: true
        });

        $("#end_hour").timepicker({
            showPeriod: true
        });

        $('#all_days').click(function () {
            $('input:checkbox[class=work_days]').prop('checked', this.checked);
        });

    </script>
@stop

<script>
    function get_discount(key)
    {
        if(key == 1)
        {
            document.getElementById('shortdis').style.display='block';
            document.getElementById('promo_validation').style.display='block';
            document.getElementById('is_promo').style.display='block';
        }
        else
        {
            document.getElementById('shortdis').style.display='none';
            document.getElementById('is_promo').style.display='none';
            document.getElementById('shortdis').value='';
            document.getElementById('promo_validation').style.display='none';
        }
    }
</script>