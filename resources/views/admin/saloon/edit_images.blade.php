@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

{{--        @include('admin.saloon.assign_salon')--}}

        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif

            @if($errors->has('work_days'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{$errors->first('work_days')}}
                </div>
            @endif

            @if($errors->has('type_id'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{$errors->first('type_id')}}
                </div>
            @endif

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li class=""><a href="{{url('admin/saloon/'.$id .'/edit')}}">Salon Details</a></li>
                    @if(empty($working_days))
                        <li class=""><a href="{{url('admin/saloon/services_edit/'.$id)}}" >Service Details</a></li>
                        <li class=""><a href="#">Working Days</a></li>
                    @else
                        <li class=""><a href="{{url('admin/saloon/services_edit/'.$id)}}" >Service Details</a></li>
                        <li class=""><a href="{{url('admin/saloon/work_days/'.$id)}}">Working Days</a></li>
                    @endif
                    <li class="active"><a href="{{url('admin/saloon/images/'.$id.'/edit')}}">Salon Images</a></li>
                </ul>

                {!! Form::model($saloon,['url' => url('admin/saloon/images/'.$saloon->id.'/update'),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true]) !!}

                <input type="hidden" name="mod" id="mod" value="Edit">

                <div class="tab-content">
                    <div class="tab-pane active" id="saloonManagement">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">
                                    <div class="box-body">
                                        @include ('admin.saloon.add_images')
                                    </div>
                                    <div class="box-footer">
                                        <a href="javascript: history.go(-1)"><button class="btn btn-default" type="button">Back</button></a>
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save_next">Save & Next</button>
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save" style="margin-right: 5px;">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </section>
    </div>
@endsection

<script>

    $("#image").fileinput({
        showUpload: false,
        showCaption: false,
        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });

    function AjaxUploadImage(obj, id) {
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            $('#previewing' + URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }

        function imageIsLoaded(e) {

            $('#DisplayImage' + id).css("display", "block");
            $('#DisplayImage' + id).attr('src', e.target.result);
            $('#DisplayImage' + id).attr('width', '75');

        };

    }
</script>