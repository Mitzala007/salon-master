@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Add</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Add</li>
            </ol>
        </section>

        <section class="content">
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="box box-info">--}}
                        {{--<div class="box-header with-border">--}}
                            {{--<h3 class="box-title">ADD {{$menu}}</h3>--}}
                        {{--</div>--}}
                        {{--{!! Form::open(['url' => url('admin/saloon'), 'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return validation();']) !!}--}}
                        {{--<input type="hidden" name="mod" id="mod" value="Add">--}}
                        {{--<div class="nav-tabs-custom">--}}
                            {{--<div class="box-body">--}}
                                {{--@include ('admin.saloon.form')--}}
                            {{--</div>--}}
                            {{--<div class="box-footer">--}}
                                {{--<a href="{{ url('admin/saloon') }}" ><button class="btn btn-default" type="button">Back</button></a>--}}
                                {{--<button class="btn btn-info pull-right" type="submit">Add</button>--}}
                            {{--</div>--}}
                            {{--{!! Form::close() !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#saloonManagement" data-toggle="tab" aria-expanded="true">Salon Details</a></li>
                    <li class=""><a href="#" data-toggle="tab" aria-expanded="false">Service Details</a></li>
                    <li class=""><a href="#" data-toggle="tab" aria-expanded="false">Working Days</a></li>
                    <li class=""><a href="#">Salon Images</a></li>
                </ul>

                {!! Form::open(['url' => url('admin/saloon'), 'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return validation();']) !!}

                <input type="hidden" name="mod" id="mod" value="Add">

                <div class="tab-content">
                    <div class="tab-pane active" id="saloonManagement">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">
                                    <div class="box-body">
                                        @include ('admin.saloon.form')
                                    </div>
                                    <div class="box-footer">
                                        <a href="{{ url('admin/saloon') }}"><button class="btn btn-default" type="button">Back</button></a>
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save_next">Save & Next</button> 
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save" style="margin-right: 5px;">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </section>
    </div>
@endsection



