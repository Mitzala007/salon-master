@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

{{--        @include('admin.saloon.assign_salon')--}}

        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('error')}}
                </div>
            @endif

                <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs">
                        <li class=""><a href="{{url('admin/saloon/'.$id .'/edit')}}" >Salon Details</a></li>
                        <li class=""><a href="{{url('admin/saloon/services_edit/'.$id)}}" >Service Details</a></li>
                        <li class="active"><a href="#">Working Days</a></li>
                        <li class=""><a href="{{url('admin/saloon/images/'.$id.'/edit')}}">Salon Images</a></li>
                    </ul>

                    {!! Form::model($saloon,['url' => url('admin/saloon/save_days/'.$saloon->id),'method'=>'put' ,'class' => 'form-horizontal']) !!}
                    <div class="box box-info">
                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="example2">
                                <thead>
                                <tr>
                                    <th style="width: 20%"><label class="control-label" for="work_days">Work Days</label></th>
                                    <th style="width: 20%"><label class="control-label" for="start_hour">Start Hour</label></th>
                                    <th style="width: 20%"> <label class="control-label" for="end_hour">End Hour</label></th>
                                </tr>
                                <tr>
                                    <td colspan="3" class="label-check">
                                        <div class="checkbox">
                                            <input type="checkbox" id="all_days">
                                            <label for="all_days">All Days</label>
                                        </div>
                                    </td>
                                </tr>
                                </thead>
                                <tbody id="">
                                <?php $cnt = 1;?>
                                @foreach (\App\saloon::$work_days as $day => $display)
                                    <tr>
                                        <td class="label-check">
                                            <div class="checkbox">
                                                <input class="work_days" type="checkbox" id="check1_Opt{{$cnt}}" @if(isset($working_days[$day])) checked @endif name="work_days[]" value="{{ $day }}">
                                                <label for="check1_Opt{{$cnt}}">{{$display}}</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-2 bootstrap-timepicker">
                                                <input type="text" class="form-control timepicker" name="start_hour[{{ $day }}]" value="{{ ( ! empty($working_days[$day]['start_hour']) ? $working_days[$day]['start_hour'] : '08:00') }}" autocomplete="off">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-2 bootstrap-timepicker">
                                                <input type="text" class="form-control timepicker" name="end_hour[{{ $day }}]" value="{{ ( ! empty($working_days[$day]['end_hour']) ? $working_days[$day]['end_hour'] : '20:00') }}" autocomplete="off">
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $cnt++;?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url('admin/saloon/services/'.$saloon->id) }}"><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit" name="save" value="save_next">Save & Submit</button> 
                            <button class="btn btn-info pull-right" type="submit" name="save" value="save" style="margin-right: 5px;">Save</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
        </section>
    </div>
@endsection

@section('jquery')
    <script type="text/javascript">
        $("#start_hour").timepicker({
            showPeriod: true
        });

        $("#end_hour").timepicker({
            showPeriod: true
        });

        $('#all_days').click(function () {
            $('input:checkbox[class=work_days]').prop('checked', this.checked);
        });
    </script>
@endsection
