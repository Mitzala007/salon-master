@extends('admin.layouts.app')

@section('content')
    <style>
        .select2-container{
            width: 65%!important;
        }
    </style>

    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>



        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('error')}}
                </div>
            @endif

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li class=""><a href="{{url('admin/saloon/'.$id .'/edit')}}">Salon Details</a></li>
                    <li class="active"><a href="#">Service Details</a></li>
                    <li class=""><a href="#">Working Days</a></li>
                    <li class=""><a href="#">Salon Images</a></li>
                </ul>

                {!! Form::model($saloon,['url' => url('admin/saloon/save_services/'.$saloon->id),'method'=>'put' ,'class' => 'form-horizontal']) !!}
                <div class="box box-info">
                    <div class="box-body">
                        <table class="table table-bordered table-striped" id="example2">
                            <thead>
                            <tr>
                                <th style="width: 20%"><label class="control-label">Service Type</label></th>
                                <th style="width: 20%"><label class="control-label">Charges (USD)</label></th>
                                <th style="width: 20%"><label class="control-label">Duration (Minutes)</label></th>
                                <th><label class="control-label">Avail Discount</label></th>
                                <th><label class="control-label">Discount (%)</label></th>
                            </tr>
                            </thead>
                            <tbody id="">
                            @foreach ($type as $key => $value)
                                <tr>
                                    <td>{!! Form::checkbox('type_id[]', $key, isset($services[$key])? 1 : null, ['class' => 'flat-red'])!!}&nbsp; {{$value}}</td>
                                    <td>
                                        <div class="col-sm-9 form-group{{ $errors->has('charges_'.$key) ? ' has-error' : '' }}">
                                            <input type="text" class="form-control" name="charges_{{ $key }}" @if(isset($services[$key]['charges'])) value="{{$services[$key]['charges']}}"@endif placeholder="0.00">
                                            @if ($errors->has('charges_'.$key))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('charges_'.$key) }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </td>
                                    <td>{!! Form::select('duration_'.$key, \App\Saloon_services::$durations, isset($services[$key]['duration'])?$services[$key]['duration']:null, ['class' => 'select2 form-control']) !!}</td>
                                    <td>
                                        @if($saloon->global_discount == 0)
                                            {!! Form::select('avail_discount_'.$key, $avail_discount, isset($services[$key]['avail_discount'])?$services[$key]['avail_discount']:null, ['class' => 'select2 form-control', 'disabled']) !!}
                                        @else
                                            {!! Form::select('avail_discount_'.$key, $avail_discount, isset($services[$key]['avail_discount'])?$services[$key]['avail_discount']:null, ['class' => 'select2 form-control']) !!}
                                        @endif
                                    </td>
                                    <td>
                                        @if($saloon->global_discount == 0)
                                            {!! Form::select('discount_'.$key, \App\Saloon_services::$discounts, isset($services[$key]['discount'])?$services[$key]['discount']:null, ['class' => 'select2 form-control','disabled']) !!}
                                        @else
                                            {!! Form::select('discount_'.$key, \App\Saloon_services::$discounts, isset($services[$key]['discount'])?$services[$key]['discount']:null, ['class' => 'select2 form-control']) !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/saloon/'.$saloon->id.'/edit') }}"><button class="btn btn-default" type="button">Back</button></a>
                        <button class="btn btn-info pull-right" type="submit" name="save" value="save_next">Save & Next</button> 
                        <button class="btn btn-info pull-right" type="submit" name="save" value="save" style="margin-right: 5px;">Save</button>
                   </div>
                </div>


                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection