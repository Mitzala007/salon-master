@extends('admin.layouts.app')
<?php $plan = 0; ?>
@if(\Illuminate\Support\Facades\Auth::user()->membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->membership_plan; ?>
@elseif(\Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->admin_membership_plan?>
@endif
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/saloon') }}"><i class="fa fa-dashboard"></i> Salon</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/saloon'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            {{--<div class="col-md-4 col-sm-5 col-xs-12" style="padding-top: 5px">--}}
                                {{--<select name="type" style="width: 100%" class="select2 form-control" id="type">--}}
                                    {{--<option value="">Please Select</option>--}}
                                    {{--<option value="owner" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='owner') selected="selected" @endif>Owner</option>--}}

                                    {{--<option value="title" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='title') selected="selected" @endif>Salon Name</option>--}}

                                    {{--<option value="start_hour" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='start_hour') selected="selected" @endif>Start Hour</option>--}}

                                    {{--<option value="end_hour" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='end_hour') selected="selected" @endif>End Hour</option>--}}

                                    {{--<option value="status" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='status') selected="selected" @endif>Status</option>--}}
                                {{--</select>--}}
                                {{--@if ($errors->has('type'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('type') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search" onkeyup="search_filters(event)">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                                {{--<a href="{{ url('admin/saloon/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>--}}
                                @if($plan==1 || $plan==2 || $plan==3)
                                    <button class="btn btn-info" type="button" data-toggle="modal" data-target="#myPlanModal"><span class="fa fa-plus"></span></button>
                                @else
                                    <a href="{{ url('admin/saloon/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>
                                @endif
                            @endif
                        <a href="{{ url('admin/saloon') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                    </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.saloon.table')
                </div>
            </div>
        </section>
    </div>

    <!--********************Plan Warning Model********************-->
    <div id="myPlanModal" class="fade modal modal-danger" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>!</b> Alert</h4>
                </div>
                <div class="modal-body">
                    <p>Please upgrade your plan for add multiple salons.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>
{{--<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">--}}
{{--<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>--}}
{{--<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">--}}
{{--<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>--}}
{{--<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>--}}


<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({

            url : page

        }).done(function (data) {

            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true

            });

        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        e.preventDefault();
        var search = $('#search').val();
        var type = $('#type').val();
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({

            url: '{{url('admin/saloon')}}',

            type: "get",

            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true

                });
            }

        });
    });
</script>