
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">

            <div class="box-body">
                <div class="col-sm-12">

                    <div class="col-sm-2">
                        <label class="control-label" for="address">Work Days</label>
                    </div>

                    <div class="col-sm-2">
                        <label class="control-label" for="start_hour">Start Hour</label>
                    </div>

                    <div class="col-sm-2">
                        <label class="control-label" for="end_hour">End Hour</label>
                    </div>

                    <br/>
                    <br/>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <input type="checkbox" id="all_days"> All Days
                            </div>
                        </div>
                    </div>
                    @foreach (\App\saloon::$work_days as $day => $display)
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <input class="work_days" type="checkbox" @if(isset($working_days[$day])) checked @endif name="work_days[]" value="{{ $day }}"> {{$display}}
                                </div>
                                <div class="col-sm-2 bootstrap-timepicker">
                                    <input type="text" class="form-control timepicker" name="start_hour[{{ $day }}]" value="{{ ( ! empty($working_days[$day]['start_hour']) ? $working_days[$day]['start_hour'] : '10:00') }}">
                                </div>
                                <div class="col-sm-2 bootstrap-timepicker">
                                    <input type="text" class="form-control timepicker" name="end_hour[{{ $day }}]" value="{{ ( ! empty($working_days[$day]['end_hour']) ? $working_days[$day]['end_hour'] : '22:00') }}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
