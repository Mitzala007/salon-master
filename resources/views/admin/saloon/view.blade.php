@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                Salon
                <small>View</small>
            </h1>
        </section>
        <section class="content">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="@if($service =="saloon_management") active @endif"><a href="#saloonManagement" data-toggle="tab" aria-expanded="true">Salon Details
                            Management</a></li>
                    @if (isset($saloon->id))
                        <li class="@if(isset($service) && $service =="subadmin_service") active @endif"><a href="#servicesManagement" data-toggle="tab" aria-expanded="false">Service Details</a></li>
                        <li class=""><a href="#workDays" data-toggle="tab" aria-expanded="false">Working Days</a></li>
                        <li class=""><a href="#salon_images" data-toggle="tab" aria-expanded="false">Salon Images</a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-pane @if($service =="saloon_management") active @endif" id="saloonManagement">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <th style="width: 8%">Salon Name</th>
                                                <td>{{$saloon->title}}</td>
                                            </tr>
                                            <tr>
                                                <th>Location</th>
                                                <td>{{$saloon->location}}</td>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td>{!! $saloon->description !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Phone</th>
                                                <td>{{$saloon->saloon_phone}}</td>
                                            </tr>
                                            <tr>
                                                <th>Salon Logo</th>
                                                <td>
                                                    @if($saloon->image!="" && file_exists($saloon->image))
                                                        <img src="{{ url($saloon->image) }}" width="150" style="padding-bottom:5px">
                                                    @else
                                                        <img src="{{ url('assets/dist/img/default-user.png') }}" width="150" style="padding-bottom:5px">
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Website Url</th>
                                                <td><a href="{{$saloon->website_url}}" target="_blank"> {{$saloon->website_url}} </a></td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>{!!  $saloon['status']=='active'? '<span class="label label-success" style="padding:5px 8px; font-size: 14px;">Active</span>' : '<span class="label label-danger" style="padding:5px 8px; font-size: 14px;">In-active</span>' !!}</td>
                                            </tr>

                                            <tr>
                                                <th>Global Discount </th>
                                                <td>{!!  $saloon['global_discount']=='1'? '<span class="label label-success" style="padding:5px 8px; font-size: 14px;">Yes</span>' : '<span class="label label-danger" style="padding:5px 8px; font-size: 14px;">No</span>' !!}</td>
                                            </tr>

                                            <tr>
                                                <th>Created At</th>
                                                <td>{{$saloon->created_at}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {{ Form::open(array('url' => 'admin/saloon/'.$saloon['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                                            <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Details" data-trigger="hover" type="submit" style="margin-top: 0.5%;" >Edit Details</button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (isset($saloon->id))
                        <div class="tab-pane @if(isset($service) && $service =="subadmin_service") active @endif" id="servicesManagement">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <div class="alert {{ $saloon->global_discount == 1 ? 'alert-success' : 'alert-danger' }}">
                                                Global Discount : {{ $saloon->global_discount == 1 ? 'Yes' : 'No' }}
                                            </div>
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th>Service Type</th>
                                                    <th>Charges (USD)</th>
                                                    <th>Duration (Minutes)</th>
                                                    <th>Avail Discount</th>
                                                    <th>Discount (%)</th>
                                                </tr>
                                                @foreach ($type as $key => $value)
                                                    @if(isset($services[$key]))
                                                        <tr>
                                                            <td>
                                                                {{ $value }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['charges'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['duration'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['avail_discount'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['discount'] }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if (isset($saloon->id))
                        <div class="tab-pane" id="workDays">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th>Working Days</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                </tr>
                                                @foreach ($work_days as $day)
                                                    <tr>
                                                        <td>
                                                            {{ ucfirst($day['work_day']) }}
                                                        </td>
                                                        <td>
                                                            {{ $day['start_hour'] }}
                                                        </td>
                                                        <td>
                                                            {{ $day['end_hour'] }}
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (isset($saloon->id))
                        <div class="tab-pane" id="salon_images">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th colspan="3">Salon Images</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <?php
                                                $no_cols = 12;
                                                $no_rows = 0;
                                                $td = 12 / $no_cols;
                                            ?>
                                            <div class="row" style="padding-top: 2%">
                                            @foreach($s_images as $s_img)
                                                <div class="col-md-{{$td}}">
                                                    <div style="background-image: url({{url($s_img['image_thumbnail'])}}); background-position: center;
                                                            background-repeat: no-repeat; background-size: contain; height: 80px; cursor: pointer;"
                                                         data-toggle="modal" data-target="#imageModal{{$s_img['id']}}"></div>
                                                </div>
                                               <?php $no_rows++;?>
                                                @if($no_rows % $no_cols == 0)
                                                </div>
                                                <div class="row" style="padding-top: 2%">
                                                @endif
                                            @endforeach
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection

<?php $cnt = 1;?>
@foreach($s_images as $s_img)
    <div id="imageModal{{$s_img['id']}}" class="fade modal modal-primary" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                    <h4 class="modal-title">Image - {{$cnt}}</h4>
                </div>
                @if($s_img['image']!="" && file_exists($s_img['image']))
                    <div class="modal-body" style="background-image: url({{ url($s_img->image) }}); background-position: center; background-repeat: no-repeat;
                            background-size: contain; height: 300px;">
                    </div>
                @endif
            </div>
        </div>
    </div>
    <?php $cnt++;?>
@endforeach
