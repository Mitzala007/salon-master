@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        {{--@include('admin.saloon.assign_salon')--}}

        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif

            @if($errors->has('work_days'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{$errors->first('work_days')}}
                </div>
            @endif

            @if($errors->has('type_id'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{$errors->first('type_id')}}
                </div>
            @endif

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#">Salon Details</a></li>
                    <li class=""><a href="{{url('admin/saloon/services_edit/'.$id)}}" >Service Details</a></li>
                    <li class=""><a href="{{url('admin/saloon/work_days/'.$id)}}">Working Days</a></li>    
                    <li class=""><a href="{{url('admin/saloon/images/'.$id.'/edit')}}">Salon Images</a></li>
                </ul>

                {!! Form::model($saloon,['url' => url('admin/saloon/'.$saloon->id),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return validation();']) !!}

                <input type="hidden" name="mod" id="mod" value="Edit">

                <div class="tab-content">
                    <div class="tab-pane active" id="saloonManagement">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-info">
                                    <div class="box-body">
                                        @include ('admin.saloon.form')
                                    </div>
                                    <div class="box-footer">
                                        <a href="javascript: history.go(-1)"><button class="btn btn-default" type="button">Back</button></a>
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save_next">Save & Next</button>
                                        <button class="btn btn-info pull-right" type="submit" name="save" value="save" style="margin-right: 5px;">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

