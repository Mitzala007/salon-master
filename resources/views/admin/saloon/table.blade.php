<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th class="table-text table-th">Edit</th>
        {{--<th>Id</th>--}}
        {{--@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')--}}
            {{--<th>Owner</th>--}}
        {{--@endif--}}
        <th>Salon Name</th>
        <th>Phone</th>
        <th>Rating Avg.</th>
        <th class="table-text table-th">Image</th>
        <th class="table-text table-th">Status</th>
        <th class="table-text table-th">Action</th>
    </tr>
    </thead>
    <tbody id="">
    @foreach ($saloon as $list)
        <?php
        $flag = 0;
        $check_salon_owner = \App\saloon_employees::where('saloon_id',$list['id'])->where('is_salon_owner',1)->count();
        if($check_salon_owner > 0){
            $flag = 1;
        }
        ?>
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}" @if($flag==0) data-toggle="tooltip" title="No owner is assign to this salon !" data-trigger="hover"  style="background-color: #d3d3d3;" @endif disabled="disabled">
            <td class="table-text">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/saloon/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit salon" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
{{--            <td>{{ $list['id'] }}</td>--}}
            {{--@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')--}}
                {{--<td>{{ $list['User']['email'] }}</td>--}}
            {{--@endif--}}

            <td>{{$list['title']}}</td>
            <td>{{$list['saloon_phone']}}</td>
            <td>
                @for($i=0; $i<$list['rating']; $i++)
                    <i class="fa fa-star" style="color: #daa520;"></i>
                @endfor

                @for($i=0; $i<5-$list['rating']; $i++)
                    <i class="fa fa-star" style="color: #d3d3d3;"></i>
                @endfor

                {{--{{$list['rating']}}--}}
            </td>

            <td class="table-text ">
                <a data-toggle="modal" data-target="#imageModal{{$list['id']}}" style="cursor: pointer;">
                @if($list['image']!="" && file_exists($list['image']))
                    <img src="{{ url($list->image) }}" width="30">
                @else
                    {{--<img src="{{ url('assets/dist/img/default-user.png') }}" width="30">--}}
                @endif
                </a>
            </td>
            <td class="table-text table-th">
                @if($flag==0)
                    @if($list['status'] == 'active')
                        <div class="btn-group-horizontal">
                            <button class="btn btn-success" type="button" style="height:28px; padding:0 12px" data-toggle="modal" data-target="#myModal-inactive" ><span class="ladda-label" >Active</span> </button>
                        </div>
                    @endif
                    @if($list['status'] == 'in-active')
                        <div class="btn-group-horizontal">
                            <button class="btn btn-danger" type="button" style="height:28px; padding:0 12px" data-toggle="modal" data-target="#myModal-inactive" ><span class="ladda-label">In Active</span></button>
                        </div>
                    @endif
                @else
                    @if($list['status'] == 'active')
                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                            <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px" ><span class="ladda-label" >Active</span> </button>
                        </div>
                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                            <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label" >In Active</span></button>
                        </div>
                    @endif
                    @if($list['status'] == 'in-active')
                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                            <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px" ><span class="ladda-label">In Active</span></button>
                        </div>
                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                            <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                        </div>
                    @endif
                @endif
            </td>

            <td class="table-text table-th">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/saloon/'.$list['id'], 'method' => 'get','style'=>'display:inline')) }}
                    <button class="btn btn-info tip res-btn" data-toggle="tooltip" title="View salon" data-trigger="hover" type="submit"><i class="fa fa-eye"></i></button>
                    {{ Form::close() }}
                    <span data-toggle="tooltip" title="Delete salon" data-trigger="hover">
                        <button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                    </span>
                </div>
            </td>
        </tr>


        <div id="myModal-inactive" class="fade modal modal-danger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Note!</h4>
                    </div>
                    <div class="modal-body">
                        <p>No owner is assign to this salon !</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete saloon</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this saloon ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" onclick="destroy_salon({{$list['id']}})">Delete</button>
                    </div>
                </div>
            </div>
        </div>

        <!---------------------------------------------------IMAGE MODAL---------------------------------------------------------->
        <div id="imageModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['title']}}</h4>
                    </div>
                    @if($list['image']!="" && file_exists($list['image']))
                        <div class="modal-body" style="background-image: url({{ url($list->image) }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @else
                        <div class="modal-body" style="background-image: url({{ url('assets/dist/img/default-user.png') }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @endif

                </div>
            </div>
        </div>

    @endforeach

</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $saloon])</div>

<script>

    function slideout() {
        setTimeout(function() {
            $("#responce").slideUp("slow", function() {
            });

        }, 3000);
    }

    $("#responce").hide();
    $( function() {
        $( "#sortable" ).sortable({opacity: 0.9, cursor: 'move', update: function() {
            var order = $(this).sortable("serialize") + '&update=update';
            $.get("{{url('admin/saloon/reorder')}}", order, function(theResponse) {
                $("#responce").html(theResponse);
                $("#responce").slideDown('slow');
                slideout();
            });
        }});
        $( "#sortable" ).disableSelection();
    } );



</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var user_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/saloon/assign')}}',
                type: "put",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).show();
                    $('#assign_add_'+user_id).hide();
                }
            });
        });

        $('.unassign').click(function(){
            var user_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/saloon/unassign')}}',
                type: "put",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).hide();
                    $('#assign_add_'+user_id).show();
                }
            });
        });
    });

    function destroy_salon(id)
    {
        $.ajax({
            url:'saloon/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'saloon';
                window.location.href = new_url;
            }
        });
    }

</script>
