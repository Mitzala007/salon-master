<div class="form-group">
    <div class="col-xs-5">
        <button class="btn btn-info pull-right add_field_button">Add Sub Category</button>
    </div>
</div>
<div class="input_fields_wrap">
    @if(!empty($sub_category))
        <?php $count=0; $option_array = ""; $is_ct=0; ?>

        @foreach($sub_category as $key=>$value)
            <?php $count++; ?>
            <div class="form-group" id="opt{{$value['id']}}">
                <label class="col-xs-2 control-label">Title{{$count}}</label>
                <div class="col-xs-5">
                    <input type="text" class="form-control" name="subcategory_{{$value['id']}}" id="{{ $value['id'] }}"
                           value="{{ $value['title'] }}"/>
                </div>
                <button type="button" onclick="remove_opt({{$value['id']}})" class="btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
            </div>
            <?php $option_array.= $value['id'].","; ?>

        @endforeach
        <input type="hidden" name="option_array" id="option_array" value="{{ trim($option_array,",") }}">
        <input type="hidden" name="total_array" id="total_array" value="{{ $count }}">

    @else
        <?php $count=1; ?>
        <?php $is_ct=1; ?>
        <div class="form-group">
            <label class="col-xs-2 control-label">Title1</label>
            <div class="col-xs-5">
                <input type="text" class="form-control" name="subcategory[]"/>
            </div>
            <button type="button" class="btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
        </div>
        <input type="hidden" name="total_array" id="total_array" value="{{ $count }}">
    @endif
</div>

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script>
    $(document).ready(function () {
        //var max_fields      = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = {{ $count }}; //initlal text box count
        var y = {{ $is_ct }};
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            //if(x < max_fields){ //max input box allowed
            x++; //text box increment
//                $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
            y++;

            $(wrapper).append('<div class="input_fields_wrap">' +
                '<div class="form-group">' +
                    '<label class="col-xs-2 control-label">Title' + x + '</label>' +
                    '<div class="col-xs-5">' +
                        '<input type="text" class="form-control" name="subcategory[]" />' +
                    '</div>' +
                    '<button type="button" class="btn btn-danger pull-left remove_field"><i class="fa fa-trash"></i></button>' +
                '</div>'); //add input box
            //}

            document.getElementById('total_array').value = x;
        });



        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });


    function remove_opt(id)
    {
        $('#opt'+id).remove();
    }

</script>

