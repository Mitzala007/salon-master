@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" >
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/category') }}"><i class="fa fa-dashboard"></i> Category </a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/category'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">

                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                        @if(config('siteVars.sitetitle') != "Nail Master")
                               <a href="{{ url('admin/category/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>
                        @endif
                        <a href="{{ url('admin/category') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                    </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive " id="itemlist">
                    @include('admin.category.table')
                </div>
            </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>
