<div id="load" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped" id="example2">
    <thead>
        <tr>
            <th class="table-text table-th">Edit</th>
            <th>Image</th>
            <th style="width: 30%">Category Title</th>
            <th>Sub Category</th>
            @if(config('siteVars.sitetitle') != "Nail Master")
                <th class="table-text table-th">Status</th>
            @endif
            <th>Action</th>
            {{--<th class="table-text table-th">Delete</th>--}}
        </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($category as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td class="table-text">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/category/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                    <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Category" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
            <td>
                @if($list['image']!="" && file_exists($list['image']))
                    <img src="{{ url($list->image) }}" width="50">
                @else
                @endif
            </td>
            <td>{{$list['title']}}</td>
            <td>
                <?php $sc_count = \App\Subcategory::where('category_id',$list['id'])->count(); ?>
                {{$sc_count}}
            </td>
            @if(config('siteVars.sitetitle') != "Nail Master")
            <td class="table-text">
                @if($list['status'] == 'active')
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                        <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}" url="{{url('admin/category/unassign')}}" type="button" style="height:28px; padding:0 12px" ><span class="ladda-label" >Active</span> </button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                        <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}" url="{{url('admin/category/assign')}}"   type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                @endif

                @if($list['status'] == 'in-active')
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                        <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  url="{{url('admin/category/assign')}}"   type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                        <button class="btn  btn-success unassign ladda-button" id="remove"  url="{{url('admin/category/unassign')}}" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                    </div>
                @endif
            </td>
            @endif
           <td class="table-text table-th">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/category/'.$list['id'], 'method' => 'get','style'=>'display:inline')) }}
                    <button class="btn btn-info tip res-btn" data-toggle="tooltip" title="View salon" data-trigger="hover"
                            type="submit"><i class="fa fa-eye"></i></button>
                    {{ Form::close() }}
                    {{--<span data-toggle="tooltip" title="Delete salon" data-trigger="hover">--}}
                        {{--<button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>--}}
                    {{--</span>--}}
                </div>
            </td>

            {{--<td class="table-text">--}}
                {{--<div class="btn-group-horizontal">--}}
                    {{--<span data-toggle="tooltip" title="Delete category" data-trigger="hover">--}}
                        {{--<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>--}}
                    {{--</span>--}}
                {{--</div>--}}
            {{--</td>--}}
        </tr>

        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
            {{ Form::open(array('url' => 'admin/category/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete Category</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this Category ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline">Delete</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    @endforeach

</table>
<div style="text-align:right;float:right;" class="ajaxpagination"> @include('admin.pagination.limit_links', ['paginator' => $category])</div>
<script src="{{ URL::asset('assets/dist/js/custom.js')}}"></script>

<script>

    function slideout() {
        setTimeout(function() {
            $("#responce").slideUp("slow", function() {
            });

        }, 3000);
    }

    $("#responce").hide();
    $( function() {
        $( "#sortable" ).sortable({opacity: 0.9, cursor: 'move', update: function() {
            var order = $(this).sortable("serialize") + '&update=update';
            $.get("{{url('admin/category/reorder')}}", order, function(theResponse) {
                $("#responce").html(theResponse);
                $("#responce").slideDown('slow');
                slideout();
            });
        }});
        $( "#sortable" ).disableSelection();
    } );

</script>