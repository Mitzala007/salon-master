@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>View</small>
            </h1>
            <ol class="breadcrumb">
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                <li><a href="{{url('admin/category/')}}">{{ $menu }}</a></li>
                <li class="active">Category View</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-----------------------------------SubCategory DETAILS----------------------------------->
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Category</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">

                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Id</th>
                                            <td>{{$category_id->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>title</th>
                                            <td>{{$category_id->title}}</td>
                                        </tr>
                                        <tr>
                                            <th>image</th>
                                            <td>
                                                @if($category_id->image!="" && file_exists($category_id->image))
                                                    <img src="{{ url($category_id->image) }}" height="100"></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <th>icon</th>
                                            <td> @if($category_id->image!="" && file_exists($category_id->image))
                                                    <img src="{{ url($category_id->icon) }}" height="100"></td>
                                            @endif
                                            </td>
                                           </tr>
                                        <tr>
                                            <th>Status</th>
                                            <td>{{$category_id->status}}</td>
                                        </tr>
                                          </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($category_id['subcategory']->count() > 0)
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sub Category Listing</h3>
                        </div>
                        <div class="box-body">
                            <div class="row ">
                                <div class="col-sm-12" >
                                    <table class="table table-bordered table-responsive">
                                        <tbody>
                                        @foreach($category_id['subcategory'] as $k => $v)
                                            <tr>
                                                <td>{{$v['title']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection


<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        $('#load_employee').append('<img style="position: absolute; width:80px; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        $.ajax({
            url : page
        }).done(function (data) {
            $("#booking_items").empty().html(data);
            $("#employee_table").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });

            $('#example23').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    /*BOOKING SEARCHING*/

    $(document).on('click','#booking_searchbtn',function(e){
        e.preventDefault();

        var search = $('#search').val();
        var type = $('#booking_search').val();
        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/users/booking')}}'+'/'+type,
            type: "get",
            data: {'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
//                alert(data);
                $("#booking_items").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });

    /*EMPLOYEE SEARCHING*/

    $(document).on('click','#employee_searchbtn',function(e){
        e.preventDefault();

        var search_employee = $('#search_employee').val();
        var type_employee = $('#employee_search').val();
        $('#load_employee').append('<img style="position: absolute; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/users/employee')}}'+'/'+type_employee,
            type: "get",
            data: {'search_employee':search_employee,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
//                alert(data);
                $("#employee_table").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example23').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>
