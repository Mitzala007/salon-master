{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="title_lb">Category Title <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title','id'=>'title']) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
        <strong><span id="title_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="image" id="image_lb">Logo <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        <div class="">
            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}
        </div>
        <?php
        if (!empty($category->image) && $category->image != "") {
        ?>
        <br><img id="DisplayImage" src="{{ url($category->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';
        } ?>
        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
        <strong><span id="image_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="image" id="image_lb">Icon <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        <div class="">
            {!! Form::file('icon', ['class' => '', 'id'=> 'icon', 'onChange'=>'AjaxUploadImage1(this)']) !!}
        </div>
        <?php
        if (!empty($category->icon) && $category->icon != "") {
        ?>
        <br><img id="DisplayImage1" src="{{ url($category->icon) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<br><img id="DisplayImage1" src="" width="150" style="display: none;"/>';
        } ?>
        @if ($errors->has('icon'))
            <span class="help-block">
                    <strong>{{ $errors->first('icon') }}</strong>
                </span>
        @endif
        <strong><span id="image_msg_icon" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Category::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>
<hr>
@include('admin.category.form-option')
<script type="text/javascript">

    function AjaxUploadImage(obj,id){
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }
    function imageIsLoaded(e) {
        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');
    };


    function AjaxUploadImage1(obj,id){
        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded1;
            reader.readAsDataURL(obj.files[0]);
        }
    }
    function imageIsLoaded1(e) {
        $('#DisplayImage1').css("display", "block");
        $('#DisplayImage1').attr('src', e.target.result);
        $('#DisplayImage1').attr('width', '150');
    };

</script>

<script type="text/javascript">
    function validation()
    {
        var flag = 0;

        if(document.getElementById("title").value.split(" ").join("") == "")
        {
            document.getElementById('title_msg').innerHTML = 'The Category title field is required.';
            document.getElementById("title").focus();
            document.getElementById("title").style.borderColor ='#dd4b39';
            document.getElementById("title_lb").style.color ='#dd4b39';
            document.getElementById('title_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("title").style.borderColor ='#d2d6de';
            document.getElementById("title_lb").style.color ='#333';
            document.getElementById('title_msg').style.display = 'none';
        }

        var active = 'active';
        var inactive = 'in-active';
        if(document.getElementById("status_"+active).checked == false && document.getElementById("status_"+inactive).checked == false)
        {
            document.getElementById('status_msg').innerHTML = 'The status field is required.';
            document.getElementById("status_val_"+active).style.color ='#dd4b39';
            document.getElementById("status_val_"+inactive).style.color ='#dd4b39';
            document.getElementById("status_lb").style.color ='#dd4b39';
            document.getElementById('status_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("status_val_"+active).style.color ='#333';
            document.getElementById("status_val_"+inactive).style.color ='#333';
            document.getElementById("status_lb").style.color ='#333';
            document.getElementById('status_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>