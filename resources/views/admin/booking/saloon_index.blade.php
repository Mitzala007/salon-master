@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                {{$menu}}

            </h1>

            <ol class="breadcrumb">

                <li><a href="{{ url('admin/booking') }}"><i class="fa fa-dashboard"></i> Booking</a></li>

            </ol>

        </section>

        <section class="content">

            @include ('admin.error')

            <div id="responce" name="responce" class="alert alert-success" style="display: none">

            </div>

            <div class="box box-primary form-horizontal">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-2 control-label">Start Date</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon" style="border-top-left-radius: 4px;border-bottom-left-radius: 4px;"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('start_date', null, ['class' => 'form-control date-input','id'=>'datepicker']) !!}
                            </div>
                        </div>

                        <div class="col-sm-1 control-label">End Date</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon" style="border-top-left-radius: 4px;border-bottom-left-radius: 4px;"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('end_date', null, ['class' => 'form-control date-input','id'=>'datepicker1']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2 control-label">Search Text</div>
                        <div class="col-sm-3">{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Booking Name / User','id'=>'search']) !!}</div>

                        <div class="col-sm-1 control-label">Status</div>
                        <div class="col-sm-3">{!! Form::select('status', [''=>'Please select']+\App\Booking::$status, null, ['class' => 'select2 form-control','id'=>'status']) !!}</div>
                    </div>

                </div>

                <div class="box-footer">
                    <button class="btn btn-primary pull-right" type="submit" id="searchbtn"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>

            <div class="box box-info">
                <div class="box-header">

                        <h3 class="box-title" style="float:right;">

                            <a href="{{ url('admin/booking') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>

                        </h3>

                </div>
                <!-- /.box-header -->
                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.booking.table')
                </div>

            </div>

        </section>

    </div>

@endsection



<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>

<script>Ladda.bind( 'input[type=submit]' );</script>

<script type="text/javascript">

    function Statuschange(val,id){

        if(val == '') val = 0;
        $.ajax({
            url: '{{ url('admin/booking') }}/'+val +'/'+ id,
            error:function(){
            },
            success: function(result){

            }
        });
    }

</script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({

            url : page

        }).done(function (data) {
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $("#itemlist").empty().html(data);
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true

            });
            $(".select2").select2();
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });


        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){

        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();

        var datepicker = $('#datepicker').val();
        var datepicker1 = $('#datepicker1').val();
        var status = $('#status').val();
        var search = $('#search').val();
        var type = $('#type').val();
        $('#load').append('<img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/booking/'.$id.'/list')}}',
            type: "get",
            data: {'start_date':datepicker,'end_date':datepicker1,'status':status,'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                // alert(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $("#itemlist").empty().html(data);
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true

                });
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
                $(".select2").select2();
            }
        });
    });
</script>