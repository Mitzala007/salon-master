<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script>
    $(function () {

        $(".select2").select2();
    });

    $('#datepicker_walkin').datepicker({
        format: 'm/d/yyyy',
        startDate: '+0d',
        autoclose: true
    });
</script>
<div id="add_booking_modal" class="fade modal modal-primary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                <h3 class="modal-title text-center">Add Booking</h3>
            </div>
            <div class="modal-body form-horizontal" style="color: #444!important; font-size: 14px!important;">
                {{--{!! Form::open(['class' => 'form-horizontal','files'=>true]) !!}--}}
                <div class="box-body">
                    {!! Form::hidden('redirects_to', URL::previous()) !!}

                    <div class="form-group{{ $errors->has('customer type') ? ' has-error' : '' }}" id="customer_type_div">
                        <label class="col-md-3 control-label" for="role">Customer Type</label>
                        <div class="col-md-9">
                            @foreach (\App\Booking::$customer_type as $key => $value)
                                <label class="label-text1">
                                    @if($key == '0')
                                        {!! Form::radio('customer_type',$key, null , ['id'=>'customer_type1','class' => '','checked', 'onclick'=>'get_type('.$key.')']) !!}<span class="label-text" style="margin-right: 26px; font-weight: bold;">{{ $value }}</span>
                                    @else
                                        {!! Form::radio('customer_type', $key, null , ['id'=>'customer_type2','class' => '', 'onclick'=>'get_type('.$key.')']) !!}<span class="label-text" style="margin-right: 26px; font-weight: bold;">{{ $value }}</span>
                                    @endif
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('search_user') ? ' has-error' : '' }}" id="shortdis" style=" display: none;">
                        <label class="col-md-3 control-label" for="search_user" id="search_user">Search User</label>
                        <div class="col-md-9">
                            {!! Form::select('search_user', ['Please select']+$customer, null, ['class' => 'select2 form-control', 'id'=>'user_ids','style'=>'width:100%','onchange'=>'call_user(this.value)']) !!}
                            @if ($errors->has('search_user'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('search_user') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="name" id="name_lb"><b>Name</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name','id'=>'name']) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                            <strong><span id="name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="phone" id="phone_lb"><b>Phone</b><span class="text-red">*</span></label>

                        <div class="col-md-2" id="phone_display" style="display: block">
                        <select name="country" id="country" class="select2 form-control" style="width: 100%;">
                            <option value='1'>+1</option>
                            <option value='91'>+91</option>
                            <option value='61'>+61</option>
                        </select>
                        </div>
                        <div class="col-md-7">
                            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => ' Enter Phone','id'=>'phone']) !!}
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                            <strong><span id="phone_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="email" id="email_lb"><b>Email</b><span class="text-red"></span></label>
                        <div class="col-md-9">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => ' Enter Email','id'=>'email']) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <strong><span id="email_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('bdate') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="date" id="bdate_lb"><b>Booking Date</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::text('bdate', date('m/d/Y'), ['class' => 'form-control bdate', 'placeholder' => ' Enter Date' ,'id'=>'datepicker_walkin']) !!}
                            @if ($errors->has('bdate'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bdate') }}</strong>
                                </span>
                            @endif
                            <strong><span id="bdate_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('service_type') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="service_type" id="service_lb"><b>Service Type</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <div class="row">
                                <label class="col-sm-12">
                                    <select name="service_type[]" id="service_type" class="form-control select2" style="width: 100%" multiple>
                                        @foreach ($service_type as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
{{--                                        {!! Form::select('service_type[]',$service_type, null, ['class' => 'select2 form-control','style'=>'width:100%','multiple']) !!}--}}
                                    {{--{!! Form::checkbox('service_type[]', $key, null,['class' => 'flat-red'])!!}<span style="margin-right: 10px;padding-left: 5px;">{{ $value }}</span>--}}
                                        @endforeach
                                    </select>
                                </label>
                                @if ($errors->has('service_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <strong><span id="service_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('no_of_persons') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="no_of_persons" id="person_lb"><b>No of persons</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::number('no_of_persons', 1, ['class' => 'form-control', 'placeholder' => ' Enter No of persons','id'=>'no_of_persons','min'=>'1']) !!}
                            @if ($errors->has('no_of_persons'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_of_persons') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="emp_id" id="saloon_id">Technician Name<span class="text-red"></span></label>
                        <div class="col-md-9">
                            {!! Form::select('emp_id', $emp_id, null, ['id'=>'emp_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                            @if ($errors->has('emp_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('emp_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="box-footer" style="text-align: center">
                    <button class="btn col-md-12 btn-info" onclick="do_booking()">ADD</button>
                </div>

                {{--                {!! Form::close() !!}--}}
            </div>
        </div>
    </div>
</div>
