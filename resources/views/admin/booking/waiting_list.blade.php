@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Add</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                @if($booking['salon']['status']=="open")
                    <button class="btn btn-info pull-right" data-toggle="modal" data-target="#add_booking_modal" type="button"><i class="fa fa-plus" style="margin-right: 5px;"></i> Add Walkin Customer</button>
                @endif

            </ol>
        </section>

        <section class="content">

            @if($booking['salon']['status']=="close")
                <div class="col-md-12">
                    <div class="alert alert-danger text-bold text-center">
                        Your salon is closed! <br>Click <a href="{{ url('admin/saloon/work_days/'.$booking['salon']['id']) }}">here </a>to open your Salon
                    </div>
                </div>
            @endif

            @include ('admin.error')
            <div class="row" id="itemlist">
                @include('admin.booking.waiting_list_table')

            </div>
        </section>
    </div>

@endsection
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<script>

    function do_booking() {

        /*   VALIDATION */
        var flag = 0;
        if(document.getElementById("name").value.split(" ").join("") == "")
        {
            document.getElementById('name_msg').innerHTML = 'The name field is required.';
            document.getElementById("name").focus();
            document.getElementById("name").style.borderColor ='#dd4b39';
            document.getElementById("name_lb").style.color ='#dd4b39';
            document.getElementById('name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("name").style.borderColor ='#d2d6de';
            document.getElementById("name_lb").style.color ='#333';
            document.getElementById('name_msg').style.display = 'none';
        }

        if(document.getElementById("phone").value.split(" ").join("") == "")
        {
            document.getElementById('phone_msg').innerHTML = 'The phone field is required.';
            document.getElementById("phone").focus();
            document.getElementById("phone").style.borderColor ='#dd4b39';
            document.getElementById("phone_lb").style.color ='#dd4b39';
            document.getElementById('phone_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var phone1=document.getElementById("phone").value;
            if(isNaN(phone1))
            {
                document.getElementById('phone_msg').innerHTML = 'Please enter a valid mobile number.';
                document.getElementById("phone").focus();
                document.getElementById("phone").style.borderColor ='#dd4b39';
                document.getElementById("phone_lb").style.color ='#dd4b39';
                document.getElementById('phone_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("phone").style.borderColor ='#d2d6de';
                document.getElementById("phone_lb").style.color ='#333';
                document.getElementById('phone_msg').style.display = 'none';
            }
        }

    //        if(document.getElementById("email").value.split(" ").join("") == "")
    //        {
    //            document.getElementById('email_msg').innerHTML = 'The email field is required.';
    //            document.getElementById("email").focus();
    //            document.getElementById("email").style.borderColor ='#dd4b39';
    //            document.getElementById("email_lb").style.color ='#dd4b39';
    //            document.getElementById('email_msg').style.display = 'block';
    //            flag=1;
    //        }
    //        else
    //        {
    //            document.getElementById("email").style.borderColor ='#d2d6de';
    //            document.getElementById("email_lb").style.color ='#333';
    //            document.getElementById('email_msg').style.display = 'none';
    //        }

        if(document.getElementById("datepicker_walkin").value.split(" ").join("") == "")
        {
            document.getElementById('bdate_msg').innerHTML = 'The booking date field is required.';
            document.getElementById("datepicker_walkin").focus();
            document.getElementById("datepicker_walkin").style.borderColor ='#dd4b39';
            document.getElementById("bdate_lb").style.color ='#dd4b39';
            document.getElementById('bdate_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("datepicker_walkin").style.borderColor ='#d2d6de';
            document.getElementById("bdate_lb").style.color ='#333';
            document.getElementById('bdate_msg').style.display = 'none';
        }

//        var i,
//            chks = document.getElementsByName('service_type[]');
//        var checked = 0;
//        for (i = 0; i < chks.length; i++){
//            if (chks[i].checked) {
//                checked = 1;
//            }
//        }

        var checked = 0;
        var n = document.getElementById("service_type").options.length;
        for (var i = 0;i < n; i++){
            if (document.getElementById("service_type").options[i].selected === true) {
                checked = 1;
            }
        }


        if(checked==0){
            document.getElementById('service_msg').innerHTML = 'No service is selected';
            document.getElementById("service_lb").style.color ='#dd4b39';
            document.getElementById('service_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById("service_lb").style.color ='#333';
            document.getElementById('service_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }

        /* ---------------------------------- */


        var name = document.getElementById('name').value;
        var country = document.getElementById('country').value;
        var phone = document.getElementById('phone').value;
        var phone_number = country+phone;
        var email = document.getElementById('email').value;
        var bdate = document.getElementById('datepicker_walkin').value;
        if (document.getElementById('customer_type1').checked) {
            var type1 = document.getElementById('customer_type1').value;
        }
        else{
            var type1 = document.getElementById('customer_type2').value;
        }

        var customer_type = type1;
        var no_of_persons = document.getElementById('no_of_persons').value;

        var mode = $("#service_type option:selected").map(function(){return this.value}).get().join(',');
        var service_type = mode;

        var user_id = document.getElementById('user_ids').value;
        var emp_id = document.getElementById('emp_id').value;


        //document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        //document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url: '{{ url('admin/booking/store') }}',
            type:'post',
            data:{'service_type':service_type,'no_of_persons':no_of_persons,'name':name,'phone':phone_number,'email':email,'date':bdate,'customer_type':customer_type,'user_id':user_id,'emp_id':emp_id},
            success:function(data)
            {
                //document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                var myJSON = JSON.stringify(data);


                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){
                    $('#add_booking_modal').modal('hide');
                    document.getElementById('bodyid').style.opacity=1;
                    reload_view();
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

    function serve_now(id) {

        //document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        //document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url: '{{ url('admin/serve_now') }}',
            type:'post',
            data:{'id':id},
            success:function(data)
            {
                //document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){
                    reload_view();
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

    function end_now(bid) {

        //document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        //document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        var discount_amount = document.getElementById('discount_amount_'+bid).value;
        var remarks = document.getElementById('remarks_'+bid).value;
        var final_total = document.getElementById('final_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var point = document.getElementById('point_'+bid).value;
        var tax = document.getElementById('tax_'+bid).value;
        var payment_mode  = $("input:radio[name=payment_mode_"+bid+"]:checked").val();

        $.ajax({
            url: '{{ url('admin/end_now') }}',
            type:'post',
            data:{'bid':bid,'discount_amount':discount_amount,'remarks':remarks,'final_total':final_total,'additional_amount':additional_amount,'tax':tax,'point':point,'payment_mode':payment_mode},
            success:function(data)
            {
                //alert(data);
               // document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";

                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);

                if (JSONObject["Result"]==1){
                    $('#myModal_'+bid).modal('hide');
                    document.getElementById('bodyid').style.opacity=1;
                    reload_view();
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

    function cancel_booking() {
        document.getElementById('load').style.display="block";
        var bid = document.getElementById('cancel_booking_id').value;
        var uid = document.getElementById('cancel_user_id').value;

        $.ajax({
            url: '{{ url('admin/cancel_booking') }}',
            type:'post',
            data:{'bid':bid,'uid':uid},
            success:function(data)
            {
                // document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";

                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);

                if (JSONObject["Result"]==1){
                    $('#cancel_booking_modal').modal('hide');
                    document.getElementById('bodyid').style.opacity=1;
                    reload_view();
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }

    setInterval(reload_view, 60000);

    function reload_view() {

        var dt = document.getElementById('updated_at').value;
        var type="ajax";
        $.ajax({

            url: '{{ url('admin/waiting_list') }}',
            type:'get',
            data:{'req_type':type,'updated_at':dt},
            success:function(data)
            {
                if(data!="0"){
                    $("#itemlist").empty().html(data);
                    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                }
            }
        });
    }
    
    function get_booking(id,mode) {


        document.getElementById('ename').disabled=true;
        document.getElementById('eemail').disabled=true;
        document.getElementById('ephone').disabled=true;
        if(mode=='edit'){
            document.getElementById('datepicker1').disabled=true;
        }

        var type="ajax";
        $.ajax({
            url: '{{ url('admin/get_booking_details') }}',
            type:'post',
            data:{'id':id},
            success:function(data)
            {
                var myJSON = JSON.stringify(data);
                var JSONObject = JSON.parse(myJSON);
                document.getElementById('ename').value=JSONObject['user']['name'];
                document.getElementById('eemail').value=JSONObject['user']['email'];
                document.getElementById('ephone').value=JSONObject['user']['phone'];
                document.getElementById('eno_of_persons').value=JSONObject['booking']['no_of_persons'];
                document.getElementById('book_id').value=JSONObject['booking']['id'];
                document.getElementById('eservice_type').innerHTML=JSONObject['service_dropdown'];
                document.getElementById('eemp_id').innerHTML=JSONObject['employee_dropdown'];

                $("#eservice_type").select2();
                $("#eemp_id").select2();
            }
        });
    }
    
    function edit_booking() {

//        var flag = 0;
//        var checked = 0;
//        var n = document.getElementById("eservice_type").options.length;
//        for (var i = 0;i < n; i++){
//            if (document.getElementById("eservice_type").options[i].selected === true) {
//                checked = 1;
//            }
//        }
//
//
//        if(checked==0){
//            document.getElementById('eservice_msg').innerHTML = 'No service is selected';
//            document.getElementById("eservice_lb").style.color ='#dd4b39';
//            document.getElementById('eservice_msg').style.display = 'block';
//            flag=1;
//        }
//        else{
//            document.getElementById("eservice_lb").style.color ='#333';
//            document.getElementById('eservice_msg').style.display = 'none';
//        }
//
//        if(flag==1){
//            return false;
//        }


        //document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        //document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        var booking_id  = document.getElementById('book_id').value;
        var no_of_persons  = document.getElementById('eno_of_persons').value;
        var bdate  = document.getElementById('datepicker1').value;
        var emp_id = document.getElementById('eemp_id').value;

        var mode = $("#eservice_type option:selected").map(function(){return this.value}).get().join(',');
        var service_type = mode;

        $.ajax({
            url: '{{ url('admin/edit_booking') }}',
            type:'post',
            data:{'booking_id':booking_id,'service_type':service_type,'no_of_persons':no_of_persons,'date':bdate,'emp_id':emp_id},
            success:function(data)
            {
                //document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                var myJSON = JSON.stringify(data);

                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){
                    $('#edit_booking_modal').modal('hide');
                    reload_view();
                }
                else{
                    document.getElementById('mod_body').innerHTML=JSONObject['Message'];
                    $('#myModal1').modal('show');
                }
            }
        });
    }
</script>

<script>

    function reedem_point(bid,point) {
        var available_point = document.getElementById('available_point_'+bid).value;
        if(parseInt(available_point)< parseInt(point)){
            document.getElementById('point_'+bid).value = "";
            document.getElementById('point_alert_'+bid).style.display='block';

        }
        else {
            document.getElementById('point_alert_'+bid).style.display='none';
        }
    }

    function access_redeem(bid)
    {
        if($('#redeem_point_'+bid).prop("checked") == true){
            document.getElementById('point_'+bid).removeAttribute('readonly');
            document.getElementById('point_'+bid).focus();
        }
        else if($('#redeem_point_'+bid).prop("checked") == false){
            document.getElementById('point_'+bid).setAttribute('readonly','readonly');
            document.getElementById('point_'+bid).value="00";
        }
    }

    function access_discount(bid)
    {
        if($('#add_discount_'+bid).prop("checked") == true){
            document.getElementById('by_discount_'+bid).style.display='block';
            document.getElementById('discount_amount_'+bid).focus();
        }
        else if($('#add_discount_'+bid).prop("checked") == false){
            document.getElementById('by_discount_'+bid).style.display='none';
        }
    }

    function access_payment(bid)
    {
        if($('#add_payment_'+bid).prop("checked") == true){
            document.getElementById('by_amount_div_'+bid).style.display='block';
            document.getElementById('additional_amount_'+bid).focus();
        }
        else if($('#add_payment_').prop("checked") == false){
            document.getElementById('by_amount_div_'+bid).style.display='none';
        }
    }

    function count_percentage(bid,val){
        var sub_total = document.getElementById('sub_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var by_price = document.getElementById('discount_amount_'+bid).value;
        var tax = document.getElementById('tax_'+bid).value;
        if(additional_amount==""){additional_amount = 0;}

        if(parseInt(sub_total)<parseInt(by_price)){
            document.getElementById('discount_alert_'+bid).style.display='block';
            document.getElementById('discount_alert_'+bid).innerHTML='Discount amount must be less than final total';
            document.getElementById('discount_amount_'+bid).value = 0;
            document.getElementById('discount_percentage_'+bid).value = 0;
            document.getElementById('final_total_display_'+bid).innerHTML = sub_total.toFixed(2);
            document.getElementById('final_total_'+bid).value= sub_total.toFixed(2);
            return false;
        }
        else{
            document.getElementById('discount_alert_'+bid).style.display='none';
        }

        if(by_price==""){ by_price = 0;}
        var dis_per = by_price/sub_total*100;
        document.getElementById('discount_percentage_'+bid).value = dis_per.toFixed(2);
        var dis_price = (parseFloat(sub_total)-parseFloat(by_price))+parseFloat(additional_amount);

        if (tax!="" && tax!='0.00'){
            var tax_price = (dis_price * tax)/100;
            dis_price = dis_price+tax_price;
        }

        document.getElementById('final_total_display_'+bid).innerHTML = dis_price;
        document.getElementById('final_total_'+bid).value= dis_price;
    }

    function count_price(bid,val){

        var sub_total = document.getElementById('sub_total_'+bid).value;
        var additional_amount = document.getElementById('additional_amount_'+bid).value;
        var by_per = document.getElementById('discount_percentage_'+bid).value;
        if(additional_amount==""){additional_amount = 0;}

        if(parseInt(by_per)>100){
            document.getElementById('discount_alert_'+bid).style.display='block';
            document.getElementById('discount_alert_'+bid).innerHTML='You can not enter discount percentage more than 100%';
            document.getElementById('discount_amount_'+bid).value = 0;
            document.getElementById('discount_percentage_'+bid).value = 0;
            document.getElementById('final_total_display_'+bid).innerHTML = sub_total.toFixed(2);
            document.getElementById('final_total_'+bid).value= sub_total.toFixed(2);
            return false;
        }
        else{
            document.getElementById('discount_alert_'+bid).style.display='none';
        }

        if(by_per==""){by_per = 0;}
        var dis_price = sub_total*by_per/100;
        document.getElementById('discount_amount_'+bid).value = dis_price.toFixed(2);
        var by_dis_price = document.getElementById('discount_amount_'+bid).value;
        var aft_dis_price = (parseFloat(sub_total)-parseFloat(by_dis_price))+parseFloat(additional_amount);
        document.getElementById('final_total_display_'+bid).innerHTML = aft_dis_price.toFixed(2);
        document.getElementById('final_total_'+bid).value= aft_dis_price.toFixed(2);
    }

    function call_user(id){

        if(id==0){
            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";
        }
        else {
            $.ajax({
                type: 'POST',
                data: {'id': id},
                url: '{{ url('admin/ajax_get_customer')}}',
                success: function (data) {
                    var myJSON = JSON.stringify(data)
                    var JSONObject = JSON.parse(myJSON);

                    document.getElementById('name').value = JSONObject["name"];
                    document.getElementById('phone').value = JSONObject["phone"];
                    document.getElementById('email').value = JSONObject["email"];

                    document.getElementById('name').disabled="disabled";
                    document.getElementById('email').disabled="disabled";
                    document.getElementById('phone').disabled="disabled";

                    document.getElementById('phone_display').style.display="none";

                    if(JSONObject["phone"]=="" || JSONObject["phone"]==null){
                        document.getElementById('phone_display').style.display="block";
                        document.getElementById('phone').disabled=false;
                    }

                    if(JSONObject["email"]=="" || JSONObject["email"]==null){
                        document.getElementById('email').disabled=false;
                    }
                }
            });
        }
    }

    function get_type(key)
    {
        if(key == 1)
        {
            document.getElementById('shortdis').style.display='block';
            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";
            $('#user_ids').prop('selectedIndex',0);
            document.getElementById('select2-user_ids-container').innerHTML="Please Select";
            document.getElementById('select2-user_ids-container').title="Please Select";
            document.getElementById('phone_display').style.display="none";
        }
        else
        {
            document.getElementById('shortdis').style.display='none';
            document.getElementById('phone_display').style.display="block";
            document.getElementById('shortdis').value='';

            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";


            document.getElementById('name').disabled=false;
            document.getElementById('phone').disabled=false;
            document.getElementById('email').disabled=false;
        }
    }

</script>

