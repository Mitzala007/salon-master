<div id="load" style="position: relative; text-align: center;"></div>
<div class="row">
    @foreach($salon as $saloon)
        <a href="{{url('admin/booking/'.$saloon['id'].'/list')}}">
            <div class="col-lg-2 col-xs-6">
                <div class="small-box main-menu" style="background-image: url({{ url($saloon['image'])}}); background-size: contain; background-repeat: no-repeat; background-position: center; opacity: 1">
                    <div class="inner" style="padding: 20px; background: rgba(0, 0, 0, 0.6); color: #fff; font: 18px Arial, sans-serif;">
                        <p class="dashboard-text-name" style="color: #fff; height: 28px;" data-toggle="tooltip" title="{{$saloon->title}}" data-trigger="hover">
                            @if(strlen($saloon->title) > 20)
                                {{substr($saloon->title, 0,20)}}...
                            @else
                                {{$saloon->title}}
                            @endif
                        </p>
                        <h3 class="dashboard-text-count" style="color: #fff">{{$saloon['bookings']->count()}}</h3>
                        <p class="dashboard-text-name"  style="color: #fff">Total Bookings</p>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
</div>

<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $salon])</div>