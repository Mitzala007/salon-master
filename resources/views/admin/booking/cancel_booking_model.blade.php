<div id="cancel_booking_modal" class="fade modal modal-danger" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel Booking</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to cancel this Booking ?</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="cancel_booking_id" id="cancel_booking_id" value="">
                <input type="hidden" name="cancel_user_id" id="cancel_user_id" value="">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline" onclick="cancel_booking()">Delete</button>
            </div>
        </div>
    </div>

</div>
