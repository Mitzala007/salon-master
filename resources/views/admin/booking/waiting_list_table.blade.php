<style>
    @media (min-width: 768px){
        .modal-dialog {
            width: 725px;
            margin: 30px auto;
        }
    }
</style>

<div id="load" style="position: relative; text-align: center;; display: none" >
    <img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />
</div>

@include('admin.booking.add_walkin_model')
@include('admin.booking.edit_walkin_model')
@include('admin.booking.cancel_booking_model')
<input type="hidden" value="{{$updated_at}}" id="updated_at">
<div class="col-md-12">
    <div class="col-md-6">
        <h3 class="text-center" style="margin-bottom: 20px;">Customer Waiting List : {{count($booking['waiting'])}}</h3>
        <div class="col-md-12" style="overflow-y: auto; height: 730px;">

            {{--------------------   WAITING LIST ----------------------------------}}
            @if(count($booking['waiting']) > 0)
                @foreach($booking['waiting'] as $waiting)
                    <div class="box box-info" id="wid_{{$waiting['id']}}" style="display: block">
                        <div class="box-header with-border">
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-md-7"><label class="col-md-12" style="font-size: 16px;">
                                        @if($waiting['checkin_type']==0)
                                            <img src="{{ url('assets/website/images/walker.png') }}" height="20">
                                        @elseif($waiting['checkin_type']==1)
                                                <img src="{{ url('assets/website/images/mobile_checkin.png') }}" height="20">
                                        @elseif($waiting['checkin_type']==2)
                                            <img src="{{ url('assets/website/images/desktop.png') }}" height="20">
                                        @endif
                                        {{$waiting['users']['name']}}</label></div>
                                <div class="col-md-5"><label class="col-md-12" style="font-size: 16px; text-align: right">Est. Waiting Time : {{ $waiting['wait_time']}}</label></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Services: </b></div>
                                    <div class="col-md-9">{{ $waiting['services'] }}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Technician Name: </b></div>
                                    <div class="col-md-9">{{ $waiting['booking_details'][0]['service_expert_nickname'] }}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Checkin Time: </b></div>
                                    <div class="col-md-9">{{ $waiting['checkin_date']}}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-info pull-left" data-toggle="modal" data-target="#edit_booking_modal" onclick="get_booking('{{$waiting['id']}}','add')"  style="margin-top: 10px; margin-bottom: 0px; margin-left: 15px"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-danger pull-left" data-toggle="modal" data-target="#cancel_booking_modal" onclick="document.getElementById('cancel_booking_id').value='{{$waiting['id']}}';document.getElementById('cancel_user_id').value='{{$waiting['user_id']}}'"
                                            style="margin-top: 10px; margin-bottom: 0px; margin-left: 5px">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <button class="btn btn-info pull-right" type="submit" onclick="serve_now({{$waiting['id']}})">Serve Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12"><label class="col-md-12" style="font-size: 16px; text-align: center">Waiting list is empty</label></div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <h3 class="text-center" style="margin-bottom: 20px;">Customer Serving List : {{count($booking['serving'])}}</h3>
        <div class="col-md-12" style="overflow-y: auto; height: 730px;">

            {{--------------------   SERVING LIST ----------------------------------}}
            @if(count($booking['serving']) > 0)
                @foreach($booking['serving'] as $serving)
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-md-7"><label class="col-md-12" style="font-size: 16px;">
                                        @if($serving['checkin_type']==0)
                                            <img src="{{ url('assets/website/images/walker.png') }}" height="20">
                                        @elseif($serving['checkin_type']==1)
                                            <img src="{{ url('assets/website/images/mobile_checkin.png') }}" height="20">
                                        @elseif($serving['checkin_type']==2)
                                            <img src="{{ url('assets/website/images/desktop.png') }}" height="20">
                                        @endif
                                        {{$serving['users']['name']}}</label></div>
                                <div class="col-md-5"><label class="col-md-12" style="font-size: 16px; text-align: right">Est. Remaining Time : {{ $serving['remaining']}}</label></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Services: </b></div>
                                    <div class="col-md-9">{{ $serving['services'] }}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Technician Name: </b></div>
                                    <div class="col-md-9">{{ $serving['booking_details'][0]['service_expert_nickname'] }}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3"><b>Checkin Time: </b></div>
                                    <div class="col-md-9">{{ $serving['checkin_date']}}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-info pull-left" data-toggle="modal" data-target="#edit_booking_modal" onclick="get_booking('{{$serving['id']}}','edit')"  style="margin-top: 10px; margin-bottom: 0px; margin-left: 15px"><i class="fa fa-edit"></i></button>
                                    {{--<button class="btn btn-danger pull-left" data-toggle="modal" data-target="#cancel_booking_modal" onclick="document.getElementById('cancel_booking_id').value='{{$serving['id']}}';document.getElementById('cancel_user_id').value='{{$serving['user_id']}}'"--}}
                                    {{--style="margin-top: 10px; margin-bottom: 0px; margin-left: 5px">--}}
                                    {{--<i class="fa fa-trash"></i>--}}
                                    {{--</button>--}}
                                    <button class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal_{{ $serving['id'] }}" type="submit">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--**************************************** INDIVIDUAL MODAL FOR EACH SERVING ****************************************-->

                    @include('admin.booking.done_walkin_model')
                @endforeach
            @else
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12"><label class="col-md-12" style="font-size: 16px; text-align: center">Serving list is empty</label></div>
                        </div>
                    </div>
                </div>
            @endif

            <div id="myModal1" class="fade modal modal-danger" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                            <h3 class="modal-title text-center" id="mod_body">Alert</h3>
                        </div>
                        {{--<div class="modal-body" id="mod_body" style="color: #444!important; font-size: 14px!important;">--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var waiting_counter = '<?php echo count($booking['waiting']);?>';
        document.getElementById('wait_counter').innerHTML = waiting_counter;
    </script>

</div>
</div>


