<div id="myModal_{{ $serving['id'] }}" class="fade modal modal-primary" role="dialog" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                <h3 class="modal-title text-center">Booking Report</h3>
            </div>
            <div class="modal-body" style="color: #444!important; font-size: 14px!important;">
                <div class="row">
                    <div class="col-md-12" ><h3 style="margin-top: 0px">Customer Detail</h3></div>
                    <div class="col-md-6"><label>{{ $serving['users']['name'] }}</label></div>
                    <div class="col-md-6" style="text-align: right"><label>{{ $serving['users']['phone'] }}</label></div>
                    <div class="col-md-12">
                        <div class="col-md-3 bg-info text-white" ><h4><b>Title</b></h4></div>
                        <div class="col-md-3 bg-info" ><h4><b>Price</b></h4></div>
                        <div class="col-md-3 bg-info" ><h4><b>Discount</b></h4></div>
                        <div class="col-md-3 bg-info" ><h4><b>Final Price</b></h4></div>

                        @foreach($serving['booking_details'] as $services)
                            <div class="col-md-3"><h5>{{$services['service_type']['title']}}</h5></div>
                            <div class="col-md-3"><h5>{{$services['charges']}}</h5></div>
                            <div class="col-md-3"><h5>{{$services['discount_value']}}</h5></div>
                            <div class="col-md-3"><h5>{{$services['total_charge']}}</h5></div>
                        @endforeach
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px;">
                            <h4 class="col-md-10"><b>Total Amount</b></h4>
                            <h4 class="col-md-2">{{ $serving['sub_total'] }}</h4>
                        </div>
                    </div>
                    <input type="hidden" id="sub_total_{{$serving['id']}}" value="{{ $serving['sub_total'] }}">
                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px; padding-bottom: 10px;">
                            <h4 class="col-md-10"><b>Available Reward Points</b></h4>
                            <h4 class="col-md-2">{{ $serving['available_points'] }}</h4>
                            <input type="hidden" id="available_point_{{ $serving['id'] }}" value="{{ $serving['available_points'] }}">
                            <div class="col-md-12">
                                <div class="col-md-10 label-check" style="padding: 0;">
                                    <div class="checkbox" style="margin-top: 0px">
                                        <input type="checkbox" id="redeem_point_{{$serving['id']}}" onclick="access_redeem({{$serving['id']}})" >
                                        <label for="redeem_point_{{$serving['id']}}" style="padding-left: 5px; padding-top: 5px; font-weight: bold; font-size: 15px;">Redeem reward points</label>
                                    </div>
                                </div>
                                <div class="col-md-2 text-center" style="margin-top: 0px;">
                                    <input type="text" name="point" onkeyup="reedem_point('{{$serving['id']}}',this.value)" id="point_{{$serving['id']}}" class="form-control" value="" placeholder="00" readonly style="text-align: center; height:25px !important;">
                                </div>
                                <div class="col-md-12" id="point_alert_{{$serving['id']}}" style="text-align: right; color: red; display: none">You can't enter more than available point.</div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px; padding-bottom: 10px;">
                            <div class="col-md-12 label-check">
                                <div class="checkbox">
                                    <input type="checkbox" id="add_discount_{{$serving['id']}}" onclick="access_discount({{$serving['id']}})">
                                    <label for="add_discount_{{$serving['id']}}" style="padding-left: 5px; padding-top: 5px; font-weight: bold; font-size: 16px;">Additional Discount</label>
                                </div>
                            </div>

                            <div class="col-md-12" id="by_discount_{{$serving['id']}}" style="display: none;">
                                <div class="col-md-10" style="padding: 0;">
                                    <label class="label-text1">
                                        {!! Form::radio('discount_'.$serving['id'], null, ['class' => '', 'id'=>'by_percentage_'.$serving['id']]) !!}
                                        <span class="label-text" style="padding-left: 30px;  font-size: 15px;">By Percentage (%)</span>
                                    </label>
                                </div>
                                <div class="col-md-2 text-center" style="margin-top: 0px;">
                                    <input type="text" name="discount_percentage" id="discount_percentage_{{$serving['id']}}" onkeyup="count_price({{$serving['id']}})" class="form-control" value="" placeholder="00" style=" height:25px !important;text-align: center; ">
                                </div>

                                <div class="col-md-10" style="padding: 0px">
                                    <label class="label-text1">
                                        {!! Form::radio('discount_'.$serving['id'], null, ['class' => '', 'id'=>'by_percentage_'.$serving['id']]) !!}
                                        <span class="label-text" style="padding-left: 30px;  font-size: 15px;">By Amount ($)</span>
                                    </label>
                                </div>
                                <div class="col-md-2 text-center" style="margin-top: 0px;">
                                    <input type="text" name="discount_amount" id="discount_amount_{{$serving['id']}}" onkeyup="count_percentage({{$serving['id']}})" class="form-control" value="" placeholder="00" style="height:25px !important; text-align: center; ">
                                </div>

                                <div class="col-md-12" id="discount_alert_{{$serving['id']}}" style=" text-align: right; color: red; display: none">You can't enter more than 100%.</div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px; padding-bottom: 10px;">
                            <div class="col-md-12 label-check">
                                <div class="checkbox">
                                    <input type="checkbox" id="add_payment_{{$serving['id']}}" onclick="access_payment({{$serving['id']}})">
                                    <label for="add_payment_{{$serving['id']}}" style="padding-left: 5px; padding-top: 5px; font-weight: bold; font-size: 15px;">Additional Payment</label>
                                </div>
                            </div>
                            <div class="col-md-12" id="by_amount_div_{{$serving['id']}}" style="display: none;">
                                <div class="col-md-10" style="padding: 0;">
                                    <label style="padding-left: 30px; padding-top: 0px; font-weight: bold; font-size: 15px;">By Amount ($)</label>
                                </div>
                                <div class="col-md-2 text-center" style="margin-top: 0px;">
                                    <input type="text" name="additional_amount_{{$serving['id']}}" onkeyup="count_percentage({{$serving['id']}})" id="additional_amount_{{$serving['id']}}" class="form-control" value="" placeholder="00" style="text-align: center; height:25px !important;">
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $final_total = $serving['sub_total'];
                    $salon_tax = 0.00;
                    $get_tax = \App\saloon::select('tax')->where('id',$serving['saloon_id'])->where('status','active')->first();
                    if ($get_tax)
                    {
                        $salon_tax = number_format($get_tax['tax'],2);
                        $tax_price = ($serving['sub_total'] * $salon_tax) / 100;
                        $final_total = $serving['sub_total'] + $tax_price;
                    }
                    ?>

                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px;">
                            <h4 class="col-md-10"><b>Tax(0.00%)</b></h4>
                            <div class="col-md-2 text-center" style="margin-top: 0px; padding: 0px 25px 0 2px">
                                <input type="text" name="tax_{{$serving['id']}}" onkeyup="count_percentage({{$serving['id']}})" id="tax_{{$serving['id']}}" class="form-control" value="{{ $salon_tax }}" placeholder="0.00" style="text-align: center;  margin-top: 7px; height:25px !important;">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px;">
                            <h4 class="col-md-10"><b>Payable Amount</b></h4>
                            <h4 class="col-md-2" id="final_total_display_{{$serving['id']}}">{{ $final_total }}</h4>
                            <input type="hidden" id="final_total_{{$serving['id']}}" value="{{ $final_total }}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px; padding-bottom: 10px">
                            <h4 class="col-md-12"><b>Payable Mode</b></h4>
                            <label class="col-md-4">
                                <input type="radio" id="payment_mode1_{{$serving['id']}}" name="payment_mode_{{$serving['id']}}" value="1" class="flat-red"  checked>
                                Cash
                            </label>
                            <label class="col-md-4">
                                <input type="radio" id="payment_mode1_{{$serving['id']}}" name="payment_mode_{{$serving['id']}}" value="2" class="flat-red">
                                Card/Netbanking
                            </label>
                            <label class="col-md-4">
                                <input type="radio" id="payment_mode1_{{$serving['id']}}" name="payment_mode_{{$serving['id']}}" value="3" class="flat-red">
                                Gift Card
                            </label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12" style="background-color: #f5f2f7; border-radius: 5px; margin-top: 10px;">
                            <h4 class="col-md-12"><input type="text" name="remarks" id="remarks_{{$serving['id']}}" class="form-control" placeholder="Remarks" style=" background-color:#F5F2F7;text-align: left; padding: 0px; border-radius: 0px; border: none;border-bottom: solid 1px "></h4>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <button class="col-md-12 btn btn-info" onclick="end_now('{{$serving['id']}}','{{ $serving['saloon_id']}}')" type="submit">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div >