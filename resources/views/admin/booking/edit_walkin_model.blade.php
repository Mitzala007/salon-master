<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script>
    $(function () {
        $(".select2").select2();
    });

    $('#datepicker1').datepicker({
        format: 'm/d/yyyy',
        startDate: '+0d',
        autoclose: true
    });
</script>
<div id="edit_booking_modal" class="fade modal modal-primary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                <h3 class="modal-title text-center">Edit Booking</h3>
            </div>
            <div class="modal-body form-horizontal" style="color: #444!important; font-size: 14px!important;">
                {{--{!! Form::open(['class' => 'form-horizontal','files'=>true]) !!}--}}
                <div class="box-body">
                    {!! Form::hidden('redirects_to', URL::previous()) !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label"><b>Name</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::text('ename', null, ['class' => 'form-control', 'placeholder' => 'Enter Name','id'=>'ename']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="phone"><b>Phone</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::text('ephone', null, ['class' => 'form-control', 'placeholder' => ' Enter Phone','id'=>'ephone']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label"><b>Email</b><span class="text-red"></span></label>
                        <div class="col-md-9">
                            {!! Form::text('eemail', null, ['class' => 'form-control', 'placeholder' => ' Enter Email','id'=>'eemail']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('ebdate') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="date"><b>Booking Date</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::text('ebdate', date('m/d/Y'), ['class' => 'form-control bdate', 'placeholder' => ' Enter Date' ,'id'=>'datepicker1']) !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('service_type') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="service_type" id="eservice_lb"><b>Service Type</b><span class="text-red">*</span></label>
                        <div class="col-md-9" id="service_selection">
                                <select name="eservice_type[]" id="eservice_type" class="form-control select2" style="width: 100%" multiple>
                                    {{--@foreach ($service_type as $key => $value)--}}
                                        {{--<option value="{{$key}}">{{$value}}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                                @if ($errors->has('eservice_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('eservice_type') }}</strong>
                                    </span>
                                @endif
                            <strong><span id="eservice_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('no_of_persons') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="no_of_persons" id="eperson_lb"><b>No of persons</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            {!! Form::number('eno_of_persons', 1, ['class' => 'form-control', 'placeholder' => ' Enter No of persons','id'=>'eno_of_persons']) !!}
                            @if ($errors->has('eno_of_persons'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eno_of_persons') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('eemp_id') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="eemp_id" id="esaloon_id">Technician Name <span class="text-red"></span></label>
                        <div class="col-md-9" id="employee_selection" >
                            <select name="eemp_id" id="eemp_id" class="form-control select2" style="width: 100%">
                            </select>
                            {{--{!! Form::select('eemp_id', ['Please select']+$emp_id, null, ['id'=>'eemp_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
                            @if ($errors->has('eemp_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eemp_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="book_id" id="book_id" value="">
                </div>
                <div class="box-footer" style="text-align: center">
                    <button class="btn col-md-12 btn-info" onclick="edit_booking()">EDIT</button>
                </div>

                {{--                {!! Form::close() !!}--}}
            </div>
        </div>
    </div>
</div>
