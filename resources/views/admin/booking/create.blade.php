@extends('admin.layouts.app')

<style>
    input[type="radio"] {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        opacity: 0;
        position: absolute;
        margin: 0;
        z-index: -1;
        width: 0;
        height: 0;
        overflow: hidden;
        left: 0;
        pointer-events: none;
    }
    .label-text1{
        padding-left: 0!important;
    }
    .label-text1 span{
        margin-top: 20px;
    }
    input[type="radio"] + .label-text:before{
        content: '\f10c';
        font-family: "FontAwesome";
        speak: none;
        font-size: 23px;
        font-style: normal;
        font-weight: bold;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        margin-right: 5px;
        color: #D7DCDE;
    }
    input[type="radio"]:checked + .label-text:before{
        content: "\f192";
        color: #1ABC9C;
    }
    input[type="radio"]:disabled + .label-text{
        color: #D7DCDE;
    }
    input[type="radio"]:disabled + .label-text:before{
        content: "\f111";
        color: #D7DCDE;
    }
</style>

@section('content')

    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Add</small>
                </h1>

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/booking/create')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Add</li>
            </ol>
        </section>

        <section class="content">
            @include ('admin.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">ADD {{$menu}}</h3>
                        </div>
                        {!! Form::open(['url' => url('admin/booking/store'), 'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return validation();']) !!}
                        <input type="hidden" name="mod" id="mod" value="Add">
                        <div class="nav-tabs-custom">
                            <div class="box-body">
                                {!! Form::hidden('redirects_to', URL::previous()) !!}

                                <div class="form-group{{ $errors->has('customer type') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="role">Customer Type<span class="text-red">&nbsp; &nbsp; </span></label>
                                    <div class="col-md-5 col-sm-6 ">
                                        @foreach (\App\Booking::$customer_type as $key => $value)
                                            <label class="label-text1">
                                                @if($key == '0')
                                                    {!! Form::radio('customer_type', $key, null , ['class' => '','checked', 'onclick'=>'get_type('.$key.')']) !!}<span class="label-text" style="margin-right: 26px; font-weight: bold;">{{ $value }}</span>
                                                @else
                                                    {!! Form::radio('customer_type', $key, null , ['class' => '', 'onclick'=>'get_type('.$key.')']) !!}<span class="label-text" style="margin-right: 26px; font-weight: bold;">{{ $value }}</span>
                                                @endif
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('search_user') ? ' has-error' : '' }}" id="shortdis" style=" display: none;">
                                    <label class="col-sm-2 control-label" for="search_user" id="search_user">Search User</label>
                                    <div class="col-md-5 col-sm-6">
                                        {!! Form::select('search_user', ['Please select']+$customer, null, ['class' => 'select2 form-control', 'id'=>'search_user','style'=>'width:100%','onchange'=>'call_user(this.value)']) !!}
                                        @if ($errors->has('search_user'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('search_user') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="name"><b>Name</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name','id'=>'name']) !!}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="phone"><b>Phone</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => ' Enter Phone','id'=>'phone']) !!}
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="email"><b>Email</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => ' Enter Email','id'=>'email']) !!}
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="date"><b>Booking Date</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        {!! Form::text('date', date('Y-m-d'), ['class' => 'form-control', 'placeholder' => ' Enter Date' ,'id'=>'datepicker']) !!}
                                        @if ($errors->has('date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('service_type') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="service_type"><b>Service Type</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        <div class="row">
                                            @foreach ($service_type as $key => $value)
                                                <label class="col-sm-6">
                                                    {!! Form::checkbox('service_type[]', $key, null,['class' => 'flat-red'])!!}<span style="margin-right: 10px;padding-left: 5px;">{{ $value }}</span>
                                                </label>
                                            @endforeach
                                            @if ($errors->has('service_type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('service_type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('no_of_persons') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="no_of_persons"><b>No of persons</b><span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        {!! Form::number('no_of_persons', 1, ['class' => 'form-control', 'placeholder' => ' Enter No of persons']) !!}
                                        @if ($errors->has('no_of_persons'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('no_of_persons') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="emp_id" id="saloon_id">Employee <span class="text-red"></span></label>
                                    <div class="col-sm-5">
                                        {!! Form::select('emp_id', ['Please select']+$emp_id, null, ['id'=>'emp_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                                        @if ($errors->has('emp_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('emp_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <a href="{{ url('') }}" ><button class="btn btn-default" type="button">Back</button></a>
                                <button class="btn btn-info pull-right" type="submit">Add</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

<script>
    function get_type(key)
    {
        if(key == 1)
        {
            document.getElementById('shortdis').style.display='block';
        }
        else
        {
            document.getElementById('shortdis').style.display='none';
            document.getElementById('shortdis').value='';
        }
    }

    function call_user(id){

        if(id==0){
            document.getElementById('name').value="";
            document.getElementById('phone').value="";
            document.getElementById('email').value="";
        }
        else {
            $.ajax({
                type: 'POST',
                data: {'id': id},
                url: '{{ url('admin/ajax_get_customer')}}',
                success: function (data) {
                    var myJSON = JSON.stringify(data)
                    var JSONObject = JSON.parse(myJSON);

                    document.getElementById('name').value = JSONObject["name"];
                    document.getElementById('phone').value = JSONObject["phone"];
                    document.getElementById('email').value = JSONObject["email"];

                }
            });
        }
    }

</script>

