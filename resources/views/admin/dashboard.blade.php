@extends('admin.layouts.app')
@section('content')

<?php $plan = 0; ?>
@if(\Illuminate\Support\Facades\Auth::user()->membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->membership_plan; ?>
@elseif(\Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->admin_membership_plan?>
@endif

<div class="content-wrapper">
    <section class="content-header">

        @if($plan==1 || $plan==2 || $plan==3)
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <select class="select2 form-control" style="width: 100%" onchange="change_salon(this.value)">
                    @foreach($salon_list as $values)
                        <option value="{{$values['id']}}" @if($salon_id['id'] && $salon_id['id']==$values['id']) selected @endif>{{$values['title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <script>
            function change_salon(val) {
                window.location.href="dashboard_salon_session?salon_id="+val;
            }
        </script>
        @else
            <h1>
            Dashboard
            <small>Control panel</small>


            </h1>
            <ol class="breadcrumb">
            <li class="active"><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
        @endif


    </section>

    <section class="content" style="min-height: 0px">
        <div class="row">

            @if(($plan=='1' || $plan=='2' || $plan=='3') && empty($salon_id))
                <div class="col-lg-12">
                    <div class="alert alert-danger">
                        Salon not assign...
                    </div>
                </div>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_user}}</h3>
                        <p class="dashboard-text-name">Total Users</p>
                    </div>

                    <a href="{{ url('admin/users') }}" class="small-box-footer dashboard-link">View Users <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $normal_user}}</h3>
                        <p class="dashboard-text-name">Total Normal Users</p>
                    </div>
                    <a href="{{ url('admin/users') }}" class="small-box-footer dashboard-link">View Users <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $salon_user}}</h3>
                        <p class="dashboard-text-name">Total Salon Users</p>
                    </div>

                    <a href="{{ url('admin/users') }}" class="small-box-footer dashboard-link">View Users <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_salon }}</h3>
                        <p class="dashboard-text-name">Total Salons</p>
                    </div>

                    <a href="{{ url('admin/saloon') }}" class="small-box-footer dashboard-link">View salon <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner dashborard-box">
                                <h3 class="dashboard-text-count">{{ $total_service }}</h3>
                                <p class="dashboard-text-name">Total Services</p>
                            </div>

                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                                <a href="{{ url('admin/service') }}" class="small-box-footer dashboard-link">View Services <i class="fa fa-arrow-circle-right"></i></a>
                            @else
                                <a href="{{ url('admin/saloon_service/'.$id) }}" class="small-box-footer dashboard-link">View Services <i class="fa fa-arrow-circle-right"></i></a>
                            @endif

                        </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_booking }}</h3>
                        <p class="dashboard-text-name">Total Bookings</p>
                    </div>

                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                        <a href="{{ url('admin/booking') }}" class="small-box-footer dashboard-link">View Booking <i class="fa fa-arrow-circle-right"></i></a>
                    @else
                        <a href="{{ url('admin/booking/'.$salon_id.'/list') }}" class="small-box-footer dashboard-link">View Booking <i class="fa fa-arrow-circle-right"></i></a>
                    @endif
                </div>
            </div>

            @else

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $waiting_list }}</h3>
                        <p class="dashboard-text-name">Waiting List</p>
                    </div>
                    <a href="{{ url('admin/waiting_list') }}" class="small-box-footer dashboard-link">View Waiting List <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $recent_booking }}</h3>
                        <p class="dashboard-text-name">Today's Booking</p>
                    </div>
                    <a href="{{ url('admin/booking/'.$salon_id['id'].'/list') }}" class="small-box-footer dashboard-link">View Booking <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
                    {{--<div class="col-lg-2 col-xs-6">--}}
                {{--<div class="small-box bg-aqua">--}}
                    {{--<div class="inner dashborard-box">--}}
                        {{--<h3 class="dashboard-text-count">{{ $total_service }}</h3>--}}
                        {{--<p class="dashboard-text-name">Total Services</p>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('admin/saloon/services_edit/'.$salon_id['id']) }}" class="small-box-footer dashboard-link">View Services <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_booking }}</h3>
                        <p class="dashboard-text-name">Total Bookings</p>
                    </div>
                        <a href="{{ url('/admin/booking/'.$salon_id['id'].'/list') }}" class="small-box-footer dashboard-link">View Booking <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_emp }}</h3>
                        <p class="dashboard-text-name">Total Employees</p>
                    </div>

                    <a href="{{ url('admin/employee/'.$salon_id['id'].'/list') }}" class="small-box-footer dashboard-link">View Employees <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner dashborard-box">
                        <h3 class="dashboard-text-count">{{ $total_customer }}</h3>
                        <p class="dashboard-text-name">Total Customer</p>
                    </div>
                    <a href="{{ url('admin/customer/'.$salon_id['id'].'/list') }}" class="small-box-footer dashboard-link">View Customer <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

                <div class="col-lg-2 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner dashborard-box">
                            <h3 class="dashboard-text-count">{{ $total_rating }}</h3>
                            <p class="dashboard-text-name">Total Ratings</p>
                        </div>

                        <a href="{{ url('admin/rating') }}" class="small-box-footer dashboard-link">View Ratings <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            @endif
        </div>
    </section>

    <section class="content">
        <div class="row">

            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                <!---------------------------------------------------------SALON LOCATION--------------------------------------------------------->
                {{--<div class="col-md-12">--}}
                    {{--<!-- AREA CHART -->--}}
                    {{--<div class="box box-primary">--}}
                        {{--<div class="box-header with-border">--}}
                            {{--<h3 class="box-title">Salon Location</h3>--}}
                            {{--<div class="box-tools pull-right">--}}
                                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                                {{--</button>--}}
                                {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="box-body">--}}
                            {{--<div id="map" style="min-height:600px;" ></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Users</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                @foreach($user as $users)
                                    <li style="text-align: -webkit-center;">
                                         <a href="{{url('admin/users/'.$users->id)}}" target="_blank">
                                        @if($users['image']!="" && file_exists($users['image']))
                                            <div class="back_imgg" style="background-image: url({{url($users->image)}});
                                                    background-repeat: no-repeat; background-size: contain; background-position: center;  width: auto;
                                                    border-radius: 10% ;background-color: #D0D4DD; border-radius: 50% ;" ></div>
                                            {{--<img src="{{ url($users->image) }}" alt="User Image" style="height: 130px;width: 130px;border-radius: 100%;background-color: lightgrey">--}}
                                        @else
                                            <div class="back_imgg" style="background-image: url({{url('assets/dist/img/default-user.png')}});
                                                    background-repeat: no-repeat; background-size: contain; background-position: center;  border-radius: 50% ; width: auto; background-color: #D0D4DD"></div>
                                            {{--<img src="{{ url('assets/dist/img/default-user.png') }}" alt="User Image" style="height: 130px;">--}}
                                        @endif
                                        <span class="users-list-name" href="#">{{$users->name}}</span>
                                        <span class="users-list-date">{{$users->created_at->format('d M')}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="box-footer text-center">
                            <a href="{{url('admin/users')}}" class="uppercase">View All Users</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            {{--<li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>--}}
                            <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                        </ul>
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 418px;"></div>
                        </div>
                    </div>
                </div>

            @else
   <!------------------------------------------------------------------------------ LATEST BOOKING TAB VIEW --------------------------------------------------------->
            <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Today's Bookings</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="nav-tabs-custom">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="table-responsive" id="itemlist">
                                        @include('admin.latest_booking')
{{--                                        @include('admin.booking.table')--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="box-footer clearfix">--}}
                        {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
                        {{--<a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>--}}

                        {{--</div>--}}
                    </div>
                </div>
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pending Bookings <label class="label-danger pad" style="padding:5px 8px;">(Complete booking process for best future booking experience.)</label></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="nav-tabs-custom">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="table-responsive" id="pending_itemlist">
                                    @include('admin.pending_booking')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!---------------------------------------------------------MONTHLY BOOKING CHART--------------------------------------------------------->
            <div class="col-md-12">
                <!-- AREA CHART -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Monthly Booking Chart</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="bookingChart" style="height:250px"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

        <!---------------------------------------------------------MAP SCRIPT--------------------------------------------------------->
@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
<script>

    var locations = [
        @if(!empty($locations['saloons']))
            @foreach($locations['saloons'] as $v)
                ['{{$v['title']}}', '{{$v['latitude']}}', '{{$v['longitude']}}', '{{$v['id']}}','{{$v['map_pin']}}'],
            @endforeach
        @endif
    ];

    // When the user clicks the marker, an info window opens.

    function initMap() {
        var myLatLng = {lat: 41.850033, lng: -87.6500523};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: myLatLng
        });

        for (i = 0; i < locations.length; i++) {

            var pin_icon = {
//                url: 'http://localhost/nailmaster/assets/dist/img/icons/'+locations[i][4], // url
                url: '{{url('assets/dist/img/icons')}}'+'/'+locations[i][4],
                scaledSize: new google.maps.Size(80, 80), // scaled size
                origin: new google.maps.Point(0,0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//                url: 'http://localhost/nailmaster/admin/booking/'+locations[i][3]+ '/list',
                icon : pin_icon,
                map: map
            });

            marker.info = new google.maps.InfoWindow({
                content: locations [i][0]
            });

            google.maps.event.addListener(marker, 'click', function() {
//                window.location.href = marker.url;
                var marker_map = this.getMap();
                this.info.open(marker_map, this);
                // Note: If you call open() without passing a marker, the InfoWindow will use the position specified upon construction through the InfoWindowOptions object literal.
            });

//            google.maps.event.addListener(marker, 'mouseout', function() {
//                // this = marker
//                var marker_map = this.getMap();
//                this.info.close(marker_map, this);
//                // Note: If you call open() without passing a marker, the InfoWindow will use the position specified upon construction through the InfoWindowOptions object literal.
//            });
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap&libraries=places" async defer></script>

@endif

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script type="text/javascript">
    $(document).on('click','.pagination1 a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        $.ajax({

            url : page,
            data : {'type':'latest_booking'}

        }).done(function (data) {

            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true

            });
            $(".select2").select2();
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

</script>

<script type="text/javascript">
    $(document).on('click','.pagination2 a',function(e){
        e.preventDefault();
        $('#load2').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page1 = $(this).attr('href');
        window.history.pushState("", "", page1);
        get_pending_Pagination(page1);

    });

    function get_pending_Pagination(page1)
    {
        $.ajax({

            url : page1,
            data : {'type':'pending_booking'}

        }).done(function (data) {

            $("#pending_itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#pending_example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true

            });
            $(".select2").select2();
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

</script>


@section("jquery")
    <script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js')}}"></script>
    <script>
        $(function () {
        var bookingChartData = {
            labels: {!! json_encode($booking_chart_lable) !!},
            datasets: [
                {
                    label: "Bookings",
                    fillColor: "rgba(60,141,188,0.9)",
                    strokeColor: "rgba(60,141,188,0.9)",
                    pointColor: "#3b8bba",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: {!! json_encode($booking_chart_data) !!}
                }
            ]
        };

        var bookingChartCanvas = $("#bookingChart").get(0).getContext("2d");
        var bookingChart = new Chart(bookingChartCanvas);
        var bookingChartOptions = {
            showScale: true,
            scaleShowGridLines: false,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            bezierCurve: true,
            bezierCurveTension: 0.3,
            pointDot: false,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            {{--legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",--}}
              //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: true,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true
            };

            //Create the line chart
            bookingChart.Line(bookingChartData, bookingChartOptions);

            //-------------
            //- LINE CHART -
            //--------------
//            var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
//            var lineChart = new Chart(lineChartCanvas);
//            var lineChartOptions = areaChartOptions;
//            lineChartOptions.datasetFill = false;
//            lineChart.Line(areaChartData, lineChartOptions);
  });
</script>
<script src="{{ URL::asset('assets/dist/js/pages/dashboard.js')}}"></script>
@endsection