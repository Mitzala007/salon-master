
<style>
    .dropdown-menu>li>a:hover {
        background-color: #e1e3e9;
        color: #333;
    }
</style>
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th>Salon Name</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th class="table-text table-th">Business Type</th>
        <!--<th>Role</th>-->
        <th class="table-text table-th">Image</th>
        <th class="table-text table-th">Action</th>
        <th class="table-text table-th">Delete</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($user as $list)
        <?php
                $users = \App\User::where('id',$list['user_id'])->first();
                if ($users){
                    $name = $users['name']." ".$users['last_name'];
                    $email = $users['email'];
                    $phone = $users['phone'];
                    $phone = $users['phone'];

                    ?>
                    <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">

                        <td>{{$list['salon_name']}}</td>
                        <td>{{$name}} </td>
                        <td>{{$email}}</td>
                        <td>{{$phone}}</td>
                        <td style="padding-top: 16px;" class="table-text table-th">
                            @if($list['is_freelancer']==1) Freelancer @else Salon Owner @endif
                        </td>

                        <td class="table-text">
                            <a data-toggle="modal" data-target="#imageModal{{$users['id']}}" style="cursor: pointer;">
                                @if($list['image']!="" && file_exists($users['image']))
                                    <img src="{{ url($users->image) }}" width="30">
                                @else
                                    <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                                @endif
                            </a>
                        </td>

                        <td class="table-text">
                            <div class="btn-group-horizontal" id="assign_add_{{ $users['id'] }}" >
                                <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $users['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Send Email/SMS</span> </button>
                            </div>
                            <div class="btn-group-horizontal" id="assign_remove_{{ $users['id'] }}"  style="display: none"  >
                                <button class="btn btn-success assign ladda-button" data-style="slide-left" id="assign" uid="{{ $users['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Send Again</span></button>
                            </div>
                        </td>


                        <td class="table-text table-th">
                            <div class="btn-group-horizontal">
                                <span data-toggle="tooltip" title="Delete Request" data-trigger="hover">
                                    <button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Delete Request</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete this Request ?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-outline" onclick="destroy_request({{$list['id']}})">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
        ?>

    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $user])</div>


<script>
    function destroy_request(id)
    {
        $.ajax({
            url:'become_nailmaster/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'become_nailmaster';
                window.location.href = new_url;
            }
        });
    }
</script>