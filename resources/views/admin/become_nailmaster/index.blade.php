@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu. " request list"}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/become_nailmaster') }}"><i class="fa fa-dashboard"></i> Request List</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-10">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-2 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search" onkeyup="search_filters(event)">
                            </span>

                            <span style="float:left; padding-top: 5px" class="col-md-1 col-sm-1 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>

                            <!-----------------------------------------ROLE SEARCHING END---------------------------------------->
                        </div>
                        <input type="hidden" id="user_role" value="{{\Illuminate\Support\Facades\Auth::user()->role}}">
                    </div>

                    <div class="col-md-2">
                        <h3 class="box-title" style="float:right;">
                            <a href="{{ url('admin/become_nailmaster') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->

                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.become_nailmaster.table')
                </div>
            </div>

        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>


<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();

        var search = $('#search').val();
        var type = $('#type').val();
        $.ajax({

            url: '{{url('admin/become_nailmaster')}}',

            type: "get",

            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
                //alert(data);

                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.assign').click(function(){

            var user_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/become_nailmaster/send_email')}}',
                type: "get",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).show();
                    $('#assign_add_'+user_id).hide();
                }
            });
        });
    });
</script>



