@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/emails') }}"><i class="fa fa-dashboard"></i> Employee</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box box-info">
                <div class="box-header">
                    {{--<div class="col-md-5">--}}
                        {{--{!! Form::open(['url' => url('admin/emails'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}--}}
                        {{--<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">--}}
                            {{--<span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">--}}
                                {{--<input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">--}}
                            {{--</span>--}}
                            {{--<span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">--}}
                                {{--<input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">--}}
                            {{--</span>--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                    <div class="col-md-12">
                        <h3 class="box-title" style="float:right;">
                            {{--<a href="{{ url('admin/emails/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>--}}
                            <a href="{{ url('admin/emails') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.emails.table')
                </div>
            </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);
    })

    function getPagination(page)
    {
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        e.preventDefault();
        var search = $('#search').val();
        var type = $('#type').val();
        $('#load').append('<img style="position: absolute; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/emails')}}',
            type: "get",
            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                //alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>

