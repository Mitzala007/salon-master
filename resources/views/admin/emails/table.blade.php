<div id="load" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th class="table-text table-th">Edit</th>
        <th>Title</th>
        <th>Subject</th>
        <th>From Email</th>
        {{--<th class="table-text table-th">Status</th>--}}
        {{--<th class="table-text table-th">Delete</th>--}}
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($emails as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td class="table-text table-th">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/emails/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Email" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
            <td>{{$list['title']}}</td>
            <td>{{$list['subject']}}</td>
            <td>{{$list['email']}}</td>
            {{--<td class="table-text table-th">--}}
                {{--@if($list['status'] == 'active')--}}
                    {{--<div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >--}}
                        {{--<button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>--}}
                    {{--</div>--}}
                    {{--<div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >--}}
                        {{--<button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>--}}
                    {{--</div>--}}
                {{--@endif--}}
                {{--@if($list['status'] == 'in-active')--}}
                    {{--<div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >--}}
                        {{--<button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>--}}
                    {{--</div>--}}
                    {{--<div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >--}}
                        {{--<button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>--}}
                    {{--</div>--}}
                {{--@endif--}}
            {{--</td>--}}
            {{--<td class="table-text table-th">--}}
                {{--<div class="btn-group-horizontal">--}}
                    {{--<span data-toggle="tooltip" title="Delete Email" data-trigger="hover">--}}
                        {{--<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>--}}
                    {{--</span>--}}
                {{--</div>--}}
            {{--</td>--}}
        </tr>
        {{--<div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">--}}
            {{--<div class="modal-dialog">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                            {{--<span aria-hidden="true">&times;</span></button>--}}
                        {{--<h4 class="modal-title">Delete Email</h4>--}}
                    {{--</div>--}}
                    {{--<div class="modal-body">--}}
                        {{--<p>Are you sure you want to delete this Email ?</p>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>--}}
                        {{--<button type="submit" class="btn btn-outline" onclick="destroy_employee({{$list['id']}})">Delete</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    @endforeach
</table>

<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $emails])</div>

{{--<script type="text/javascript">--}}
    {{--$(document).ready(function(){--}}
        {{--$('.assign').click(function(){--}}
            {{--var emp_id = $(this).attr('uid');--}}
            {{--var l = Ladda.create(this);--}}
            {{--l.start();--}}
            {{--$.ajax({--}}
                {{--url: '{{url('admin/emails/assign')}}',--}}
                {{--type: "put",--}}
                {{--data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},--}}
                {{--success: function(data){--}}
                    {{--l.stop();--}}
                    {{--$('#assign_remove_'+emp_id).show();--}}
                    {{--$('#assign_add_'+emp_id).hide();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
        {{--$('.unassign').click(function(){--}}
            {{--var emp_id = $(this).attr('ruid');--}}
            {{--var l = Ladda.create(this);--}}
            {{--l.start();--}}
            {{--$.ajax({--}}
                {{--url: '{{url('admin/emails/unassign')}}',--}}
                {{--type: "put",--}}
                {{--data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},--}}
                {{--success: function(data){--}}
                    {{--l.stop();--}}
                    {{--$('#assign_remove_'+emp_id).hide();--}}
                    {{--$('#assign_add_'+emp_id).show();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--});--}}

    {{--function destroy_employee(id)--}}
    {{--{--}}
        {{--$.ajax({--}}
            {{--url:'emails/'+id,--}}
            {{--type:'delete',--}}
            {{--data:{'id':id},--}}
            {{--success:function(data)--}}
            {{--{--}}
                {{--var new_url = 'emails';--}}
                {{--window.location.href = new_url;--}}
            {{--}--}}
        {{--});--}}
    {{--}--}}
{{--</script>--}}