{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Email Title<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="subject">Email Subject<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Enter Subject']) !!}
        @if ($errors->has('subject'))
            <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="email">From Email<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email','style'=>'text-transform:lowercase']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="content">Content<span class="text-red">&nbsp;&nbsp;</span></label>
    <div class="col-sm-5">
        {!! Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'Optional','rows'=>'10','cols'=>'80', 'id'=>'editor1']) !!}
        @if ($errors->has('content'))
            <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role">Status<span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Email::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red', 'checked'])!!} <span style="margin-right: 10px">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red'])!!} <span style="margin-right: 10px">{{ $value }}</span>
                @endif
            </label>
        @endforeach

        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>