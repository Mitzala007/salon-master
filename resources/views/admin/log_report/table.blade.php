<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th>User Email</th>
        <th>Section</th>
        <th>Section Id</th>
        <th>Action</th>
        <th>Description</th>
        <th>Created Date</th>
        <th class="table-text table-th">View</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($logs as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{$list['user_id']}}</td>
            <td>{{ucfirst($list['content_type'])}}</td>
            <td>{{$list['content_id']}}</td>
            <td>{{$list['action']}}</td>
            <td>{{$list['description']}}</td>
            <td>{{$list['created_at']->format('Y-m-d')}}</td>
            <td class="table-text">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="View Details" data-trigger="hover">
                        <button class="btn btn-info res-btn" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-eye"></i></button>
                    </span>
                </div>
            </td>
        </tr>
    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $logs])</div>

@foreach($logs as $list)
        <!---------------------------------------------------View Details MODAL---------------------------------------------------------->
        <div id="myModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['user_id']}}</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Section</th>
                                    <td>{{ucfirst($list['content_type'])}}</td>
                                </tr>
                                <tr>
                                    <th>Section Id</th>
                                    <td>{{ $list['content_id'] }}</td>
                                </tr>
                                <tr>
                                    <th>Action</th>
                                    <td>{{ $list['action'] }}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{{ $list['description'] }}</td>
                                </tr>
                                <tr>
                                    <th>Updated Fields</th>
                                    <td>{{ rtrim($list['details'], ',')  }}</td>
                                </tr>
                                <tr>
                                    <th>Ip Address</th>
                                    <td>{{ $list['ip_address'] }}</td>
                                </tr>
                                <tr>
                                    <th>Created Date</th>
                                    <td>{{ $list['created_at']->format('Y-m-d')  }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
@endforeach
