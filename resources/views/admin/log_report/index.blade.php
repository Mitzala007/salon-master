@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/log_report') }}"><i class="fa fa-dashboard"></i> Log Report</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            {!! Form::open(['url' => url('admin/log_report'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search</h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-2 control-label">User Email:</div>
                        <div class="col-sm-7">
                            {!! Form::text('user_id', null, ['class' => 'form-control', 'placeholder' => 'User Email', 'id'=>'user_email']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 control-label">Start Date:</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('start-date', null, ['class' => 'form-control date-input', 'id'=>'datepicker', 'placeholder' => 'Start Date']) !!}
                            </div>
                        </div>

                        <div class="col-sm-1 control-label">End Date:</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('end-date', null, ['class' => 'form-control date-input', 'id'=>'datepicker1', 'placeholder' => 'End Date']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2 control-label">Action:</div>
                        <div class="col-sm-3">
                            {!! Form::select('action[]', App\Login_details::$action, null, ['class' => 'select2 form-control', 'multiple', 'id'=>'action']) !!}
                        </div>

                        <div class="col-sm-1 control-label">Section:</div>
                        <div class="col-sm-3">{!! Form::select('section[]', App\Login_details::$section, null, ['class' => 'select2 form-control', 'multiple', 'id'=>'section']) !!}</div>
                    </div>
                </div>

                <div class="box-footer">
                    <button class="btn btn-info pull-right" id="searchbtn" type="submit" value="search"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
            {!! Form::close() !!}


            @include('admin.loader')
            <div class="box box-info">
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.log_report.table')
                </div>
            </div>
        </section>
    </div>

@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();
        var user_email = $('#user_email').val();
        var start_date = $('#datepicker').val();
        var end_date = $('#datepicker1').val();
        var action = $('#action').val();
        var section = $('#section').val();
        var search = $('#searchbtn').val();

        $.ajax({
            url: '{{url('admin/log_report')}}',
            type: "get",
            data: {'search':search,'user_email':user_email, 'start_date':start_date, 'end_date':end_date, 'action':action, 'section':section,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
//                alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>

