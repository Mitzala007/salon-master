<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th style="width: 15%">User</th>
        <th style="width: 18%">Email</th>
        <th>Comment</th>
        <th style="width: 9%">Created Date</th>
        <th class="table-text table-th" style="width: 5%">View</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($feedback as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{$list['User']['name']}}</td>
            <td>{{$list['email']}}</td>
            <td>{{$list['comment']}}</td>
            <td>{{$list['created_at']->format('Y-m-d')}}</td>
            <td class="table-text">
                <div class="btn-group-horizontal">
                    <span data-toggle="tooltip" title="View Details" data-trigger="hover">
                        <button class="btn btn-info res-btn" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-eye"></i></button>
                    </span>
                </div>
            </td>
        </tr>
    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $feedback])</div>

@foreach($feedback as $list)
        <!---------------------------------------------------View Details MODAL---------------------------------------------------------->
<div id="myModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                <h4 class="modal-title">{{$list['User']['name']}}</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>First Name</th>
                            <td>
                                @if(!empty($list['first_name']))
                                    {{$list['first_name']}}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Last Name</th>
                            <td>
                                @if(!empty($list['last_name']))
                                    {{$list['last_name']}}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $list['email'] }}</td>
                        </tr>
                        <tr>
                            <th>Salon</th>
                            <td>
                                @if(!empty($list['saloon_id']))
                                    {{$list['Saloon']['title']}}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Comment</th>
                            <td>
                                {{$list['comment']}}
                            </td>
                        </tr>
                        <tr>
                            <th>Created Date</th>
                            <td>{{ $list['created_at']->format('Y-m-d')  }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endforeach