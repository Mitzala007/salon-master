@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/feedback') }}"><i class="fa fa-dashboard"></i> Log Report</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
{{--                        {!! Form::open(['url' => url('admin/feedback'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}--}}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="User Email" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
{{--                        {!! Form::close() !!}--}}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">

{{--                            <a href="{{ url('admin/feedback/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>--}}

                            <a href="{{ url('admin/feedback') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>

                        </h3>
                    </div>
                </div>

                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.feedback.table')
                </div>
            </div>
        </section>
    </div>

@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();
        var search = $('#search').val();
        $.ajax({

            url: '{{url('admin/feedback')}}',

            type: "get",

            data: {'search':search,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
                //alert(data);

                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true

                });
            }

        });
    });
</script>

