<div id="load" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th style="width: 10%;">Booking Name</th>
        <th style="width: 15%;">User</th>
        <th style="width: 15%;">No of persons</th>
        <th style="width: 15%;">Final Total</th>
        <th style="width: 15%;" class="table-text table-th">Status</th>
        <th>Services</th>
    </tr>
    </thead>
    <tbody>
    @foreach($booking as $list)
        <tr>
            <td >{{$list['booking_name']}}</td>
            <td>{{$list['Users']['name']}}</td>
            <td>{{$list['no_of_persons']}}</td>
            <td>{{$list['final_total']}}</td>


            <td style="padding-top: 11px;" class="table-text table-th">
                <a  href="{{ url('admin/waiting_list') }}"> <label class="label @if($list['status'] == 'in-progress') label-info @elseif($list['status'] == 'waiting') label-warning @elseif($list['status'] == 'cancel') label-danger @elseif($list['status'] == 'completed') label-success @endif" style="padding:5px 8px; font-size: 14px; cursor: pointer">{{ucfirst($list['status'])}}</label></a>
            </td>
            <td>
                <?php
                $service = '';

                foreach($list['Booking_details'] as $details)
                {
                    $service .= $details['service_type']['title'].',';
                }
                $ser_title = rtrim($service,',');
                ?>
                    {{$ser_title}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $booking])</div>