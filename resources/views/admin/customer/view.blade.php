<style>

    .mg-table{
        text-align: center;
        font-size: 13px;
    }
    table td{ padding: 7px}

    .tb-border {
        border: 1px solid #000;
        border-collapse: collapse;
    }

    .td-border {
        border: 1px solid #000;
    }

    .column {
        float: left;
        /*width: 50%;*/
        padding: 5px;
        color:#000000;
    }

    /* Clearfix (clear floats) */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
    }

    .rgt{
        text-align: right;
        font-weight: bold;
    }

    .modal-body table td{ color:#000000}

    @media (min-width: 768px){
        .modal-dialog {
            width: 725px;
            margin: 30px auto;
        }
    }
</style>
<body style="font-family: notosans">
<div class="row">
    <div class="column">
       @if($customer['image']!="" && file_exists($customer['image']))
            <img src="{{base_path($customer['saloon']['image'])}}" alt="Snow" style="width:50%">@else   @endif
        {{--@if( file_exists1.'/resource/saloon/2rUj8DEcreTSlmq80wgH.jpg')--}}
         {{--<img src="{{url('/resource/saloon/2rUj8DEcreTSlmq80wgH.jpg')}}" alt="Show" style="width:50%">--}}
        {{--@endif--}}
    </div>
    <div class="column">
        <div>
            Inv. No.: {{$customer['id'].\Carbon\Carbon::now()->format('ymd').$customer['saloon_id']}}<br>
            Booking. No.: {{$customer['booking_name']}}<br>
            Date: {{\Carbon\Carbon::now()->format('m/d/y')}}<br>
            Pay Status: @if($customer['final_total']!="") Paid  @else incomplete @endif
      </div>
    </div>
    <div class="column">
        <div style="margin-left: 300px;"><b>Salon Detail</b><br>
            Salon: {{$customer['saloon']['title']}}<br>
            Phone: {{$customer['saloon']['saloon_phone']}}<br>
          </div>
    </div>
    <div class="column">
        <div style="margin-left: 300px;"><b>Customer Detail</b><br>
            Customer: {{$customer['users']['name']}}&nbsp;{{$customer['users']['last_name']}}<br>
            Phone: {{$customer['users']['phone']}}<br>
        </div>
    </div>
</div>

{{--<table style="border-bottom: 2px solid #482162; margin-bottom: 2%; width: 700px;">--}}
    {{--<tr>--}}
        {{--<th style= "margin-left: 96px">&nbsp;Salon Detail</th>--}}
        {{--<th style="width: 320px; line-height: 25px;  font-size:14px; padding-left: 110px;">Customer Detail</th>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td style="width: 320px; line-height: 25px" >--}}
            {{--Salon: {{$booking['saloon']['title']}}<br>--}}
            {{--Phone: {{$booking['saloon']['saloon_phone']}}<br>--}}
        {{--</td>--}}
        {{--<td style="line-height: 20px ; font-size:14px; padding-left: 110px;">--}}
            {{--Customer: {{$booking['users']['name']}}&nbsp;{{$booking['users']['last_name']}}<br>--}}
            {{--Phone: {{$booking['users']['phone']}}<br>--}}
        {{--</td>--}}
    {{--</tr>--}}
{{--</table>--}}
<table class="tb-border" style="width: 700px;">
    <tr style="color: #000;">
        <th class="mg-table td-border" style="width:50px; height: 50px;">Sr. No.</th>
        <th class="mg-table td-border" style="width:150px;">Service Name</th>
        <th class="mg-table td-border" style="width:100px;">Time Duration</th>
        <th class="mg-table td-border" style="width:95px;">Price</th>
        <th class="mg-table td-border" style="width:70px;">Discount (%)</th>
        <th class="mg-table td-border" style="width:100px;">Final Price</th>
    </tr>
    <?php $cnt = 1; $tamt = '';?>
    @foreach($customer['booking_details'] as $book_detail)
        <tr>
            <td class="mg-table td-border">{{$cnt}}</td>
            <td class="mg-table td-border">{{$book_detail['service_type']['title']}}</td>
            <td class="mg-table td-border">{{$book_detail['service_type']['min_duration']}} Min.</td>
            <td class="mg-table td-border">{{ number_format((float)$book_detail['charges'], 2, '.', '')}}</td>
            <td class="mg-table td-border">{{number_format((float)$book_detail['discount'], 2, '.', '')}}</td>
            <td class="mg-table td-border">{{number_format((float)$book_detail['total_charge'], 2, '.', '')}}</td>
        </tr>
        <?php $cnt++;?>
    @endforeach

    <tr>
        <td colspan="5" class="mg-table td-border rgt"><span>Total Amount</span></td>
        <td class="mg-table td-border ">{{number_format((float)$customer['sub_total'], 2, '.', '')}}</td>
    </tr>

    @if($customer['additional_amount']!=null && $customer['additional_amount']!=0)
        <?php $additional_amount = $customer['additional_amount']; ?>
        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Additional Amount</span></td>
            <td class="mg-table td-border">{{number_format((float)$additional_amount, 2, '.', '')}}</td>
        </tr>
    @endif

    @if($customer['discount_amount']!=null && $customer['discount_amount']!=0)
        <?php $discount_amount = $customer['discount_amount']; ?>
        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Discount Amount</span></td>
            <td class="mg-table td-border">{{"- ".number_format((float)$discount_amount, 2, '.', '')}}</td>
        </tr>
    @endif

    <tr>
        <td colspan="5" class="mg-table td-border rgt"><span>Net Payable Amount</span></td>
        <td class="mg-table td-border">{{number_format((float)$customer['final_total'], 2, '.', '')}}</td>
    </tr>

</table>
@if($customer['remarks']!=null && $customer['remarks']!="")
    <h5 style="font-weight: normal;color: #000000;">{{ $customer['remarks'] }}</h5>
    {{--<h5>Remarks</h5>--}}
@endif
{{--<br>--}}
{{--<div style="float:right;margin-top: -18px;">--}}
{{--@if($booking['final_total']!="")--}}
{{--<button onclick="window.print()" style="background-color: #008CBA;width: 65px; height: 30px;">Print</button>--}}
    {{--@else--}}
    {{--@endif--}}
{{--</div>--}}
</body>
