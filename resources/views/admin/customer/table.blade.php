<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th width="5%" style="text-align: center; padding-right: 0px">Image</th>
        <th width="10%">User</th>
        <th width="20%">Email</th>
        <th width="20%">Phone</th>
        <th width="20%">Total Rewards Point</th>
        <th width="15%">Total Visit</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($customer as $list)
        <tr class="ui-state-default " id="arrayorder_{{$list['id']}}"  >
{{--        <tr class="ui-state-default clickable-row" id="arrayorder_{{$list['id']}}"  data-href='{{url('admin/customer/'.$list['id'].'/details')}}'>--}}
            <td class="table-text">
                <a data-toggle="modal" data-target="#imageModal{{$list['id']}}" style="cursor: pointer;">
                    @if($list['image']!="" && file_exists($list['image']))
                        <img src="{{ url($list['image']) }}" width="30">
                    @else
                        <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                    @endif
                </a>
            </td>
            <td>{{$list['name']}}</td>
            <td>{{$list['email']}}</td>
            <td>{{$list['phone']}}</td>
            <?php
                $total_visit = App\Booking::where('saloon_id',$id)->where('user_id',$list['id'])->where('status','completed')->count();
                $point_count = App\Point_history::where('user_id',$list['id'])->orderBy('id','DESC')->first();
            ?>
            <td>@if(!empty($point_count)){{$point_count->point_after}}@else 0 @endif</td>
            <td>{{$total_visit}}</td>
            <td>
                <button class="btn btn-info clickable-row" type="button" style="height:28px; padding:0 12px" data-href='{{url('admin/customer/'.$list['id'].'/details')}}'>
                    <span class="ladda-label">View</span>
                </button>
            </td>

        </tr>

        <!---************************IMAGE MODAL************************-->
        <div id="imageModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['name']}}</h4>
                    </div>
                    @if($list['image']!="" && file_exists($list['image']))
                        <div class="modal-body" style="background-image: url({{ url($list['image']) }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @else
                        <div class="modal-body" style="background-image: url({{ url('assets/dist/img/default-user.png') }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $customer])</div>

<script>
    function destroy_booking(vid)
    {
        $.ajax({
            url:'delete/'+vid,
            type:'delete',
            data:{'id':vid},
            success:function(data)
            {
                var new_url = 'list';
                window.location.href = new_url;
            }
        });
    }

    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });

</script>