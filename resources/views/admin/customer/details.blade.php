<style>
    @media (min-width: 768px){
        .modal-dialog {
            width: 725px !important;
            margin: 30px auto !important;
            }
        }
    </style>
@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}} Details
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/customer') }}"><i class="fa fa-dashboard"></i> Customer</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <!--********************************Customer Details********************************-->
            <div class="box box-info">
                <div class="box-header">
                    <table class="table table-bordered table-striped" id="example2">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Nick Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Total Visit</th>
                            <th class="table-text">Image</th>
                            {{--<th class="table-text">Status</th>--}}
                        </tr>
                        </thead>
                        <tbody id="sortable">
                            <tr class="ui-state-default">
                                <td>{{$user->name}}</td>
                                <td>{{$user->nick_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>{{$total_visit}}</td>
                                <td class="table-text">
                                    <a data-toggle="modal" data-target="#imageModal{{$user->id}}" style="cursor: pointer;">
                                        @if($user->image!="" && file_exists($user->image))
                                            <img src="{{ url($user->image) }}" width="30">
                                        @else
                                            <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                                        @endif
                                    </a>
                                </td>
                                {{--<td class="table-text">--}}
                                    {{--@if($user->status == 'active')--}}
                                        {{--<div class="btn-group-horizontal" id="assign_remove_{{ $user->id }}" >--}}
                                            {{--<button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $user->id }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>--}}
                                        {{--</div>--}}
                                        {{--<div class="btn-group-horizontal" id="assign_add_{{ $user->id }}"  style="display: none"  >--}}
                                            {{--<button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $user->id }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                    {{--@if($user->status == 'in-active')--}}
                                        {{--<div class="btn-group-horizontal" id="assign_add_{{ $user->id }}"   >--}}
                                            {{--<button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $user->id }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>--}}
                                        {{--</div>--}}
                                        {{--<div class="btn-group-horizontal" id="assign_remove_{{ $user->id }}" style="display: none" >--}}
                                            {{--<button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $user->id }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                            </tr>
                    </table>
                </div>
            </div>

            <!--************************POINT ADD OR REMOVE FORM************************-->
            <section class="content-header" style="padding-bottom: 1%;">
                <h1>Rewards Points - <label class="label label-success" style="padding:5px 8px; font-size: 14px;">@if(!empty($point_count)){{$point_count->point_after}} @else 0 @endif</label></h1>
            </section>
            {!! Form::open(['url' => url('admin/point_history/add'), 'class' => 'form-horizontal','files'=>false]) !!}
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <input type="hidden" name="salon_id" value="{{$salon_id}}">
                <div class="box box-primary">
                    <div class="box-header" style="border-bottom: 1px solid #f4f4f4;">
                        <div class="col-md-2">
                            <h3 class="box-title control-label">Points Add/Remove</h3>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-1 control-label">Type:</div>
                            <div class="col-sm-3">
                                {!! Form::select('type', App\Point_history::$type, null, ['class' => 'select2 form-control', 'id'=>'type']) !!}
                            </div>

                            <div class="col-sm-1 control-label">Point:</div>
                            <div class="col-sm-3">
                                {!! Form::text('point', null, ['class' => 'form-control', 'placeholder' => 'Enter Point','id'=>'point']) !!}
                            </div>

                            <div class="col-sm-1 ">
                                <button class="btn btn-info pull-right" type="submit" value="search">Submit</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive" id="itemlist">
                        @include('admin.loader')
                        @include('admin.users.point_table')
                    </div>
                </div>
            {!! Form::close() !!}

            <!--************************BOOKING HISTORY************************-->
            <section class="content-header" style="padding-bottom: 1%;">
                <h1>Booking - <label class="label label-success" style="padding:5px 8px; font-size: 14px;">{{$booking_count->count()}}</label></h1>
            </section>
            <div class="box box-info">
                <div class="box-body table-responsive" >

                    <table class="table table-bordered table-striped" id="example33">
                        <thead>
                        <tr>
                            <th>Booking Name</th>
                            <th>Date</th>
                            <th class="table-text">Status</th>
                            <th class="table-text">Action</th>
                        </tr>
                        </thead>
                        <tbody id="sortable">
                        @foreach($booking as $list)
                            <tr class="ui-state-default">
                                <td>{{$list['booking_name']}}</td>
                                <td>{{$list['date']}}</td>
                                <td style="padding-top: 16px;" class="table-text table-th">
                                    <label class="label @if($list['status'] == 'in-progress') label-info @elseif($list['status'] == 'waiting') label-warning @elseif($list['status'] == 'cancel') label-danger @elseif($list['status'] == 'completed') label-success @endif" style="padding:5px 8px; font-size: 14px;">{{ucfirst($list['status'])}}</label>
                                </td>
                                <td class="table-text table-th">
                                    <div class="btn-group-horizontal">
                                        <span data-toggle="tooltip" title="View Details" data-trigger="hover" style="margin-right: 8px;">
                                            {{--<button class="btn btn-primary res-btn" type="button" data-toggle="modal" onclick="booking_detail({{$list['id']}})"--}}
                                                    {{--data-target="#booking_view_model{{$list['id']}}"><i class="fa fa-eye"></i></button>--}}
                                            <button class="btn btn-primary res-btn" type="button" data-toggle="modal" onclick="booking_detail({{$list['id']}})"
                                                    data-target="#booking_view_model{{$list['id']}}"><i class="fa fa-eye"></i></button>
                                        </span>
                                    </div>
                                </td>
                            </tr>


                            <div id="booking_view_model{{$list['id']}}" class="fade modal modal-primary" role="dialog" >
                                <div class="modal-dialog">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{{$list['booking_name']}}</h4>
                                    </div>
                                    <div class="modal-body" id="booking_information_{{$list['id']}}"></div>
                                    {{--<div class="modal-footer">--}}
                                    {{--<button type="submit" class="btn btn-outline" onclick="destroy_booking({{$list['id']}})"> Print </button>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        @endforeach
                    </table>

                </div>
            </div>
        </section>
    </div>

    <!---************************IMAGE MODAL************************-->
    <div id="imageModal{{$user->id}}" class="fade modal modal-primary" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                    <h4 class="modal-title">{{$user->name}}</h4>
                </div>
                @if($user->image!="" && file_exists($user->image))
                    <div class="modal-body" style="background-image: url({{ url($user->image) }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                    </div>
                @else
                    <div class="modal-body" style="background-image: url({{ url('assets/dist/img/default-user.png') }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection



<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>


<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example1').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }
</script>
<script>
    function booking_detail(bid)
    {
        $.ajax({
            url:'{{url('admin/booking')}}/'+bid,
            type:'get',
            error:function () {
            },
            success:function (result) {
                $('#booking_view_model'+bid).modal('show');
                document.getElementById('booking_information_'+bid).innerHTML=result;

            }
        });
    }
</script>