{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="category_id" id="category_lb">Category <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {{--{!! Form::select('category_id', [''=>'Please Select']+$category , null, ['id'=>'category_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {!! Form::select('category_id', [''=>'Please select']+$category, null, ['id'=>'category_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
        <strong><span id="category_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('sub_category_id') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="sub_category_id_lb">Sub Category <span class="text-red"></span></label>
    <div class="col-sm-5">
        {!! Form::select('sub_category_id', [''=>'Please select']+$sub_category, !empty($sub_category_id)?$sub_category_id:null, ['id'=>'sub_category_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('sub_category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('sub_category_id') }}</strong>
            </span>
        @endif
        <strong><span id="sub_category_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="title_lb">Service Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title','id'=>'title']) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
        <strong><span id="title_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('min_duration') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="min_duration_lb">Default Service Time <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('min_duration', [''=>'Please select']+\App\Service_type::$minutes, null, ['id'=>'min_duration', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('min_duration'))
            <span class="help-block">
                <strong>{{ $errors->first('min_duration') }}</strong>
            </span>
        @endif
        <strong><span id="min_duration_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\Service_type::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
            <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<script type="text/javascript">
    function validation()
    {
        var flag = 0;

        if(document.getElementById("category_id").value.split(" ").join("") == "")
        {
            document.getElementById('category_msg').innerHTML = 'The Category field is required.';
            document.getElementById("category_id").focus();
            document.getElementById("category_id").style.borderColor ='#dd4b39';
            document.getElementById("category_lb").style.color ='#dd4b39';
            document.getElementById('category_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("category_id").style.borderColor ='#d2d6de';
            document.getElementById("category_lb").style.color ='#333';
            document.getElementById('category_msg').style.display = 'none';
        }


        if(document.getElementById("title").value.split(" ").join("") == "")
        {
            document.getElementById('title_msg').innerHTML = 'The service name field is required.';
            document.getElementById("title").focus();
            document.getElementById("title").style.borderColor ='#dd4b39';
            document.getElementById("title_lb").style.color ='#dd4b39';
            document.getElementById('title_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("title").style.borderColor ='#d2d6de';
            document.getElementById("title_lb").style.color ='#333';
            document.getElementById('title_msg').style.display = 'none';
        }

        if(document.getElementById("min_duration").value.split(" ").join("") == "")
        {
            document.getElementById('min_duration_msg').innerHTML = 'The default service time field is required.';
            document.getElementById("min_duration").focus();
            document.getElementById("min_duration").style.borderColor ='#dd4b39';
            document.getElementById("min_duration_lb").style.color ='#dd4b39';
            document.getElementById('min_duration_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("min_duration").style.borderColor ='#d2d6de';
            document.getElementById("min_duration_lb").style.color ='#333';
            document.getElementById('min_duration_msg').style.display = 'none';
        }

        var active = 'active';
        var inactive = 'in-active';
        if(document.getElementById("status_"+active).checked == false && document.getElementById("status_"+inactive).checked == false)
        {
            document.getElementById('status_msg').innerHTML = 'The status field is required.';
            document.getElementById("status_val_"+active).style.color ='#dd4b39';
            document.getElementById("status_val_"+inactive).style.color ='#dd4b39';
            document.getElementById("status_lb").style.color ='#dd4b39';
            document.getElementById('status_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("status_val_"+active).style.color ='#333';
            document.getElementById("status_val_"+inactive).style.color ='#333';
            document.getElementById("status_lb").style.color ='#333';
            document.getElementById('status_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>

@section('jquery')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#category_id").change(function() {
                if ($(this).val() != '') {
                    $.ajax({
                        url: '{{ url('admin/service/get_sub_category') }}/' + $(this).val(),
                        error: function () {
                        },
                        success: function (result) {
                            //alert(result);
                            $("#sub_category_id").select2().empty();
                            $("#sub_category_id").html(result);
                            $('#sub_category_id').select2()
                        }
                    });
                } else {

                    $("#sub_category_id").empty();
                    $("#sub_category_id").html('');
                    $('#sub_category_id').select2()
                }
            });
        });
    </script>
@endsection
