@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Update</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/salon_guide')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Update</li>
            </ol>
        </section>

        <section class="content">
            @include ('admin.error')
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Update {{$menu}}</h3>
                        </div>
                        {!! Form::model($guide,['url' => url('admin/salon_guide_update/'.$guide->id.'/edit'),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return validation();']) !!}
                        <input type="hidden" name="mod" id="mod" value="Update">
                        <div class="nav-tabs-custom">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="file" id="file_lb">File <span class="text-red">*</span></label>
                                    <div class="col-sm-5">
                                        <div class="">
                                            {!! Form::file('file', ['class' => '', 'id'=> 'file']) !!}
                                        </div>

                                        @if(isset($guide) && $guide->file !="")
                                            <a href="{{url($guide->file)}}" target="_blank">
                                                <img src="{{url('assets/dist/img/file_2.png')}}" style="height: 100px; padding-top: 1.5%;">
                                                <?php
                                                    $file = explode('/',$guide->file);
                                                ?>
                                                <p>{{$file[3]}}</p>
                                            </a>
                                        @endif

                                        @if ($errors->has('file'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('file') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <a href="{{ url('admin/salon_guide') }}" ><button class="btn btn-default" type="button">Back</button></a>
                                <button class="btn btn-info pull-right" type="submit">Edit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection



