<!DOCTYPE html>
<html>
<head>
    {{--<meta http-equiv="refresh" content="5" />--}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('siteVars.sitetitle')}} | {{ $menu }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/dist/img/Logo_web.png') }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/datepicker3.css')}}">
    <!-- Icheck radio -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/all.css')}}">
    <!-- SELECT  -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">

    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">

    <style type="text/css">
        .select2-container .select2-selection--single {
            height: 34px !important;
        }
    </style>
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Bootstrap datatable -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">

    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/summernote/summernote.css') }}">

    <style>
        button[disabled], html input[disabled]{cursor: not-allowed !important;}
    </style>

    <!--******************CSS FOR COUNTRY FLAG******************-->

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dist/css/msdropdown/dd.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dist/css/msdropdown/flags.css') }}" />

    <!--******************CSS FOR CHECKBOX & RADIO******************-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dist/css/checkbox.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dist/css/radio.css') }}">

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    <?php $plan = 0; ?>
    @if(\Illuminate\Support\Facades\Auth::user()->membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '3')
        <?php $plan = \Illuminate\Support\Facades\Auth::user()->membership_plan; ?>
    @elseif(\Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '3')
        <?php $plan = \Illuminate\Support\Facades\Auth::user()->admin_membership_plan?>
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->role != 'admin' || $plan == '1' || $plan == '2' || $plan == '3')
    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5bed010c70ff5a5a3a7241be/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    @endif

</head>
<body class="hold-transition skin-blue sidebar-mini" id="bodyid">
<div class="wrapper">

    <header class="main-header">
        <a href="{{ url('admin/dashboard') }}" class="logo">
            <span class="logo-mini"><b>{{config('siteVars.minititle')}}</b></span>
            <span class="logo-lg"><b>{{config('siteVars.sitetitle')}}</b> Admin</span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php $user_image = Auth::user()->image; ?>
                            @if(!empty($user_image))
                                <img src="{{url($user_image)}}" class="user-image" alt="User Image">
                            @else
                                <img src="{{url('assets/dist/img/default-user.png')}}" class="user-image" alt="User Image">
                            @endif
                            <span class="hidden-xs">{{ $user = Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">

                                @if(!empty($user_image))
                                    <img src="{{url($user_image)}}" class="img-circle" alt="User Image">
                                @else
                                    <img src="{{url('assets/dist/img/default-user.png')}}" class="img-circle" alt="User Image">
                                @endif
                                <p>
                                    {{ $user = Auth::user()->name }} <br>
                                    <small></small>
                                </p>

                                <div class="pull-left" style="margin-top: -12px;">
                                    <a href="{{ url('admin/profile_update/'.$user = Auth::user()->id).'/edit' }}"
                                       class="btn btn-danger btn-flat">Profile</a>
                                </div>
                                <div class="pull-right" style="margin-top: -12px;">
                                    <a href="{{ url('admin/logout') }}" class="btn btn-danger btn-flat">Sign out</a>
                                </div>

                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="@if($menu=='Dashboard') active  @endif treeview">
                    <a href="{{ url('admin/dashboard') }}">
                        <i class="fa fa-dashboard"></i><span>Dashboard</span>
                    </a>
                </li>

                @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')

                    <li class="treeview @if($menu=='User' || $menu == 'Deleted User') active  @endif" >
                        <a href="#">
                            <i class="fa fa-th"></i><span>User Management</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu">
                            <li class="@if($menu=='User') active  @endif treeview">
                                <a href="{{ url('admin/users') }}">
                                    <i class="fa fa-circle-o"></i><span>User</span>
                                </a>
                            </li>

                            <li class="@if($menu=='Deleted User') active    @endif treeview">
                                <a href="{{ url('admin/deleted-user') }}">
                                    <i class="fa fa-circle-o"></i><span>Deleted User</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="@if($menu=='Join '.config('siteVars.title')) active  @endif treeview">
                        <a href="{{ url('admin/become_nailmaster') }}">
                            <?php $bcount =  \App\Salon_owner_request::where('status',0)->count(); ?>
                            <i class="fa fa-th"></i><span>Join {{ config('siteVars.title') }} </span>
                            <span class="label label-danger pull-right"><?php echo $bcount;?></span>
                        </a>
                    </li>

                    <li class="@if($menu=='Category') active  @endif treeview">
                        <a href="{{ url('admin/category') }}">
                            <i class="fa fa-th"></i><span>Category Management</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Service Type') active  @endif treeview">
                        <a href="{{ url('admin/service') }}">
                            <i class="fa fa-th"></i><span>Service Type Management</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Salon') active  @endif treeview">
                        <a href="{{ url('admin/saloon') }}">

                            <i class="fa fa-th"></i><span>Salon Management</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Booking') active  @endif treeview">
                        @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                            <a href="{{ url('admin/booking') }}">
                                <i class="fa fa-th"></i><span>Booking Management</span>
                            </a>
                        @endif
                    </li>

                    <li class="@if($menu=='Notification') active  @endif treeview">
                        <a href="{{ url('admin/notification') }}">
                            <i class="fa fa-th"></i><span>Notification Management</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Membership') active  @endif treeview">
                        <a href="{{ url('admin/membership') }}">
                            <i class="fa fa-th"></i><span>Membership Management</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Log') active  @endif treeview">
                        <a href="{{ url('admin/log_report') }}">
                            <i class="fa fa-th"></i><span>Log Report</span>
                        </a>
                    </li>

                    <li class="@if($menu=='Feedback') active  @endif treeview">
                        <a href="{{ url('admin/feedback') }}">
                            <i class="fa fa-th"></i><span>Feedback</span>
                        </a>
                    </li>

                    <li class="@if(isset($menu) && $menu=='Email') active @endif">
                        <a href="{{ url('admin/emails') }}"><i class="fa fa-th"></i><span>Email Management </span></a>
                    </li>

                    <li class="treeview @if($menu=='Terms and Conditions' || $menu == 'Loyalty Program') active  @endif" >
                        <a href="#">
                            <i class="fa fa-th"></i><span>Terms & Conditions</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu">
                            <li class="@if($menu=='Terms and Conditions') active  @endif treeview">
                                <a href="{{ url('admin/staticpage/1/edit') }}">
                                    <i class="fa fa-circle-o"></i><span>Application Terms</span>
                                </a>
                            </li>

                            <li class="@if($menu=='Loyalty Program') active  @endif treeview">
                                <a href="{{ url('admin/staticpage/2/edit') }}">
                                    <i class="fa fa-circle-o"></i><span>Loyalty Program</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview @if($menu=='User Manual' || $menu == 'Salon Manual') active  @endif" >
                        <a href="#">
                            <i class="fa fa-th"></i><span>Help & Support</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu">
                            <li class="@if($menu=='User Manual') active  @endif treeview">
                                <a href="{{ url('admin/staticpage/3/edit') }}">
                                    <i class="fa fa-circle-o"></i><span>User Manual</span>
                                </a>
                            </li>

                            <li class="@if($menu=='Salon Manual') active  @endif treeview">
                                <a href="{{ url('admin/staticpage/4/edit') }}">
                                    <i class="fa fa-circle-o"></i><span>Salon Manual</span>
                                </a>
                            </li>

                            <li class="@if(isset($menu) && $menu=='Salon Management Guide') active  @endif treeview">
                                <a href="{{ url('admin/salon_guide/1') }}">
                                    <i class="fa fa-circle-o"></i><span>Salon Management Guide</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                @else
                    <?php $get_session = Session::get('salon_id'); ?>

                    @if($get_session=="")
                            <li>
                                <a href="#">
                                    <i class="fa fa-th"></i><span>Waiting List</span>
                                    <span class="label label-danger pull-right" id="wait_counter">0</span>
                                </a>
                            </li>

                            <li class="@if($menu=='Salon') active  @endif treeview">
                                @if($get_session!="")
                                    <a href="{{ url('admin/saloon/'.$get_session.'/edit') }}"><i class="fa fa-th"></i><span>Salon Management</span></a>
                                @else
                                    <a href="{{ url('admin/saloon/create') }}"><i class="fa fa-th"></i><span>Salon Management</span></a>
                                @endif
                            </li>

                            <li>
                                <a href="#"><i class="fa fa-th"></i><span>Booking History</span></a>
                            </li>

                            @if($plan==1)
                            <li>
                                <a href="#"><i class="fa fa-th"></i><span>Employee Management</span></a>
                            </li>
                            @endif

                            <li>
                                <a href="#"><i class="fa fa-th"></i><span>View Reviews & Ratings</span></a>
                            </li>


                                <li>
                                    <a href="#"><i class="fa fa-th"></i><span>Customer Management</span></a>
                                </li>

                    @else
                            <li class="@if($menu=='Waiting List') active  @endif treeview">
                                <a href="{{ url('admin/waiting_list') }}">
                                    <?php $wait_counter =  \App\Booking::where('date',date('Y-m-d'))
                                        ->where('saloon_id',$get_session)
                                        ->Where(function ($query){
                                            $query->where('status', 'waiting')
                                                ->orwhere('status', 'in-progress');
                                        })->count(); ?>
                                    <i class="fa fa-th"></i><span>Waiting List</span>
                                    <span class="label label-danger pull-right" id="wait_counter"><?php echo $wait_counter;?></span>
                                </a>
                            </li>

                            <li class="@if($menu=='Salon') active  @endif treeview">
                                @if($get_session!="")
                                    <a href="{{ url('admin/saloon/'.$get_session.'/edit') }}"><i class="fa fa-th"></i><span>Salon Management</span></a>
                                @else
                                    <a href="{{ url('admin/saloon/create') }}"><i class="fa fa-th"></i><span>Salon Management</span></a>
                                @endif
                            </li>

                            <li class="@if($menu=='Booking') active  @endif treeview">
                                @if($get_session!="")
                                    <a href="{{ url('admin/booking/'.$get_session.'/list') }}"><i class="fa fa-th"></i><span>Booking History</span></a>
                                @else
                                    <a href="{{ url('admin/booking') }}"><i class="fa fa-th"></i><span>Booking History</span></a>
                                @endif
                            </li>

                            @if($plan==1)
                            <li class="@if($menu=='Employee') active  @endif treeview">
                                @if($get_session!="")
                                    <a href="{{ url('admin/employee/'.$get_session.'/list') }}"><i class="fa fa-th"></i><span>Employee Management</span></a>
                                @else
                                    <a href="{{ url('admin/employee') }}"><i class="fa fa-th"></i><span>Employee Management</span></a>
                                @endif
                            </li>
                            @endif

                            <li class="@if($menu=='Reviews') active  @endif treeview">
                                <a href="{{ url('admin/rating') }}"><i class="fa fa-th"></i><span>View Reviews & Ratings</span></a>
                            </li>


                                <li class="@if($menu=='Customer') active  @endif treeview">
                                @if($get_session!="")
                                    <a href="{{ url('admin/customer/'.$get_session.'/list') }}"><i class="fa fa-th"></i><span>Customer Management</span></a>
                                @else
                                    <a href="{{ url('admin/customer/') }}"><i class="fa fa-th"></i><span>Customer Management</span></a>
                                @endif
                            </li>


                            <li class="@if($menu=='Membership') active  @endif treeview">
                                <a href="{{ url('admin/my_membership') }}"><i class="fa fa-th"></i><span>Membership</span></a>
                            </li>
                    @endif


                @endif
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    @yield('content')
    <footer class="main-footer">
        <strong>{{config('siteVars.sitetitle')}} Admin</strong>
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- jQuery 2.2.0 -->
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/dist/js/msdropdown/jquery.dd.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $("#countries").msDropdown();
    })
</script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script type="text/javascript">
    $('.calltype').click(function () {
        alert(this.val());
    });
</script>

<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap datatables -->
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>


<script src="{{ URL::asset('assets/plugins/typeahead.js')}}"></script>

<script>
    $(function () {

        $(".select2").select2();
//Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        $("#example11").DataTable();
        $("#example3").DataTable({"paging": false});
        var table=$('#example2').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });

        $('#example23').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });

        $('#example33').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });

        $('#example51').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });

        $('#reservation').daterangepicker({
            format: 'YYYY/MM/DD'
        });

        $('#event_timing').daterangepicker({timePicker: true, timePickerIncrement: 1, use24hours: true,format: 'YYYY-MM-DD HH:mm',showMeridian:true  });
        $('#available_timing').daterangepicker({timePicker: true, timePickerIncrement: 1, use24hours: true,format: 'YYYY-MM-DD HH:mm',showMeridian:true  });
        $('#lobby_timing').daterangepicker({timePicker: true, timePickerIncrement: 1, use24hours: true,format: 'YYYY-MM-DD HH:mm',showMeridian:true  });

        $('#datepicker').datepicker({
            format: 'm/d/yyyy',
            autoclose: true
        });

        $('#datepicker1').datepicker({
            format: 'm/d/yyyy',
            autoclose: true
        });

        $('#bdate').datepicker({
            format: 'yyyy-m-d',
            autoclose: true
        });

        $('#datepicker_walkin').datepicker({
            format: 'm/d/yyyy',
            startDate: '+0d',
            autoclose: true,
            //maxViewMode: 0
        });

//Timepicker
        $(".timepicker").timepicker({
            showInputs: false,
            showMeridian: true,
        });

    });


    var val = document.getElementById('type').value;
    if(val == 'URL'){
        document.getElementById('URL_div').style.display='block';
        document.getElementById('image_div').style.display='none';
    }
    if(val == 'image'){
        document.getElementById('URL_div').style.display='none';
        document.getElementById('image_div').style.display='block';
    }



</script>

{{--@yield('jquery')--}}
<!-- Bootstrap 3.3.6 -->
<script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>


<script src="{{ URL::asset('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Morris.js charts -->

<!-- InputMask -->
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{ URL::asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ URL::asset('assets/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{ URL::asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{ URL::asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{ URL::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ URL::asset('assets/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('assets/dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ URL::asset('assets/dist/js/pages/dashboard.js')}}"></script>--}}

<script src="{{ URL::asset('assets/dist/js/demo.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>



<script src="{{ URL::asset('assets/plugins/summernote/summernote.js') }}"></script>
{{--<script src="{{ url('js/summernote-file.js')}}"></script>--}}

<script type="text/javascript">

    $(function (){

//         $("textarea[name=description]").summernote({
//            height: 300,
//            toolbar: [
//                // [groupName, [list of button]]
//
//                ['style', ['bold', 'italic', 'underline', 'clear']],
//                ['font', ['strikethrough', 'superscript', 'subscript']],
//                ['color', ['color']],
//                ['insert', ['link','map','minidiag']],
//                ['misc', ['fullscreen', 'codeview']],
//            ],
//            callbacks: {
//                onImageUpload: function(files) {
//                    for (var i = 0; i < files.length; i++)
//                        upload_image(files[i], this);
//                }
//            },
//        });

        $("textarea[name=content]").summernote({
            height: 300,
            toolbar: [
                // [groupName, [list of button]]

                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize','font', 'height']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['table','picture','video','link','map','minidiag']],
                ['misc', ['fullscreen', 'codeview']],
            ],
            callbacks: {
                onImageUpload: function(files) {
                    for (var i = 0; i < files.length; i++)
                        upload_image(files[i], this);
                }
            },
        });

        $("textarea[name=description]").summernote({
            height: 300,
            toolbar: [
                // [groupName, [list of button]]

                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize', 'font', 'height']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['table', 'picture', 'video', 'link', 'map', 'minidiag']],
                ['misc', ['fullscreen', 'codeview']],
            ],
            callbacks: {
                onImageUpload: function (files) {
                    for (var i = 0; i < files.length; i++)
                        upload_image(files[i], this);
                }
            },
        });


    });


    function upload_image(file, el) {
        var form_data = new FormData();
        form_data.append('image', file);
        $.ajax({
            data: form_data,
            type: "POST",
            url: '{{url('admin/image/upload')}}',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $(el).summernote('editor.insertImage', url);
            }
        });
    }

</script>

<script>
    function search_filters(event){
        var x = event.keyCode;
        if (x == 13) {  // 13 is the Enter key
            $("#searchbtn").click();
        }
    }
</script>

@yield('jquery')
</body>
</html>
