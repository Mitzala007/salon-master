<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PDF</title>
    <style>

        body{ margin: 0px auto; width: 700px;}
        .mg-table{
            text-align: center;
            font-size: 13px;
        }
        table td{ padding: 7px}

        .tb-border {
            border: 1px solid #482162;
            border-collapse: collapse;
        }

        .td-border {
            border: 1px solid #482162;
        }

        .column {
            float: left;
            width: 50%;
            padding: 5px;
        }

        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: center;
        }

        .rgt{
            text-align: right;
            font-weight: bold;
        }
    </style>
</head>
<body style="font-family: notosans">

<div class="row">
    <div class="column">
        <img src="{{base_path($booking['saloon']['image'])}}" alt="Snow" style="width:50%">
    </div>
    <div class="column">
        <div style="margin-left: 90px;">
            Inv. No.: {{$booking['id'].\Carbon\Carbon::now()->format('ymd').$booking['saloon_id']}}<br>
            Booking. No.: {{$booking['booking_name']}}<br>
            Date: {{\Carbon\Carbon::now()->format('m/d/y')}}<br>
        </div>
    </div>
</div>

<table style="border-bottom: 2px solid #482162; margin-bottom: 2%; width: 700px;">
    <tr>
        <th style="width: 320px; line-height: 25px">&nbsp;Salon Detail</th>
        <th style="line-height: 20px ; font-size:14px; padding-left: 110px;">Customer Detail</th>
    </tr>
    <tr>
        <td style="width: 320px; line-height: 25px" >
            Salon: {{$booking['saloon']['title']}}<br>
            Phone: {{$booking['saloon']['saloon_phone']}}<br>
        </td>
        <td style="line-height: 20px ; font-size:14px; padding-left: 110px;">
            Customer: {{$booking['users']['name']}}&nbsp;{{$booking['users']['last_name']}}<br>
            Phone: {{$booking['users']['phone']}}<br>
        </td>
    </tr>
</table>

<table class="tb-border" style="width: 700px;">
    <tr style="color: #000;">
        <th class="mg-table td-border" style="width:50px; height: 50px;">Sr. No.</th>
        <th class="mg-table td-border" style="width:150px;">Service Name</th>
        <th class="mg-table td-border" style="width:100px;">Time Duration</th>
        <th class="mg-table td-border" style="width:95px;">Price</th>
        <th class="mg-table td-border" style="width:70px;">Discount (%)</th>
        <th class="mg-table td-border" style="width:100px;">Final Price</th>
    </tr>
    <?php $cnt = 1; $tamt = '';?>
    @foreach($booking['booking_details'] as $book_detail)
        <tr>
            <td class="mg-table td-border">{{$cnt}}</td>
            <td class="mg-table td-border">{{$book_detail['service_type']['title']}}</td>
            <td class="mg-table td-border">{{$book_detail['service_type']['min_duration']}} Min.</td>
            <td class="mg-table td-border">{{ number_format((float)$book_detail['charges'], 2, '.', '')}}</td>
            <td class="mg-table td-border">{{number_format((float)$book_detail['discount'], 2, '.', '')}}</td>
            <td class="mg-table td-border">{{number_format((float)$book_detail['total_charge'], 2, '.', '')}}</td>
        </tr>
        <?php $cnt++;?>
    @endforeach

    <tr>
        <td colspan="5" class="mg-table td-border rgt"><span>Total Amount</span></td>
        <td class="mg-table td-border ">{{number_format((float)$booking['sub_total'], 2, '.', '')}}</td>
    </tr>

    @if($booking['additional_amount']!=null && $booking['additional_amount']!=0)
        <?php $additional_amount = $booking['additional_amount']; ?>
        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Additional Amount</span></td>
            <td class="mg-table td-border">{{number_format((float)$additional_amount, 2, '.', '')}}</td>
        </tr>
    @endif

    @if($booking['discount_amount']!=null && $booking['discount_amount']!=0)
        <?php $discount_amount = $booking['discount_amount']; ?>
        <tr>
            <td colspan="5" class="mg-table td-border rgt"><span>Discount Amount</span></td>
            <td class="mg-table td-border">{{"- ".number_format((float)$discount_amount, 2, '.', '')}}</td>
        </tr>
    @endif

    <tr>
        <td colspan="5" class="mg-table td-border rgt"><span>Net Payable Amount</span></td>
        <td class="mg-table td-border">{{number_format((float)$booking['final_total'], 2, '.', '')}}</td>
    </tr>

    @if($booking['remarks']!=null && $booking['remarks']!="")

    <h5 style="font-weight: normal">{{ $booking['remarks'] }}</h5>
    <h5>Remarks</h5>
    @endif

</table>
<br>

<div class="footer">
    <p>Powerd by: Nail Master.Co LLC</p>
</div>
</body>
</html>