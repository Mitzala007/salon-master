@extends('admin.layouts.app')
<style>
    .select2{
        width: 100%!important;
    }
</style>
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/users') }}"><i class="fa fa-dashboard"></i> User</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>


            <!--************************POINT ADD OR REMOVE FORM************************-->

            {!! Form::open(['url' => url('admin/point_history/add'), 'class' => 'form-horizontal','files'=>false]) !!}
                <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <div class="col-md-10">
                        <h3 class="box-title">Point Add/Remove</h3>
                    </div>
                    <div class="col-md-2">
                        <span data-toggle="tooltip" data-trigger="hover" class="pull-right">
                            <button type="button" class="btn btn-info pull-right" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-2 control-label">Salon:</div>
                        <div class="col-sm-7">
                            {!! Form::select('salon_id', $salon, null, ['id'=>'salon_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 control-label">Type:</div>
                        <div class="col-sm-3">
                            {!! Form::select('type', App\Point_history::$type, null, ['class' => 'select2 form-control', 'id'=>'type']) !!}
                        </div>

                        <div class="col-sm-1 control-label">Point:</div>
                        <div class="col-sm-3">
                            {!! Form::text('point', null, ['class' => 'form-control', 'placeholder' => 'Enter Point','id'=>'point']) !!}
                        </div>
                    </div>
                </div>
                    <input type="hidden" name="user_id" value="{{$id}}">
                <div class="box-footer">
                    <button class="btn btn-info pull-right" type="submit" value="search">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}

            <!--************************SEARCH FORM************************-->

            {!! Form::open(['url' => url('admin/point_history/'.$id), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
            <input type="hidden" name="user_id" value="{{$id}}" id="user_id">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <div class="col-md-10">
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="col-md-2">
                        <span data-toggle="tooltip" data-trigger="hover" class="pull-right">
                            <button type="button" class="btn btn-info pull-right" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <div class="col-sm-2 control-label">Salon:</div>
                        <div class="col-sm-3">
                            {!! Form::select('salon_id', $salon, null, ['id'=>'saloon_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                        </div>

                        <div class="col-sm-1 control-label">Type:</div>
                        <div class="col-sm-3">
                            {!! Form::select('type', [''=>'Please Select']+App\Point_history::$type, null, ['class' => 'select2 form-control', 'id'=>'add_remove']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 control-label">Start Date:</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('start-date', null, ['class' => 'form-control date-input', 'id'=>'datepicker', 'placeholder' => 'Start Date']) !!}
                            </div>
                        </div>

                        <div class="col-sm-1 control-label">End Date:</div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                {!! Form::text('end-date', null, ['class' => 'form-control date-input', 'id'=>'datepicker1', 'placeholder' => 'End Date']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button class="btn btn-info pull-right" id="searchbtn" type="submit" value="search"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title" style="float:right;">
                        <a href="{{ url('admin/point_history/'.$id) }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                    </h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.users.point_table')
                </div>
            </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>


<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        e.preventDefault();
        var user_id = $('#user_id').val();
        var saloon_id = $('#saloon_id').val();
        var start_date = $('#datepicker').val();
        var end_date = $('#datepicker1').val();
        var type = $('#add_remove').val();
        var point = $('#user_point').val();
        var search = $('#searchbtn').val();

        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/point_history')}}'+'/'+user_id,
            type: "get",
            data: {'search':search,'saloon_id':saloon_id, 'start_date':start_date, 'end_date':end_date, 'type':type, 'point':point,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
//                alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });

</script>
