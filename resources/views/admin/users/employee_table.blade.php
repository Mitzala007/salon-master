<div id="load_employee" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped" id="example23">
    <thead>
    <tr>
        <th class="table-text table-th">Edit</th>
        {{--<th>Id</th>--}}
        @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')
            <th>Salon</th>
        @endif
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Available</th>
        <th class="table-text table-th">Image</th>
        <th class="table-text table-th">Status</th>
        {{--<th class="table-text table-th">Delete</th>--}}
    </tr>
    </thead>

    <tbody id="sortable">
    @foreach ($employees as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td class="table-text table-th">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/employees/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                    <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Employee" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
            {{--            <td>{{ $list['id'] }}</td>--}}
            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')
                <td>{{ $list['saloon_master']['title'] }}</td>
            @endif
            <td>{{$list['first_name'] . ' ' . $list['last_name']}}</td>
            <td>{{$list['email']}}</td>
            <td>{{$list['phone']}}</td>
            <td>{{$list['available']}}</td>
            <td class="table-text table-th">
                <a data-toggle="modal" data-target="#imageModal{{$list['id']}}" style="cursor: pointer;">
                    @if($list['image']!="" && file_exists($list['image']))
                        <img src="{{ url($list->image) }}" width="30">
                    @else
                        <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                    @endif
                </a>
            </td>
            <td class="table-text table-th">
                @if($list['status'] == 'active')
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                        <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                        <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                @endif

                @if($list['status'] == 'in-active')
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                        <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                        <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                    </div>
                @endif
            </td>

        </tr>



        <!---------------------------------------------------IMAGE MODAL---------------------------------------------------------->
        <div id="imageModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['first_name'] . ' ' . $list['last_name']}}</h4>
                    </div>
                    @if($list['image']!="" && file_exists($list['image']))
                        <div class="modal-body" style="background-image: url({{ url($list->image) }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @else
                        <div class="modal-body" style="background-image: url({{ url('assets/dist/img/default-user.png') }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @endif
                </div>
            </div>
        </div>

    @endforeach

</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $employees])</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var emp_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/employees/assign')}}',
                type: "put",
                data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+emp_id).show();
                    $('#assign_add_'+emp_id).hide();
                }
            });
        });
        $('.unassign').click(function(){
            var emp_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/employees/unassign')}}',
                type: "put",
                data: {'id': emp_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+emp_id).hide();
                    $('#assign_add_'+emp_id).show();
                }
            });
        });
    });

</script>