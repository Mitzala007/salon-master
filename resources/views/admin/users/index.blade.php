@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/users') }}"><i class="fa fa-dashboard"></i> User</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-10">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <span style="float:left; padding-top: 5px" class="col-md-2 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search" onkeyup="search_filters(event)">
                            </span>



                            <!--------------------------------------------ROLE SEARCHING START---------------------------------------->

                            <span style="padding-top: 5px;" class="col-md-2 col-sm-2 col-xs-6">
                                <select name="role_search" class="form-control select2" onchange="user_search(this.value)">
                                    <option value="all">All</option>
                                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                                        <option value="super admin">Super Admin</option>
                                    @endif
                                    <option value="salon owner">Salon Owner</option>
                                    <option value="user">User</option>
                                    <option value="employee">Employee</option>
                                    @foreach($membership_plan as $key => $plan)
                                        <option value="{{$key}}">{{$plan}}</option>
                                    @endforeach
                                </select>
                            </span>
                            <!-----------------------------------------ROLE SEARCHING END---------------------------------------->

                            <span style="float:left; padding-top: 5px" class="col-md-1 col-sm-1 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>

                        </div>
                        <input type="hidden" id="user_role" value="{{\Illuminate\Support\Facades\Auth::user()->role}}">
                    </div>

                    <div class="col-md-2">
                        <h3 class="box-title" style="float:right;">
                            <a href="{{ url('admin/users/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>
                            <a href="{{ url('admin/users') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                        </h3>
                    </div>
                </div>
                <!-- /.box-header -->

                @include('admin.loader')

                <div class="box-body table-responsive" id="itemlist">
                    @include('admin.users.table')
                </div>
            </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>


<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();

        var search = $('#search').val();
        var role_search = $('#role_search').val();
        //var type = $('#type').val();

        $.ajax({

            url: '{{url('admin/users')}}',

            type: "get",

            data: {'role_search': role_search,'search':search,'_token' : $('meta[name=_token]').attr('content')},

            success: function(data){
                //alert(data);

                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });

    function user_search(type) {

        var user_search = type;
        var role = document.getElementById('user_role').value;

        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({

            url: '{{url('admin/users')}}',
            type: "get",
            data: {'search':user_search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                if(type == 'all')
                {
                    document.getElementById('all_btn').style.backgroundColor='#D8004E';
                    if(role == 'admin')
                    {
                        document.getElementById('super_btn').style.backgroundColor='#482162';
                    }
                    document.getElementById('sub_btn').style.backgroundColor='#482162';
                    document.getElementById('user_btn').style.backgroundColor='#482162';
                    document.getElementById('search').value = '';
                }
                if(role == 'admin')
                {
                    if(type == 'super admin')
                    {
                        document.getElementById('super_btn').style.backgroundColor='#D8004E';
                        document.getElementById('all_btn').style.backgroundColor='#482162';
                        document.getElementById('sub_btn').style.backgroundColor='#482162';
                        document.getElementById('user_btn').style.backgroundColor='#482162';
                        document.getElementById('emp_btn').style.backgroundColor='#482162';
                        document.getElementById('search').value = '';
                    }
                }

                if(type == 'salon owner')
                {
                    document.getElementById('sub_btn').style.backgroundColor='#D8004E';
                    document.getElementById('all_btn').style.backgroundColor='#482162';
                    if(role == 'admin')
                    {
                        document.getElementById('super_btn').style.backgroundColor='#482162';
                    }
                    document.getElementById('user_btn').style.backgroundColor='#482162';
                    document.getElementById('emp_btn').style.backgroundColor='#482162';
                    document.getElementById('search').value = '';
                }
                if(type == 'user')
                {
                    document.getElementById('user_btn').style.backgroundColor='#D8004E';
                    document.getElementById('emp_btn').style.backgroundColor='#482162';
                    document.getElementById('all_btn').style.backgroundColor='#482162';
                    document.getElementById('sub_btn').style.backgroundColor='#482162';
                    if(role == 'admin')
                    {
                        document.getElementById('super_btn').style.backgroundColor='#482162';
                    }
                    document.getElementById('search').value = '';
                }
                if(type == 'employee')
                {

                    document.getElementById('emp_btn').style.backgroundColor='#D8004E';
                    document.getElementById('user_btn').style.backgroundColor='#482162';
                    document.getElementById('all_btn').style.backgroundColor='#482162';
                    document.getElementById('sub_btn').style.backgroundColor='#482162';
                    if(role == 'admin')
                    {
                        document.getElementById('super_btn').style.backgroundColor='#482162';
                    }
                    document.getElementById('search').value = '';
                }


                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    }
</script>
