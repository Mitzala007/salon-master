<div id="load" style="position: relative; text-align: center;">
    <img style="position: absolute; left: 650px; top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />
</div>

<?php $plan = 0; ?>
@if(\Illuminate\Support\Facades\Auth::user()->membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->membership_plan; ?>
@elseif(\Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '3')
    <?php $plan = \Illuminate\Support\Facades\Auth::user()->admin_membership_plan?>
@endif

@if($plan=='1' || $plan=='2' || $plan=='3')
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th>User</th>
        @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
            <th>Salon</th>
        @endif
        <th>Date</th>
        <th class="table-text table-th">Status</th>
        <th class="table-text table-th">Action</th>
    </tr>
    </thead>
    <tbody id="sortable">

        @foreach ($booking as $list)
            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
                <td>{{$list['Users']['name']}}</td>
                @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                    <td>{{$list['saloon']['title']}}</td>
                @endif
                <td>{{$list['date']}}</td>
                <td style="padding-top: 16px;" class="table-text table-th">
                    <label class="label @if($list['status'] == 'in-progress') label-info @elseif($list['status'] == 'waiting') label-warning @elseif($list['status'] == 'cancel') label-danger @elseif($list['status'] == 'completed') label-success @endif" style="padding:5px 8px; font-size: 14px;">{{ucfirst($list['status'])}}</label>
                </td>
                <td class="table-text table-th">
                    <div class="btn-group-horizontal">
                        <span data-toggle="tooltip" title="View Details" data-trigger="hover">
                            <button class="btn btn-info res-btn tip" title="View salon" data-trigger="hover" type="submit" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-eye"></i></button>
                        </span>
                    </div>
                </td>
            </tr>
        @endforeach

</table>

@foreach ($booking as $list)
    <div id="myModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                    <h4 class="modal-title">{{$list['Users']['name']}}</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' )
                                <th width="50%">Salon Name</th>
                                <td width="50%">{{$list['Saloon']['title']}}</td>
                            @endif
                        </tr>

                        <tr>
                            <th>User</th>
                            <td>{{$list['Users']['name']}}</td>
                        </tr>
                        <tr>
                            <th>Date</th>
                            <td>{{ $list['date'] }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td style="padding-top: 11px;">
                                <label class="label @if($list['status'] == 'in-progress') label-info @elseif($list['status'] == 'waiting') label-warning @elseif($list['status'] == 'cancel') label-danger @elseif($list['status'] == 'completed') label-success @endif" style="padding:5px 8px; font-size: 14px;">{{ucfirst($list['status'])}}</label>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <th width="25%">Service</th>
                            <th width="25%">Duration</th>
                            <th width="25%">Discount Type</th>
                            <th width="25%">Discount Value</th>
                        </tr>

                        @foreach ($list['Booking_details'] as $details)
                            <tr>
                                <td>{{ $details->service_type['title'] }}</td>
                                <td>{{ $details->duration }}</td>
                                <td>{{ $details->discount_type }}</td>
                                <td>{{ $details->discount_value }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endforeach

<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $booking])</div>
@endif
