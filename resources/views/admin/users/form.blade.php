{!! Form::hidden('redirects_to', URL::previous()) !!}
{{--<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="type_id">User Role <span class="text-red">*</span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')--}}
            {{--{!! Form::select('role', \App\User::$user_role , null, ['id'=>'role', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@else--}}
            {{--{!! Form::select('role', \App\User::$user_role_1 , null, ['id'=>'role', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@endif--}}
        {{--@if ($errors->has('role'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('role') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}


<div class="form-group{{ $errors->has('membership_plan') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="membership_plan">Membership Plan<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-sm-5">
        {!! Form::select('membership_plan', [''=>'Please Select','admin'=>'Super Admin']+$membership_plan , null, ['id'=>'membership_plan', 'class' => 'select2 form-control', 'style' => 'width: 100%','disabled'=>'disabled']) !!}
        @if ($errors->has('membership_plan'))
            <span class="help-block">
                <strong>{{ $errors->first('membership_plan') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(\Illuminate\Support\Facades\Auth::user()->id == 1)
    <div class="form-group{{ $errors->has('admin_membership_plan') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label" for="admin_membership_plan">Admin Membership Plan<span class="text-red">&nbsp; &nbsp; </span></label>
        <div class="col-sm-1 label-check">
            <div class="checkbox">
                <input type="checkbox" @if(isset($user) && ($user->role=="admin" || $user->admin_membership_plan!=0)) checked @endif name="admin_membership_plan" id="admin_membership_plan" onclick="check_admin_plan()">
                {{--            {!! Form::checkbox('admin_membership_plan', null, null,['class' => '', 'id'=>'admin_membership_plan', 'onclick'=>'check_admin_plan()'])!!}--}}
                <label for="admin_membership_plan"></label>
            </div>
            @if ($errors->has('admin_membership_plan'))
                <span class="help-block">
                <strong>{{ $errors->first('admin_membership_plan') }}</strong>
            </span>
            @endif
        </div>
        <div class="col-sm-4" id="admin_plan_div" style="@if(isset($user) && ($user->role=="admin" || $user->admin_membership_plan != 0))display: block; @else display: none; @endif">
            <select class="select2 form-control" name="admin_plan" id="admin_plan" style="width: 100%">
                <option value="">Please Select</option>
                <option value="admin" @if(isset($user) && $user->role=="admin") selected="selected" @endif>Super Admin</option>
                @foreach($membership_plan as $k=>$v)
                    <option value="{{ $k }}" @if(isset($user) && $user->admin_membership_plan == $k) selected @endif >{{$v}}</option>
                @endforeach
            </select>
            {{--        {!! Form::select('admin_plan', [''=>'Please Select','admin'=>'Super Admin']+$membership_plan , isset($user->admin_membership_plan)?$user->admin_membership_plan:null, ['id'=>'admin_plan', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        </div>
    </div>
@endif

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="name_lb">First Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter First Name','id'=>'name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <strong><span id="name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="last_name" id="last_name_lb">Last Name &nbsp; &nbsp;<span class="text-red"> </span></label>
    <div class="col-sm-5">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Last Name','id'=>'last_name']) !!}
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
        <strong><span id="last_name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('nick_name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="nick_name" id="nick_name_lb">Nick Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('nick_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Nick Name','id'=>'nick_name']) !!}
        @if ($errors->has('nick_name'))
            <span class="help-block">
                <strong>{{ $errors->first('nick_name') }}</strong>
            </span>
        @endif
        <strong><span id="nick_name_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>


<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="email" id="email_lb">Email <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address','id'=>'email','style'=>'text-transform:lowercase']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <strong><span id="email_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="password" id="password_lb">Password <span class="text-red">*</span></label>
    <div class="col-sm-5">
        <input type="password" placeholder="Enter Password" autocomplete="off"  id="password" maxlength="10" name="password" class="form-control" >


        @if(isset($user->password) && $user->password !="")
           <input type="hidden" id="pass_val" value="{{$user->password}}">

        @else
            @if(isset($user->is_facebook) && $user->is_facebook == 1)
                <input type="hidden" id="pass_val" value="1">
            @else
                <input type="hidden" id="pass_val" value="">
            @endif

        @endif



        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <strong><span id="password_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="phone_lb">Phone <span class="text-red">*</span></label>

    <div class="col-md-2 col-sm-5 phone-code">
        @if(isset($user) && $user!="")
            <?php
            $fcode = substr($user['phone'],0, 1);
            if ($fcode==1){
                $code = 1;
            }
            else{
                $code = substr($user['phone'],0, 2);
            }
            ?>
        @endif
        <select name="countries" id="countries" class="select2 form-control">
            <option value='1' @if(isset($code) && $code == 1) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag us" data-title="United State">United State</option>
            <option value='91' @if(isset($code) && $code == 91) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag in" data-title="India">India</option>
            <option value='61' @if(isset($code) && $code == 61) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag au" data-title="Australia">Australia</option>
        </select>
    </div>

    <div class="col-md-3 col-sm-5">
        @if(isset($user) && $user!="")
            <?php
            $fcode = substr($user['phone'],0, 1);
            if ($fcode==1){
                $phone = substr($user['phone'],1);
            }
            else{
                $phone = substr($user['phone'],2);
            }
            ?>
            {!! Form::text('phone', $phone, ['class' => 'form-control', 'placeholder' => 'Enter Phone','id'=>'phone']) !!}
        @else
            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone','id'=>'phone']) !!}
        @endif

        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
        <strong><span id="phone_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Gender <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('gender', \App\User::$user_gender, !empty($gender)?$gender:null, ['id'=>'gender', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="end_date">Date of Birth &nbsp; &nbsp;<span class="text-red"></span></label>
    <div class="col-sm-5">
        {!! Form::text('birthdate', null, ['class' => 'form-control', 'id'=>'bdate', 'placeholder' => 'Date of Birth']) !!}
        @if ($errors->has('birthdate'))
            <span class="help-block">
                <strong>{{ $errors->first('birthdate') }}</strong>
            </span>
        @endif
    </div>
</div>


{{--<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="title" id="country_lb">Country <span class="text-red">*</span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::select('country', [''=>'Please select'] + $countries, !empty($country)?$country:null, ['id'=>'country', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@if ($errors->has('country'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('country') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
        {{--<strong><span id="country_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="title" id="state_id_lb">State <span class="text-red">*</span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::select('state_id', $states, !empty($state_id)?$state_id:null, ['id'=>'state_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@if ($errors->has('state_id'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('state_id') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
        {{--<strong><span id="state_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="title" id="city_id_lb">City <span class="text-red">*</span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::select('city_id', $cities, !empty($city_id)?$city_id:null, ['id'=>'city_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}--}}
        {{--@if ($errors->has('city_id'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('city_id') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
        {{--<strong><span id="city_id_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('landmark') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="title">Landmark<span class="text-red">&nbsp; &nbsp; </span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--{!! Form::text('landmark', null, ['class' => 'form-control', 'placeholder' => 'Enter Landmark']) !!}--}}
        {{--@if ($errors->has('landmark'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('landmark') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group{{ $errors->has('addressline_1') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="addressline_1_lb">Address Line 1 <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('addressline_1', null, ['class' => 'form-control', 'placeholder' => 'Address Line 1','id'=>'addressline_1']) !!}
        @if ($errors->has('addressline_1'))
            <span class="help-block">
                <strong>{{ $errors->first('addressline_1') }}</strong>
            </span>
        @endif
        <strong><span id="addressline_1_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('addressline_2') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Address Line 2<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-sm-5">
        {!! Form::text('addressline_2', null, ['class' => 'form-control', 'placeholder' => 'Address Line 2','id'=>'addressline_2']) !!}
        @if ($errors->has('addressline_2'))
            <span class="help-block">
                <strong>{{ $errors->first('addressline_2') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="address1"> <span class="text-red">&nbsp;&nbsp;</span></label>
    <div class="col-md-5 col-sm-6">
        <p>Drag Marker to your Location.</p>
        <div id="map" style="min-height:300px;" >
        </div>
    </div>
</div>

<div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="timezone">Timezone</label>
    <div class="col-md-5 col-sm-6">
        {!! Form::text('timezone', null, ['class' => 'form-control', 'id' => 'timezone', 'placeholder' => 'Timezone','readonly']) !!}
    </div>
</div>


<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title" id="zip_code_lb">Zip Code <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('zip_code', null, ['class' => 'form-control', 'placeholder' => 'Zip Code','id'=>'zip_code']) !!}
        @if ($errors->has('zip_code'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
        <strong><span id="zip_code_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="image">Profile Picture<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-sm-5">
        <div class="">
            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}
        </div>
        <?php
        if (!empty($user->image) && $user->image != "") {
        ?>
        <br><img id="DisplayImage" src="{{ url($user->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >
        <?php
        }else{
            echo '<img id="DisplayImage" src="" width="150" style="display: none;"/>';
        } ?>

        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('is_email_verified') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="is_email_verified">Verify Email<span class="text-red">&nbsp; &nbsp; </span></label>
    <div class="col-sm-5">
        <label>
            {!! Form::checkbox('is_email_verified', null, null,['class' => 'flat-red'])!!}
        </label>
        @if ($errors->has('is_email_verified'))
            <span class="help-block">
                <strong>{{ $errors->first('is_email_verified') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--@if(\Illuminate\Support\Facades\Auth::user()->id == 1)--}}
{{--<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">--}}
    {{--<label class="col-sm-2 control-label" for="role">Is Admin<span class="text-red">&nbsp; &nbsp; </span></label>--}}
    {{--<div class="col-sm-5">--}}
        {{--<label>--}}
            {{--@if(isset($user) && $user->role == 'admin')--}}
                {{--<input type="checkbox" name="role" class="flat-red" id="role" checked>--}}
            {{--@else--}}
                {{--<input type="checkbox" name="role" class="flat-red" id="role">--}}
            {{--@endif--}}
        {{--</label>--}}
        {{--@if ($errors->has('role'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('role') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endif--}}

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="status" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-sm-5">
        @foreach (\App\User::$status as $key => $value)
           <label>
               @if($key == 'active')
                   {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
               @else
                   {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
               @endif
           </label>
       @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
             <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
          <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>

<input type="hidden" name="latitude" id="latitude" value="<?php
if (!empty($user) && $user['latitude'] != "") {
    echo $user["latitude"];
}
?>">
<input type="hidden" name="longitude" id="longitude" value="<?php
if (!empty($user) && $user['longitude'] != "") {
    echo $user["longitude"];
}
?>">


<script type="text/javascript">
    var map;
    var markersArray = [];
            <?php
            if (!empty($user) && !empty($user["latitude"]) && !empty($user["longitude"])) {
                echo 'var myLatLng = {lat: ' . $user["latitude"] . ', lng: ' . $user["longitude"] . '};';
            } else {
                echo 'var myLatLng = {lat: 37.9642529, lng: -91.8318334};';
            }
            ?>
    var latitude;
    var longitude;
    var myLatLng;
    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 6
        });

        var marker = new google.maps.Marker({
            draggable: true,
            position: myLatLng,
            map: map,
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('addressline_1'));

        google.maps.event.addListener(searchBox, 'places_changed', function() {
            searchBox.set('map', null);

            var places = searchBox.getPlaces();

            var bounds = new google.maps.LatLngBounds();
            var i, place;
            if(places[0] !== undefined) {

                place = places[0];

                $("#latitude").val(place.geometry.location.lat());
                $("#longitude").val(place.geometry.location.lng());

                marker.setPosition( place.geometry.location );
                map.panTo( place.geometry.location );
                $('#addressline_1').val(place.formatted_address);

            }

            var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {
                        $('#addressline_1').val(results[0].formatted_address);

                        url = "https://maps.googleapis.com/maps/api/timezone/json?location="+place.geometry.location.lat()+","+place.geometry.location.lng()+"&timestamp="+(Math.round((new Date().getTime())/1000)).toString()+"&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";

                        $.ajax({
                                    url:url,
                                })
                                .done(function(response){
                                    if(response.status == 'OK'){
                                        $('#timezone').val(response.timeZoneId);
                                    }
                                });

                    }
                }
            });

            searchBox.set('map', map);
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            $("#latitude").val(event.latLng.lat());
            $("#longitude").val(event.latLng.lng());

            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#addressline_1').val(results[0].formatted_address);

                        url = "https://maps.googleapis.com/maps/api/timezone/json?location="+event.latLng.lat()+","+event.latLng.lng()+"&timestamp="+(Math.round((new Date().getTime())/1000)).toString()+"&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";

                        $.ajax({
                                    url:url,
                                })
                                .done(function(response){
                                    if(response.status == 'OK'){
                                        $('#timezone').val(response.timeZoneId);
                                    }
                                });

                    }
                }
            });
        });


    }

    function clearMarkers() {
        setMapOnAll(null);
    }


    function placeMarker(location) {
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });

        markersArray.push(marker);
    }

    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap&libraries=places" async defer></script>

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script type="text/javascript">

   $("#image").fileinput({
        showUpload: false,
        showCaption: false,

        showPreview: false,
        showRemove: false,
        browseClass: "btn btn-primary btn-lg btn_new",
    });
    function AjaxUploadImage(obj,id){

        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }
   function imageIsLoaded(e) {
        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').css("margin-top", "2.5%");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');

   };
</script>

<script type="text/javascript">
    function validation()
    {
        var flag = 0;

        if(document.getElementById("name").value.split(" ").join("") == "")
        {
            document.getElementById('name_msg').innerHTML = 'The First name field is required.';
            document.getElementById("name").focus();
            document.getElementById("name").style.borderColor ='#dd4b39';
            document.getElementById("name_lb").style.color ='#dd4b39';
            document.getElementById('name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("name").style.borderColor ='#d2d6de';
            document.getElementById("name_lb").style.color ='#333';
            document.getElementById('name_msg').style.display = 'none';
        }

        if(document.getElementById("nick_name").value.split(" ").join("") == "")
        {
            document.getElementById('nick_name_msg').innerHTML = 'The Nick name field is required.';
            document.getElementById("nick_name").focus();
            document.getElementById("nick_name").style.borderColor ='#dd4b39';
            document.getElementById("nick_name_lb").style.color ='#dd4b39';
            document.getElementById('nick_name_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("nick_name").style.borderColor ='#d2d6de';
            document.getElementById("nick_name_lb").style.color ='#333';
            document.getElementById('nick_name_msg').style.display = 'none';
        }

        if(document.getElementById("email").value.split(" ").join("") == "")
        {
            document.getElementById('email_msg').innerHTML = 'The email field is required.';
            document.getElementById("email").focus();
            document.getElementById("email").style.borderColor ='#dd4b39';
            document.getElementById("email_lb").style.color ='#dd4b39';
            document.getElementById('email_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var email1 = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            if (!document.getElementById("email").value.match(email1)) {
                document.getElementById('email_msg').innerHTML = 'The email must be a valid email address.';
                document.getElementById("email").style.borderColor ='#dd4b39';
                document.getElementById("email_lb").style.color ='#dd4b39';
                document.getElementById('email_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("email").style.borderColor ='#d2d6de';
                document.getElementById("email_lb").style.color ='#333';
                document.getElementById('email_msg').style.display = 'none';
            }
        }

        if(document.getElementById("pass_val").value.split(" ").join("") == "")
        {
            if(document.getElementById("password").value.split(" ").join("") == "")
            {
                document.getElementById('password_msg').innerHTML = 'The password field is required.';
                document.getElementById("password").focus();
                document.getElementById("password").style.borderColor ='#dd4b39';
                document.getElementById("password_lb").style.color ='#dd4b39';
                document.getElementById('password_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("password").style.borderColor ='#d2d6de';
                document.getElementById("password_lb").style.color ='#333';
                document.getElementById('password_msg').style.display = 'none';
            }

        }

        if(document.getElementById("phone").value.split(" ").join("") == "")
        {
            document.getElementById('phone_msg').innerHTML = 'The phone field is required.';
            document.getElementById("phone").focus();
            document.getElementById("phone").style.borderColor ='#dd4b39';
            document.getElementById("phone_lb").style.color ='#dd4b39';
            document.getElementById('phone_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var phone1=document.getElementById("phone").value;
            if(isNaN(phone1))
            {
                document.getElementById('phone_msg').innerHTML = 'Please enter a valid mobile number.';
                document.getElementById("phone").focus();
                document.getElementById("phone").style.borderColor ='#dd4b39';
                document.getElementById("phone_lb").style.color ='#dd4b39';
                document.getElementById('phone_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("phone").style.borderColor ='#d2d6de';
                document.getElementById("phone_lb").style.color ='#333';
                document.getElementById('phone_msg').style.display = 'none';
            }
        }

        if(document.getElementById("country").value.split(" ").join("") == "")
        {
            document.getElementById('country_msg').innerHTML = 'The country field is required.';
            document.getElementById("country").focus();
            document.getElementById("country").style.borderColor ='#dd4b39';
            document.getElementById("country_lb").style.color ='#dd4b39';
            document.getElementById('country_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("country").style.borderColor ='#d2d6de';
            document.getElementById("country_lb").style.color ='#333';
            document.getElementById('country_msg').style.display = 'none';
        }

        if(document.getElementById("state_id").value.split(" ").join("") == "")
        {
            document.getElementById('state_id_msg').innerHTML = 'The state field is required.';
            document.getElementById("state_id").focus();
            document.getElementById("state_id").style.borderColor ='#dd4b39';
            document.getElementById("state_id_lb").style.color ='#dd4b39';
            document.getElementById('state_id_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("state_id").style.borderColor ='#d2d6de';
            document.getElementById("state_id_lb").style.color ='#333';
            document.getElementById('state_id_msg').style.display = 'none';
        }

        if(document.getElementById("city_id").value.split(" ").join("") == "")
        {
            document.getElementById('city_id_msg').innerHTML = 'The city field is required.';
            document.getElementById("city_id").focus();
            document.getElementById("city_id").style.borderColor ='#dd4b39';
            document.getElementById("city_id_lb").style.color ='#dd4b39';
            document.getElementById('city_id_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("city_id").style.borderColor ='#d2d6de';
            document.getElementById("city_id_lb").style.color ='#333';
            document.getElementById('city_id_msg').style.display = 'none';
        }

        if(document.getElementById("addressline_1").value.split(" ").join("") == "")
        {
            document.getElementById('addressline_1_msg').innerHTML = 'The city field is required.';
            document.getElementById("addressline_1").focus();
            document.getElementById("addressline_1").style.borderColor ='#dd4b39';
            document.getElementById("addressline_1_lb").style.color ='#dd4b39';
            document.getElementById('addressline_1_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("addressline_1").style.borderColor ='#d2d6de';
            document.getElementById("addressline_1_lb").style.color ='#333';
            document.getElementById('addressline_1_msg').style.display = 'none';
        }

        if(document.getElementById("zip_code").value.split(" ").join("") == "")
        {
            document.getElementById('zip_code_msg').innerHTML = 'The zip code field is required.';
            document.getElementById("zip_code").focus();
            document.getElementById("zip_code").style.borderColor ='#dd4b39';
            document.getElementById("zip_code_lb").style.color ='#dd4b39';
            document.getElementById('zip_code_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("zip_code").style.borderColor ='#d2d6de';
            document.getElementById("zip_code_lb").style.color ='#333';
            document.getElementById('zip_code_msg').style.display = 'none';
        }

        var active = 'active';
        var inactive = 'in-active';
        if(document.getElementById("status_"+active).checked == false && document.getElementById("status_"+inactive).checked == false)
        {
            document.getElementById('status_msg').innerHTML = 'The status field is required.';
            document.getElementById("status_val_"+active).style.color ='#dd4b39';
            document.getElementById("status_val_"+inactive).style.color ='#dd4b39';
            document.getElementById("status_lb").style.color ='#dd4b39';
            document.getElementById('status_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("status_val_"+active).style.color ='#333';
            document.getElementById("status_val_"+inactive).style.color ='#333';
            document.getElementById("status_lb").style.color ='#333';
            document.getElementById('status_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>

<script>
   function check_admin_plan(){
       var check = document.getElementById('admin_membership_plan').checked;
       if(check == true)
       {
           document.getElementById('admin_plan_div').style.display='block';
       }else{
           document.getElementById('admin_plan_div').style.display='none';
       }
   }
</script>

@section('jquery')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#country").change(function() {
                if ($(this).val() != '') {
                    $.ajax({
                        url: '{{ url('admin/users/get_states') }}/' + $(this).val(),
                        error: function () {
                        },
                        success: function (result) {
                            //alert(result);
                            $("#state_id").select2().empty();
                            $("#state_id").html(result);
                            $('#state_id').select2()
                        }
                    });
                } else {

                    $("#state_id").empty();
                    $("#state_id").html('');
                    $('#state_id').select2()
                }
            });
            $("#state_id").change(function() {
                if ($(this).val() != '') {
                    var cc = $('#country').val();
                    $.ajax({
                        url: '{{ url('admin/users/get_cities') }}/'+cc+'/'+$(this).val(),
                        error: function () {
                        },
                        success: function (result) {
                            //alert(result);
                            $("#city_id").select2().empty();
                            $("#city_id").html(result);
                            $('#city_id').select2()
                        }
                    });
                } else {

                    $("#city_id").empty();
                    $("#city_id").html('');
                    $('#city_id').select2()
                }
            });
        });
    </script>
@endsection

