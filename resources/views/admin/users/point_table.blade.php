<div id="load" style="position: relative; text-align: center;"></div>
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th>Salon Name</th>
        <th>Booking Id</th>
        <th>Type</th>
        <th>Point Before</th>
        <th>Point</th>
        <th>Point After</th>
        <th>Remark</th>
        <th>Created Date</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($point as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td>{{$list['Salon']['title']}}</td>
            <td>{{$list['Booking']['booking_name']}}</td>
            <td>{{$list['type']}}</td>
            <td>{{$list['point_before']}}</td>
            <td>{{$list['point']}}</td>
            <td>{{$list['point_after']}}</td>
            <td>{{$list['remark']}}</td>
            <td>{{$list['created_at']}}</td>
        </tr>
    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $point])</div>

<script type="text/javascript">
    function destroy_user(id)
    {
        $.ajax({
            url:'users/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'users';
                window.location.href = new_url;
            }
        });
    }
</script>



<!-- LIGHTBOX JS -->
