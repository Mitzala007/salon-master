@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('admin/my_membership') }}"><i class="fa fa-dashboard"></i>Membership</a></li>
            </ol>
        </section>

        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                            {{--@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role == 'super_admin')--}}
                            {{--<a href="{{ url('admin/membership/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>--}}
                            {{--@elseif( ! count($membership) && \Illuminate\Support\Facades\Auth::user()->role == 'sub_admin')--}}
                            {{--<a href="{{ url('admin/membership/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>--}}
                            {{--@endif--}}
                        </h3>
                    </div>
                </div>

                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        @foreach($membership as $item)
                            <tr>
                                <td>{{$item->title}}</td>
                                <td>{{$item->amount}}</td>
                                <td><button class="btn btn-success"  style="height:28px; padding:0 12px"><span>Active</span></button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </section>
    </div>

@endsection
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>

<script>Ladda.bind( 'input[type=submit]' );</script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })
    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        $.ajax({
            url : page
        }).done(function (data) {

            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });

        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        e.preventDefault();
        var search = $('#search').val();
        var type = $('#type').val();
        $.ajax({
            url: '{{url('admin/membership')}}',
            type: "get",
            data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
                //alert(data);
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>


