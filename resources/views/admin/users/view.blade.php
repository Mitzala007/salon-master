@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>View</small>
            </h1>
            <ol class="breadcrumb">
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                <li><a href="{{url('admin/user/')}}">{{ $menu }}</a></li>
                <li class="active">View User</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-----------------------------------USER DETAILS----------------------------------->
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">User Details</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">

                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Id</th>
                                            <td>{{$user->id}}</td>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <td>{{$user->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <th>Gender</th>
                                            <td>{{$user->gender}}</td>
                                        </tr>
                                        <tr>
                                            <th>Addressline 1</th>
                                            <td>{{$user->addressline_1}}</td>
                                        </tr>
                                        <tr>
                                            <th>Addressline 2</th>
                                            <td>{{$user->addressline_2}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mobile No.</th>
                                            <td>{{$user->phone}}</td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td>{{$user->address}}</td>
                                        </tr>
                                        <tr>
                                            <th>Role</th>
                                            <td><?php if($user['role']=="sub_admin") { echo "Salon Owner"; } else { echo "User";}  ?></td>
                                        </tr>
                                        <tr>
                                            <th>Image</th>
                                            <td>
                                                @if($user->image!="" && file_exists($user->image))
                                                    <img src="{{ url($user->image) }}" width="50">
                                                @else
                                                    <img src="{{ url('assets/dist/img/default-user.png') }}" width="50">
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Facebook Login</th>
                                            <td>{{ $user->is_facebook }}</td>
                                        </tr>
                                        @if(!empty($saloon_detail))
                                            <tr>
                                                <th>Employee In Salon</th>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            @if($saloon_detail->image!="" && file_exists($saloon_detail->image))
                                                                <img src="{{ url($saloon_detail->image) }}" width="50">
                                                            @else
                                                                <img src="{{ url('assets/dist/img/default-user.png') }}" width="50">
                                                            @endif
                                                        </div>
                                                        <div class="col-md-5" style="padding-top: 2.5%;">
                                                            {{ $saloon_detail->title}}
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th>Created At</th>
                                            <td>{{ $user->created_at}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php $plan = 0; ?>
            @if(\Illuminate\Support\Facades\Auth::user()->membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->membership_plan == '3')
                <?php $plan = \Illuminate\Support\Facades\Auth::user()->membership_plan; ?>
            @elseif(\Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '1' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '2' || \Illuminate\Support\Facades\Auth::user()->admin_membership_plan == '3')
                <?php $plan = \Illuminate\Support\Facades\Auth::user()->admin_membership_plan?>
            @endif

            @if($plan == 1 || $plan==2 || $plan==3)
                    <!-----------------------------------SALOON DETAILS----------------------------------->
                    <div class="col-md-6">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Salon Details</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            @if(!empty($saloon))
                                <div class="box-body">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="@if($service =="saloon_management") active @endif"><a href="#saloonManagement" data-toggle="tab" aria-expanded="true">Salon Detail</a></li>
                                            @if (isset($saloon->id))
                                                <li class="@if(isset($service) && $service =="subadmin_service") active @endif"><a href="#servicesManagement" data-toggle="tab" aria-expanded="false">Service Details</a></li>
                                                <li><a href="#workDays" data-toggle="tab" aria-expanded="false">Working Days</a></li>
                                            @endif
                                            <span class="pull-right">
                                                @if(!empty($saloon))
                                                    {{ Form::open(array('url' => 'admin/saloon/'.$saloon['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                                                    <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Details" data-trigger="hover" type="submit">Edit Details</button>
                                                    {{ Form::close() }}
                                                @endif
                                            </span>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane @if($service =="saloon_management") active @endif" id="saloonManagement">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="box box-info">
                                                            <div class="box-body">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th>Salon Name</th>
                                                                        <td>{{$saloon->title}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Location</th>
                                                                        <td>{{$saloon->location}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Description</th>
                                                                        <td>{!! $saloon->description !!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Status</th>
                                                                        <td style="padding-top: 1.5%;">{!!  $saloon['status']=='active'? '<span class="label label-success">Active</span>' : '<span class="label label-danger">In-active</span>' !!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Created At</th>
                                                                        <td>{{$saloon->created_at}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Salon Logo</th>
                                                                        <td>
                                                                            @if($saloon->image!="" && file_exists($saloon->image))
                                                                                <img src="{{ url($saloon->image) }}" width="150">
                                                                            @else
                                                                                <img src="{{ url('assets/dist/img/default-user.png') }}" width="150">
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @if (isset($saloon->id))
                                                <div class="tab-pane @if(isset($service) && $service =="subadmin_service") active @endif" id="servicesManagement">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="box box-info">
                                                                <div class="box-body">
                                                                    <div class="alert {{ $saloon->global_discount == 1 ? 'alert-success' : 'alert-danger' }}">
                                                                        Global Discount : {{ $saloon->global_discount == 1 ? 'Yes' : 'No' }}
                                                                    </div>
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Service Type</th>
                                                                            <th>Charges (USD)</th>
                                                                            <th>Duration (Minutes)</th>
                                                                            <th>Avail Discount</th>
                                                                            <th>Discount (%)</th>
                                                                        </tr>
                                                                        @foreach ($type as $key => $value)
                                                                            @if(isset($services[$key]))
                                                                                <tr>
                                                                                    <td>
                                                                                        {{ $value }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $services[$key]['charges'] }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $services[$key]['duration'] }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $services[$key]['avail_discount'] }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ $services[$key]['discount'] }}
                                                                                    </td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if (isset($saloon->id))
                                                <div class="tab-pane" id="workDays">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="box box-info">
                                                                <div class="box-body">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Working Days</th>
                                                                            <th>Start Hour</th>
                                                                            <th>End Hour</th>
                                                                        </tr>
                                                                        @foreach ($work_days as $day)
                                                                            <tr>
                                                                                <td>{{ ucfirst($day['work_day']) }}</td>
                                                                                <td>{{ $day['start_hour'] }}</td>
                                                                                <td>{{ $day['end_hour'] }}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th style="color: #D8004E; font-size: 20px;">No salon is assign to this user !</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <!-----------------------------------BOOKING DETAILS----------------------------------->
                    @if(($plan == 1 || $plan==2 || $plan==3) && $booking->count()>0)
                        <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Booking Details</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->


                                <div class="box-body table-responsive">
                                    <div class="col-md-5">
                                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                                                <input type="hidden" value="{{$id}}" id="booking_search">
                                            </span>
                                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                                <input type="submit" class="btn btn-info pull-right" id="booking_searchbtn" name="submit" value="Search">
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body table-responsive" id="booking_items">
                                    @include('admin.users.booking_table')
                                </div>
                            </div>
                        </div>
                    @endif

                    <!-----------------------------------EMPLOYEE DETAILS----------------------------------->
                    @if(($plan == 1 || $plan==2 || $plan==3) && $employees->count()>0)
                        <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Employee Details</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->

                                <div class="box-body table-responsive">
                                    <div class="col-md-5">
                                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                                <input  class="form-control" type="text" @if(!empty($search_employee)) value="{{$search_employee}}" @else placeholder="Search" @endif name="search_employee" id="search_employee">
                                                <input type="hidden" value="{{$id}}" id="employee_search">
                                            </span>
                                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                                <input type="submit" class="btn btn-info pull-right" id="employee_searchbtn" name="submit" value="Search">
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body table-responsive" id="employee_table">
                                    @include('admin.users.employee_table')
                                </div>
                            </div>
                        </div>
                    @endif

                @endif
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection


<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>

<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        $('#load').append('<img style="position: absolute; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        $('#load_employee').append('<img style="position: absolute; width:80px; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        $.ajax({
            url : page
        }).done(function (data) {
            $("#booking_items").empty().html(data);
            $("#employee_table").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });

            $('#example23').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    /*BOOKING SEARCHING*/

    $(document).on('click','#booking_searchbtn',function(e){
        e.preventDefault();

        var search = $('#search').val();
        var type = $('#booking_search').val();
        $('#load').append('<img style="position: absolute;  left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/users/booking')}}'+'/'+type,
            type: "get",
            data: {'search':search,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
//                alert(data);
               $("#booking_items").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });

    /*EMPLOYEE SEARCHING*/

    $(document).on('click','#employee_searchbtn',function(e){
        e.preventDefault();

        var search_employee = $('#search_employee').val();
        var type_employee = $('#employee_search').val();
        $('#load_employee').append('<img style="position: absolute; left: 650px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />');
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        $.ajax({
            url: '{{url('admin/users/employee')}}'+'/'+type_employee,
            type: "get",
            data: {'search_employee':search_employee,'_token' : $('meta[name=_token]').attr('content')},
            success: function(data){
//                alert(data);
                $("#employee_table").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                $('#example23').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });
            }
        });
    });
</script>
