
<style>
    .dropdown-menu>li>a:hover {
        background-color: #e1e3e9;
        color: #333;
    }
</style>
<table class="table table-bordered table-striped" id="example2">
    <thead>
    <tr>
        <th class="table-text table-th">Edit</th>
        {{--<th>Id</th>--}}
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th class="table-text table-th">User Type</th>
        <!--<th>Role</th>-->
        <th class="table-text table-th">Image</th>
        <th class="table-text table-th">Status</th>
        <th class="table-text table-th">Action</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @foreach ($user as $list)
        <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
            <td class="table-text">
                <div class="btn-group-horizontal">
                    {{ Form::open(array('url' => 'admin/users/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                    <button class="btn btn-info tip" data-toggle="tooltip" title="Edit User" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                    {{ Form::close() }}
                </div>
            </td>
            {{--<td>{{ $list['id'] }}</td>--}}
            <td>{{$list['name']}} {{ !empty($list['last_name'])?$list['last_name']:""}} </td>
            <td>{{$list['email']}}</td>
            <td>{{$list['phone']}}</td>
            <td style="padding-top: 16px;" class="table-text table-th">
                <?php $plan = 0; ?>
                @if($list['membership_plan'] == '1' || $list['membership_plan'] == '2' || $list['membership_plan'] == '3')
                    <?php $plan = $list['membership_plan']; ?>
                @elseif($list['admin_membership_plan']== '1' || $list['admin_membership_plan']=='2' || $list['admin_membership_plan']=='3')
                    <?php $plan = $list['admin_membership_plan'];?>
                @endif

                @if($plan==1 || $plan==3)
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        Salon Owner
                    </label>
                @elseif($plan==2)
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        Freelancer
                    </label>
                @else
                    <label class="label label-success" style="padding:5px 8px; font-size: 14px;">
                        User
                    </label>
                @endif
            </td>

            <td class="table-text">
                <a data-toggle="modal" data-target="#imageModal{{$list['id']}}" style="cursor: pointer;">
                @if($list['image']!="" && file_exists($list['image']))
                    <img src="{{ url($list->image) }}" width="30">
                @else
                    <img src="{{ url('assets/dist/img/default-user.png') }}" width="30">
                @endif
                </a>
            </td>
            <td class="table-text">
                @if($list['status'] == 'active')
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                        <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                        <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                @endif
                @if($list['status'] == 'in-active')
                    <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                        <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                    </div>
                    <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                        <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                    </div>
                @endif
            </td>

            <td class="table-text">
                <div class="btn-group-horizontal">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger">Action</button>
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" style="height: 34px;">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="margin-left: -30px;">
                            <li>
                                <a href="{{url('admin/point_history/'.$list['id'])}}">
                                    <i class="fa fa-money"></i>View Points
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/users', $list['id'])}}">
                                    <i class="fa fa-eye"></i>View Users
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#myModal{{$list['id']}}">
                                    <i class="fa fa-trash"></i>Delete Users
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                {{--<div class="btn-group-horizontal">--}}
                    {{--<a href="{{url('admin/point_history/'.$list['id'])}}">--}}
                        {{--<button class="btn btn-danger res-btn" data-toggle="tooltip" title="View Points" data-trigger="hover" type="submit" ><i class="fa fa-money"></i></button>--}}
                    {{--</a>--}}

                    {{--{!! Form::open(array('url' => array('admin/users', $list['id']), 'method' => 'get','style'=>'display:inline'))!!}--}}
                    {{--<button class="btn btn-info res-btn" data-toggle="tooltip" title="View User" data-trigger="hover" type="submit" ><i class="fa fa-eye"></i></button>--}}
                    {{--{!! Form::close()!!}--}}

                    {{--<span data-toggle="tooltip" title="Delete User" data-trigger="hover">--}}
                        {{--<button class="btn btn-danger res-btn" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>--}}
                    {{--</span>--}}
                {{--</div>--}}
            </td>
        </tr>

        <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
{{--            {{ Form::open(array('url' => 'admin/users/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}--}}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this User ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline" onclick="destroy_user({{$list['id']}})">Delete</button>
                    </div>
                </div>
            </div>
{{--            {{ Form::close() }}--}}
        </div>

        <!---------------------------------------------------IMAGE MODAL---------------------------------------------------------->
        <div id="imageModal{{$list['id']}}" class="fade modal modal-primary" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times" style="color: #ffffff;"></i></span></button>
                        <h4 class="modal-title">{{$list['name']}}</h4>
                    </div>
                    @if($list['image']!="" && file_exists($list['image']))
                        <div class="modal-body" style="background-image: url({{ url($list->image) }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @else
                        <div class="modal-body" style="background-image: url({{ url('assets/dist/img/default-user.png') }}); background-position: center; background-repeat: no-repeat; background-size: contain; height: 300px;">
                        </div>
                    @endif
                </div>
            </div>
        </div>


    @endforeach
</table>
<div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $user])</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var user_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/users/assign')}}',
                type: "put",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).show();
                    $('#assign_add_'+user_id).hide();
                }
            });
        });

        $('.unassign').click(function(){
            var user_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url('admin/users/unassign')}}',
                type: "put",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).hide();
                    $('#assign_add_'+user_id).show();
                }
            });
        });
    });


    function destroy_user(id)
    {
        $.ajax({
            url:'users/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {
                var new_url = 'users';
                window.location.href = new_url;
            }
        });
    }
</script>



<!-- LIGHTBOX JS -->
