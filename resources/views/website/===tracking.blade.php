<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Directions Service</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script>

    function initMap() {

        @if(isset($salon) && isset($user))

            var sid = '{{$salon['id']}}';
            var slatitude = parseFloat('{{$salon['latitude']}}');
            var slongitude = parseFloat('{{$salon['longitude']}}');

            var ulatitude = parseFloat('{{$user['latitude']}}');
            var ulongitude = parseFloat('{{$user['longitude']}}');


            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: slatitude,
                    lng: slongitude},
                zoom: 14
            });
            var pos = {
                lat: slatitude,
                lng: slongitude
            };
            map.setZoom(14);
            map.setCenter(pos);
            var markers = [];

            directionsDisplay.setMap(map);
            var start = new google.maps.LatLng(slatitude,slongitude);
            var end = new google.maps.LatLng(ulatitude, ulongitude);
            calculateAndDisplayRoute(directionsService,directionsDisplay,start,end);

            get_salons(sid,slatitude,slongitude,0);


        @else
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                center: {lat: 41.85, lng: -87.65}
            });
            directionsDisplay.setMap(map);
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        @endif


        function calculateAndDisplayRoute(directionsService, directionsDisplay,salon,user) {

            var start = salon;
            var end = user;
            directionsService.route({
                origin: start,
                destination: end,
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    var leg = response.routes[0].legs[0];
                    directionsDisplay.setDirections(response);
                    //directionsDisplay.setOptions( { suppressMarkers: true } );

                    //setMapOnAll(null);
                    function setMapOnAll(map) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                        }
                    }

                    //makeMarker(leg.start_location, "Freelancer", map,'freelancer');
                    //makeMarker(leg.end_location, 'Home', map,'home');
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });

        }

        function makeMarker(position, title, map,type) {

            if(type=='home'){
                var pin_icon = {
                    url: '{{url('assets/dist/img/icons/')}}'+'/home.png',
                    scaledSize: new google.maps.Size(50, 50), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(20, 30) // anchor
                };
            }
            else{
                var pin_icon = {
                    url: '{{url('assets/dist/img/icons/')}}'+'/Freelancer_pin.png',
                    scaledSize: new google.maps.Size(80, 80), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(40, 80) // anchor
                };
            }

            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: pin_icon,
                title: title,
                //draggable: true,
                //animation: google.maps.Animation.DROP,
            });

            markers.push(marker);
        }


        function get_salons(sid,latitude,longitude,mode) {
            $.ajax({
                url: '{{url("get_salon_location")}}',
                type: 'GET',
                data: {sid: sid},
                success: function (data) {

                    var myJSON = JSON.stringify(data)
                    var JSONObject = JSON.parse(myJSON);
//                    var sid = sid;
                    var plat = JSONObject['latitude'];
                    var plng = JSONObject['longitude'];
                    if(JSONObject['message']==0){

                        if(JSONObject['latitude']!=latitude && JSONObject['longitude']!=longitude){

                            var slatitude = parseFloat(JSONObject['latitude']);
                            var slongitude = parseFloat(JSONObject['longitude']);

                            var ulatitude = parseFloat('{{$user['latitude']}}');
                            var ulongitude = parseFloat('{{$user['longitude']}}');

                            directionsDisplay.setMap(map);
                            var start = new google.maps.LatLng(slatitude,slongitude);
                            var end = new google.maps.LatLng(ulatitude,ulongitude);
                            calculateAndDisplayRoute(directionsService,directionsDisplay,start,end);
                        }

//                        if(mode==0){
//
//                        }

                    }
                    setTimeout(function(){
                        get_salons(sid,plat,plng,1);
                    },5000);
                }
            });
        }
    }


</script>
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap">
</script>
</body>
</html>