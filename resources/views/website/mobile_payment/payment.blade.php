<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Text Rotate -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/simpletextrotator.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <!--******************CSS FOR COUNTRY FLAG******************-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/dd.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/flags.css') }}" />

    <style>


        select{
            display: block;
            width: 100%;
            height: 50px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #d9d9d9;
            border-radius: 5px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        @media only screen and (max-device-width: 480px) {
            .mob_reg {
                display: none !important;
            }

            .break_cont {
                display: none !important;
            }
        }
        #loadingDiv{
            position:fixed;
            top:0;
            right:0;
            width:100%;
            height:100%;
            background-color:#fff;
            background-image:url('{{url('assets/website/images/91.gif')}}');
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            /*opacity: 0.8;*/
            filter: alpha(opacity=40); /* For IE8 and earlier */
        }
        .centered {
            position: absolute;
            top: 58%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        /* RADIO  */
        .radios .radio {
            cursor: pointer;
            width: 100%;
            //min-height: 76px;
            //color: #ffffff;
            //background: #777779;
            //background-color: rgb(119, 119, 121);
            //border: 1px solid #777779;
            //border-top-color: rgb(119, 119, 121);
            //border-right-color: rgb(119, 119, 121);
            //border-bottom-color: rgb(119, 119, 121);
            //border-left-color: rgb(119, 119, 121);
            //box-shadow: 0 2px 4px 0 rgba(0,0,0,0.17);
            //border-radius: 100px;
            //font-size: 23px;
            //position: relative;
            //text-align: center;
            //padding-top: 5%;
            //padding-bottom: 4%;
            //padding-right: 12%;
            //outline: 0;
            //border:none ;
        }

        .radios input[type=radio] {
            display: none;
        }

        .radio, .checkbox{
            margin-top: 0px;
        }

        .radios input[type=radio]:checked + .radio{
            /*display: block;*/
        }

    </style>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>
    <![endif]-->
</head>
<body style="font-family:Avenir, Helvetica, sans-serif;">



<!-- =========================
        FEATURES
    ========================== -->
<section id="main_form1" style="padding-top: 2%; padding-bottom: 2%; display: block">
    <div class="container" id="frm1">
        <div class="row">
            {{--<form style="border: none;" class="col-md-8 col-sm-8 com-md-offset-2 col-sm-offset-2" action="{{ url('api/payment_form') }}" method="post">--}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <input type="hidden" name="session_token" value="{{ $token }}" id="session_token" >
                <div  style="text-align: center; padding: 20px 0px">
                    <img src="{{ URL::asset('assets/website/Logo_web.png') }}" height="160">
                </div>

                <div class="theme_color" style="padding: 10px; font-size: 18px">
                    Get Online Customers and waiting list
                </div>

                <div class="theme_color1" style="padding: 0 10px 20px 10px; font-size: 20px; ">
                    No Commitment. Cancel anytime.
                </div>

                @foreach($plan as $val)

                <div class="membership_pg radios">
                    <input type="radio"  value="{{$val['id']}}" id="{{$val['id']}}" onclick="call_plan(this.value)" />

                    <label class="col-md-10 col-sm-10 col-xs-10 radio"  for="{{$val['id']}}" style="float: left; padding: 0px; ">
                        <div style="padding:5px 0px; padding-top: 10px; font-size: 20px; font-weight: bold">{{$val['title']}}</div>
                        <h4 style="font-size: 16px; font-weight: bold">$ {{$val['amount']}}/month after trial ends</h4>
                        @if($val['id']==1)
                            <img id="chk_def" style="float:right; margin-top: -65px; display: block" src="{{url('assets/website/images/white-tick.png')}}" height="50">
                        @else
                            <img class="chk_tck" id="chk_tck_{{$val['id']}}" style="float:right; margin-top: -65px; display: none" src="{{url('assets/website/images/white-tick.png')}}" height="50">
                        @endif

                    </label>

                    {{--<div class="col-md-10 col-sm-10 col-xs-10" for="{{$val['id']}}" style="float: left; padding: 0px; ">--}}
                        {{--<div style="padding:5px 0px; padding-top: 10px; font-size: 20px; font-weight: bold">{{$val['title']}}</div>--}}
                        {{--<h4 style="font-size: 16px; font-weight: bold">$ {{$val['amount']}}/month after trial ends</h4>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-2 col-sm-2 col-xs-2 " style="float: right; text-align: center; padding: 0px; margin: 15px 0 0 0;" >--}}
                        {{--<img src="{{url('assets/website/images/white-tick.png')}}" height="50">--}}
                    {{--</div>--}}
                </div>
                @endforeach

                <input type="hidden" name="plan_id" id="plan_id" value="1">

                <script>
                    function call_plan(val) {
                        var plan_count = {{ count($plan) }};
                        if (val > 1)
                        {
                            for (var i=2;i<=plan_count;i++){
                                document.getElementById('chk_tck_'+i).style.display = 'none';

                            }

                            document.getElementById('chk_def').style.display = 'none';
                            document.getElementById('chk_tck_'+val).style.display = 'block';
                        }
                        else
                        {
                            document.getElementById('chk_def').style.display = 'block';
                            for (var i=2;i<=plan_count;i++){
                                document.getElementById('chk_tck_'+i).style.display = 'none';
                            }
                        }
                        document.getElementById('plan_id').value=val;
                    }
                </script>


                <div class="theme_color1" style="text-align: center; font-size: 20px; padding: 10px 0px;">
                    Plan automatically renews monthly
                </div>
                @if(count($plan) > 0)
                <button class="btn btn-primary1 center-block" style="width:98%;  height: 48px; font-size: 20px; padding: 10px 12px; margin: 0px auto" id="payment_btn" onclick="checkout();">Start Trial</button>
                @else
                <button class="btn btn-primary1 center-block" style="width:98%;  height: 48px; font-size: 20px; padding: 10px 12px; margin: 0px auto" id="payment_btn" onclick="call_back_done()">Visit site</button>
                @endif
            {{--</form>--}}
        </div>
    </div><!-- End Container -->


    <div class="container" id="frm2" style="display: none">
        <div class="row">
            <div class="col-md-12 heading" style="margin-bottom: 0px;">
                <h2>Payment Form</h2>
            </div>
        </div>
        <div class="row">
            <div>


                {{--<form style="border: none;" class="col-md-8 col-sm-8 com-md-offset-2 col-sm-offset-2" action="{{ url('api/payment_form_submit') }}" method="post">--}}

                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <input type="hidden" name="user_id" value="" id="user_id">
                <input type="hidden" name="amount" value="59.99" id="amount">
                <input type="hidden" name="session_token" value="" id="session_token">
                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('cardname') ? ' has-error' : '' }}">
                        <label class="control-label requiredField">Name On Card<span class="asteriskField">*</span></label>
                        <div class="controls">
                            {!! Form::text('cardname', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter Card Name','id'=>'cardname','required']) !!}
                            @if ($errors->has('cardname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('cardname') }}</strong>
                                    </span>
                            @endif

                            <strong><span id="cardname_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('cardnumber') ? ' has-error' : '' }}">
                        <label class="control-label requiredField">Credit card number<span class="asteriskField">*</span></label>
                        <div class="controls ">
                            <div name="card-container">
                                <input type="number" id="cardnumber" name="cardnumber" placeholder="xxxx xxxx xxxx xxxx" pattern="[0-9]*" class="textinput textInput form-control" required >
                                <div id="logo"></div>
                            </div>
                            @if ($errors->has('cardnumber'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('cardnumber') }}</strong>
                                    </span>
                            @endif

                            <strong><span id="cardnumber_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('expmonth') ? ' has-error' : '' }}">
                        <label class="control-label requiredField">Exp Month<span class="asteriskField">*</span></label>
                        <div class="controls">
                            <select name="expmonth" id="expmonth" class="select form-control"  required>
                                <option value="">Please Select</option>
                                <?php
                                for($m=1; $m<=12; ++$m){
                                    echo '<option value="'.$m.'">'.date('F', mktime(0, 0, 0, $m, 1)).'</option>';
                                }
                                ?>
                            </select>
                            @if ($errors->has('expmonth'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('expmonth') }}</strong>
                                    </span>
                            @endif

                            <strong><span id="expmonth_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('expyear') ? ' has-error' : '' }}">
                        <label class="control-label requiredField">Exp Year<span class="asteriskField">*</span></label>
                        <div class="controls">
                            <select name="expyear" id="expyear" class="form-control" required>
                                <option value="">Please Select</option>
                                <?php
                                for ($i = 2019; $i <= 2050; ++$i){
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                            </select>
                            @if ($errors->has('expyear'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('expyear') }}</strong>
                                    </span>
                            @endif

                            <strong><span id="expyear_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
                        <label class="control-label requiredField">CVV<span class="asteriskField">*</span></label>
                        <div class="controls">
                            {!! Form::number('cvv', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter CVV','id'=>'cvv','pattern'=>'[0-9]*']) !!}
                            @if ($errors->has('cvv'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('cvv') }}</strong>
                                    </span>
                            @endif

                            <strong><span id="cvv_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary1 center-block" style="height: 34px; font-size: 18px; padding: 4px 12px;" id="payment_btn" onclick="card_submit();">Payment</button>
                </fieldset>

            </div>

        </div>
    </div><!-- End Container -->


    <div class="container"  id="frm3" style="text-align: center; margin-top: 50px; display: none" >
        <div class="row">
            <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 thank_you">
                <div class="intro-text" >

                    <div class="article-header" style="text-align: center">
                        <h2 id="incomplete_message"></h2>
                    </div>
                    <br>
                    @if($type=="android")
                    <button class="btn  btn-primary1 center-block membership_pg_btn" type="submit"
                            onclick="call_back1()">DONE</button>
                    @else
                    <button class="btn  btn-primary1 center-block membership_pg_btn"  type="submit"
                                onclick="call_back_done()">DONE</button>
                    @endif
                </div>
            </div>
        </div>
    </div><!-- End Container -->


    <div class="container" id="frm4" style="text-align: center; display: none">
        <div class="row">
            <div class="col-md-12 col-sm-12 thank_you">
                <div class="intro-text">

                    <div class="col-md-12 heading">
                        <div style="background-image: url('{{url('assets/website/images/green_tick.png')}}'); background-size: contain;
                                     background-position: center; background-repeat: no-repeat; height: 170px;">
                        </div>
                    </div>

                    <div class="article-header" style="text-align: center">
                        <h2 id="complete_message"></h2>
                    </div>
                    <br>
                    @if($type=="android")
                        <button onclick="call_back1()" class="btn  btn-primary1 center-block membership_pg_btn"  type="submit">Go</button>
                    @else
                        <button onclick="call_back_done()" class="btn  btn-primary1 center-block membership_pg_btn"  type="submit">Go</button>
                    @endif

                </div>
            </div>
        </div>
    </div><!-- End Container -->

    <div class="container" id="loadingDiv" style="display: none;">
        <div class="centered">
            <h1>Please Wait For Your Payment Process...</h1>
        </div>
    </div>
</section><!-- /END Feature Three -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/website/js/vanilla-masker.min.js')}}"></script>
<script src="{{ URL::asset('assets/website/js/app.bundle.js')}}"></script>

{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>--}}
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>

<script>
    function checkout()
    {
        var token = document.getElementById('token').value;
        var session_token = document.getElementById('session_token').value;
        var plan_id = document.getElementById('plan_id').value;
        document.getElementById('loadingDiv').style.display='block';
        //document.getElementById('main_form1').style.display='none';

        $.ajax({
            url: '{{url('api/payment_form')}}',
            type: "POST",
            data: {"_token": token,"session_token":session_token,"plan_id":plan_id},
            success: function(data){
                //document.getElementById('frm').style.display='none';
                var data = JSON.parse(data);

                if(data.status == 0){
                    //var new_url = '{{url('api/payment_incomplete?message=')}}'+data.message;
                    //window.location.href = new_url;

                    document.getElementById('loadingDiv').style.display='none';
                    document.getElementById('frm1').style.display='none';
                    document.getElementById('frm2').style.display='none';
                    document.getElementById('frm3').style.display='block';
                    document.getElementById('frm4').style.display='none';
                    document.getElementById('incomplete_message').innerHTML="<b>"+data.message+"</b>";
                }
                else{
                    document.getElementById('loadingDiv').style.display='none';
                    document.getElementById('frm1').style.display='none';
                    document.getElementById('frm2').style.display='block';
                    document.getElementById('frm3').style.display='none';
                    document.getElementById('frm4').style.display='none';
                    document.getElementById('user_id').value=data.uid;
                    document.getElementById('session_token').value=data.session_token;
                    //var new_url = '{{url('api/card_details?user_id=')}}'+data.uid+'&session_token='+data.session_token;
                    //window.location.href = new_url;
                }
            }
        });
    }

    function card_submit() {
        var token = document.getElementById('token').value;
        var cardname = document.getElementById('cardname').value;
        var cardno = document.getElementById('cardnumber').value;

        var expmonth = document.getElementById('expmonth').value;
        var expyear = document.getElementById('expyear').value;
        var cvv = document.getElementById('cvv').value;
        var user_id = document.getElementById('user_id').value;
        var amount = document.getElementById('amount').value;
        var plan_id = document.getElementById('plan_id').value;

        var flag = 0;
        if(document.getElementById("cardname").value.split(" ").join("") == "")
        {

            document.getElementById('cardname_msg').innerHTML = 'Card name is required!';
            document.getElementById("cardname").focus();
            document.getElementById("cardname").style.borderColor ='#dd4b39';
            document.getElementById('cardname_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("cardname").style.borderColor ='#d2d6de';
            document.getElementById('cardname_msg').style.display = 'none';
        }


        if(document.getElementById("cardnumber").value.split(" ").join("") == "")
        {

            document.getElementById('cardnumber_msg').innerHTML = 'Card Number is required!';
            document.getElementById("cardnumber").focus();
            document.getElementById("cardnumber").style.borderColor ='#dd4b39';
            document.getElementById('cardnumber_msg').style.display = 'block';
            flag=1;
        }
        else
        {

            document.getElementById("cardnumber").style.borderColor ='#d2d6de';
            document.getElementById('cardnumber_msg').style.display = 'none';

            var cardnumber=document.getElementById("cardnumber").value;
            if(isNaN(cardnumber))
            {
                document.getElementById('cardnumber_msg').innerHTML = 'Please enter a valid Card Number.';
                document.getElementById("cardnumber").focus();
                document.getElementById("cardnumber").style.borderColor ='#dd4b39';
                document.getElementById('cardnumber_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("cardnumber").style.borderColor ='#d2d6de';
                document.getElementById('cardnumber_msg').style.display = 'none';
            }
        }

        if(document.getElementById("expmonth").value.split(" ").join("") == "")
        {
            document.getElementById('expmonth_msg').innerHTML = 'Expire Month is required!';
            document.getElementById("expmonth").focus();
            document.getElementById("expmonth").style.borderColor ='#dd4b39';
            document.getElementById('expmonth_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("expmonth").style.borderColor ='#d2d6de';
            document.getElementById('expmonth_msg').style.display = 'none';
        }

        if(document.getElementById("expyear").value.split(" ").join("") == "")
        {
            document.getElementById('expyear_msg').innerHTML = 'Expire Year is required!';
            document.getElementById("expyear").focus();
            document.getElementById("expyear").style.borderColor ='#dd4b39';
            document.getElementById('expyear_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("expyear").style.borderColor ='#d2d6de';
            document.getElementById('expyear_msg').style.display = 'none';
        }

        if(document.getElementById("cvv").value.split(" ").join("") == "")
        {
            document.getElementById('cvv_msg').innerHTML = 'Cvv is required!';
            document.getElementById("cvv").focus();
            document.getElementById("cvv").style.borderColor ='#dd4b39';
            document.getElementById('cvv_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("cvv").style.borderColor ='#d2d6de';
            document.getElementById('cvv_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }

        document.getElementById('loadingDiv').style.display='block';

        $.ajax({
            url: '{{url('api/payment_form_submit')}}',
            type: "POST",
            data: {"_token": token,"cardname":cardname,"cardnumber":cardno,"expmonth":expmonth,"expyear":expyear,"cvv":cvv,"user_id":user_id,"amount":amount,"plan_id":plan_id},
            success: function(data){
                var data = JSON.parse(data);

                if(data.status == 0){

                    document.getElementById('loadingDiv').style.display='none';
                    document.getElementById('frm1').style.display='none';
                    document.getElementById('frm2').style.display='none';
                    document.getElementById('frm3').style.display='block';
                    document.getElementById('frm4').style.display='none';
                    document.getElementById('incomplete_message').innerHTML="<b>"+data.message+"</b>";
                    //var new_url = '{{url('api/payment_incomplete?message=')}}'+data.message;
                    //window.location.href = new_url;
                }
                if(data.status == 1){

                    document.getElementById('loadingDiv').style.display='none';
                    document.getElementById('frm1').style.display='none';
                    document.getElementById('frm2').style.display='none';
                    document.getElementById('frm3').style.display='none';
                    document.getElementById('frm4').style.display='block';
                    document.getElementById('complete_message').innerHTML="<b>"+data.message+"</b>";
                    //var new_url = '{{url('api/payment_complete?message=')}}'+data.message;
                    //window.location.href = new_url;
                }
            }
        });
    }
</script>

<script>
    function call_back(){
        //location.href="nailmaster://cancel";
        document.getElementById('loadingDiv').style.display='none';
        document.getElementById('frm1').style.display='none';
        document.getElementById('frm2').style.display='block';
        document.getElementById('frm3').style.display='none';
        document.getElementById('frm4').style.display='none';
    }

    function call_back1(){
        location.href="{{config('siteVars.done_link')}}";
    }

    function call_back2(){
        location.href="{{config('siteVars.siteurl')}}";
    }

    function call_back_done() {
        location.href="{{config('siteVars.siteurl')}}";
    }
</script>

</body>
</html>
