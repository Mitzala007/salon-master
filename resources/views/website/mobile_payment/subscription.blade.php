<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Text Rotate -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/simpletextrotator.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <!-- Color Switcher -->
    {{--    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/switcher.core.min.css') }}">--}}
    {{--    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/switcher.helper.css') }}">--}}

            <!--[if lt IE 9]>
    <!--<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->
    <style>
        @media only screen and (max-device-width: 480px) {
            .mob_reg {
                display: none !important;
            }

            .break_cont {
                display: none !important;
            }

            .thank_you{
                margin-top: 25% !important;
            }
        }

        #loadingDiv{
            position:fixed;
            top:0;
            right:0;
            width:100%;
            height:100%;
            background-color:#fff;
            background-image:url('{{url('assets/website/images/91.gif')}}');
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            /*opacity: 0.8;*/
            filter: alpha(opacity=40); /* For IE8 and earlier */
        }

        .centered {
            position: absolute;
            top: 58%;
            left: 50%;
            transform: translate(-50%, -50%);
        }


    </style>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>
    <![endif]-->
</head>
<body>

    <!-- =========================
            Intro
        ========================== -->
    <section style="margin-top: 20px">
        <div class="container" id="frm1" style="display: block">
            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <div class="intro-text">
                        <div class="article-header">
                            <h4 style="line-height: 32px; font-size: 18px">
                                <ul>
                                    <li><b>Subscription type</b>: Monthly plan</li>
                                    <li><b>Payment period</b>: {{ $payment_period }}</li>
                                    <li><b>Order number</b>: {{ $order_no }} <br></li>
                                    <li><b>Order date</b>: {{ $order_date }}<br></li>
                                    <li><b>Price per period</b>: {{ $amount }} <br></li>
                                    <li><b>Subscription status</b>: {{ $status }} <br></li>
                                    <li><b>Subscription type</b>: Monthly plan <br><br></li>

                                    <li style="line-height: 25px;"><b> Auto renew</b>: Payment method {{ !empty($card_type)?$card_type:'' }} {{ $card_number }} has been setup for automatic payments. I authorise {{config('siteVars.url_text')}} LLC to use this card as payment method for automatic renewal of my subscription.<br></li>
                                </ul>
                                <br>

                            </h4>
                        </div>

                        <input type="hidden" name="subscription_id" id="subscription_id" value="{{$subscription_id}}">
                        <div style="margin: 13px">
                            <input type="checkbox" checked disabled> : I authorize charges for future automatic purchases according to {{config('siteVars.url_text')}} LLC's <a href="#" style="color: #333">Terms and Conditions</a></div>
                        <button class="btn  btn-primary1 center-block membership_pg_btn" id="payment_btn" onclick="cancel_subscription();">Cancel Subscription</button>
                        {{--<a href="{{url('api/cancel_subscription/'.$subscription_id)}}"  class="btn  btn-primary1 center-block" style="width:95%; color: #fff;  background-color: #471E61;border: none !important;  height: 48px; font-size: 20px; padding: 10px 12px; margin: 10px" >Cancel Subscription</a>--}}
                    </div>
                </div>
            </div>
        </div><!-- End Container -->


        <div class="container" id="frm2" style="display: none">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 thank_you">
                    <div class="intro-text">

                        <div class="col-md-12 heading">
                            <div style="background-image: url('{{url('assets/website/images/green_tick.png')}}'); background-size: contain;
                                         background-position: center; background-repeat: no-repeat; height: 170px;">
                            </div>
                        </div>

                        <div class="article-header" style="text-align: center">
                            <h2><b>You have cancelled your subscription successfully!</b></h2>
                        </div>
                        <br>
                        <a href="{{config('siteVars.logout_link')}}" class="btn center-block membership_pg_btn" style="color: #fff;">Go</a>
                    </div>
                </div>
            </div>
        </div><!-- End Container -->


        <div class="container" id="loadingDiv" style="display: none;">
            <div class="centered">
                <h1>Please Wait For Your Payment Process...</h1>
            </div>
        </div>
    </section>

<script>
    function call_back(){
        location.href="{{config('siteVars.cancel_link')}}";
    }
</script>

<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>
        <!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>


<script>
    function  cancel_subscription() {

        var subscription_id = document.getElementById('subscription_id').value;
        document.getElementById('loadingDiv').style.display='block';
        document.getElementById('frm1').style.display='none';
        $.ajax({
            url: '{{url('api/cancel_subscription')}}'+'/'+subscription_id,
            type: "GET",
            //data: {"_token": token,"session_token":session_token},
            success: function(data){
                var data = JSON.parse(data);
                document.getElementById('loadingDiv').style.display='none';
                document.getElementById('frm1').style.display='none';
                document.getElementById('frm2').style.display='block';
            }
        });
    }
</script>

</body>
</html>
