

<link href="{{url('assets/website/map_lightbox/responsive.min.css')}}" rel="stylesheet" />
<script src="{{url('assets/website/map_lightbox/responsive.ie10mobilefix.min.js')}}"></script>


<style>
    .select2-container{z-index: 999999;}
    .select2-search--dropdown{ z-index: 999999 !important}
    .select2-container--open .select2-dropdown{ z-index: 999999 !important;}
    .select2-container--default .select2-selection--multiple .select2-selection__choice{ font-size: 14px !important;}
    .select2-container--default .select2-selection--multiple .select2-selection__rendered{ font-size: 14px !important;}
    .select2-results__option{ font-size: 14px !important;}
    .form-control { height: 35px !important;}
    .select2-container--default .select2-search--inline .select2-search__field{ color: #000; font-weight: normal; margin-left: 8px }
</style>

<section class="app-screenshot" id="check-in" style="background: #FEFCFD">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">ONLINE CHECK-IN</h2>
            </div>
        </div>
    </div>

    <div class="col-md-12 form-group" style="text-align: center">
        <div class="row text-center">
            <div class="col-sm-12 text-center">
                <input id="pac-input" class="controls" type="text" placeholder="Enter your address or Zip Code">
                <i class="fa fa-search" id="search_icon"></i>

                {{-- CATEGORY AND SUB CATEGORY LISTING --}}

                @if(config('siteVars.sitetitle')=="Salon Master")
                    <div id="category_selection"  style="text-align: center !important;" >
                        <div class="row text-center">
                            <div class="modal-body form-horizontal" >
                                <div class="box-body" id="cat_div" style="text-align: center">
                                    @foreach($category as $key=>$val)
                                        <div class="col-md-1 col-sm-6 col-xs-6 cat_box col-centered">
                                            <div onclick="call_sub_cat({{$val['id']}})" class="category-image controls" @if($val['subcategory']->count()<=0) data-lightbox-modal-trigger=".modal1" @endif style="background-image: url({{url($val['image'])}});
                                                    background-size: contain; background-repeat: no-repeat; float: left;
                                                    background-position: center; height: 150px; width: 150px;border-radius: 10px;">
                                                {{--<img src="https://37.media.tumblr.com/bddaeb8fe12eda6bb40cf6a0a18d9efa/tumblr_n8zm8ndGiY1st5lhmo1_1280.jpg" width="200" height="200" />--}}
                                                <div class="category-text">
                                                    <h3>{{ $val['title'] }}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                @foreach($category as $cat)
                                    <div class="box-body" id="sub_cat_div_{{$cat['id']}}" style="display: none">
                                        <div class="col-md-12 cat_title ">
                                            <i class="fa fa-arrow-circle-left" aria-hidden="true" style=";cursor: pointer;margin-top: 10px;"  onclick="back_to_cat({{$cat['id']}})" id="back_icon" ></i>
                                            {{$cat['title']}}
                                            <hr>
                                        </div>
                                        @foreach($cat['subcategory'] as $k=>$v)
                                            <div class="col-md-1 col-sm-6 col-xs-6 cat_box col-centered" onclick="set_category_session({{$v['id']}})">

                                                <div class="category-image controls" data-lightbox-modal-trigger=".modal1" style="background-image: url({{url($cat['icon'])}});
                                                        background-size: contain; background-repeat: no-repeat; float: left; border-radius: 10px;background-position: center; height: 150px; width: 150px" >
                                                    <div class="category-text">
                                                        <h3>{{ $v['title'] }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                                {{--<div class="box-footer" style="text-align: center">--}}
                                {{--<button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block" data-lightbox-target="#sub_category_selection">Proceed</button>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-8" style="display: block" id="check_in_map" >
        <div id="load" style=" text-align: center; display: none; position:absolute; width:100%;  height:100%;  z-index:10000000; opacity: 1; ">
            <img style="" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />
        </div>
        <div class="col-sm-12 con-map" id="map" style="min-height:600px; margin-bottom:25px;"></div>
        <div id="infowindow-content">
            <img src="" width="16" height="16" id="place-icon">
            <span id="place-name"  class="title"></span><br>
            <span id="place-address"></span>
        </div>

    </div>

    <div class="col-md-4 pac-card" id="pac-card" style="display: block; height: auto">
        <div>
            {{--<div id="title">--}}
            {{--<input type="text" placeholder="Search Salon" id="salon_search" name="salon_search" class="salon_search">--}}
            {{--</div>--}}
            <div id="salon-listing" class="pac-controls">
                <div style="text-align: center;margin-top:25%">
                    <img src="{{ url('assets/website/images/alert.png') }}" height="100" >
                    <h2>
                        Allow location Access to get nearby salon list or search manually!
                    </h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" style="display: none" id="check_in_image">
        <div class="row">
            <div class="col-sm-12 screenshots wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s" style="text-align: center">
                <img src="{{url('assets/website/images/animation.gif')}}" id="checkin_video" class="checkin_video" onclick="display_map()" style="cursor: pointer">
            </div>
        </div>
    </div>

</section>

<a href="#" class="modal1" data-lightbox-target="#category_selection" id="call_category" style="display: block">Launch internal overlay</a>
{{--<a href="#" title="Close (Esc)" id="close_lbox" class="lightbox-close fade-out fade-in">X</a>--}}

<input type="hidden" name="sid" id="sid" value="{{ session()->get('SESS_SUB_CAT') }}">
<input type="hidden" name="cid" id="cid" value="{{ session()->get('SESS_CAT') }}">
<input type="hidden" name="latitude" id="latitude" value="">
<input type="hidden" name="longitude" id="longitude" value="">

<div id="checkin" class="hidden">
    <input type="hidden" name="salon_ids" id="salon_ids" value="">
    <input type="hidden" name="cat_data" id="cat_data" value="">
    <input type="hidden" name="category_id" id="category_id" value="">
    <div class="modal-header">
        <h3 class="modal-title text-center">
            {{--<i class="fa fa-arrow-circle-left" aria-hidden="true" style="float: left;cursor: pointer" id="back_icon" ></i>--}}
            Booking</h3>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="modal-body form-horizontal" id="booking_form" style="color: #444!important; font-size: 14px!important; display: block">
                <div class="box-body">
                    {!! Form::hidden('redirects_to', URL::previous()) !!}

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="username" id="username_lb"><b>Name</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="username" id="username" value="" placeholder = 'Enter Name' class="form-control">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                            <strong><span id="username_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('user_phone') ? ' has-error' : '' }}">
                        <label class="col-md-3 col-sm-12 col-xs-12 control-label" for="user_phone" id="user_phone_lb"><b>Phone</b><span class="text-red">*</span></label>

                        <div class="col-md-2 col-sm-4 col-xs-4" id="user_phone_display" style="display: block">
                            <select name="user_country" id="user_country" class="form-control select2" style="width: 100%;">
                                <option value='1'>+1</option>
                                <option value='91'>+91</option>
                                <option value='61'>+61</option>
                            </select>
                        </div>
                        <div class="col-md-7 col-sm-8 col-xs-8">
                            <input type="text" name="user_phone" id="user_phone" placeholder=" Enter Phone" value="" class="form-control">
                            @if ($errors->has('user_phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_phone') }}</strong>
                                </span>
                            @endif
                            <strong><span id="user_phone_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="user_email" id="user_email_lb"><b>Email</b><span class="text-red"></span></label>
                        <div class="col-md-9">
                            <input type="email" name="user_email" id="user_email" placeholder=" Enter Email" class="form-control">
                            @if ($errors->has('user_email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_email') }}</strong>
                                </span>
                            @endif
                            <strong><span id="user_email_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>



                    <div class="form-group{{ $errors->has('user_bdate') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="user_bdate" id="user_bdate_lb"><b>Booking Date</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="user_bdate" id="datepicker_walkin" value="<?php echo date('m/d/Y'); ?>" placeholder=" Enter Date" class="form-control bdate">
                            @if ($errors->has('user_bdate'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_bdate') }}</strong>
                                </span>
                            @endif
                            <strong><span id="user_bdate_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    {{--@if(config('siteVars.sitetitle') == "Salon Master")--}}
                    {{--<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="category" id="category_lb"><b>Category</b><span class="text-red"></span></label>--}}
                    {{--<div class="col-md-9">--}}

                    {{--<select name="category" id="category" class="form-control select2" style="width: 100%; color: #CCCCCC" onchange="get_sub_cat(this.value)">--}}
                    {{--</select>--}}

                    {{--@if ($errors->has('category'))--}}
                    {{--<span class="help-block">--}}
                    {{--<strong>{{ $errors->first('category') }}</strong>--}}
                    {{--</span>--}}
                    {{--@endif--}}

                    {{--<strong><span id="category_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group{{ $errors->has('sub_category') ? ' has-error' : '' }}">--}}
                    {{--<label class="col-md-3 control-label" for="sub_category" id="sub_category_lb"><b>Sub Category</b><span class="text-red"></span></label>--}}
                    {{--<div class="col-md-9">--}}


                    {{--<select name="sub_category" id="sub_category" class="form-control select2" data-placeholder="Please Select" style="width: 100%" onchange="get_sub_services(this.value)">--}}
                    {{--</select>--}}

                    {{--@if ($errors->has('sub_category'))--}}
                    {{--<span class="help-block">--}}
                    {{--<strong>{{ $errors->first('sub_category') }}</strong>--}}
                    {{--</span>--}}
                    {{--@endif--}}

                    {{--<strong><span id="sub_category_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endif--}}

                    <div class="form-group{{ $errors->has('service_type') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="service_type" id="service_lb"><b>Service Type</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <div class="row">
                                <label class="col-md-12 col-sm-12 col-xs-12">
                                    <select name="service_type[]" id="service_type" class="form-control select2" data-placeholder="Select Services"  style="width: 100%" multiple >
                                    </select>
                                </label>
                                @if ($errors->has('service_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <strong><span id="service_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('no_of_persons') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="no_of_persons" id="person_lb"><b>No of persons</b><span class="text-red">*</span></label>
                        <div class="col-md-9">
                            <input type="number" min="1" name="no_of_persons" id="no_of_persons" value="1" placeholder=" Enter No of persons" class="form-control">
                            @if ($errors->has('no_of_persons'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_of_persons') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="emp_id" id="saloon_id">Technician Requested<span class="text-red"></span></label>
                        <div class="col-md-9">
                            <select name="emp_id" id="emp_id" class="form-control select2" style="width: 100%">
                            </select>
                            @if ($errors->has('emp_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('emp_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label" for="emp_id" id="saloon_id"><span class="text-red"></span></label>
                        <div class="col-md-9">
                            <div class="controls ">
                                <div class="g-recaptcha" data-sitekey="{{config('siteVars.captcha_key')}}"></div>
                                <strong><span id="bcaptche_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="box-footer" style="text-align: center">
                    <button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block" onclick="do_booking()">ADD</button>
                </div>
            </div>
            <div class="modal-body form-horizontal" id="booking_success" style="color: #444!important; font-size: 14px!important; display: none">
                <div class="box-body">
                    <div style="text-align: center" id="booking_success_msg">
                        <img src="{{url('assets/website/img/green_tick.png')}}" height="100 "><h3>CheckIn successfully done !</h3>
                    </div>
                </div>
            </div>

            <div class="modal-body form-horizontal" id="inner-loder" style="color: #444!important; font-size: 14px!important; display: none; height: 115px">
                <div class="box-body">
                    <div style="text-align: center">
                        <img style="z-index: 100000;position: absolute; right: 30%" src="{{URL::asset('assets/dist/img/pink_load.gif') }}" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    function display_map() {
        document.getElementById('check_in_map').style.display="block";
        document.getElementById('check_in_image').style.display="none";
    }
</script>

<script>

    function initMap(opt) {

        var clat = document.getElementById('latitude').value;
        var clng = document.getElementById('longitude').value;

        var map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: 37.9642529,
                lng: -91.8318334},
            zoom: 5
        });



        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var countries = document.getElementById('salon-listing');

        var autocomplete = new google.maps.places.Autocomplete(input);

        @if(config('siteVars.sitetitle')=='Nail Master')
        // Set initial restrict to the greater list of countries.
            autocomplete.setComponentRestrictions(
            {'country': ['usa']});
        @endif
        // Specify only the data fields that are needed.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });



        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                document.getElementById('latitude').value = pos['lat'];
                document.getElementById('longitude').value = pos['lng'];

                //document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 255 + ',' + 255 + ',' + 255 + 0.3 +' )';
                //document.getElementById('bodyid').style.opacity=0.5;
                //document.getElementById('load').style.display="block";

//                if(clat!="" && clng!=""){
//                    var mode = "cat";
//                }
//                else{
//                    var mode = "auto";
//                }


                var mode = "auto";
                get_saloons(pos['lat'],pos['lng'],mode);
                map.setZoom(14);
                map.setCenter(pos);
            });
        }

        autocomplete.addListener('place_changed', function() {

            document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 255 + ',' + 255 + ',' + 255 + 0.3 +' )';
            document.getElementById('bodyid').style.opacity=0.5;
            document.getElementById('load').style.display="block";

            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('pac-input').value;

            /* GET CURRENT SELECTION LAT LONG AND PASS IN AJAX METHOD */
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    /* --------------- AJAX SALON PIN CODE ----------------  */

                    var mode = "search";
                    get_saloons(latitude,longitude,mode);

                }
            });
        });

        function get_saloons(latitude,longitude,mode) {

            var sid = document.getElementById('sid').value;
            var cid = document.getElementById('cid').value;

            $.ajax({
                url: '{{url("get_salons")}}',
                type: "POST",
                //data: {"_token": '{{csrf_token()}}',"latitude":latitude,"longitude":longitude},
                data: {"_token": '{{csrf_token()}}',"latitude":latitude,"longitude":longitude,"category_id":cid,"sub_category_id":sid},

                success: function(data){


                    document.getElementById('bodyid').style.opacity=1;
                    document.getElementById('load').style.display="none";
                    document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 255 + ',' + 255 + ',' + 255 + ')';

                    var myJSON = JSON.stringify(data)
                    var JSONObject = JSON.parse(myJSON);
                    var saloons_count = JSONObject['locations']['saloons'].length;

                    var locations = [];
                    if(saloons_count > 0){
                        for (var j=0;j<saloons_count;j++){
                            var a =  [JSONObject['locations']['saloons'][j]['title'],
                                JSONObject['locations']['saloons'][j]['latitude'],
                                JSONObject['locations']['saloons'][j]['longitude'],
                                JSONObject['locations']['saloons'][j]['id'],
                                JSONObject['locations']['saloons'][j]['map_pin']];
                            locations.push(a);
                        }
                    }

                    infowindow.close();

                    if(mode=="search"){
                        var place = autocomplete.getPlace();

                        if (!place.geometry) {
                            // User entered the name of a Place that was not suggested and
                            // pressed the Enter key, or the Place Details request failed.
                            var geocoder = new google.maps.Geocoder();
                            var address = document.getElementById('pac-input').value;
                            geocoder.geocode( { 'address': address}, function(results, status) {

                                if (status == google.maps.GeocoderStatus.OK) {
                                    var latitude = results[0].geometry.location.lat();
                                    var longitude = results[0].geometry.location.lng();
                                    var pos = {
                                        lat:latitude,
                                        lng: longitude
                                    };

                                    document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 255 + ',' + 255 + ',' + 255 + 0.3 +' )';
                                    document.getElementById('bodyid').style.opacity=0.5;
                                    document.getElementById('load').style.display="block";

                                    var mode = "auto";
                                    get_saloons(pos['lat'],pos['lng'],mode);
                                    map.setZoom(14);
                                    map.setCenter(pos);
                                }
                            });
                            return;
                        }

                        // If the place has a geometry, then present it on a map.
                        if (place.geometry.viewport) {
                            map.fitBounds(place.geometry.viewport);
                        } else {
                            map.setCenter(place.geometry.location);
                            map.setZoom(10);  // Why 17? Because it looks good.
                        }
                    }

                    var salon_listing = "<table class='table table-bordered table-responsive' id='example23'>";
                    var loc_count = 0;
                    for (i = 0; i < locations.length; i++) {

                        loc_count ++;
                        var pin_icon = {
                            url: '{{url('assets/dist/img/icons/')}}'+'/'+locations[i][4],
                            scaledSize: new google.maps.Size(80, 80), // scaled size
                            origin: new google.maps.Point(0, 0), // origin
                            anchor: new google.maps.Point(0, 0) // anchor
                        };

                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map,
                            icon : pin_icon
                        });

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                infowindow.setContent(locations[i][0]);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));

                        /* --------------------  MAP RIGHT SIDE DISPLAY SALON LISTING ------------------------ */

                        var rating = JSONObject['locations']['saloons'][i]['rating'];
                        var selected_star = "";
                        for (var k = 0; k < rating; k++) {
                            selected_star +="<i class='fa fa-star' style='color: #daa520;'></i>";
                        }
                        var unselected_star =""
                        for (var l = 0; l<5-rating;l++){
                            unselected_star +="<i class='fa fa-star' style='color: #d3d3d3;'></i>";
                        }

                        var workdays_length=JSONObject['locations']['saloons'][i]['work_days'].length;
                        var workdays = "";
                        for (var m=0;m<workdays_length;m++){
                            workdays += "<tr>" +
                                "<td><div class='days_lbl'>"+JSONObject['locations']['saloons'][i]['work_days'][m]['work_day'].toUpperCase()+"</div></td>" +
                                "<td>"+JSONObject['locations']['saloons'][i]['work_days'][m]['start_hour']+"</td>" +
                                "<td>"+JSONObject['locations']['saloons'][i]['work_days'][m]['end_hour']+"</td>" +
                                "</tr>";
                        }

                        var services_length=JSONObject['locations']['saloons'][i]['saloon_services'].length;
                        var services = "";
                        for (var o=0;o<services_length;o++) {

                            var charges = "";
                            if(JSONObject['locations']['saloons'][i]['saloon_services'][o]['discount']==1){
                                charges = JSONObject['locations']['saloons'][i]['saloon_services'][o]['charges'];
                            }
                            services += "<tr>" +
                                "<td>"+JSONObject['locations']['saloons'][i]['saloon_services'][o]['service_name']+"</td>" +
                                "<td style='text-decoration: line-through'>"+charges+"</td>" +
                                "<td>"+JSONObject['locations']['saloons'][i]['saloon_services'][o]['discount_price']+"</td>" +
                                "</tr>";
                        }

                        var salon_id = JSONObject['locations']['saloons'][i]['id'];
                        var service_list = JSONObject['locations']['saloons'][i]['saloon_services'];
                        var employee_list = JSONObject['locations']['saloons'][i]['employees'];
                        var salon_images = JSONObject['locations']['saloons'][i]['image_total'];
                        var thumb_images = "";
                        for (p=0;p<salon_images;p++){
                            thumb_images += " <div id='bg_img_"+salon_id+'_'+p+"'  style='background-image: url({{url('assets/website/images/35.gif')}});" +
                                "                                            background-size: contain;" +
                                "                                            background-repeat: no-repeat; float: left; margin-left:10px;" +
                                "                                            background-position: center; height: 30px; width: 30px'></div>";
                        }

                        if(JSONObject['locations']['saloons'][i]['status']!='close'){
                            var checkin_button = "<input type='button' class='checkin_btn' value='Check In'  data-lightbox-target='#checkin' onclick='set_checkin_fields("+salon_id+","+JSON.stringify(employee_list)+")' ></button>";
                        }
                        else{
                            var checkin_button = "<input type='button' disabled class='btn btn-info checkin_btn' value='Closed'  data-lightbox-target='#checkin' onclick='set_checkin_fields("+salon_id+","+JSON.stringify(employee_list)+")' ></button>";
                        }


                        salon_listing +="<tr data-lightbox-target='#overlay"+salon_id+"' onclick='load_images("+salon_id+")' style='cursor: pointer'>" +
                            "<td>" +
                            "                            <div class='row'>" +
                            "                            <div class='col-md-2 col-sm-2 col-xs-2 salon_pin_img'><img src='assets/dist/img/icons/"+locations[i][4]+"'></div>" +
                            "                            <div class='col-md-10 col-sm-10 col-xs-10'>" +
                            "                                <span class='salon_tit_table'>"+JSONObject['locations']['saloons'][i]['title']+"</span>" +
                            "                                <br>" +
                            "                                <span class='salon_address_table'>"+JSONObject['locations']['saloons'][i]['location'] +
                            "                            </div>" +
                            "                            </div>" +
                            "    <div id='overlay"+salon_id+"' class='hidden' >" +
                            "        <div class='lightbox_head row nomarg'>" +
                            "            <div class='col-md-2 ' style='text-align: center'>" +
                            "                <img src='"+JSONObject['locations']['saloons'][i]['image']+"' height='100'>" +
                            "                <br>" + selected_star+unselected_star+
                            "            </div>" +
                            "            <div class='col-md-6 salon_tit_body' style='line-height: 14px'>" +
                            "                <h2 class='salon_tit' id='light_title'>"+JSONObject['locations']['saloons'][i]['title']+"</h2>" +
                            "                <p><i class='fa fa-phone' aria-hidden='true'></i>&nbsp;&nbsp;"+JSONObject['locations']['saloons'][i]['saloon_phone']+"</p>" +
                            "                <p><i class='fa fa-globe' aria-hidden='true'></i>&nbsp;&nbsp;<a href='"+JSONObject['locations']['saloons'][i]['website_url']+"' target='_blank'>"+JSONObject['locations']['saloons'][i]['website_url']+"</a></p>" +
                            "            </div>" +
                            "            <div class='col-sm-4 col-sm-12' style='text-align: center; margin-top: 30px'>" + checkin_button +
                            "            </div>" +
                            "        </div>" +
                            "        <br>" +
                            "        <div class='lightbox_body row nomarg' style='text-align: left'>" +
                            "            <div class='light_location col-md-12 ' style='text-align: left'>" + thumb_images + "</div>" +
                            "        </div>" +
                            "        <br>" +
                            "        <div class='lightbox_body row nomarg' style='text-align: left'>" +
                            "            <div class='light_location col-md-12 ' >" +
                            "                <p class='salon_addr'>" +
                            "                    <img src='{{url('assets/website/images/address.png')}}' height='40'><span id='light_address'>&nbsp;"+JSONObject['locations']['saloons'][i]['location']+"</span>" +
                            "                </p>" +
                            "            </div>" +
                            "        </div>" +
                            "        <div class='lightbox_body row nomarg'>" +
                            "            <div class='light_location col-md-6 ' >" +
                            "                <h4 class='salon_hr'>Opening Hours</h4>" +
                            "                <table class='table table-responsive table-bordered table-striped'>" +
                            "                    <tr>" +
                            "                        <td width='10%'>Days</td>" +
                            "                        <td>Open Time</td>" +
                            "                        <td>Close Time</td>" +
                            "                    </tr>" + workdays +
                            "                </table>" +
                            "            </div>" +
                            "            <div class='light_location col-md-6 ' >" +
                            "                <h4 class='salon_hr'>More Information</h4>" +
                            "                <div class='tab_in_sec'>" +
                            "                    <div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>" +
                            "                        <div class='panel panel-default'>" +
                            "                            <div class='panel-heading' role='tab' id='headingOne'>" +
                            "                                <h4 class='panel-title'>" +
                            "                                    <a style='color: #471E61; background-color: rgb(241,242,243);' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapseOne' aria-expanded='true' aria-controls='collapseOne' class='active' onclick='ga('send', 'event', 'Button', 'Click', 'Program Overview', 1);'>" +
                            "                                        <img src='{{url('assets/website/images/bestoffers.png')}}' height='30'>Check out our offered services" +
                            "                                    </a>" +
                            "                                </h4>" +
                            "                            </div>" +
                            "                            <div id='collapseOne' class='panel-body panel-collapse collapse in' role='tabpanel' aria-labelledby='headingOne' style='padding: 0px;'>" +
                            "                                <table class='table table-responsive table-striped'>" +
                            "                                    <tr>" +
                            "                                        <th>Service</th>" +
                            "                                        <th align='center'>&nbsp</th>" +
                            "                                        <th>Amount</th>" +
                            "                                    </tr>" + services +
                            "                                </table>" +
                            "                            </div>" +
                            "                        </div>" +
                            "                    </div>" +
                            "                </div>" +
                            "            </div>" +
                            "        </div>" +
                            "        <br>" +
                            "    </div>"+
                            "    </td>" +
                            "</tr>";
                    }
                    salon_listing += "</table>";

                    if(loc_count<=0)
                    {
                        salon_listing = "<div style='text-align: center; margin-top:25%'>" +
                            "                <img src='{{ url("assets/website/images/alert.png") }}' height='100' >\n" +
                            "                <h2 style='text-align: center'>No nearby salon found!</h2></div>";
                    }

                    document.getElementById('salon-listing').innerHTML = salon_listing;
                    if(loc_count>0) {

                        /* --------------------  MAP RIGHT SIDE SALON LISTING ------------------------ */
                        document.getElementById('pac-card').style.display = "block";
                        document.getElementsByClassName('.app-screenshot').style.minHeight="auto";

                        if ($(window).width() < 500) {
                            //map.controls[google.maps.ControlPosition.TOP_CENTER].push(card);
                        }
                        else{
                            //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
                        }
                    }

                    $("#service_type").select2('val', '');
                }
            });
        }
    }

    function load_images(salon_id) {
        $.ajax({
            url: '{{ url('get_salon_images') }}',
            type:'post',
            data: {"_token": '{{csrf_token()}}',"salon_id":salon_id},
            success:function(data)
            {
                var myJSON = JSON.stringify(data);
                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){
                    for(var i = 0;i< JSONObject['salon_images'].length;i++){
                        document.getElementById('bg_img_'+salon_id+'_'+i).style.backgroundImage="url('"+JSONObject['salon_images'][i]['image_thumbnail']+"')";
                        document.getElementById('bg_img_'+salon_id+'_'+i).style.height="50px";
                        document.getElementById('bg_img_'+salon_id+'_'+i).style.width="50px";
                    }
                }
                else{
                    alert('not success');
                }
            }
        });
    }

    function set_checkin_fields(salon_id,employee_list) {
        $(function () {
            $('#datepicker_walkin').datepicker({
                format: 'yyyy-m-d',
                startDate: '+0d',
                autoclose: true
            });
            $(".select2").select2();
        });

        document.getElementById('username').value = "";
        document.getElementById('user_country').value = "";
        document.getElementById('user_phone').value = "";
        document.getElementById('user_email').value = "";
        //document.getElementById('datepicker_walkin').value = "";
        document.getElementById('no_of_persons').value = 1;
        //document.getElementById('service_type').innerHTML= "";
        $("#service_type").select2('val', '');
        $("#category").select2('val', '');
        $("#sub_category").select2('val', '');

        document.getElementById('salon_ids').value = salon_id;
//            var opts = "";
//            for(var l=0; l<service_list.length;l++){  // SERVICE LISTING
//                opts +="<option value='"+service_list[l]['type_id']+"'>"+service_list[l]['service_name']+"</option>";
//            }
//            document.getElementById('service_type').innerHTML = opts;
        var emp_ids = "";

        for (var u=0;u<employee_list.length;u++){  // EMPLOYEE LISTING
            var selec = "";
            if (u==0){ selec = "selected";}
            emp_ids +="<option value='"+employee_list[u]['id']+"' selec >"+employee_list[u]['nick_name']+"</option>";
        }
        document.getElementById('select2-emp_id-container').innerHTML = "No Preferences";
        document.getElementById('emp_id').innerHTML = emp_ids;

        //var back_icon = document.getElementById('back_icon');

        //document.getElementById('booking_form').style.display="block";
        //document.getElementById('booking_success').style.display="none";
        //back_icon.setAttribute('data-lightbox-target','#overlay'+salon_id);
        get_services();
        get_category();

        function get_services() {
            $.ajax({
                url: '{{ url('get_services') }}',
                type:'post',
                data:{"_token": '{{csrf_token()}}','salon_id':salon_id ,'type':'service_list'},
                success:function(data)
                {
                    document.getElementById('service_type').innerHTML=data;
                }
            });
        }

        function get_category() {
            $.ajax({
                url: '{{url("get_category")}}',
                type: "POST",
                data: {"_token": '{{csrf_token()}}',},
                success: function(data){
                    document.getElementById('bodyid').style.opacity=1;
                    document.getElementById('load').style.display="none";
                    document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 255 + ',' + 255 + ',' + 255 + ')';

                    var myJSON = JSON.stringify(data);
                    var JSONObject = JSON.parse(myJSON);
                    var category_count = JSONObject['category_list']['Category'].length;

                    var opts1 = "<option value='0'>Please Select</option>";
                    for (i = 0; i < category_count; i++) {
                        var category_name = JSONObject['category_list']['Category'][i]['title'];
                        var category_id = JSONObject['category_list']['Category'][i]['id'];

                        var selec1 = "";
                        if (i==0){ selec1 = "selected";}
                        opts1 +="<option value='"+category_id+"' selec1>"+category_name+"</option>";
                    }
                    document.getElementById('category').innerHTML = opts1;
                    document.getElementById('select2-category-container').innerHTML = "Please Select";
                    document.getElementById('cat_data').value=myJSON;
                }
            });
        }
    }

    function get_sub_cat(category_id){
        document.getElementById('select2-sub_category-container').innerHTML='Select Sub Category';
        $("#service_type").select2('val', '');
        var MyJson = document.getElementById('cat_data').value;
        var JSONObject = JSON.parse(MyJson);
        var category_count = JSONObject['category_list']['Category'].length;
        var opts2 = "<option value='0'>Please Select</option>";

        for (i = 0; i < category_count; i++) {
            if(category_id==JSONObject['category_list']['Category'][i]['id']){
                var sub_count = JSONObject['category_list']['Category'][i]['subcategory'].length;
                for (j = 0; j < sub_count; j++) {
                    var subcategory_name = JSONObject['category_list']['Category'][i]['subcategory'][j]['title'];
                    var subcategory_id = JSONObject['category_list']['Category'][i]['subcategory'][j]['id'];

                    var selec2 = "";
                    if (j==0){ selec2 = "selected";}
                    opts2 +="<option value='"+subcategory_id+"' selec2>"+subcategory_name+"</option>";
                }
            }
        }
        document.getElementById('sub_category').innerHTML = opts2;
        document.getElementById('select2-sub_category-container').innerHTML = "Please Select";
        var salon_id = document.getElementById('salon_ids').value;
        document.getElementById('category_id').value = category_id;

        $.ajax({
            url: '{{ url('get_services') }}',
            type:'post',
            data:{"_token": '{{csrf_token()}}','salon_id':salon_id,'category_id':category_id,'type':'cat_service_list'},
            success:function(data)
            {
                document.getElementById('service_type').innerHTML=data;
            }
        });

    }

    function get_sub_services(sub_cat_id) {

        $("#service_type").select2('val', '');
        var salon_id = document.getElementById('salon_ids').value;
        var category_id = document.getElementById('category_id').value;

        $.ajax({
            url: '{{ url('get_services') }}',
            type:'post',
            data:{"_token": '{{csrf_token()}}','salon_id':salon_id,'sub_cat_id':sub_cat_id,'type':'sub_cat_service_list','category_id':category_id},
            success:function(data)
            {
                document.getElementById('service_type').innerHTML=data;
            }
        });
    }

    function do_booking() {

        /*   VALIDATION */
        var flag = 0;
        if(document.getElementById("username").value.split(" ").join("") == "")
        {
            document.getElementById('username_msg').innerHTML = 'The username field is required.';
            document.getElementById("username").focus();
            document.getElementById("username").style.borderColor ='#dd4b39';
            document.getElementById("username_lb").style.color ='#dd4b39';
            document.getElementById('username_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("username").style.borderColor ='#d2d6de';
            document.getElementById("username_lb").style.color ='#333';
            document.getElementById('username_msg').style.display = 'none';
        }

        if(document.getElementById("user_phone").value.split(" ").join("") == "")
        {
            document.getElementById('user_phone_msg').innerHTML = 'The phone field is required.';
            document.getElementById("user_phone").focus();
            document.getElementById("user_phone").style.borderColor ='#dd4b39';
            document.getElementById("user_phone_lb").style.color ='#dd4b39';
            document.getElementById('user_phone_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            var phone1=document.getElementById("user_phone").value;
            if(isNaN(phone1))
            {
                document.getElementById('user_phone_msg').innerHTML = 'Please enter a valid mobile number.';
                document.getElementById("user_phone").focus();
                document.getElementById("user_phone").style.borderColor ='#dd4b39';
                document.getElementById("user_phone_lb").style.color ='#dd4b39';
                document.getElementById('user_phone_msg').style.display = 'block';
                flag=1;
            }
            else
            {
                document.getElementById("user_phone").style.borderColor ='#d2d6de';
                document.getElementById("user_phone_lb").style.color ='#333';
                document.getElementById('user_phone_msg').style.display = 'none';
            }
        }

        if(document.getElementById("datepicker_walkin").value.split(" ").join("") == "")
        {
            document.getElementById('user_bdate_msg').innerHTML = 'The booking date field is required.';
            document.getElementById("datepicker_walkin").focus();
            document.getElementById("datepicker_walkin").style.borderColor ='#dd4b39';
            document.getElementById("user_bdate_lb").style.color ='#dd4b39';
            document.getElementById('user_bdate_msg').style.display = 'block';
            flag=1;
        }
        else
        {
            document.getElementById("datepicker_walkin").style.borderColor ='#d2d6de';
            document.getElementById("user_bdate_lb").style.color ='#333';
            document.getElementById('user_bdate_msg').style.display = 'none';
        }

        var checked = 0;
        var n = document.getElementById("service_type").options.length;
        for (var i = 0;i < n; i++){
            if (document.getElementById("service_type").options[i].selected === true) {
                checked = 1;
            }
        }

        if(checked==0){
            document.getElementById('service_msg').innerHTML = 'No service is selected';
            document.getElementById("service_lb").style.color ='#dd4b39';
            document.getElementById('service_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById("service_lb").style.color ='#333';
            document.getElementById('service_msg').style.display = 'none';
        }


        if (grecaptcha.getResponse() == ""){

            document.getElementById('bcaptche_msg').innerHTML = 'Please select captcha to proceed!';
            document.getElementById('bcaptche_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById('bcaptche_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }

        /* ---------------------------------- */

        var name = document.getElementById('username').value;
        var country = document.getElementById('user_country').value;
        var phone = document.getElementById('user_phone').value;
        var phone_number = country+phone;
        var email = document.getElementById('user_email').value;
        var bdate = document.getElementById('datepicker_walkin').value;
        var salon_id = document.getElementById('salon_ids').value;
        var no_of_persons = document.getElementById('no_of_persons').value;

        var mode = $("#service_type option:selected").map(function(){return this.value}).get().join(',');
        var service_type = mode;

        var user_id = 0;
        var emp_id = document.getElementById('emp_id').value;

        document.getElementById('inner-loder').style.display="block";
        document.getElementById('booking_form').style.display="none";
        document.getElementById('booking_success').style.display="none";

        $.ajax({
            url: '{{ url('do_booking') }}',
            type:'post',
            data:{"_token": '{{csrf_token()}}','salon_id':salon_id,'service_type':service_type,'no_of_persons':no_of_persons,'name':name,'phone':phone_number,'email':email,'date':bdate,'user_id':user_id,'emp_id':emp_id},
            success:function(data)
            {
                var myJSON = JSON.stringify(data);
                var JSONObject = JSON.parse(myJSON);
                if (JSONObject["Result"]==1){
                    document.getElementById('booking_form').style.display="none";
                    document.getElementById('booking_success').style.display="block";
                    document.getElementById('inner-loder').style.display="none";
                }
                else{
                    document.getElementById('booking_success_msg').innerHTML=JSONObject['Message'];
                }
            }
        });
    }

</script>
<script>
    function call_sub_cat(cid){


        //alert(cid);
        $.ajax({
            url: '{{ url('set_category_session') }}',
            type:'post',
            data:{"_token": '{{csrf_token()}}','cid':cid,'type':'category'},
            success:function(data)
            {
                $('#close_lbox').trigger('click');
                if(data=='empty_data'){
                    //var sid = document.getElementById('sid').value;
                    document.getElementById('cid').value=cid;
                    document.getElementById('sid').value = "";
                    // light box close code and reload map //
                    initMap('options');

                }
                else{
                    document.getElementById('sub_cat_div_'+cid).style.display='block';
                    document.getElementById('cat_div').style.display='none';
                    //initMap();
                }
            }
        });
    }

    function back_to_cat(cid){
        document.getElementById('sub_cat_div_'+cid).style.display='none';
        document.getElementById('cat_div').style.display='block';
    }

    function set_category_session(sid){
        $.ajax({
            url: '{{ url('set_category_session') }}',
            type:'post',
            data:{"_token": '{{csrf_token()}}','sid':sid,'type':'sub_category'},
            success:function(data)
            {
                document.getElementById('cid').value = "";
                document.getElementById('sid').value = sid;
                initMap('options');
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap&libraries=places" async defer></script>