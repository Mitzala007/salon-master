<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Text Rotate -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/simpletextrotator.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <!--******************CSS FOR COUNTRY FLAG******************-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/dd.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/flags.css') }}" />

    <style>

        select{
            display: block;
            width: 100%;
            height: 50px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #d9d9d9;
            border-radius: 5px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        @media only screen and (max-device-width: 480px) {
            .mob_reg {
                display: none !important;
            }

            .break_cont {
                display: none !important;
            }
        }
        #loadingDiv{
            position:fixed;
            top:0;
            right:0;
            width:100%;
            height:100%;
            background-color:#fff;
            background-image:url('{{url('assets/website/images/91.gif')}}');
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            /*opacity: 0.8;*/
            filter: alpha(opacity=40); /* For IE8 and earlier */
        }
        .centered {
            position: absolute;
            top: 58%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

    </style>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>
    <![endif]-->
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<div id="loadingDiv" style="display: none;">
    <div class="centered">
        <h1>Please Wait For Your Payment Process...</h1>
    </div>
</div>
<!-- =========================
     HEADER SECTION
     ========================= -->
<header id="intro" style="background: none; min-height:0">
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a data-scroll href="{{url('/register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME {{config('siteVars.sitetitle1')}}
                    </button>
                </a>


                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
                {{--<a class="navbar-brand" data-scroll="" href="#intro"><span>GET FREE APP TODAY</span>Apps</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('/register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME {{config('siteVars.sitetitle1')}}</button></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header><!-- /END HEADER -->


<!-- =========================
        FEATURES
    ========================== -->
<section id="main_form" style="padding-top: 2%; padding-bottom: 2%">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading" style="margin-bottom: 0px;">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Payment Form</h2>
            </div>
        </div>
        <div class="row">
            <div>


                <form style="border: none;" class="col-md-8 col-sm-8 com-md-offset-2 col-sm-offset-2" action="{{ url('checkout') }}" method="post">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <input type="hidden" name="user_id" value="{{$user_id}}" id="user_id">
                    <input type="hidden" name="amount" value="59.99" id="amount">
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('cardname') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Name On Card<span class="asteriskField">*</span></label>
                            <div class="controls">
                                {!! Form::text('cardname', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter Card Name','id'=>'cardname']) !!}
                                @if ($errors->has('cardname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cardname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('cardnumber') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Credit card number<span class="asteriskField">*</span></label>
                            <div class="controls ">
                                <div name="card-container">
                                    <input type="text" id="" name="cardnumber" placeholder="" class="textinput textInput form-control">
                                    <div id="logo"></div>
                                </div>
                                @if ($errors->has('cardnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cardnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('expmonth') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Exp Month<span class="asteriskField">*</span></label>
                            <div class="controls">
                                <select name="expmonth" id="expmonth" class="select form-control" >
                                    <option>Please Select</option>
                                    <?php
                                    for($m=1; $m<=12; ++$m){
                                        echo '<option value="'.$m.'">'.date('F', mktime(0, 0, 0, $m, 1)).'</option>';
                                    }
                                    ?>
                                </select>
                                @if ($errors->has('expmonth'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('expmonth') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('expyear') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Exp Year<span class="asteriskField">*</span></label>
                            <div class="controls">
                                <select name="expyear" id="expyear" class="form-control">
                                    <option>Please Select</option>
                                    <?php
                                    for ($i = 2019; $i <= 2050; ++$i){
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                </select>
                                @if ($errors->has('expyear'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('expyear') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">CVV<span class="asteriskField">*</span></label>
                            <div class="controls">
                                {!! Form::text('cvv', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter CVV','id'=>'cvv']) !!}
                                @if ($errors->has('cvv'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cvv') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{--<button class="btn btn-primary1 center-block" style="height: 34px; font-size: 18px; padding: 4px 12px;" id="payment_btn" onclick="checkout();">Payment</button>--}}
                    <button class="btn btn-primary1 center-block" style="height: 34px; font-size: 18px; padding: 4px 12px;" id="payment_btn" type="submit">Payment</button>
                </form>
                </fieldset>

                {{--<script src="{{ URL::asset('assets/website/js/vanilla-masker.min.js')}}"></script>--}}
                {{--<script src="{{ URL::asset('assets/website/js/app.bundle.js')}}"></script>--}}
            </div>

        </div>
    </div><!-- End Container -->
</section><!-- /END Feature Three -->

<!-- =========================
    FOOTER
    ========================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">{{config('siteVars.sitetitle')}}</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Container -->
</footer><!-- /END FOOTER SECTION -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/website/js/vanilla-masker.min.js')}}"></script>
<script src="{{ URL::asset('assets/website/js/app.bundle.js')}}"></script>

{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>--}}
        <!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>

<script>
    $(function () {

        $(".select2").select2();

        $('#datepicker').datepicker({
            format: 'yyyy-m-d',
            autoclose: true
        });
    });
</script>

<!-- datepicker -->
<script src="{{ URL::asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>


<!--***************************REDIRECT TO PAYMENT METHOD BY USING AJAX***************************-->
<script>
    function checkout()
    {
        var token = document.getElementById('token').value;
        var cardname = document.getElementById('cardname').value;
        var cardno = document.getElementById('card').value;
        var expmonth = document.getElementById('expmonth').value;
        var expyear = document.getElementById('expyear').value;
        var cvv = document.getElementById('cvv').value;
        var user_id = document.getElementById('user_id').value;
        var amount = document.getElementById('amount').value;

        document.getElementById('loadingDiv').style.display='block';

        $.ajax({
            url: '{{url('/checkout')}}',
            type: "POST",
            data: {"_token": token,"cardname":cardname,"cardnumber":cardno,"expmonth":expmonth,"expyear":expyear,"cvv":cvv,"user_id":user_id,"amount":amount,},
            success: function(data){
                alert(data);
//                document.getElementById('loadingDiv').style.display='none';
                var new_url = '{{url('thank-you')}}';
                window.location.href = new_url;
            }
        });
    }
</script>
</body>
</html>
