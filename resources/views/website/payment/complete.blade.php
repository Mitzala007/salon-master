@extends('website.layout')
@section('content')

    <section class="app-description" style="padding: 0px 0 70px 0;!important;">
        <div class="container">
            <div class="row">
                <br>
                <center><img height="100" src="{{ URL::asset('assets/imgpsh_fullsize.png')}}"  alt=""></center><br>
                <div class="col-md-12 heading">
                    <p style="font-size: 17px;color: black;">We have received your application! If you pass you will receive an SMS invite to an interview session. </p>
                </div>
                <br><br>
                <a href="{{url('/')}}" style="text-decoration: none;">
                    <button class="btn btn-primary center-block" style="height: 60px;font-size: 24px;">Go To Home</button>
                </a>
            </div>
        </div>
    </section>

@endsection