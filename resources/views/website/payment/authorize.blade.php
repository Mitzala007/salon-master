<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/payment_form_style.css') }}">
</head>
<body>
<h2 align="center">Payment Form</h2>
<div class="row">
    <div class="col-75">
        {!! Form::open(['url' => url('/checkout'), 'class' => 'form-horizontal','files'=>true]) !!}
            <input type="hidden" name="user_id" value="{{$user_id}}">
            <input type="hidden" name="amount" value="{{$amount}}">
        <div class="row">
            <div class="col-50">
                <h3>Payment</h3>
                <label for="fname">Accepted Cards</label>
                <div class="icon-container">
                    <i class="fa fa-cc-visa" style="color:navy;"></i>
                    <i class="fa fa-cc-amex" style="color:blue;"></i>
                    <i class="fa fa-cc-mastercard" style="color:red;"></i>
                    <i class="fa fa-cc-discover" style="color:orange;"></i>
                </div>
                <label for="cname">Name on Card</label>
                <input type="text" id="cname" name="cardname" placeholder="">
                <label for="ccnum">Credit card number</label>
                <div name="card-container">
                    <input type="text" id="card" name="cardnumber" placeholder="">
                    <div id="logo"></div>
                </div>
                <label for="expmonth">Exp Month</label>
                {!! Form::selectMonth('expmonth',['id'=>'expmonth']) !!}
                <div class="row">
                    <div class="col-50">
                        <label for="expyear">Exp Year</label>
                        {!! Form::selectYear('expyear', 2019, 2050, ['id'=>'expyear']) !!}
                    </div>
                    <div class="col-50">
                        <label for="cvv">CVV</label>
                        <input type="text" id="cvv" name="cvv" placeholder="">
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" value="Continue" class="btn">
        {!! Form::close() !!}
        <script src="{{ URL::asset('assets/website/js/vanilla-masker.min.js')}}"></script>
        <script src="{{ URL::asset('assets/website/js/app.bundle.js')}}"></script>
    </div>
</div>
</body>
</html>
