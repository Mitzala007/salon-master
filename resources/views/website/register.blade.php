<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/datepicker3.css')}}">

    <!-- Text Rotate -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/simpletextrotator.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <!--******************CSS FOR COUNTRY FLAG******************-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/dd.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/website/css/msdropdown/flags.css') }}" />
    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <style>
        ul{
            font-size: 120%;
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- =========================
     HEADER SECTION
     ========================= -->
<header id="intro" style="background: none; min-height:0">
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a data-scroll href="{{url('/register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME {{config('siteVars.sitetitle1')}}
                    </button>
                </a>


                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
                {{--<a class="navbar-brand" data-scroll="" href="#intro"><span>GET FREE APP TODAY</span>Apps</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('/register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME {{config('siteVars.sitetitle1')}}</button></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header><!-- /END HEADER -->


<!-- =========================
        FEATURES
    ========================== -->
<section id="" style="padding-top: 2%; padding-bottom: 2%">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading" style="margin-bottom: 0px;">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Personal Information</h2>
            </div>
        </div>
        <div class="row">
            <div>
                {!! Form::open(['url' => url('register_user'), 'class' => 'form-horizontal','files'=>true,'onSubmit'=>'javascript:return register_validation();']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset style="border: none;" class="col-md-8 col-sm-8 com-md-offset-2 col-sm-offset-2">
                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">First Name
                                <span class="asteriskField">*</span>
                            </label>
                            <div class="controls">
                                {!! Form::text('name', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter First Name','id'=>'name','required']) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Last Name
                                {{--<span class="asteriskField">*</span>--}}
                            </label>
                            <div class="controls ">
                                {!! Form::text('last_name', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter Last Name','id'=>'last_name']) !!}
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Email
                                <span class="asteriskField">*</span>
                            </label>
                            <div class="controls ">
                                {!! Form::text('email', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter Email Address','id'=>'email','required']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}" style="margin-bottom: 0px;">
                            <label class="control-label requiredField">Phone
                                <span class="asteriskField">*</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <div class="controls col-md-4 cou-padding">
                            <select name="countries" id="countries" class="select form-control" style="width: 100%" required>
                                <option value='1' @if(isset($code) && $code == 1) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag us" data-title="United State">United State</option>
                                <option value='91' @if(isset($code) && $code == 91) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag in" data-title="India">India</option>
                                <option value='61' @if(isset($code) && $code == 61) selected @endif data-image="{{url('assets/dist/images/msdropdown/icons/blank.gif')}}" data-imagecss="flag au" data-title="Australia">Australia</option>
                            </select>
                        </div>
                        <div class="controls col-md-8">
                            {!! Form::text('phone', null, ['class' => 'textinput textInput form-control', 'placeholder' => 'Enter Phone','id'=>'phone','required']) !!}
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Date of Birth
                                {{--<span class="asteriskField">*</span>--}}
                            </label>
                            <div class="controls ">
                                {!! Form::text('birthdate', null, ['class' => 'textinput textInput form-control', 'id'=>'datepicker', 'placeholder' => 'Date of Birth','required']) !!}
                                @if ($errors->has('birthdate'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('birthdate') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Password
                                <span class="asteriskField">*</span>
                            </label>
                            <div class="controls ">
                                <input type="password" placeholder="Enter Password" autocomplete="off"  id="password" name="password" class="textinput textInput form-control" required >
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="control-label requiredField">Confirm Password
                                <span class="asteriskField">*</span>
                            </label>
                            <div class="controls ">
                                <input type="password" placeholder="Enter Confirm Password" autocomplete="off"  id="password_confirmation" name="password_confirmation" class="textinput textInput form-control" required >
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12" style="padding-top: 1.5%">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="controls ">
                                <div class="g-recaptcha" data-sitekey="{{config('siteVars.captcha_key')}}"></div>
                                <strong><span id="captche_msg" style="color: #a94442; display: none;">Please select captcha to proceed!</span></strong>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary1 center-block" style="height: 34px; font-size: 18px; padding: 4px 12px;">Submit & Next</button>

                </fieldset>



                {!! Form::close() !!}
            </div>

        </div>
    </div><!-- End Container -->
</section><!-- /END Feature Three -->

<!-- =========================
    FOOTER
    ========================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">{{config('siteVars.sitetitle1')}}</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Container -->
</footer><!-- /END FOOTER SECTION -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/dist/js/msdropdown/jquery.dd.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $("#countries").msDropdown();
    })
</script>

{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>--}}
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>

<script>
    $(function () {

        $(".select2").select2();

        $('#datepicker').datepicker({
            format: 'yyyy-m-d',
            autoclose: true
        });
    });
</script>

<!-- datepicker -->
<script src="{{ URL::asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>



<script type="text/javascript">
    function AjaxUploadImage(obj,id){

        var file = obj.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
        {
            $('#previewing'+URL).attr('src', 'noimage.png');
            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
            return false;
        } else{
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(obj.files[0]);
        }
    }
    function imageIsLoaded(e) {

        $('#DisplayImage').css("display", "block");
        $('#DisplayImage').attr('src', e.target.result);
        $('#DisplayImage').attr('width', '150');

    };
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#country").change(function() {
            if ($(this).val() != '') {
                $.ajax({
                    url: '{{ url('get_states') }}/' + $(this).val(),
                    error: function () {
                    },
                    success: function (result) {
                        $("#state_id").html(result);
                    }
                });
            } else {

                $("#state_id").empty();
                $("#state_id").html('');
            }
        });
        $("#state_id").change(function() {
            if ($(this).val() != '') {
                var cc = $('#country').val();
                $.ajax({
                    url: '{{ url('get_cities') }}/'+cc+'/'+$(this).val(),
                    error: function () {
                    },
                    success: function (result) {
                        //alert(result);
                        $("#city_id").html(result);
                    }
                });
            } else {

                $("#city_id").empty();
                $("#city_id").html('');

            }
        });
    });
</script>

<script>
    function register_validation()
    {
        var flag = 0;
        if (grecaptcha.getResponse() == ""){
            document.getElementById('captche_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById('captche_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>


</body>
</html>
