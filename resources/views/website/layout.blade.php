<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="RxApps is a App Landing Page">
    <meta name="keywords" content="RxApps, Bootstrap, Landing page, Template, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/sweepsouth_2.png') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">
    {{--<link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/venobox.css') }}">--}}

    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>
</head>
<body>

<header id="intro">
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a data-scroll href="{{url('register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME CLEANER
                    </button>
                </a>


                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
                {{--<a class="navbar-brand" data-scroll="" href="#intro"><span>GET FREE APP TODAY</span>Apps</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME CLEANER</button></a></li>
                    {{--<li><a data-scroll href="{{url('register')}}">Register</a></li>--}}
                </ul>
            </div>
        </div>
    </nav>

    <!-- =========================
            Intro
        ========================== -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="intro-text">
                        <h1 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">BECOME AN INDEPENDENT CLEANING PRO AND EARN <font color="green">ON THE GO</font></h1>
                        <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Available for IOS &amp; Android </h4>
                        <a data-scroll href="#" class="btn btn-primary1 btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s"><i class="fa fa-cloud-download"></i> Download</a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="intro-image">
                        <div class="mobile-light wow slideInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <img src="{{ URL::asset('assets/website/images/mockup/screen_1.png') }}" alt="" class="img-responsive" />
                        </div>
                        <div class="mobile-dark wow slideInUp"  data-wow-duration="1.5s" data-wow-delay=".6s">
                            <img src="{{ URL::asset('assets/website/images/mockup/screen_2.png') }}" alt="" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Container -->
    </section>
</header><!-- /END HEADER -->
<section style="background-color: #ffffff!important;" id="app-features">
    <div class="container">
        <div class="row">
            <div class="row form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                        <li @if(isset($submenu) && $submenu=='Step1') class="active" @else class="disabled" @endif><a href="javascript:;">
                                <h4 class="list-group-item-heading" @if(isset($submenu) && $submenu=='Step1') style="color: #ffffff;" @endif>Step 1</h4>
                                <p class="list-group-item-text" @if(isset($submenu) && $submenu=='Step1') style="color: #ffffff;" @endif>First step Personal Info</p>
                            </a></li>
                        <li @if(isset($submenu) && $submenu=='Step2') class="active" @else class="disabled" @endif><a href="javascript:;">
                                <h4 class="list-group-item-heading" @if(isset($submenu) && $submenu=='Step2') style="color: #ffffff;" @endif>Step 2</h4>
                                <p class="list-group-item-text" @if(isset($submenu) && $submenu=='Step2') style="color: #ffffff;" @endif>Second step Extra Info</p>
                            </a></li>
                        <li @if(isset($submenu) && $submenu=='Step3') class="active" @else class="disabled" @endif><a href="javascript:;">
                                <h4 class="list-group-item-heading" @if(isset($submenu) && $submenu=='Step3') style="color: #ffffff;" @endif>Step 3</h4>
                                <p class="list-group-item-text" @if(isset($submenu) && $submenu=='Step3') style="color: #ffffff;" @endif>Third step Want To Work</p>
                            </a></li>
                        <li @if(isset($submenu) && $submenu=='Step4') class="active" @else class="disabled" @endif><a href="javascript:;">
                                <h4 class="list-group-item-heading" @if(isset($submenu) && $submenu=='Step4') style="color: #ffffff;" @endif>Step 4</h4>
                                <p class="list-group-item-text" @if(isset($submenu) && $submenu=='Step4') style="color: #ffffff;" @endif>Fourth step Who are you?</p>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@yield('content')


<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">Clean On The Go</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>

<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>

@yield('jquery')

<script type="text/javascript">
    function autocompletesearch(){
        var input = document.getElementById('autosearch');
        var autocom = new google.maps.places.Autocomplete(input);
    }


</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeSdAVIXo8HCE0R5rb6eBkj9z3eUiIeWY&libraries=places&callback=autocompletesearch"></script>


</body>
</html>
