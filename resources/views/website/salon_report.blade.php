<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('siteVars.sitetitle')}} </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/skins/_all-skins.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition  sidebar-m">
        <section class="content ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><h2 class="text-center">Booking Report</h2></div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="padding:0">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center" style="padding:0">
                            <h4>Today's booking</h4>
                            <h4 >{{$today_booking_today}}</h4>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6  text-center"  style="padding:0">
                            <h4>Today's earning </h4>
                            <h4>{{$total_earning_today}}</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                        <h3 class="text-center">Employee wise booking</h3>
                        </p>

                        @foreach($employee_list as $lists)
                            <?php $link = "Master://".$lists['user_id'].'/'.$lists['salon_id']; ?>
                            <div class="progress-group" onclick="window.location.href='{{$link}}'" style="cursor:pointer;" >
                                <span class="progress-text">{{ $lists['employee_name'] }}</span>
                                <span class="progress-number"><b>{{$lists['employee_booking_count']}}</b>/{{$total_booking_monthly}}</span>
                                <div class="progress sm">
                                    <div class="progress-bar @if($lists['total_percentage'] < 20) progress-bar-red @elseif($lists['total_percentage'] >= 20 && $lists['total_percentage'] < 70) progress-bar-yellow @elseif($lists['total_percentage'] >= 70) progress-bar-green @endif "  style="width: {{$lists['total_percentage']}}%"></div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="col-md-12" style="padding:0">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-center" style="padding:0">
                            <h4>Total booking</h4>
                            <h4 >{{$total_booking_monthly}}</h4>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6  text-center"  style="padding:0">
                            <h4>Total earning </h4>
                            <h4>{{$total_earning_monthly}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <i class="fa fa-bar-chart-o"></i>
                                <h3 class="box-title">Chart Monthly</h3>
                            </div>
                            <div class="box-body">
                                <div id="bar-chart" style="height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
        <script src="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/fastclick/fastclick.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/jQuery/jquery.flot.categories.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/jQuery/jquery.flot.min.js')}}"></script>
<script>
    $(function () {
        var bar_data = {
            data : {!! json_encode($chart_data) !!},
            color: '#3c8dbc'
        }
        $.plot('#bar-chart', [bar_data], {
            grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
            },
            series: {
                bars: {
                    show    : true,
                    barWidth: 0.3,
                    align   : 'center'
                }
            },
            xaxis : {
                mode      : 'categories',
                tickLength: 0
            }
        })
    })
    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + '<br>'
                + Math.round(series.percent) + '%</div>'
    }
</script>
</body>
</html>
