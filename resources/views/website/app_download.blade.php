<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

            <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>


</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- =========================HEADER SECTION========================= -->
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a data-scroll href="{{url('/register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME {{config('siteVars.sitetitle1')}}
                    </button>
                </a>
                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('/register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME {{config('siteVars.sitetitle1')}}</button></a></li>
                </ul>
            </div>
        </div>
    </nav>

<!-- =========================Download App ========================== -->
<section class="app-downloads" id="app-download">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Get Free APP TODAY<span></span></h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Download your app now and become {{config('siteVars.sitetitle')}}</p>
            </div>
            <div class="col-sm-12 app-download-btns wow bounceInDown" data-wow-duration="1.5s" data-wow-delay=".3s">
                <ul class="list-unstyled">
                    <li><a href="{{config('siteVars.itunes_link')}}" target="_blank"><i class="fa fa-apple"></i> App Store</a></li>
                    <li><a href="{{config('siteVars.play_store_link')}}" target="_blank"><i class="fa fa-android"></i> Play Store</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>



<!-- =========================FOOTER========================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">{{config('siteVars.sitetitle')}}</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Container -->
</footer><!-- /END FOOTER SECTION -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>
<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>

</body>
</html>
