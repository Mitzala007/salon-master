<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Text Rotate -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/simpletextrotator.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- =========================
     HEADER SECTION
     ========================= -->
<header id="intro" style="background: none; min-height:0">
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a data-scroll href="{{url('/register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME NAIL MASTER
                    </button>
                </a>


                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
                {{--<a class="navbar-brand" data-scroll="" href="#intro"><span>GET FREE APP TODAY</span>Apps</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('/register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME NAIL MASTER</button></a></li>
               </ul>
            </div>
        </div>
    </nav>
</header><!-- /END HEADER -->

<section class="blog-wrapper policy-p" style="padding-bottom: 30px;">
    <div class="container">

        <div class="">
            <div class="col-md-12 heading" style="padding-top: 35px; margin-bottom: 20px;">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Privacy Policy</h2>
            </div>

            <div class="article-content">
                <p>Effective date: November 23, 2018</p>

                <p>{{config('siteVars.url_text')}} LLC ("us", "we", or "our") operates the {{config('siteVars.siteurl')}} website and the {{config('siteVars.sitetitle')}} mobile application (the "Service").</p>

                <p>This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data. Our Privacy Policy for {{config('siteVars.url_text')}} LLC is managed through Free Privacy Policy Website.</p>

                <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p>
            </div>

            <div class="article-header">
                <h2>Information Collection And Use</h2>
            </div>
            <div class="article-content">
                <p>We collect several different types of information for various purposes to provide and improve our Service to you.</p>
            </div>

            <div class="article-header">
                <h2>Types of Data Collected</h2>
            </div>
            <div class="article-content">
                <p>Personal Data</p>

                <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p>

                <div class="widget-content col-md-12">
                    <ul class="list-unstyled archives">
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> Email address</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> First name and last name</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> Phone number</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> Address, State, Province, ZIP/Postal code, City</p></li>
                        <li><p><i class="fa fa-angle-right privacy-data"></i> Cookies and Usage Data</p></li>
                    </ul>
                </div>

                <p>Usage Data</p>

                <p>We may also collect information that your browser sends whenever you visit our Service or when you access the Service by or through a mobile device ("Usage Data").</p>

                <p>This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>

                <p>When you access the Service by or through a mobile device, this Usage Data may include information such as the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data.</p>

                <p>Tracking & Cookies Data</p>

                <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>

                <p>Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>

                <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>

                <p>Examples of Cookies we use:</p>

                <div class="widget-content col-md-12" style="padding-bottom: 6px;">
                    <ul class="list-unstyled archives">
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> <b>Session Cookies.</b> We use Session Cookies to operate our Service.</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> <b>Preference Cookies.</b> We use Preference Cookies to remember your preferences and various settings.</p></li>
                        <li><p><i class="fa fa-angle-right privacy-data"></i> <b>Security Cookies.</b> We use Security Cookies for security purposes.</p></li>
                    </ul>
                </div>
            </div>

            <div class="article-header">
                <h2>Use of Data</h2>
            </div>
            <div class="article-content">
                <p>{{config('siteVars.url_text')}} LLC uses the collected data for various purposes:</p>

                <div class="widget-content col-md-12" style="padding-bottom: 8px;">
                    <ul class="list-unstyled archives">
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To provide and maintain the Service</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To notify you about changes to our Service</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To allow you to participate in interactive features of our Service when you choose to do so</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To provide customer care and support</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To provide analysis or valuable information so that we can improve the Service</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To monitor the usage of the Service</p></li>
                        <li><p><i class="fa fa-angle-right privacy-data"></i> To detect, prevent and address technical issues</p></li>
                    </ul>
                </div>
            </div>

            <div class="article-header">
                <h2>Transfer Of Data</h2>
            </div>
            <div class="article-content">
                <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>

                <p>If you are located outside United States and choose to provide information to us, please note that we transfer the data, including Personal Data, to United States and process it there.</p>

                <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>

                <p>{{config('siteVars.url_text')}} LLC will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>
            </div>

            <div class="article-header">
                <h2>Disclosure Of Data</h2>
                <h3>Legal Requirements</h3>
            </div>
            <div class="article-content">
                <p>{{config('siteVars.url_text')}} LLC may disclose your Personal Data in the good faith belief that such action is necessary to:</p>

                <div class="widget-content col-md-12" style="padding-bottom: 6px;">
                    <ul class="list-unstyled archives">
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To comply with a legal obligation</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To protect and defend the rights or property of {{config('siteVars.url_text')}} LLC</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To prevent or investigate possible wrongdoing in connection with the Service</p></li>
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> To protect the personal safety of users of the Service or the public</p></li>
                        <li><p><i class="fa fa-angle-right privacy-data"></i> To protect against legal liability</p></li>
                    </ul>
                </div>
            </div>

            <div class="article-header">
                <h2>Security Of Data</h2>
            </div>
            <div class="article-content">
                <p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>
            </div>

            <div class="article-header">
                <h2>Service Providers</h2>
            </div>
            <div class="article-content">
                <p>We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>

                <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>
            </div>

            <div class="article-header">
                <h2>Analytics</h2>
            </div>
            <div class="article-content">
                <p>We may use third-party Service Providers to monitor and analyze the use of our Service.</p>

                <div class="widget-content col-md-12">
                    <h4><i class="fa fa-angle-right privacy-data"></i><b>Google Analytics</b></h4>

                    <div class="col-md-12">
                        <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>

                        <p>For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: https://policies.google.com/privacy?hl=en</p>
                    </div>
                </div>
            </div>

            <div class="article-header">
                <h2>Links To Other Sites</h2>
            </div>
            <div class="article-content">
                <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>

                <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
            </div>

            <div class="article-header">
                <h2>Children's Privacy</h2>
            </div>
            <div class="article-content">
                <p>Our Service does not address anyone under the age of 18 ("Children").</p>

                <p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>
            </div>

            <div class="article-header">
                <h2>Changes To This Privacy Policy</h2>
            </div>
            <div class="article-content">
                <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>

                <p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>

                <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>
            </div>

            <div class="article-header">
                <h2>Contact Us</h2>
            </div>
            <div class="article-content">
                <p>If you have any questions about this Privacy Policy, please contact us:</p>

                <div class="widget-content col-md-12">
                    <ul class="list-unstyled archives">
                        <li><p class="privacy-li"><i class="fa fa-angle-right privacy-data"></i> By email: {{config('siteVars.support_email')}} </p></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>




<!-- =========================
    FOOTER
    ========================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">{{config('siteVars.sitetitle')}}</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Container -->
</footer><!-- /END FOOTER SECTION -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>
{{--<script src="{{ URL::asset('assets/website/js/switcher.core.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.helper.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.load.js') }}"></script>--}}
        <!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>


</body>
</html>
