<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="smartbanner:title" content="{{config('siteVars.sitetitle')}} Online Check In">
    <meta name="smartbanner:author" content="{{config('siteVars.sitetitle')}}">
    <meta name="smartbanner:price" content="FREE">
    <meta name="smartbanner:price-suffix-apple" content=" - On the App Store">
    <meta name="smartbanner:price-suffix-google" content=" - In Google Play">
    <meta name="smartbanner:icon-apple" content="{{ URL::asset('assets/website/Logo_web.png') }}">
    <meta name="smartbanner:icon-google" content="{{ URL::asset('assets/website/Logo_web.png') }}">
    <meta name="smartbanner:button" content="VIEW">
    <meta name="smartbanner:button-url-apple" content="{{config('siteVars.itunes_link')}}">
    <meta name="smartbanner:button-url-google" content="{{config('siteVars.play_store_link')}}">
    <meta name="smartbanner:enabled-platforms" content="android,ios">

    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>

    <!-- SMART BANNER -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/smartbanner.css') }}">
    <script src="{{ URL::asset('assets/website/js/smartbanner.js') }}"></script>

    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/datepicker3.css')}}">


    <!-- SELECT  -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/select2/select2.min.css')}}">

    <style type="text/css">
        .select2-container .select2-selection--single {
            height: 34px !important;
        }
    </style>
    <!------------------------------------Google Recptcha Script------------------------------------>
    {{--<script src='https://www.google.com/recaptcha/api.js?render=6Lfrk4AUAAAAAN5Arh0H-mut9pfp0AJv4lqWHrux'></script>--}}
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <style>
        video{
            outline: 0;
        }
    </style>


</head>
<body id="bodyid">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- =========================
     HEADER SECTION
     ========================= -->
<header id="intro">
    <nav class="navbar" id="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rx-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a data-scroll href="{{url('/register')}}">
                    <button type="button" class="navbar-toggle btn btn-primary1" >
                        BECOME {{config('siteVars.sitetitle1')}}
                    </button>
                </a>
                <a href="{{url('/')}}"> <img class="" src="{{ URL::asset('assets/website/Logo_web.png') }}" height="60" style="margin-top: -6px;"></a>
                {{--<a class="navbar-brand" data-scroll="" href="#intro"><span>GET FREE APP TODAY</span>Apps</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="rx-navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a data-scroll href="{{url('/')}}">Home</a></li>
                    <li><a data-scroll href="{{url('/#app-features')}}">ABOUT APP</a></li>
                    <li><a data-scroll href="{{url('/#screenshots')}}">HOW IT WORKS</a></li>
                    <li><a data-scroll href="{{url('/#pricing-plan')}}">ESTIMATES</a></li>
                    <li><a data-scroll href="{{url('/#app-download')}}">Download</a></li>
                    <li><a data-scroll href="{{url('/#contact')}}">Contact</a></li>
                    <li class="active mob_reg" style="margin-top: -10px;" id="s"><a data-scroll href="{{url('/register')}}"><button class="ui-btn ui-corner-all ui-shadow btn btn-primary1 btn-block">BECOME {{config('siteVars.sitetitle1')}}</button></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- =========================
            Intro Slider
        ========================== -->




    <div id="rx-background-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="15000">


        <ol class="carousel-indicators">
            <li data-target="#rx-background-carousel" data-slide-to="0" class="active"> </li>
            <li data-target="#rx-background-carousel" data-slide-to="1"> </li>
            <li data-target="#rx-background-carousel" data-slide-to="2"> </li>
            <li data-target="#rx-background-carousel" data-slide-to="3"> </li>
        </ol>



        <div class="carousel-inner">

            <div class="item active">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="intro-text">
                                <h1 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s">Check-In</h1>
                                <ul style="list-style-type:square">
                                    <li>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Use our check-in service to reserve your time </h4>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">slot to further reduce your wait time without </h4>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">having to call in during business hours. </h4>

                                        <div class="intro-text" style="margin-top: 20px">
                                            <a data-scroll href="#check-in" class="btn btn-success btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><i class="fa fa-cloud-download"></i> Check In</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="intro-image">
                                <div class="mobile-light wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Checkin3.png') }}" alt="" class="img-responsive" />
                                </div>
                                <div class="mobile-dark wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".6s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Map2.png') }}" alt="" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Container -->
            </div>

            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="intro-text">
                                <h1 class="wow animated zoomIn" data-wow-duration="1.5s" data-wow-delay=".2s">Getting Started</h1>
                                <ul style="list-style-type:square">
                                    <li>
                                        <h4 class="wow animated zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Tap the “Check in” icon to be brought to your local map.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Pick the salon that is perfect for you.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">You’ll have access to information such as promotional events, service menu, and client reviews to help you further make your decision</h4>
                                    </li>

                                    <div class="intro-text" style="margin-top: 20px">
                                        <a data-scroll href="#check-in" class="btn btn-success btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><i class="fa fa-cloud-download"></i> Check In</a>
                                    </div>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="intro-image">
                                <div class="mobile-light wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/S1.png') }}" alt="" class="img-responsive" />
                                </div>
                                <div class="mobile-dark wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".6s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Menu2.png') }}" alt="" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Container -->
            </div>

            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="intro-text">
                                <h1 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s">Salon Profile</h1>
                                <ul style="list-style-type:square">
                                    <li>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">After selecting a salon, go ahead and choose your service.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">You can also pick from an available list of technicians.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Tap “Check Me In” to finalize your booking.</h4>
                                    </li>

                                    <div class="intro-text" style="margin-top: 20px">
                                        <a data-scroll href="#check-in" class="btn btn-success btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><i class="fa fa-cloud-download"></i> Check In</a>
                                    </div>

                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="intro-image">
                                <div class="mobile-light wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/SalonInfo3.png') }}" alt="" class="img-responsive" />
                                </div>
                                <div class="mobile-dark wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".6s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Checkmein1.png') }}" alt="" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Container -->
            </div>

            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="intro-text">
                                <h1 class="wow animated fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">Our Other Features</h1>
                                <ul style="list-style-type:square">
                                    <li>
                                        <h4 class="wow animated fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Chat with your technicians before and after your booking.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">Get notifications for recurring appointments.</h4>
                                    </li>
                                    <li>
                                        <h4 class="wow animated fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" style="list-style-type:square">And much more!</h4>
                                    </li>

                                    <div class="intro-text" style="margin-top: 20px">
                                        <a data-scroll href="#check-in" class="btn btn-success btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><i class="fa fa-cloud-download"></i> Check In</a>
                                    </div>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="intro-image">
                                <div class="mobile-light wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Chat.png') }}" alt="" class="img-responsive" />
                                </div>
                                <div class="mobile-dark wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".6s">
                                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Reminder1.png') }}" alt="" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Container -->
            </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    {{--<div class="intro-text" style="position: relative; text-align: center; margin-top: 10px">--}}
                    {{--<a data-scroll href="#app-download" class="btn btn-success btn-lg wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><i class="fa fa-cloud-download"></i> Check In Now</a>--}}
                    {{--</div>--}}

                    <div class="intro-text" style="position: relative; text-align: center; margin-top: -10px; z-index: 9999" >
                        <img src="{{url('assets/website/images/arrow.gif')}}"  >
                    </div>


                </div>
            </div>
        </div>
    </section>
</header><!-- /END HEADER -->

@include('website.checkin')


<!-- =========================
        FEATURES
    ========================== -->
<section id="app-features">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">About Our App</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s"><font class="theme_color">{{config('siteVars.sitetitle')}}</font> is a location-based application and web service which allows for real-time interconnectivity between clients and nail salons. We strive to make running salons easily with a user-friendly application that you and your clients would love.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 features-left wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                <div class="single-feature sf-top">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 single-feature-content">
                            <h3>Check-In</h3>
                            <p>See estimated wait time and availability of the salons near you. Use our check-in service to reserve your time slot to further reduce your wait time without having to call in during business hours. Know exactly your spot in line with our digital display accessible on any device.</p>
                        </div>
                        <div class="col-md-4 col-sm-6 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-desktop" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-feature sf-center">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 single-feature-content">
                            <h3>Dynamic Dashboard</h3>
                            <p>Our dashboard display everything you would need to manage your salon on your phone! You can see upcoming bookings, employees’ current status, send messages and notifications, and so much more.</p>
                        </div>
                        <div class="col-md-4 col-sm-6 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-feature sf-bottom">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 single-feature-content">
                            <h3>Engage with Clients</h3>
                            <p>Turn new clients into regulars with thorough engagement. Remind clients of recurring appointments and reduce no-shows with conformational email, text, or push notification. There are endless opportunities with much more features to come.</p>
                        </div>
                        <div class="col-md-4 col-sm-6 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-gears" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End .col-md-4 -->
            <div class="col-md-4 featured-center wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                <img src="{{ URL::asset('assets/website/images/mockup/new_screen/Map2.png') }}" class="img-responsive" alt="app-o2" style="margin-top: 30%;">
            </div>
            <div class="col-md-4 features-right wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">
                <div class="single-feature sf-top">
                    <div class="row">
                        <div class="col-sm-4 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 single-feature-content">
                            <h3>Go Ahead and Chat!</h3>
                            <p style="margin-bottom: 9%">Our messaging system allows for communication between clients and their technicians before and after bookings. Share photos for inspiration to get you the perfect style and design.</p><br><br><br>
                        </div>
                    </div>
                </div>
                <div class="single-feature sf-center">
                    <div class="row">
                        <div class="col-sm-4 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 single-feature-content">
                            <h3>Improve Business</h3>
                            <p>Have any promotions? Go ahead and share them with new and returning clients through push notifications. Completely customizable from service discounts, time-sensitive promotional events, etc.</p>
                        </div>
                    </div>
                </div>
                <div class="single-feature sf-bottom">
                    <div class="row">
                        <div class="col-sm-4 single-feature-icon">
                            <div class="feature-icon-box">
                                <div class="icon-hover">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 single-feature-content">
                            <h3>Loyalty Program</h3>
                            <p>Gain an edge on your competition with our loyalty program. Clients will able to view and spend their reward points based on your customizable rewards menu. You will have full control over the type of rewards, tiers, and distribution of points.</p>
                        </div>
                    </div>
                </div>
            </div><!-- End .col-md-4 -->
        </div>
    </div><!-- End Container -->
</section><!-- /END Feature Three -->

<!-- =========================
    App Description
    ========================== -->
<section class="app-description">
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-sm-12 ad-mobiles">
                <div class="ad-frame-one wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s">
                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/S4.png') }}" alt="">
                </div>
                <div class="ad-frame-two wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".4s">
                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/S1.png') }}" alt="">
                </div>
            </div>
            <div class="col-sm-7 col-sm-12">
                <div class="col-md-12 heading">
                    <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">OUR VISION AND MISSION </h2>
                </div>
                <div class="col-md-12 app-description-content wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <h3>Our Vision</h3>
                    <p>A platform that seamlessly integrates the service-based industry with the ever-growing technological world. </p>

                    <h3>Our Mission</h3>
                    <p>Help run and manage salons by providing the best and innovative mobile-friendly solutions to problems. </p>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Container -->


<div class="break_cont">
    <br><br><br><br><br><br><br>
</div>

<!-- =========================
    Benefits
    ========================== -->
<section class="benifits">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Your Benefits</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Technology that works for you and available anytime, everywhere.</p>
            </div>
            <div class="col-sm-4 benefits-left wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s">
                <div class="row">
                    <div class="col-sm-9">
                        <h3>Reliable And Secure Platform</h3>
                        <p>Secure cloud-based storage with knowledgeable staff to provide technical assistance when needed. Call us, email us, or even chat with us. We will always be glad to assist you.</p><br>
                    </div>
                    <div class="col-sm-3">
                        <div class="bl-icon"><i class="fa fa-lock"></i></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <h3>Advanced Analytics</h3>
                        <p>We have a comprehensive and easy Dashboard which gives you a visual of your day-to-day operation. Get detailed reports of analytics to help you grow your salon.</p>
                    </div>
                    <div class="col-sm-3">
                        <div class="bl-icon"><i class="fa fa-file-archive-o"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 center-mobile-frame wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                <div class="benifits-image">
                    <img src="{{ URL::asset('assets/website/images/mockup/new_screen/SalonInfo3.png') }}" alt="">
                </div>
                <span class="bullets-left"></span>
                <span class="bullets-right"></span>
            </div>
            <div class="col-sm-4 benefits-right wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".2s">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="br-icon"><i class="fa fa-eye"></i></div>
                    </div>
                    <div class="col-sm-9">
                        <h3>Constant Improvement</h3>
                        <p>To keep up with our vision, we constantly engage with clients and business leaders to improve our services. Our engineering team is committed to producing the best updates.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="br-icon"><i class="fa fa-thumbs-up"></i></div>
                    </div>
                    <div class="col-sm-9">
                        <h3>Stunning Flexibility</h3>
                        <p>iOS and Android compatibilities for all phones, tablets, and web services.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================
    App Screenshots
    ========================== -->
<section class="app-screenshot" id="screenshots">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">HOW OUR APP WORKS</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Allow us to help with management, so you will have more time with your clients. See how our app works.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 screenshots wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                <div id="screenshot-carousel" class="owl-carousel">
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/S4.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/S1.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Map2.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Menu2.png') }}" alt=""></div>
                    {{--                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Dashboard.png') }}" alt=""></div>--}}
                    {{--                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/pay.png') }}" alt=""></div>--}}
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Checkin1.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Checkmein2.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Reminder1.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/SalonInfo1.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Fav3.png') }}" alt=""></div>
                    <div><img src="{{ URL::asset('assets/website/images/mockup/new_screen/Settings2.png') }}" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- =========================
    Pricing Plan
    ========================== -->
<section class="pricing-plan" id="pricing-plan">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">GENERAL ESTIMATES</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Our standard package includes everything you need to connect you to your clients. Get the first month FREE when you sign up. </p>
            </div>
            <div class="pricing-wrapper col-md-12">
                <div class="col-sm-6 no-padding wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <div class="row">
                        <div class="plan-single f-plan">
                            <div class="pricing-header">
                                <h4>Standard Package</h4>
                                <p>For Monthly</p>
                            </div>
                            <div class="price">
                                <p>
                                    <sup class="currency">$</sup>
                                    59.99
                                    <sub class="duration">month</sub>
                                </p><br>
                                <h4><sup class="currency">$</sup><strike>99.99 / month</strike></h4>
                            </div>
                            <div class="pricing-content">
                                <ul class="list-unstyled">
                                    <li>Online Booking <span><i class="fa fa-check"></i></span></li>
                                    <li>Send Push Notifications <span><i class="fa fa-check"></i></span></li>
                                    <li>SMS, Email, & Chat capabilities <span><i class="fa fa-check"></i></span></li>
                                    <li>Professional Profiles <span><i class="fa fa-check"></i></span></li>
                                    <li>Analytics Report <span><i class="fa fa-check"></i></span></li>
                                    <li>24/7 Monitoring <span><i class="fa fa-check"></i></span></li>
                                    <li>Technical Support <span><i class="fa fa-check"></i></span></li>
                                    <li>Secured Cloud-Based Systems <span><i class="fa fa-check"></i></span></li>
                                    <li>Customizable Loyalty &amp; VIP Program <span><i class="fa fa-check"></i></span></li>
                                </ul>
                                <p><a href="" class="choose-plan-btn">Choose Plan</a></p>
                            </div>
                        </div><!-- Plan Single Ends -->
                    </div>
                </div>

                {{--<div class="col-sm-3 no-padding wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">--}}
                {{--<div class="plan-single featured-plan">--}}
                {{--<div class="pricing-header">--}}
                {{--<h4>Freelancer: Work Anytime, Anywhere</h4>--}}
                {{--<p>For Freelancer</p>--}}
                {{--</div>--}}
                {{--<div class="price">--}}
                {{--<p>--}}
                {{--<sup class="currency">$</sup>--}}
                {{--39.99--}}
                {{--<sub class="duration">month</sub>--}}
                {{--</p>--}}
                {{--</div>--}}
                {{--<div class="pricing-content">--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li>Online Booking <span>15</span></li>--}}
                {{--<li>Send Push Notifications <span><i class="fa fa-check"></i></span></li>--}}
                {{--<li>SMS, Email, & Chat capabilities <span><i class="fa fa-check"></i></span></li>--}}
                {{--<li>Professional Profiles <span><i class="fa fa-close"></i></span></li>--}}
                {{--<li>Analytics Report <span><i class="fa fa-close"></i></span></li>--}}
                {{--<li>24/7 Monitoring <span><i class="fa fa-close"></i></span></li>--}}
                {{--<li>Technical Support <span><i class="fa fa-close"></i></span></li>--}}
                {{--<li>Secured Cloud-Based Systems <span><i class="fa fa-close"></i></span></li>--}}
                {{--</ul>--}}
                {{--<p><a href="" class="choose-plan-btn">Choose Plan</a></p>--}}
                {{--</div>--}}
                {{--</div><!-- Plan Single Ends -->--}}
                {{--</div>--}}

                <div class="col-sm-6 no-padding wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".3s">
                    <div class="row">
                        <div class="plan-single t-plan">
                            <div class="hero-image1">
                                <div class="hero-text1">
                                    <div>
                                        <h2>Freelancer Package</h2>
                                        <p style="color: #ffffff;">For Freelancer</p>
                                    </div>
                                    <h2>Coming Soon</h2>
                                </div>

                            </div>

                            {{--<div class="price">--}}
                            {{--<p>--}}
                            {{--<sup class="currency">$</sup>--}}
                            {{--XX--}}
                            {{--<sub class="duration">****</sub>--}}
                            {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="pricing-content">--}}
                            {{--<ul class="list-unstyled">--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-check"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-check"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-check"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-close"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-close"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-close"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-close"></i></span></li>--}}
                            {{--<li>Lorem Ipsum <span><i class="fa fa-close"></i></span></li>--}}
                            {{--</ul>--}}
                            {{--<p><a href="" class="choose-plan-btn">Coming Soon</a></p>--}}
                            {{--</div>--}}
                        </div><!-- Plan Single Ends -->

                    </div>
                </div>
            </div><!-- Pricing Wrapper Ends -->
        </div>
    </div>
</section>


<!-- =========================
    Subscribe
    ========================== -->
<section class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                <h4>Subscribe Now</h4>
            </div>
            <div class="col-sm-8 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                <form id="mc-form">
                    <input type="email" id="mc-email" placeholder="Your Email">
                    <input type="submit" value="Subscribe">
                    <label for="mc-email"></label>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- =========================
    Video
    ========================== -->
<section class="app-video">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Description With Video</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">A video from our {{config('siteVars.sitetitle')}} team on how {{config('siteVars.sitetitle')}} can benefit you, This video speaks to Service providers and also customers</p>
            </div>
            <div class="col-sm-12 wow zoomIn" data-wow-duration="1.5s" data-wow-delay=".3s">
                <div class="hero-image">
                    <div class="hero-text">
                        <h1 style="font-size:50px">Video</h1>
                        <h1 style="font-size:50px">Coming Soon</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================
    Download App
    ========================== -->
<section class="app-downloads" id="app-download">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Get Free APP TODAY<span></span></h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Download your app now and become {{config('siteVars.sitetitle')}}</p>
            </div>
            <div class="col-sm-12 app-download-btns wow bounceInDown" data-wow-duration="1.5s" data-wow-delay=".3s">
                <ul class="list-unstyled">
                    <li><a href="{{config('siteVars.itunes_link')}}" target="_blank"><i class="fa fa-apple"></i> App Store</a></li>
                    <li><a href="{{config('siteVars.play_store_link')}}" target="_blank"><i class="fa fa-android"></i> Play Store</a></li>
                </ul>
            </div>
        </div>
        <div class="review-carousel wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
            <div id="app-reviews" class="owl-carousel">
                <div>
                    <div class="review-content">
                        <p>App layout design is very clean and easy flow. I really enjoyed quick checkin to nearby salon. Also it is showing realistic waiting time of salons with waiting list. I can track my turn and reach salon without wasting my time. Excellent app</p>
                    </div>
                    <div class="client-details">
                        <img src="{{url('assets/website/images/105x105.png')}}" alt="" class="img-circle img-responsive">
                        <p>
                            <span>KEYUR ZALA</span>
                            <span>{{config('siteVars.sitetitle1')}} USER</span>
                        </p>
                    </div>
                </div>
                <div>
                    <div class="review-content">
                        <p>Awesome app really enjoyed using this app for my nail service booking. It’s straight forward and easy to use, i can track my waiting time from my mobile it’s great.</p>
                    </div>
                    <div class="client-details">
                        <img src="{{url('assets/website/images/105x105.png')}}" alt="" class="img-circle img-responsive">
                        <p>
                            <span>MUKUND H.</span>
                            <span>{{config('siteVars.sitetitle1')}} USER</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================
    Contact Us
    ========================== -->
<section class="contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 heading">
                <h2 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Contact Us</h2>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">Get in touch with us now. Drop us a text and we will get back to you soonest</p>
            </div>
            <div class="col-md-6 contact-form">
                <form id="ajax-contact" method="post" action="#" onsubmit="javascript:return contact_validation();">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s" style="padding-bottom: 3.2%;">
                                <input type="text" placeholder="Your Name" id="name" name="name" required>
                            </div>
                            <div class="col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                                <input type="email" placeholder="Your Email" id="email" name="email" required>
                            </div>
                            <div class="col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                                <textarea placeholder="Message" id="message" name="message" required></textarea>
                            </div>
                            <div class="col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s" style="padding-top: 1.5%">
                                <div class="g-recaptcha" data-sitekey="{{config('siteVars.captcha_key')}}"></div>
                                <strong><span id="captche_msg" style="color: #ff0000; display: none;">Please select captcha to proceed!</span></strong>
                            </div>
                            <div class="col-sm-12 form-btn wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".9s" style="text-align: left;">
                                <button type="submit">Send Message</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="form-messages"></div>
            </div>

            <div class="col-md-6 contact-form">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                            {{--<div class="col-sm-12 wow fadeInUp con-map" data-wow-duration="1.5s" data-wow-delay=".3s" id="map" style="min-height:250px; margin-bottom:25px;"></div>--}}
                        </div>
                        <div class="col-md-12 col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <ul class="con-span" style="list-style-type:none; display: table-cell;">
                                <li>
                                    <i class="fa fa-map-marker col-sm-1 theme_color" style=" font-size: 20px;"></i>
                                    <span class="col-sm-11">18931 E Valley View Pkwy Ste D Independence, MO 64055</span>
                                </li>

                                <style>
                                    a[href^=tel] {
                                        color: inherit;
                                        text-decoration: none;
                                    }
                                </style>

                                <li>
                                    <i class="fa fa-phone col-sm-1 theme_color" style="font-size: 20px;"></i>
                                    <span class="col-sm-11"><a href style="color:#482162" >{{ "+1 816 719 1925" }}</a></span>
                                </li>

                                <li>
                                    <i class="fa fa-envelope-o col-sm-1 theme_color" style="font-size: 20px;"></i>
                                    <span class="col-sm-11">{{config('siteVars.support_email')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="form-messages"></div>
            </div>
        </div>
    </div>
</section>


<!-- =========================
    FOOTER
    ========================== -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 footer-content">
                <h4 class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s">{{config('siteVars.sitetitle')}}</h4>
                <p class="wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".4s">Copyright &copy;  2018 - All rights reserved</p>
                <ul class="list-unstyled wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".6s">
                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-youtube-play"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!-- End Container -->
</footer><!-- /END FOOTER SECTION -->


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
{{--<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>--}}

<!-- Select2 -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js')}}"></script>


{{--<script src="{{ URL::asset('assets/website/map_lightbox/vendor/jquery-2.1.0.min.js') }}"></script>--}}
<!--<![endif]-->

<script src="{{ URL::asset('assets/website/map_lightbox/responsive.min.js') }}"></script>

<script>
    $(function () {
        $('#datepicker').datepicker({
            format: 'yyyy-m-d',
            startDate: '+0d',
            autoclose: true
        });

        $('#datepicker_walkin').datepicker({
            format: 'm/d/yyyy',
            startDate: '+0d',
            autoclose: true,
        });

        $(".select2").select2();
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        //$('#call_category').trigger('click');
    });
</script>


<!-- datepicker -->
<script src="{{ URL::asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>
{{--<script src="{{ URL::asset('assets/website/js/switcher.core.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.helper.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.load.js') }}"></script>--}}
<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>

<script>
    function contact_validation()
    {
        var flag = 0;
        if (grecaptcha.getResponse() == ""){
            document.getElementById('captche_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById('captche_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initMap" async defer></script>--}}

{{--<script>--}}
{{--var map;--}}
{{--var markersArray = [];--}}
{{--var myLatLng = {lat: 39.033730, lng: -94.355670};--}}
{{--var latitude;--}}
{{--var longitude;--}}

{{--function initMap() {--}}
{{--map = new google.maps.Map(document.getElementById('map'), {--}}
{{--center: myLatLng,--}}
{{--zoom: 12--}}
{{--});--}}
{{--var marker = new google.maps.Marker({--}}
{{--position: myLatLng,--}}
{{--map: map,--}}
{{--//title: 'Hello World!'--}}
{{--});--}}
{{--google.maps.event.addListener(map, 'click', function(event) {--}}
{{--placeMarker(event.latLng);--}}
{{--// Remove Current Marker--}}
{{--marker.setMap(null);--}}
{{--$("#latitude").val(event.latLng.lat());--}}
{{--$("#longitude").val(event.latLng.lng());--}}

{{--var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());--}}
{{--var geocoder = geocoder = new google.maps.Geocoder();--}}
{{--geocoder.geocode({'latLng': latlng}, function(results, status) {--}}
{{--if (status == google.maps.GeocoderStatus.OK) {--}}
{{--if (results[0]) {--}}
{{--$('#Address').val(results[0].formatted_address);--}}
{{--}--}}
{{--}--}}
{{--});--}}
{{--});--}}
{{--}--}}

{{--</script>--}}

</body>
</html>
