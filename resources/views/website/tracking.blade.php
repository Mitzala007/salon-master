<!doctype html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.19.0.min.js"></script>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
    <div id="map"></div>
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<script>

    var sid = '{{$salon['id']}}';
    var slatitude = parseFloat('{{$salon['latitude']}}');
    var slongitude = parseFloat('{{$salon['longitude']}}');

    //alert(slatitude+"--"+slongitude);
    window.lat = slatitude;
    window.lng = slongitude;

    function get_salons(sid,latitude,longitude) {
        $.ajax({
            url: '{{url("get_salon_location")}}',
            type: 'GET',
            data: {sid: sid},
            success: function (data) {

                var myJSON = JSON.stringify(data)
                var JSONObject = JSON.parse(myJSON);

                //alert(JSONObject['latitude']+"--"+JSONObject['longitude']);

                window.lat = parseFloat(JSONObject['latitude']);
                window.lng = parseFloat(JSONObject['longitude']);

                //alert("convert-"+window.lat+"--"+window.lng);
            }
        });
    }

    setInterval(function(){get_salons(sid);}, 5000);

    function currentLocation() {
        //alert("current_pos="+lat+"--"+lng);
        return {lat:window.lat, lng:window.lng};
    };

    var map;
    var mark;

    var initialize = function() {
        map  = new google.maps.Map(document.getElementById('map'), {center:{lat:lat,lng:lng},zoom:16});
        mark = new google.maps.Marker({position:{lat:lat, lng:lng}, map:map});
    };

    window.initialize = initialize;

    var redraw = function(payload) {
        lat = payload.message.lat;
        lng = payload.message.lng;

        map.setCenter({lat:lat, lng:lng, alt:0});
        mark.setPosition({lat:lat, lng:lng, alt:0});
    };

    var pnChannel = "map2-channel";

    var pubnub = new PubNub({
        publishKey:   'pub-c-7f56981a-3b4f-4f0d-81e7-2843420443c3',
        subscribeKey: 'sub-c-3a35f0a2-a45e-11e9-a74d-4e0ad457cfa9'
    });

    pubnub.subscribe({channels: [pnChannel]});
    pubnub.addListener({message:redraw});

    setInterval(function() {
        pubnub.publish({channel:pnChannel, message:currentLocation()});
    }, 5000);
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA&callback=initialize"></script>
</body>
</html>