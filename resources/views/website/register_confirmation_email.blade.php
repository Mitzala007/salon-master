<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{config('siteVars.sitetitle')}} is a App Landing Page">
    <meta name="keywords" content="{{config('siteVars.sitetitle')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('siteVars.sitetitle')}}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/website/Logo_web.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}">

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/font-awesome.min.css') }}">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/owl.transitions.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/animate.css') }}">

<!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Exo:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/assets/responsive.css') }}">
    <script src="{{ URL::asset('assets/website/js/vendor/modernizr-3.3.1.min.js') }}"></script>

    <script src="{{ URL::asset('assets/website/css/assets/bootstrap.css') }}"></script>
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- =========================
     HEADER SECTION
     ========================= -->

@if($message=="Invalid Link")
<section class="bg0 p-t-104 p-b-50" style="margin-top: 50px">
    <div class="container menu">
        <center>
            <div class="size-210  p-lr-70 p-t-55 p-lr-15-lg w-full-md">
                <img src="{{url('assets/website/img/green_tick.png')}}" style="height:150px; width: 150px;" class="p-l-10">
                <h2 class="stext-115 cl2 txt-center p-b-30">
                    <b>{{$message}}</b>
                </h2>
            </div>
        </center>
    </div>
</section>
@else
    <section class="bg0 p-t-104 p-b-50" style="margin-top: 50px">
        <div class="container menu">
            <center>
                <div class="size-210  p-lr-70 p-t-55 p-lr-15-lg w-full-md">
                    <img src="{{url('assets/website/img/green_tick.png')}}" style="height:150px; width: 150px;" class="p-l-10">
                    <h2 class="stext-115 cl2 txt-center p-b-30">
                        <b>{{$message}}</b>
                    </h2>
                </div>
            </center>
        </div>
    </section>
@endif


<!-- =========================
        SCRIPTS
============================== -->
<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ URL::asset('assets/website/js/vendor/jquery-1.12.0.min.js') }}"><\/script>')</script>
<!-- Bootstrap JS -->
<script src="{{ URL::asset('assets/website/js/assets/bootstrap.min.js') }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ URL::asset('assets/website/js/assets/owl.carousel.min.js') }}"></script>
<!-- WOW Js -->
<script src="{{ URL::asset('assets/website/js/assets/wow.min.js') }}"></script>
<!-- Sticky JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.sticky.js') }}"></script>
<!-- Smooth Scrool -->
<script src="{{ URL::asset('assets/website/js/assets/smooth-scroll.js') }}"></script>
<!-- AjaxChimp JS -->
<script src="{{ URL::asset('assets/website/js/assets/jquery.ajaxchimp.js') }}"></script>
<!-- Text Rotate JS -->
{{--<script src="{{ URL::asset('assets/website/js/assets/jquery.simple-text-rotator.min.js') }}"></script>--}}
<!-- Color Switcher -->
<script src="{{ URL::asset('assets/website/js/base.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/jquery.cookie.js') }}"></script>
{{--<script src="{{ URL::asset('assets/website/js/switcher.core.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.helper.js') }}"></script>--}}
{{--<script src="{{ URL::asset('assets/website/js/switcher.load.js') }}"></script>--}}
<!-- Custom JS -->
<script src="{{ URL::asset('assets/website/js/plugins.js') }}"></script>
<script src="{{ URL::asset('assets/website/js/function.js') }}"></script>

<script>
    function contact_validation()
    {
        var flag = 0;
        if (grecaptcha.getResponse() == ""){
            document.getElementById('captche_msg').style.display = 'block';
            flag=1;
        }
        else{
            document.getElementById('captche_msg').style.display = 'none';
        }

        if(flag==1){
            return false;
        }
    }
</script>

</body>
</html>
