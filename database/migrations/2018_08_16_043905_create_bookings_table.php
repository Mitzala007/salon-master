<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('bookings', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('order_id');
//            $table->integer('user_id');
//            $table->integer('saloon_id');
//            $table->integer('type_id');
//            $table->string('date')->nullable();
//            $table->string('time')->nullable();
//            $table->string('status')->comment("waiting,in-progress,cancel,completed")->nullable();
//            $table->double('subtotal')->nullable();
//            $table->double('discount_amount')->nullable();
//            $table->double('point')->nullable();
//            $table->double('final_total')->nullable();
//            $table->text('remarks')->nullable();
//            $table->text('checkin_type')->nullable();
//            $table->text('device_type')->nullable();
//            $table->text('device_token')->nullable();
//            $table->text('is_notify')->default(0);
//            $table->double('tax')->default('0.00');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
