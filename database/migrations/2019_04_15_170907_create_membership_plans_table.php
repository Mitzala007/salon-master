<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('membership_plans', function (Blueprint $table) {
//            $table->increments('id');
//            $table->text('title')->nullable();
//            $table->double('amount')->nullable();
//            $table->integer('is_recurring')->default(0);
//            $table->integer('total_recurring_occurance')->nullable();
//            $table->integer('free_trial')->comment('in days')->nullable();
//            $table->string('status')->default('active');
//            $table->string('description')->nullable();
//            $table->string('short_description')->nullable();
//            $table->integer('displayorder')->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_plans');
    }
}
