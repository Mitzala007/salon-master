<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('states', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('country_code_char2', 2)->nullable();
//            $table->string('country_code_char3', 3)->nullable();
//            $table->string('state_subdivision_name', 80)->nullable();
//            $table->string('state_subdivision_alt_name', 200)->nullable();
//            $table->string('primary_level_name', 80)->nullable();
//            $table->string('state_subdivision_code', 50)->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
