<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemaindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('remainders', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->nullable();
//            $table->text('title')->nullable();
//            $table->text('description');
//            $table->date('remainder_date');
//            $table->timestamp('remainder_time');
//            $table->integer('is_notify');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remainders');
    }
}
