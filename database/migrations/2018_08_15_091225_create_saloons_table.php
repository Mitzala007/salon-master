<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaloonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('saloons', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id');
//            $table->text('title')->nullable();
//            $table->text('description')->nullable();
//            $table->text('location')->nullable();
//            $table->text('address1')->nullable();
//            $table->text('address2')->nullable();
//            $table->text('latitude')->nullable();
//            $table->text('longitude')->nullable();
//            $table->text('timezone')->nullable();
//            $table->text('start_hour')->nullable();
//            $table->text('end_hour')->nullable();
//            $table->tinyInteger('global_discount');
//            $table->integer('displayorder')->nullable();
//            $table->text('image')->nullable();
//            $table->integer('saloon_phone')->nullable();
//            $table->text('website_url')->nullable();
//            $table->integer('rating')->nullable();
//            $table->integer('reg_step')->nullable();
//            $table->integer('code')->nullable();
//            $table->integer('is_display')->default(1);
//            $table->text('promotional_title')->nullable();
//            $table->integer('is_promo_display')->default(1);
//            $table->text('promotional_description')->nullable();
//            $table->integer('is_online')->nullable();
//            $table->integer('loyalty_program')->nullable();
//            $table->double('tax')->default('0.00');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saloons');
    }
}
