<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('payment_histories', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id')->nullable();
//            $table->text('invoice_number')->nullable();
//            $table->integer('subscription_id')->nullable();
//            $table->double('amount')->nullable();
//            $table->integer('customer_profile_id')->nullable();
//            $table->integer('customer_payment_id')->nullable();
//            $table->integer('transaction_id')->nullable();
//            $table->text('payment_status')->nullable();
//            $table->text('subscription_status')->nullable();
//            $table->text('payment_method')->nullable();
//            $table->integer('payment_number')->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_histories');
    }
}
