<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('notifications', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('sender')->nullable();
//            $table->integer('receiver')->nullable();
//            $table->text('message');
//            $table->string('notification_type');
//            $table->integer('is_read');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
