<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSubscriptionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('payment_subscription__histories', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('subscription_id');
//            $table->double('amount');
//            $table->string('subscription_status');
//            $table->double('total_occurrences');
//            $table->double('past_occurrences');
//            $table->text('invoice');
//            $table->string('currencyCode');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_subscription__histories');
    }
}
