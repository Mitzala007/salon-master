<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('point_histories', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('user_id');
//            $table->integer('salon_id');
//            $table->integer('booking_id')->nullable();
//            $table->integer('plan_id')->nullable();
//            $table->string('type')->comment('CREDIT,DEBIT');
//            $table->integer('point_before');
//            $table->integer('point');
//            $table->text('point_after');
//            $table->text('remark');
//            $table->timestamps();
//            $table->softDeletes();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_histories');
    }
}
