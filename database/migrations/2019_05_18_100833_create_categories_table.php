<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('categories', function (Blueprint $table) {
//            $table->increments('id');
//            $table->text('title')->nullable();
//            $table->text('description')->nullable();
//            $table->text('image')->nullable();
//            $table->text('icon')->nullable();
//            $table->string('status')->default('active');
//            $table->string('displayorder')->nullable();
//            $table->softDeletes();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
