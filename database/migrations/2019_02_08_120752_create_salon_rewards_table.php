<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('salon_rewards', function (Blueprint $table) {
//            $table->increments('id');
//            $table->integer('salon_id')->nullable();
//            $table->text('name')->nullable();
//            $table->text('description')->nullable();
//            $table->date('start_date')->nullable();
//            $table->date('end_date')->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_rewards');
    }
}
