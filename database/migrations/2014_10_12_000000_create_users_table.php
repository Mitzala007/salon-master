<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('users', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('name');
//            $table->string('last_name');
//            $table->string('nick_name');
//            $table->string('email')->unique();
//            $table->string('gender')->nullable();
//            $table->string('cat_id')->nullable();
//            $table->string('password');
//            $table->string('country')->nullable();
//            $table->integer('state_id')->nullable();
//            $table->integer('city_id')->nullable();
//            $table->text('landmark')->nullable();
//            $table->text('addressline_1')->nullable();
//            $table->text('addressline_2')->nullable();
//            $table->text('zip_code')->nullable();
//            $table->string('phone')->nullable();
//            $table->date('birthdate')->nullable();
//            $table->text('address')->nullable();
//            $table->string('role');
//            $table->string('image')->nullable();
//            $table->integer('otp')->nullable();
//            $table->string('status')->default('active');
//            $table->string('facebookid')->nullable();
//            $table->integer('is_facebook')->default('0');
//            $table->integer('is_email_verified')->default('0');
//            $table->integer('is_notification_sms')->default('0');
//            $table->integer('is_notification_email')->default('0');
//            $table->integer('is_notification_push')->default('0');
//            $table->integer('is_remainder_sms')->default('0');
//            $table->integer('is_remainder_email')->default('0');
//            $table->integer('is_remainder_push')->default('0');
//            $table->integer('membership_plan')->default('0');
//            $table->integer('admin_membership_plan')->default('0');
//            $table->softDeletes();
//            $table->rememberToken();
//            $table->timestamps();
//        });
//        \Illuminate\Support\Facades\DB::table('users')->insert([
//                'name' => 'admin',
//                'email' => 'admin@admin.com',
//                'password' => bcrypt('123456'),
//                'status' =>'active',
//                'role' => 'admin'
//            ]
//        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

    }
}
