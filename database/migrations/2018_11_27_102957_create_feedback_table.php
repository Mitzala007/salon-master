<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('feedback', function (Blueprint $table) {
//            $table->increments('id');
//            $table->text('first_name')->nullable;
//            $table->text('last_name')->nullable;
//            $table->text('email')->nullable;
//            $table->integer('saloon_id')->nullable;
//            $table->integer('user_id')->nullable;
//            $table->date('visit_date')->nullable;
//            $table->longText('comment')->nullable;
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
