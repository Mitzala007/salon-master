<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //$this->call(UsersTableSeeder::class);
       //$this->call(SaloonTableSeeder::class);
       //$this->call(WorkdaysSeeder::class);
       //$this->call(SaloonServiceTableSeeder::class);
       $this->call(EmployeeTableSeeder::class);
    }
}
