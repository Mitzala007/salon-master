<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $salon = \App\saloon::get();
        $images = ['a.png','b.png','c.png','d.png','e.png','f.png','g.png','h.png','i.png'];
        foreach ($salon as $saloons){
            for ($i=1 ; $i<=2 ; $i++){
                    DB::table('employees')->insert([
                        'saloon_id' => $saloons->id,
                        'first_name' => $faker->word,
                        'last_name' => $faker->word,
                        'nick_name' => $faker->word,
                        'address' => $faker->paragraph(),
                        'phone' => rand('10000000','99999999'),
                        'email' => str_random(5).$faker->email,
                        'image' => "public/resource/employees/".$images[rand(0,8)],
                        'available' => "present",
                        'status' => 'active',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
            }
        }
    }
}
