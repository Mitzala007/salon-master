<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SaloonServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $salon = \App\saloon::get();
        $images = ['a.png','b.png','c.png','d.png','e.png','f.png','g.png','h.png','i.png'];
        foreach ($salon as $saloons){

            $services = \App\Service_type::get();
            foreach ($services as $serv){
                DB::table('saloon_services')->insert([
                    'saloon_id' => $saloons->id,
                    'type_id' => $serv->id,
                    'charges' => rand('1','500'),
                    'duration' => 15,
                    'avail_discount' => 1,
                    'discount' => rand('1','50'),
                    'status' => 'active',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
