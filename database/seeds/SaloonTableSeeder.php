<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class SaloonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $faker = Faker::create();
          $user = \App\User::where('role','sub_admin')->get();
          $images = ['a.png','b.png','c.png','d.png','e.png','f.png','g.png','h.png','i.png'];
          foreach ($user as $users){

              DB::table('saloons')->insert([
                  'user_id' => $users->id,
                  'title' => $faker->word,
                  'description' => $faker->paragraph,
                  'location' => $faker->word,
                  'latitude' => "22.".rand(1000000,9999999),
                  'longitude' => "70.".rand(00000000,99999999),
                  'timezone' => "Asia/Calcutta",
                  'start_hour' => "08:00 AM",
                  'end_hour' => "08:00 PM",
                  'global_discount' => 1,
                  'displayorder' => null,
                  'image' => "public/resource/product/saloon/".$images[rand(0,8)],
                  'saloon_phone' => rand(10000000,99999999),
                  'website_url' => "http://google.com",
                  'rating' => 0,
                  'reg_step' => 3,
                  'code' => null,
                  'is_display' => 1,
                  'promotional_title' => $faker->word,
                  'is_promo_display' => 1,
                  //'promotional_description' => 1,
                  'status' => 'active',
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
              ]);
          }

        //factory(App\saloon::class, 5)->create();
    }

}
