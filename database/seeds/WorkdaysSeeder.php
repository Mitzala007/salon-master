<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WorkdaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $salon = \App\saloon::get();
        $days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
        foreach ($salon as $saloons){

            foreach ($days as $day)
            DB::table('work_days')->insert([
                'saloon_id' => $saloons->id,
                'work_day' => $day,
                'start_hour' => "12:00 AM",
                'end_hour' => "11:59 PM",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}