<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/* USER TABLE */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $arrayValues = ['user', 'sub_admin'];
    $images = ['a.png','b.png','c.png','d.png','e.png','f.png','g.png','h.png','i.png'];
    $country = ['IND'];
    return [
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'gender' => "male",
        'email' => str_random(5).$faker->email,
        'password' => bcrypt('123456'),
        'country' => "IND",
        'state_id' => "284",
        'city_id' => "748",
        'landmark' => $faker->word,
        'addressline_1' => $faker->word,
        'addressline_2' => $faker->word,
        'zip_code' => "64063",
        'phone' => '1'.rand(1000000,9999999).rand(100,999),
        'image' => "public/resource/user/".$images[rand(0,8)],
        'birthdate' => date('Y-m-d'),
        'address' => str_random(40),
        'role' => $arrayValues[rand(0,1)],
        //'otp' => rand(100000,900000),
        'otp' => null,
        'status' => "active",
        'facebookid' => null,
        'is_facebook' => 0,
        'is_email_verified' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});

