<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'admin/home'], function () {

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('resetpassword','\App\Http\Controllers\Auth\ForgotPasswordController@reset_password');
    Route::post('sendemail','\App\Http\Controllers\Auth\ForgotPasswordController@send_password');
    Route::get('generate_password','\App\Http\Controllers\Auth\ForgotPasswordController@generate_password');
    Route::post('generate_password','\App\Http\Controllers\Auth\ForgotPasswordController@generate_password_store');
    Route::auth();
    Route::get('/', 'admin\DashboardController@index');

    Route::get('dashboard', 'admin\DashboardController@index');
    Route::get('dashboard_salon_session', 'admin\DashboardController@dashboard_salon_session');
    Route::resource('staticpage', 'admin\StaticpageController');

    Route::get('log_report', 'admin\DashboardController@log_report');

    /* USER MANAGEMENT */
    Route::put('users/assign', 'admin\UserController@assign');
    Route::put('users/unassign', 'admin\UserController@unassign');
    Route::get('users/get_states/{cc}', 'admin\UserController@get_states');
    Route::get('users/get_cities/{cc}/{st}', 'admin\UserController@get_cities');
    Route::get('users/booking/{id}', 'admin\UserController@booking_show');
    Route::resource('users', 'admin\UserController');
    Route::resource('profile_update', 'admin\ProfileupdateController');
    Route::get('deleted-user','admin\UserController@deleted_index');
    Route::put('deleted-user/recover','admin\UserController@recover_user');
    Route::delete('deleted-user/force-delete/{id}','admin\UserController@force_delete');
    Route::delete('become_nailmaster/{id}','admin\UserController@become_nailmaster_delete');

    /* BECOME NAILMSTER*/
    Route::get('become_nailmaster','admin\UserController@become_nailmaster');
    Route::get('become_nailmaster/send_email','admin\UserController@send_email');

    /* SALOON EMPLOYEE MANAGEMENT */
//    Route::put('employees/assign', 'admin\EmployeeController@assign');
//    Route::put('employees/unassign', 'admin\EmployeeController@unassign');
//    Route::resource('employees', 'admin\EmployeeController');

    Route::get('employee/{id}/list','admin\SaloonController@employeelist');

    /* SALOON MANAGEMENT */
    Route::get('saloon/get_user', 'admin\SaloonController@get_user');
    Route::get('saloon/reorder', 'admin\SaloonController@reorder');
    Route::put('saloon/assign', 'admin\SaloonController@assign');
    Route::put('saloon/unassign', 'admin\SaloonController@unassign');
    Route::get('saloon/services/{id}', 'admin\SaloonController@services');
    Route::get('saloon/services_edit/{id}', 'admin\SaloonController@services_edit');
    Route::put('saloon/update_services/{id}', 'admin\SaloonController@update_services');
    Route::put('saloon/save_services/{id}', 'admin\SaloonController@save_services');
    Route::get('saloon/work_days/{id}', 'admin\SaloonController@work_days');
    Route::put('saloon/save_days/{id}', 'admin\SaloonController@save_days');
    Route::get('saloon/images/{id}/edit', 'admin\SaloonController@images_edit');
    Route::patch('saloon/images/{id}/update', 'admin\SaloonController@images_update');
    Route::get('saloon_service/{id}', 'admin\SaloonController@saloon_service');
    Route::get('saloon/assign_owner', 'admin\SaloonController@assign_owner');
    Route::delete('saloon/delete_assign_owner/{id}/{salon_id}', 'admin\SaloonController@delete_assign_owner');
    Route::get('saloon/employee_attendance', 'admin\SaloonController@employee_attendance');
    Route::resource('saloon', 'admin\SaloonController');

    /* SERVICE TYPE MANAGEMENT */

    Route::get('service/reorder', 'admin\ServiceTypeController@reorder');
    Route::put('service/assign', 'admin\ServiceTypeController@assign');
    Route::put('service/unassign', 'admin\ServiceTypeController@unassign');
    Route::get('service/get_sub_category/{cc}', 'admin\ServiceTypeController@get_sub_category');
    Route::resource('service', 'admin\ServiceTypeController');


    /* CATEGORY MANAGEMENT */

    Route::get('category/reorder', 'admin\CategoryController@reorder');
    Route::put('category/assign', 'admin\CategoryController@assign');
    Route::put('category/unassign', 'admin\CategoryController@unassign');
    Route::resource('category', 'admin\CategoryController');

    /* BOOKING MANAGEMENT */
    Route::get('booking/create', 'admin\BookingController@create');
    Route::post('booking/store', 'admin\BookingController@store');

    Route::get('booking/{id}/list', 'admin\BookingController@index');
    //Route::get('booking/{id}/list/{vid}', 'admin\BookingController@show');
    Route::get('booking/{status}/{id}', 'admin\BookingController@update_status');
    Route::get('booking', 'admin\BookingController@booking');
    Route::delete('booking/{id}/delete/{vid}', 'admin\BookingController@destroy');
    Route::get('booking/{bid}', 'admin\BookingController@show');

    Route::get('waiting_list', 'admin\BookingController@waiting_list');
    Route::post('do_booking', 'admin\BookingController@store');
    Route::post('serve_now', 'admin\BookingController@serve_now');
    Route::post('end_now', 'admin\BookingController@end_now');
    Route::post('get_booking_details', 'admin\BookingController@get_booking_details');
    Route::post('edit_booking', 'admin\BookingController@edit_booking');
    Route::post('cancel_booking', 'admin\BookingController@cancel_booking');


    /* NOTIFICATION MANAGEMENT */
    Route::resource('notification', 'admin\NotificationController');

    /* RATINGS & RIVEWS MANAGEMENT */
    Route::resource('rating', 'admin\RatingController');

    /* FEEDBACK MANAGEMENT */
    Route::resource('feedback', 'admin\FeedbackController');

    /* EMAIL MANAGEMENT */
    Route::put('emails/assign', 'admin\EmailController@assign');
    Route::put('emails/unassign', 'admin\EmailController@unassign');
    Route::resource('emails', 'admin\EmailController');

    /*Point History Management*/
    Route::get('point_history/{id}', 'admin\PointHistoryController@point');
    Route::post('point_history/add', 'admin\PointHistoryController@add_point');

    /*IMAGE UPLOAD FOR SUMMER NOTE*/
    Route::post('image/upload', 'admin\imageuploadController@upload_image');

    /*Salon Management Guide*/
    Route::get('salon_guide/{id}', 'admin\DashboardController@salon_guide');
    Route::patch('salon_guide_update/{id}/edit', 'admin\DashboardController@salon_guide_update');

    /*MEMBERSHIP PLAN MANGEMENT*/
    Route::put('membership/assign', 'admin\MembershipController@assign');
    Route::put('membership/unassign', 'admin\MembershipController@unassign');
    Route::get('membership/reorder', 'admin\MembershipController@reorder');
    Route::resource('membership', 'admin\MembershipController');

    /*my_membership MANAGEMENT SALON OWNER MENU*/
    Route::get('my_membership', 'admin\MembershipController@user_membership');

    /*CUSTOMER MANAGEMENT*/
    Route::get('customer/{id}/list', 'admin\CustomerController@customer_index');
    Route::get('customer/{id}/details', 'admin\CustomerController@customer_details');
    Route::post('ajax_get_customer', 'admin\CustomerController@ajax_get_customer');

    Auth::routes();
});

/*------------------------------------ROUTE FOR WEBSITE---------------------------------*/

Route::get('/', 'website\HomeController@index');
Route::get('app-download', 'website\HomeController@app_download');
Route::post('register_user', 'website\RegisterUserController@register_user');
Route::get('register_confirmation_email', 'website\RegisterUserController@register_confirmation_email');
Route::get('get_states/{cc}', 'website\RegisterUserController@get_states');
Route::get('get_cities/{cc}/{st}', 'website\RegisterUserController@get_cities');
Route::get('register', 'website\RegisterUserController@index');
Route::get('thank-you', 'website\RegisterUserController@thank_you');
Route::get('failed', 'website\RegisterUserController@failed');
Route::get('privacy-policy', 'website\HomeController@privacy_policy');
Route::get('do_payment/{id}', 'website\RegisterUserController@do_payment');
Route::get('online_chat', 'website\HomeController@online_chat');
Route::post('get_salons', 'website\HomeController@get_salons');
Route::post('get_salon_images', 'website\HomeController@get_salon_images');
Route::post('do_booking', 'website\HomeController@do_booking');
Route::post('get_category', 'website\HomeController@get_category');
Route::post('get_services', 'website\HomeController@get_services');
Route::post('set_category_session', 'website\HomeController@set_category_session');
Route::get('tracking/{token}/{booking_id}','website\HomeController@tracking');
Route::get('get_salon_location','website\HomeController@get_salon_location');


/* PAYMENT ROUTES */
/* FRONT END */
Route::get('do_subscription/{session_token}/{amount}', 'website\AuthorizeController@index');
Route::post('checkout','website\AuthorizeController@checkout');
Route::get('get_subscription/{subscription}','website\AuthorizeController@getSubscription');
Route::get('cron_subscriptionlist','website\AuthorizeController@cron_subscriptionlist');


/* API END */
Route::get('api/payment/{token}','website\AuthorizeController@payment');
Route::post('api/payment_form','website\AuthorizeController@payment_form');
Route::post('api/payment_form_submit','website\AuthorizeController@payment_form_submit');


// Route::get('api/payment_complete','website\AuthorizeController@payment_complete');
// Route::get('api/payment_incomplete','website\AuthorizeController@payment_incomplete');
Route::get('api/subscription/{token}','website\AuthorizeController@subscription');
Route::get('api/cancel_subscription/{subscription_id}','website\AuthorizeController@cancel_subscription');
//Route::get('api/subscription_cancel','website\AuthorizeController@subscription_cancel');

Route::get('api/getTransactionDetails/{tid}','website\AuthorizeController@getTransactionDetails');

Route::get('report','website\HomeController@salon_report');

Route::get('notification/{token}','website\HomeController@notification');