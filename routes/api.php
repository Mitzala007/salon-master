<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login','api\AppUserController@login');
Route::post('verify_login','api\AppUserController@verify_login');
Route::post('check_token','api\AppUserController@check_token');
Route::post('facebook_login','api\AppUserController@facebook_login');
Route::post('mobile_login','api\AppUserController@mobile_login');
Route::post('verify_login_otp','api\AppUserController@verify_login_otp');
Route::post('add_customer','api\AppUserController@add_customer');
Route::post('logout','api\AppUserController@logout');
Route::post('register','api\AppUserController@register');
Route::post('get_profile','api\AppUserController@get_profile');
Route::post('profile_update','api\AppUserController@profile_update');
Route::post('change_password','api\AppUserController@change_password');
Route::post('forgot_password','api\AppUserController@forgot_password');
Route::post('get_saloons','api\AppUserController@get_saloons');
Route::post('get_saloon_info','api\AppUserController@get_saloon_info');
Route::post('get_saloon_images','api\AppUserController@get_saloon_images');
Route::post('get_countries','api\AppUserController@get_countries');
Route::post('get_states','api\AppUserController@get_states');
Route::post('get_cities','api\AppUserController@get_cities');
Route::post('mark_favourite','api\AppUserController@mark_favourite');
Route::post('remove_favourite','api\AppUserController@remove_favourite');
Route::post('favourite_list','api\AppUserController@favourite_list');
Route::post('do_booking','api\AppUserController@do_booking');
Route::post('edit_booking','api\AppUserController@edit_booking');
Route::post('get_booking','api\AppUserController@get_booking');
Route::post('get_booking_details','api\AppUserController@get_booking_details');
Route::post('get_booking_details_owner','api\AppUserController@get_booking_details_owner');
Route::post('send_message','api\AppUserController@send_message');
Route::post('get_message','api\AppUserController@get_message');
Route::post('message_history','api\AppUserController@message_history');
Route::post('add_rating','api\AppUserController@add_rating');
Route::post('view_rating','api\AppUserController@view_rating');
Route::post('cancel_booking','api\AppUserController@cancel_booking');
Route::post('add_remainder','api\AppUserController@add_remainder');
Route::post('delete_remainder','api\AppUserController@delete_remainder');
Route::post('get_remainder','api\AppUserController@get_remainder');
Route::post('update_display_code','api\AppUserController@update_display_code');
Route::post('is_display_salon_code','api\AppUserController@is_display_salon_code');
Route::post('do_payment','api\AppUserController@do_payment');
Route::post('dashboard_user','api\AppUserController@dashboard_user');
Route::post('security_pin','api\AppUserController@security_pin');

Route::post('saloon_login','api\AppSaloonController@login');
Route::post('get_bookings','api\AppSaloonController@get_bookings');
Route::post('serve_now','api\AppSaloonController@serve_now');
Route::post('set_technician','api\AppSaloonController@set_technician');
Route::post('end_now','api\AppSaloonController@end_now');
Route::post('walkin_book','api\AppSaloonController@walkin_book');
Route::post('dashboard','api\AppSaloonController@dashboard');
Route::post('my_salon_list','api\AppSaloonController@my_salon_list');
Route::post('r_details','api\AppSaloonController@r_details');
Route::post('booking_history','api\AppSaloonController@booking_history');
Route::post('update_promotion_details','api\AppSaloonController@update_promotion_details');

Route::post('search_salons','api\AppUserController@search_salons');


/* ADD Salon*/
Route::post('category','api\AppSaloonController@category');
Route::post('service_list','api\AppSaloonController@service_list');
Route::post('add_salon_step1','api\AppSaloonController@add_salon_step1');
Route::post('add_salon_step2','api\AppSaloonController@add_salon_step2');
Route::post('add_salon_step3','api\AppSaloonController@add_salon_step3');
Route::post('add_salon_step4','api\AppSaloonController@add_salon_step4');
Route::post('edit_salon','api\AppSaloonController@edit_salon');
Route::post('delete_salon_image','api\AppSaloonController@delete_salon_image');
Route::post('edit_salon_image','api\AppSaloonController@edit_salon_image');
Route::post('image_displayorder','api\AppSaloonController@image_displayorder');
Route::post('get_salon_step3','api\AppSaloonController@get_salon_step3');
Route::post('salon_user_list','api\AppSaloonController@salon_user_list');
Route::post('user_booking_list','api\AppSaloonController@user_booking_list');


/* Add feedback */
Route::post('add_feedback','api\AppUserController@add_feedback');

/* Reward POINTS */
Route::post('reward_lists','api\AppUserController@reward_lists');
Route::post('add_rewards','api\AppUserController@add_rewards');
Route::post('edit_rewards','api\AppUserController@edit_rewards');
Route::post('delete_rewards','api\AppUserController@delete_rewards');
Route::post('my_reward_salon_list','api\AppUserController@my_reward_salon_list');
Route::post('my_reward_list_by_salon','api\AppUserController@my_reward_list_by_salon');
Route::post('customer_point_history','api\AppUserController@customer_point_history');
Route::post('reward_point_add_remove','api\AppUserController@reward_point_add_remove');
Route::post('get_customer_reward_point','api\AppUserController@get_customer_reward_point');

/* EMPLOYEE */
Route::post('search_user','api\AppUserController@search_user');
Route::post('send_invitation','api\AppUserController@send_invitation');
Route::post('send_invitation_direct','api\AppUserController@send_invitation_direct');
Route::post('accept_reject_invitation','api\AppUserController@accept_reject_invitation');
Route::post('employee_list','api\AppUserController@employee_list');
Route::post('my_invitation_list','api\AppUserController@my_invitation_list');
Route::post('my_accept_invitation_list','api\AppUserController@my_accept_invitation_list');
Route::post('remove_employee','api\AppUserController@remove_employee');
Route::post('search_existing_user','api\AppUserController@search_existing_user');

/* Static page */
Route::get('staticpage/terms_and_condition','api\StaticpageController@terms_and_condition');
Route::get('staticpage/loyalty','api\StaticpageController@loyalty');
Route::get('long_description/{id}','api\StaticpageController@salon_long_description');
Route::get('staticpage/user_manual','api\StaticpageController@user_manual');
Route::get('staticpage/salon_manual','api\StaticpageController@salon_manual');

/* CRON JOBS */
Route::get('push_notification','api\AppUserController@send_message_cronjob');
Route::get('remainder','api\AppUserController@admin_remainder_cronjob');
Route::get('user-remainder','api\AppUserController@user_remainder_cronjob');
Route::get('booking_notification','api\AppUserController@booking_notification');

/*GENERATE PDF*/
Route::post('report','api\AppSaloonController@generate_pdf');

/* BECOME NAILMASTER */
Route::post('become_nailmaster','api\AppSaloonController@become_nailmaster');

/* ATTENDANCE */
Route::post('attendance_checkin','api\AttendanceController@attendance_checkin');
Route::post('attendance_history','api\AttendanceController@attendance_history');

Route::get('salon_report/{salon_id}','api\AppSaloonController@salon_report');
Route::post('employee_report','api\AppSaloonController@employee_report');

/* FINANCIAL REPORT */
Route::post('financial_report','api\AppUserController@financial_report');