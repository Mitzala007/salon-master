<?php
return [
    'sitetitle' => 'Salon Master',
    'sitetitle1' => 'SALON MASTER',
    'minititle' => 'SM',
    'siteurl' => 'https://salonmaster.co/',
    'url_text' => 'Salonmaster.co',
    'display_url' => 'https://display.salonmaster.co/',
    'prefix' => 'SM',
    'title' => 'Salonmaster',
    'support_email' => 'support@salonmaster.co',
    'done_link' => 'salonmaster://done',
    'logout_link' => 'salonmaster://logout',
    'cancel_link' => 'salonmaster://cancel',
    'itunes_link' => 'https://apps.apple.com/us/app/salon-master/id1441863159',
    'play_store_link' => 'https://play.google.com/store/apps/details?id=com.salon.master',
    'captcha_key' => '6LdOCKYUAAAAAEBP7OOyosFXMx2MI7MGV4nR1AgF',
];
?>