<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'production',
        'certificate' => config_path()."/salonmaster_production.pem",
        'passPhrase'  =>'NailMaster',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'yourAPIKey',
        'service'     =>'gcm'
    )

);