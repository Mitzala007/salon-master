<?php

namespace App\Http\Middleware;

use App\saloon;
use App\saloon_employees;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;


class CheckStep
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user()->id;

            $plan = 0;
            if (Auth::user()->membership_plan==1 || Auth::user()->membership_plan==2 || Auth::user()->membership_plan==3){
                $plan = Auth::user()->membership_plan;
            }
            if (Auth::user()->admin_membership_plan==1 || Auth::user()->admin_membership_plan==2 || Auth::user()->admin_membership_plan==3){
                $plan = Auth::user()->admin_membership_plan;
            }

            $logged_user = User::where('id',$user)->first();
            if($logged_user['role'] == 'admin' || $logged_user['role'] == 'super_admin')
            {
                return $next($request);
            }
            elseif($plan==1 || $plan==2 || $plan==2)
            {
                $check_for_owner = saloon_employees::where('user_id',$logged_user['id'])->where('is_salon_owner',1)->first();
                $salon = saloon::where('id', $check_for_owner['saloon_id'])->where('status','active')->first();
                if(!empty($salon))
                {
                    if($salon['reg_step'] == 1)
                    {
                        return redirect('admin/saloon/services_edit/'.$salon['id']);
                    }
                    if($salon['reg_step'] == 2)
                    {
                        return redirect('admin/saloon/work_days/'.$salon['id']);
                    }
                    if($salon['reg_step'] == 3)
                    {
                        return $next($request);
                    }
                }
                else
                {
                    return redirect('admin/saloon/create');
                }
            }
        }
        return $next($request);
    }
}
