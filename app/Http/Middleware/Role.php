<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    public function handle($request, Closure $next)
    {
        $plan = 0;
        if (Auth::user()->membership_plan==1 || Auth::user()->membership_plan==2 || Auth::user()->membership_plan==3){
            $plan = Auth::user()->membership_plan;
        }
        if (Auth::user()->admin_membership_plan==1 || Auth::user()->admin_membership_plan==2 || Auth::user()->admin_membership_plan==3){
            $plan = Auth::user()->admin_membership_plan;
        }

        if(Auth::user()->role != 'admin' && ($plan != 1 || $plan != 2 || $plan != 3))
        {
            return redirect('/admin/dashboard');
        }
        return $next($request);
    }
}
