<?php

namespace App\Http\Controllers\website;

use App\Booking;
use App\Category;
use App\Login_details;
use App\saloon;
use App\Saloon_services;
use App\Service_type;
use App\Subcategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $data['category'] = Category::with('Subcategory')->where('status','active')->get();
        return view('website.index',$data);
    }
    public function app_download()
    {
        return view('website.app_download');
    }

    public function online_chat()
    {
        return view('website.online_support_chat');
    }

    public function privacy_policy()
    {
        return view('website.privacy_policy');
    }

    public function get_salons(Request $request)
    {
        $input = $request->all();
        $input['type'] = "web";
        $payload = json_encode($input);

        /*-------------------MAP GET SALOON LOCATIONS-------------------*/

        $url = url('api/get_saloons');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        $data['locations'] = json_decode($response,true);

        //print_r($data);
        return $data;
    }

    public function get_salon_images(Request $request){

        $input = $request->all();
        $input['type'] = "web";
        $payload = json_encode($input);

        /*-------------------MAP GET SALOON LOCATIONS-------------------*/

        $url = url('api/get_saloon_images');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return json_decode($response,true);

    }

    public function do_booking(Request $request)
    {
        $input = $request->all();
        $input['type'] = "web";
        $input['service_type'] = $request['service_type'];

        /* CHECK USER */
        if ($input['email'] !=""){
            $user  = User::where('phone',$input['phone'])->orwhere('email',$input['email'])->first();
        }
        else{
            $user  = User::where('phone',$input['phone'])->first();
        }

        if (empty($user)){
            $user = User::create($input);
            $input['user_id'] = $user['id'];
        }

        $input['user_id'] = $user['id'];
        $input['checkin_type'] = 2;
        $payload = json_encode($input);

        /*------------------- WALK IN BOOKING API -------------------*/
        $url = url('api/walkin_book');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return $data['booking'] = json_decode($response,true);
    }

    public function get_category(Request $request)
    {
        $input = $request->all();
        $input['type'] = "web";
        $payload = json_encode($input);

        /*-------------------MAP GET SALOON LOCATIONS-------------------*/

        $url = url('api/category');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        $data['category_list'] = json_decode($response,true);

        //print_r($data);
        return $data;
    }

    public function get_services(Request $request)
    {
        $salon_services = Saloon_services::where('saloon_id', $request['salon_id'])->pluck('type_id');

        if($request['type']=='service_list')
        {
            $service_type = Service_type::whereIn('id', $salon_services)->where('status','active')->get();
        }
        if($request['type']=='cat_service_list')
        {
            if($request['category_id']==0){
                $service_type = Service_type::whereIn('id', $salon_services)->where('status','active')->get();
            }
            else{
                $service_type = Service_type::whereIn('id', $salon_services)->where('category_id',$request['category_id'])->where('status','active')->get();
            }
        }
        if($request['type']=='sub_cat_service_list')
        {
            if($request['sub_cat_id']==0){
                $service_type = Service_type::whereIn('id', $salon_services)->where('category_id',$request['category_id'])->where('status','active')->get();
            }
            else{
                $service_type = Service_type::whereIn('id', $salon_services)->where('category_id',$request['category_id'])->where('sub_category_id',$request['sub_cat_id'])->where('status','active')->get();
            }
        }

        $a = "";
        $a.="<option value=''>Please Select</option>";
        foreach ($service_type as $k=>$v){
            $a.="<option value='$v->id'>$v->title</option>";
        }
        return $a;
    }

    public function set_category_session(Request $request)
    {
        if($request['type']=='category'){
            $sub_cat = Subcategory::where('category_id',$request['cid'])->count();
            if($sub_cat==0){
                session()->forget('SESS_SUB_CAT');
                session(['SESS_CAT' => $request['cid']]);
                return 'empty_data';
            }
            else{
                return 'not_empty_data';
            }
        }
        else{
            session()->forget('SESS_CAT');
            session(['SESS_SUB_CAT' => $request['sid']]);
            return '';
        }

    }


    public function tracking(Request $request, $remember_token,$booking_id){
        $loginUser = Login_details::where("remember_token",$remember_token)->first();

        $response["Result"] = 0;
        if (empty($loginUser) || $remember_token == "") {
            $response["Result"] = 1;
            $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
            return response($response, 200);
        }

        if (empty($booking_id) || $booking_id == "" ){
            $response["Result"] = 1;
            $response["Message"] = "Invalid Booking";
            return response($response, 200);
        }

        $user = User::where('id',$loginUser['user_id'])->first();

        $get_booking = Booking::where('id',$booking_id)->where('user_id',$loginUser['user_id'])->where('status','in-progress')->first();
        if ($get_booking) {



            $get_salon = saloon::where('id',$get_booking['saloon_id'])->where('status','active')->first();
            if ($get_salon){
                $response['Booking'] = $get_booking;
                $response['salon'] = $get_salon;
                $response['user'] = $user;
                //return $response;
            }
            return view('website.tracking',$response);
        }
        else{
            $response['Result'] = 0;
            $response['Message'] = "Booking Not Found!";
            return view('website.tracking',$response);
        }
    }

    public function get_salon_location(Request $request){
        $input = $request->all();

        $salon = saloon::select('latitude','longitude')->where('id',$input['sid'])->first();
        $data['message'] = 1;
        if ($salon){
            $data['latitude'] = $salon['latitude'];
            $data['longitude'] = $salon['longitude'];
            $data['message'] = 0;
        }
        return $data;
    }

    public function notification(Request $request,$token){

        $title = "Notification";
        $messages = "Notification testing";
        $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($messages, array(
            'badge' => 1,
            'title'     => $title,
            'sound' => 'example.aiff',
            'actionLocKey' => 'Action button title!',
            'locKey' => 'localized key',
        ));

        \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
            ->to($token)
            ->send($message);
        return "success";

    }
}
