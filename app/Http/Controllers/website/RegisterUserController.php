<?php

namespace App\Http\Controllers\website;

use App\cities;
use App\countries;
use App\Email;
use App\states;
use App\User;
use App\Login_details;
use App\Salon_owner_request;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Regulus\ActivityLog\Models\Activity;

class RegisterUserController extends Controller
{
    public function index()
    {
        $data['countries'] = countries::pluck('countryName', 'countryID')->all();
        $data['states'] = [];
        $data['cities'] = [];
        return view('website.register', $data);
    }

    public function register_user(Request $request)
    {

        $date = date('Y');
        $year = $date - 4 ;
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'birthdate' => 'date|before:'.$year,
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'phone' => 'required',
        ]);

        $input = $request->all();
        $input['role'] = 'user';
        //$input['sent_owner_request'] = 1;
        if ($photo = $request->file('image')) {
            $input['image'] = $this->image($photo, 'User');
        }

        $input['phone'] = $request['countries'].$request['phone'];
        $input['remember_token'] = str_random(30);
        $user = User::create($input);
        
        /* SENT OWNER REQUEST */
        $inp['user_id'] = $user['id'];
        $inp['salon_name'] = '';
        Salon_owner_request::create($inp);
        
        $input1['user_id'] = $user['id'];
        $input1['remember_token'] = $input['remember_token'];
        Login_details::create($input1);
        
        Activity::log([
            'contentId'   => $user->id,
            'contentType' => 'user management',
            'action'      => 'Insert',
            'description' => 'User Inserted from website',
            'details'     => 'User Add',
        ]);


        /* ------------ SEND PAYMENT LINK TO USER ------------ */
        $become_nailmaster = Email::findorFail(4);

        /* EMAIL CODE */
        $subject = $become_nailmaster['subject'];
        $from1 = $become_nailmaster['email'];

        $link = "<a href=".config('siteVars.siteurl')."api/payment/".$input['remember_token']." target='_blank'>".config('siteVars.siteurl')."api/payment/".$input['remember_token']."</a>";
        $string = ["{link}"];
        $replace_string = [$link];
        $data['content'] = str_replace($string,$replace_string,$become_nailmaster["content"]);
        $em= $input['email'];
        $bcc = [];

        $data['title'] = $become_nailmaster['title'];

        if ($input['email']!="" || $input['email']!=null){
            Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em, $bcc) {
                $m->from($from1, config('siteVars.title'));
                $m->bcc($bcc);
                $m->to($em)->subject($subject);
            });
        }

        /* SMS CODE */
        if ($input['phone']!="" || $input['phone']!=null){
            $sms_text = strip_tags($data['content']);
            if(substr($input['phone'], 0, 1) == 1) {
                $clickatell = new \Clickatell\Rest();
                $datas = ['to' => [$input['phone']], 'from' => '15732073611', 'content' => $sms_text];
            } else {
                $clickatell = new \Clickatell\Rest('india');
                $datas = ['to' => [$input['phone']], 'content' => $sms_text];
            }
            $result = $clickatell->sendMessage($datas);
        }

        return redirect('api/payment/'.$input['remember_token']);
    }

    public function do_payment(Request $request, $id)
    {
        $data['user_id'] = $id;
        return view('website.payment.do_payment',$data);
    }

    public function thank_you()
    {
        return view('website.thank_you');
    }
    
    public function failed()
    {
        return view('website.failed');
    }
    
    public function register_confirmation_email(Request $request)
    {
        $data['menu'] = "Register";
        $token=$request['ids'];
        $user = User::where('remember_token',$token)->first();
        if ($user){
            if($user['is_email_verified']==1){
                $data['message'] = "Email already verified!";
                return view('website.register_confirmation_email',$data);
            }
            else{
                $data['is_email_verified']='1';
                $user->update($data);

                $data['message'] = "Email verified successfully!";
                return view('website.register_confirmation_email',$data);
            }
        }
        else{
            $data['message'] = "Email already verified!";
            return view('website.register_confirmation_email',$data);
        }

    }

    public function get_states($cc)
    {
        $states = states::select('stateName', 'stateID')->where('countryID', $cc)
            ->orderBy('stateName', 'ASC')->get();
        $options = '<option value="0">Select State</option>';
        foreach ($states as $state) {
            $options .= '<option value="' . $state['stateID'] . '">' . $state['stateName'] . '</option>';
        }

        echo $options;
        return '';

    }

    public function get_cities($cc, $st)
    {
        $cities = cities::select('cityName', 'cityID')->where('countryID', $cc)->where('stateID', $st)
            ->orderBy('cityName', 'ASC')->get();
        $options = '';

        foreach ($cities as $city) {
            $options .= '<option value="' . $city['cityID'] . '">' . $city['cityName'] . '</option>';
        }

        echo $options;
        return '';

    }
}
