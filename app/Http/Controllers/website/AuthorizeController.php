<?php

namespace App\Http\Controllers\website;

use App\Booking;
use App\Login_details;
use App\Membership_plan;
use App\Payment_history;
use App\PaymentSubscription_History;
use App\saloon;
use App\Salon_owner_request;
use App\saloon_employees;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Regulus\ActivityLog\Models\Activity;


class AuthorizeController extends Controller
{
    public function index($session_token,$amount)
    {
        $user_id = Login_details::where('remember_token',$session_token)->first();
        $data['user_id'] = $user_id->user_id;
        $data['amount'] = $amount;
        return view('website.payment.authorize',$data);
        date_default_timezone_get('America/Los_Angeles');
    }



    function checkout(Request $request,$intervalLength=30)
    {

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        $refId = 'ref' . time();
        $card_number = $request['cardnumber'];
        // Subscription Type Info
        $subscription = new AnetAPI\ARBSubscriptionType();
        $subscription->setName("Membership Plan");

        $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
        $interval->setLength($intervalLength);
        $interval->setUnit("days");

        $paymentSchedule = new AnetAPI\PaymentScheduleType();
        $paymentSchedule->setInterval($interval);
        $s_date = Carbon::today()->format("Y-m-d");
        $paymentSchedule->setStartDate(new DateTime($s_date));
        $paymentSchedule->setTotalOccurrences("120");
        $paymentSchedule->setTrialOccurrences("1");

        $subscription->setPaymentSchedule($paymentSchedule);
        $subscription->setAmount($request->amount);
        $subscription->setTrialAmount("0.00");

        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_number);
        $ex_date = $request->expyear.'-'.$request->expmonth;
        $creditCard->setExpirationDate($ex_date);

        $payment = new AnetAPI\PaymentType();
        $payment->setCreditCard($creditCard);
        $subscription->setPayment($payment);

        $order = new AnetAPI\OrderType();
        $total_booking = Payment_history::count() + 1;
        $invoice_number = config('siteVars.prefix')."-" . Carbon::today()->format("Ymd") . "-" . (10000 + $total_booking);
        $order->setInvoiceNumber($invoice_number);
        $order->setDescription("Description of the subscription");
        $subscription->setOrder($order);

        $billTo = new AnetAPI\NameAndAddressType();
        $billTo->setFirstName($request->cardname);
        $billTo->setLastName("-");

        $subscription->setBillTo($billTo);

//        echo "<pre>";
//        print_r($subscription);
//        return;


        $request1 = new AnetAPI\ARBCreateSubscriptionRequest();
        $request1->setmerchantAuthentication($merchantAuthentication);
        $request1->setRefId($refId);
        $request1->setSubscription($subscription);
        $controller1 = new AnetController\ARBCreateSubscriptionController($request1);

        $response1 = $controller1->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
//            echo "<pre>";
//            print_r($response1);
//            return;

        if (($response1 != null) && ($response1->getMessages()->getResultCode() == "Ok") )
        {
            /*GET SUBSCRIPTION() METHOD CODE START*/
            $request2 = new AnetAPI\ARBGetSubscriptionRequest();
            $request2->setMerchantAuthentication($merchantAuthentication);
            $request2->setRefId($refId);
            $request2->setSubscriptionId($response1->getSubscriptionId());
            $request2->setIncludeTransactions(true);

            // Controller
            $controller2 = new AnetController\ARBGetSubscriptionController($request2);

            // Getting the response
            $response2 = $controller2->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
            if ($response2 != null)
            {
                if($response2->getMessages()->getResultCode() == "Ok")
                {
                    $transactions = $response2->getSubscription()->getArbTransactions();
//                    $pay_num = '';
//                    if($transactions != null){
//                        foreach ($transactions as $transaction) {
//                            echo "Transaction ID : ".$transaction->getTransId()." -- ".$transaction->getResponse()." -- Pay Number : ".$transaction->getPayNum()."\n";
//                            $pay_num = $transaction->getPayNum();
//                        }
//                    }

//                    echo "<pre>";
//                    print_r($response2->getSubscription());
//                    return;

                    if($response2->getSubscription()->getStatus() == 'active')
                    {
                        /*ENTRY IN DATABASE CODE START*/
                        $payment = new Payment_history();

                        $payment['user_id'] = $request->user_id;
                        $payment['invoice_number'] = $invoice_number;
                        $payment['subscription_id'] = $response1->getSubscriptionId();
                        $payment['amount'] = $request->amount;
                        $payment['customer_profile_id'] = $response2->getSubscription()->getProfile()->getCustomerProfileId();
                        $payment['customer_payment_id'] = $response2->getSubscription()->getProfile()->getPaymentProfile()->getCustomerPaymentProfileId();
                        $payment['transaction_id'] = '';
                        $payment['payment_status'] = 'success';
                        $payment['subscription_status'] = $response2->getSubscription()->getStatus();
                        $payment['payment_method'] = 'authorize';
                        $payment['payment_number'] = 0;
                        $payment->save();

                        $user = User::where('id',$request->user_id)->first();
                        if($response2->getSubscription()->getStatus() == 'active')
                        {
                            $input['is_email_verified'] = 1;
                            $user->update($input);

                            $get_request = Salon_owner_request::where('user_id',$user['id'])->where('status',0)->first();
                            if ($get_request){
                                $input1['status'] = 1;
                                $get_request->update($input1);
                            }
                        }

                        session(['payment_status' => 'success']);
                        return redirect('thank-you');
                        /*ENTRY IN DATABASE CODE END*/
                    }
                }
                else
                {
                    session(['payment_status' => 'unsuccess']);
                    return redirect('failed');
                }
            }
            else
            {
                session(['payment_status' => 'unsuccess']);
                return redirect('failed');
            }
            /*GET SUBSCRIPTION() METHOD CODE END*/
        }
        else
        {
            session(['payment_status' => 'unsuccess']);
            return redirect('failed');
        }
    }

    public function checkout_confirmation()
    {
        $data['payment_status'] = session()->get('payment_status');
        return view('website.payment.checkout_confirmation',$data);
    }

    function getSubscription($subscriptionId)
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        $refId = 'ref' . time();

        // Creating the API Request with required parameters
        $request = new AnetAPI\ARBGetSubscriptionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscriptionId($subscriptionId);
        $request->setIncludeTransactions(true);

        // Controller
        $controller = new AnetController\ARBGetSubscriptionController($request);

        // Getting the response
        $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if ($response != null)
        {
            if($response->getMessages()->getResultCode() == "Ok")
            {

                echo "<pre>";
                echo "SUCCESS: GetSubscription:" . "\n";
                // Displaying the details
                echo "Subscription Name: " . $response->getSubscription()->getName(). "\n";
                echo "Subscription amount: " . $response->getSubscription()->getAmount(). "\n";
                echo "Subscription status: " . $response->getSubscription()->getStatus(). "\n";
                echo "Subscription Description: " . $response->getSubscription()->getProfile()->getDescription(). "\n";
                echo "Customer Profile ID: " .  $response->getSubscription()->getProfile()->getCustomerProfileId() . "\n";
                echo "Customer payment Profile ID: ". $response->getSubscription()->getProfile()->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";

                $cnt = 0;
                $transactions = $response->getSubscription()->getArbTransactions();
                $get_payment = Payment_history::where('subscription_id',$subscriptionId)->first();
                if ($response->getSubscription()->getStatus()!="active"){

                    $input['subscription_status'] = $response->getSubscription()->getStatus();
                    $get_payment->update($input);

                    $get_user = User::where('id',$get_payment['user_id'])->first();
                    if ($get_user){
                        $dat['role']="user";
                        $get_user->update($dat);
                    }
                }
                else{
                    if($transactions != null){
                        foreach ($transactions as $transaction) {
                            $cnt++;
                            echo "Transaction ID : ".$transaction->getTransId()." -- ".$transaction->getResponse()." -- Pay Number : ".$transaction->getPayNum()."\n";
                        }

                        if ($response->getSubscription()->getStatus()=="active"){
                            $get_payment = Payment_history::where('subscription_id',$subscriptionId)->first();
                            $input['subscription_status'] = $response->getSubscription()->getStatus();
                            $input['payment_number'] = $cnt;
                            $get_payment->update($input);
                        }
                        else{

                            $get_payment = Payment_history::where('subscription_id',$subscriptionId)->first();
                            $input['subscription_status'] = $response->getSubscription()->getStatus();
                            $get_payment->update($input);

                            $get_user = User::where('id',$get_payment['user_id'])->first();
                            if ($get_user){
                                $dat['role']="user";
                                $dat['membership_plan']=0;
                                $get_user->update($dat);
                            }
                        }
                    }
                    else{
                        $get_payment = Payment_history::where('subscription_id',$subscriptionId)->first();
                        $input['subscription_status'] = $response->getSubscription()->getStatus();
                        $get_payment->update($input);
                    }
                }
            }
        }
    }

    function getSubscriptionStatus($subscriptionId)
    {
        /* Create a merchantAuthenticationType object with authentication details
           retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        $refId = 'ref' . time();

        $request = new AnetAPI\ARBGetSubscriptionStatusRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscriptionId($subscriptionId);

        $controller = new AnetController\ARBGetSubscriptionStatusController($request);

        $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
        {
            echo "SUCCESS: Subscription Status : " . $response->getStatus() . "\n";
        }
        else
        {
            echo "ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
        }

        //return $response;
    }


    function get_subscriptionlist()
    {
        /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        $refId = 'ref' . time();

        $sorting = new AnetAPI\ARBGetSubscriptionListSortingType();
        $sorting->setOrderBy("id");
        $sorting->setOrderDescending(true);

        $paging = new AnetAPI\PagingType();
        $paging->setLimit("1000");
        $paging->setOffset("1");


        /*STATUS ACTIVE CODE*/
        $request = new AnetAPI\ARBGetSubscriptionListRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setSearchType("subscriptionActive");
        $request->setSorting($sorting);
        $request->setPaging($paging);

        $controller = new AnetController\ARBGetSubscriptionListController($request);

        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            echo "<pre>";
            echo "SUCCESS: Subscription Details:" . "\n\n\n";
            foreach ($response->getSubscriptionDetails() as $subscriptionDetails) {
                echo "Subscription Name: " . $subscriptionDetails->getName(). "\n";
                echo "Subscription ID: " . $subscriptionDetails->getId() . "\n";
                echo "Subscription amount: " . $subscriptionDetails->getAmount(). "\n";
                echo "Subscription status: " . $subscriptionDetails->getStatus(). "\n";
                echo "Subscription Past Occurrences: " . $subscriptionDetails->getfirstName(). "\n\n\n\n";
            }
            echo "Total Number In Results:" . $response->getTotalNumInResultSet() . "\n";
        } else {
            echo "ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
        }


        /*STATUS INACTIVE CODE*/
        $request = new AnetAPI\ARBGetSubscriptionListRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setSearchType("subscriptionInactive");
        $request->setSorting($sorting);
        $request->setPaging($paging);

        $controller = new AnetController\ARBGetSubscriptionListController($request);

        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            echo "SUCCESS: Subscription Details:" . "\n\n\n";
            foreach ($response->getSubscriptionDetails() as $subscriptionDetails) {
                echo "Subscription Name: " . $subscriptionDetails->getName(). "\n";
                echo "Subscription ID: " . $subscriptionDetails->getId() . "\n";
                echo "Subscription amount: " . $subscriptionDetails->getAmount(). "\n";
                echo "Subscription status: " . $subscriptionDetails->getStatus(). "\n";
                echo "Subscription Past Occurrences: " . $subscriptionDetails->getfirstName(). "\n\n\n\n";
            }
            echo "Total Number In Results:" . $response->getTotalNumInResultSet() . "\n";
        } else {
            echo "ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
        }

        return $response;
    }

    public function cron_subscriptionlist(Request $request)
    {
        $payment_history  = Payment_history::where('subscription_status','active')->get();
        foreach ($payment_history as $k => $v){
            $this->getSubscription($v['subscription_id']);
        }
    }

    /* API DO PAYMENT PAGE */
    public function payment(Request $request,$token){
        $loginUser = Login_details::where("remember_token", $token)->first();
        if (empty($loginUser) || $token == "") {
            $data['message'] = "Invalid User";
            return view('website.mobile_payment.incomplete',$data);
        }

        $data['type'] = "other";
        if (isset($request['type'])){
            $data['type'] = $request['type'];
        }

        $data['token'] = $token;

        $data['plan'] = Membership_plan::OrderBy('displayorder','ASC')->where('status','active')->get();
        return view('website.mobile_payment.payment',$data);
    }

    public function payment_form(Request $request){

        $input = $request['session_token'];
        $loginUser = Login_details::where("remember_token", $request['session_token'])->first();
        if (empty($loginUser) || $request['session_token'] == "") {
            $data['message'] =  "Invalid User";
            $data['status'] = "0";
            return json_encode($data);
        }

        $data['uid'] =  $loginUser->user_id;
        $data['plan_id'] =  $request['plan_id'];
        $data['session_token'] = $request['session_token'];
        return json_encode($data);
    }

    // public function card_details(Request $request){

    //     //return $input = $request->all();
    //     $data['user_id'] = $request['user_id'];
    //     $data['session_token'] = $request['session_token'];
    //     return view('website.mobile_payment.payment_form',$data);
    // }

    public function payment_form_submit(Request $request,$intervalLength=30){

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

//             $merchantAuthentication->setName('3n746DKzn5');
//             $merchantAuthentication->setTransactionKey('7MAr537vzP8Bb35b');

        // Set the transaction's refId
        $refId = 'ref' . time();

        /* check membership */
        $plan = Membership_plan::where('id',$request['plan_id'])->where('status','active')->first();
        if ($plan){
            $amount = $plan['amount'];
            $is_recurring = $plan['is_recurring'];
            $total_occurance = $plan['total_recurring_occurance'];
            if ($is_recurring==0){
                $total_occurance = 1;
            }
            $plan_title = $plan['title'];
        }
        else{

            $data['message'] =  "Plan Expired!";
            $data['status'] = "0";
            return json_encode($data);
        }


        // Create the payment data for a credit card
        $card_auth_amount = "0.01";
        $card_number = $request['cardnumber'];
        $ex_date = $request->expyear.'-'.$request->expmonth;
        $cvv = $request['cvv'];
        $card_name = $request['cardname'];
        $user_id = $request['user_id'];

        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_number);
        $creditCard->setExpirationDate($ex_date);
        $creditCard->setCardCode($cvv);

        // Add the payment data to a paymentType object
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create order information

        $total_booking = Payment_history::count() + 1;
        $invoice_number = config('siteVars.prefix')."-" . Carbon::today()->format("Ymd") . "-" . (10000 + $total_booking);
        $order = new AnetAPI\OrderType();
        $order->setInvoiceNumber($invoice_number);
        $order->setDescription("Card authorization for membership plan.");


        // Create a TransactionRequestType object and add the previous objects to it
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($card_auth_amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);

        // Assemble the complete transaction request
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        // Create the controller and get the response
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if ($response != null) {


            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == "Ok") {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $a = "";
//                        $a .=  " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
//                        $a .= " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
//                        $a .= " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
//                        $a .= " Auth Code: " . $tresponse->getAuthCode() . "\n";
//                        $a .=" Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";


                    $transaction_status = $this->getTransactionDetails($tresponse->getTransId());

//                        if ($transaction_status=="FDSPendingReview"){
//
//                            /* Create a merchantAuthenticationType object with authentication details
//                            retrieved from the constants file */
//                            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
//                            $merchantAuthentication->setName('6q836tGbDQn');
//                            $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');
//
//                            // Set the transaction's refId
//                            $refId = 'ref' . time();
//
//                            // Now capture the previously authorized  amount
//                            //echo "Capturing the Authorization with transaction ID : " . $tresponse->getTransId() . "\n";
//                            $transactionRequestType = new AnetAPI\TransactionRequestType();
//                            $transactionRequestType->setTransactionType("priorAuthCaptureTransaction");
//                            $transactionRequestType->setRefTransId($tresponse->getTransId());
//
//
//                            $request = new AnetAPI\CreateTransactionRequest();
//                            $request->setMerchantAuthentication($merchantAuthentication);
//                            $request->setTransactionRequest( $transactionRequestType);
//
//                            $controller = new AnetController\CreateTransactionController($request);
//                            $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
//
//                            $a= "";
//                            if ($response != null)
//                            {
//                                if($response->getMessages()->getResultCode() == "Ok")
//                                {
//                                    $tresponse = $response->getTransactionResponse();
//
//                                    if ($tresponse != null && $tresponse->getMessages() != null)
//                                    {
//                                        $a.=" Transaction Response code : " . $tresponse->getResponseCode() . "\n";
//                                        $a.="Successful." . "\n";
//                                        $a.= "Capture Previously Authorized Amount, Trans ID : " . $tresponse->getRefTransId() . "\n";
//                                        $a.= " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
//                                        $a.= " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
//
//                                    }
//                                    else
//                                    {
//                                        $a.= "Transaction Failed \n";
//                                        if($tresponse->getErrors() != null)
//                                        {
//                                            $a.= " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                                            $a.= " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    $a.= "Transaction Failed \n";
//                                    $tresponse = $response->getTransactionResponse();
//                                    if($tresponse != null && $tresponse->getErrors() != null)
//                                    {
//                                        $a.= " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                                        $a.= " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
//                                    }
//                                    else
//                                    {
//                                        $a.= " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
//                                        $a.= " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                $a.=  "No response returned \n";
//                            }
//
//                            $data['message'] =  $a;
//                            $data['status'] = "0";
//                            return json_encode($data);
//                        }

//                        if ($transaction_status == "FDSPendingReview" || $transaction_status=="capturedPendingSettlement" || $transaction_status=="settledSuccessfully"){
                    if ($transaction_status=="capturedPendingSettlement" || $transaction_status=="settledSuccessfully")
                    {
                        // Recurring code
                        // Set the transaction's refId
                        $refId = 'ref1' . time();
                        // Subscription Type Info
                        $subscription = new AnetAPI\ARBSubscriptionType();
                        $subscription->setName($plan_title);

                        $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
                        $interval->setLength($intervalLength);
                        $interval->setUnit("days");

                        $paymentSchedule = new AnetAPI\PaymentScheduleType();
                        $paymentSchedule->setInterval($interval);
                        $s_date = Carbon::today()->format("Y-m-d");
                        $paymentSchedule->setStartDate(new DateTime($s_date));
                        $paymentSchedule->setTotalOccurrences($total_occurance);

                        $check_prev_payment = Payment_history::where('user_id',$user_id)->count();
                        if ($check_prev_payment>0){
                            $paymentSchedule->setTrialOccurrences("0");
                        }
                        else{
                            $paymentSchedule->setTrialOccurrences("1");
                        }

                        $subscription->setPaymentSchedule($paymentSchedule);
                        $subscription->setAmount($amount);
                        $subscription->setTrialAmount("0.00");

                        $creditCard = new AnetAPI\CreditCardType();
                        $creditCard->setCardNumber($card_number);
                        $creditCard->setExpirationDate($ex_date);

                        $payment = new AnetAPI\PaymentType();
                        $payment->setCreditCard($creditCard);
                        $subscription->setPayment($payment);

                        $order = new AnetAPI\OrderType();
                        $total_booking = Payment_history::count() + 1;
                        $invoice_number = config('siteVars.prefix')."-" . Carbon::today()->format("Ymd") . "-" . (90000 + $total_booking);
                        $order->setInvoiceNumber($invoice_number);
                        $order->setDescription("Description of the subscription");
                        $subscription->setOrder($order);

                        $billTo = new AnetAPI\NameAndAddressType();
                        $billTo->setFirstName($card_name);
                        $billTo->setLastName("-");

                        $subscription->setBillTo($billTo);

                        $request1 = new AnetAPI\ARBCreateSubscriptionRequest();
                        $request1->setmerchantAuthentication($merchantAuthentication);
                        $request1->setRefId($refId);
                        $request1->setSubscription($subscription);
                        $controller1 = new AnetController\ARBCreateSubscriptionController($request1);

                        $response1 = $controller1->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
                        if (($response1 != null) && ($response1->getMessages()->getResultCode() == "Ok") )
                        {
                            /*GET SUBSCRIPTION() METHOD CODE START*/
                            $request2 = new AnetAPI\ARBGetSubscriptionRequest();
                            $request2->setMerchantAuthentication($merchantAuthentication);
                            $request2->setRefId($refId);
                            $request2->setSubscriptionId($response1->getSubscriptionId());
                            $request2->setIncludeTransactions(true);

                            // Controller
                            $controller2 = new AnetController\ARBGetSubscriptionController($request2);

                            // Getting the response
                            $response2 = $controller2->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
                            if ($response2 != null)
                            {
                                if($response2->getMessages()->getResultCode() == "Ok")
                                {
                                    if($response2->getSubscription()->getStatus() == 'active')
                                    {
                                        /*ENTRY IN DATABASE CODE START*/
                                        $payment = new Payment_history();

                                        $payment['user_id'] = $user_id;
                                        $payment['plan_id'] = !empty($plan['id'])?$plan['id']:1;
                                        $payment['invoice_number'] = $invoice_number;
                                        $payment['subscription_id'] = $response1->getSubscriptionId();
                                        $payment['amount'] = $amount;
                                        $payment['customer_profile_id'] = $response2->getSubscription()->getProfile()->getCustomerProfileId();
                                        $payment['customer_payment_id'] = $response2->getSubscription()->getProfile()->getPaymentProfile()->getCustomerPaymentProfileId();
                                        $payment['transaction_id'] = '';
                                        $payment['payment_status'] = 'success';
                                        $payment['subscription_status'] = $response2->getSubscription()->getStatus();
                                        $payment['payment_method'] = 'authorize';
                                        $payment['payment_number'] = 0;
                                        $payment->save();

                                        /* check salon exist then make it active */
//                                            $get_salon  = saloon::where('user_id',$user_id)->first();
//                                            if ($get_salon){
//                                                $inp['status']='active';
//                                                $get_salon->update($inp);
//                                            }


                                        $user = User::where('id',$user_id)->first();
                                        if($user)
                                        {
                                            $input['is_email_verified'] = 1;
                                            //$input['role'] = 'user';
                                            $input['membership_plan'] = !empty($plan['id'])?$plan['id']:1;
                                            $user->update($input);

                                            $get_request = Salon_owner_request::where('user_id',$user['id'])->where('status',0)->first();
                                            if ($get_request){
                                                $input1['status'] = 1;
                                                $get_request->update($input1);
                                            }

                                        }

                                        $data['message'] = "Thank you! We have received your payment.<br>Please login into the ".config('siteVars.sitetitle')." App using the login information you have entered.";
                                        $data['status'] = "1";
                                        return json_encode($data);
                                    }
                                }
                                else
                                {
                                    $data['message'] =  "01 Payment Failed!";
                                    $data['status'] = "0";



                                    return json_encode($data);
                                }
                            }
                            else
                            {
                                $data['message'] =  "02 Payment Failed!";
                                $data['status'] = "0";



                                return json_encode($data);
                            }
                        }
                        else
                        {
                            //$data['message'] = "Credit Card Number is invalid.";
                            //return view('website.mobile_payment.incomplete',$data);
                            $data['message'] =  "03 Payment Failed!";
                            $data['status'] = "0";



                            return json_encode($data);
                        }
                    }
                    else{
                        //echo $transaction_status;
                        $data['message'] =  "04 Payment Failed! = ".$a;
                        $data['status'] = "0";



                        return json_encode($data);
                    }
                } else {
                    if ($tresponse->getErrors() != null) {
                        //echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                        //echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                        $data['message'] =  $tresponse->getErrors()[0]->getErrorText();
                        $data['status'] = "0";
                        return json_encode($data);
                    }
                }
                // Or, print errors if the API request wasn't successful
            } else {
                //echo "Transaction Failed \n";
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {



//                        echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
//                        echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    $data['message'] = $tresponse->getErrors()[0]->getErrorText().("<br>Error Code:".$tresponse->getErrors()[0]->getErrorCode());
                    $data['status'] = "0";



                    return json_encode($data);
                } else {
                    //echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
                    //echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";

                    $data['message'] =  $response->getMessages()->getMessage()[0]->getText().("<br>Error Code:".$response->getMessages()->getMessage()[0]->getCode());
                    $data['status'] = "0";



                    return json_encode($data);
                }
            }
        } else {
            echo  "No response returned \n";
        }

        /*-------------------------------------*/
    }

    public function payment_complete(Request $request){
        $data['message'] = $request['message'];
        return view('website.mobile_payment.complete',$data);
    }

    public function payment_incomplete(Request $request){

        $data['message'] = $request['message'];
        return view('website.mobile_payment.incomplete',$data);
    }

    public function subscription(Request $request,$session_token){

        $loginUser = Login_details::where("remember_token", $session_token)->first();
        if (empty($loginUser) || $session_token == "") {
            $data['message'] = "Invalid User";
            return view('website.mobile_payment.incomplete',$data);
        }
        $data['user_id'] = $loginUser->user_id;
        $data['session_token'] = $request['session_token'];

        $get_payment = Payment_history::where('user_id',$loginUser->user_id)->where('subscription_status','active')->orderBy('id','DESC')->first();


        if ($get_payment){
            $subscriptionId = $get_payment->subscription_id;
            /* GET SUBSCRIPTION DETAILS */

            /* Create a merchantAuthenticationType object with authentication details
              retrieved from the constants file */
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName('6q836tGbDQn');
            $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

            // Set the transaction's refId
            $refId = 'ref' . time();

            // Creating the API Request with required parameters
            $request = new AnetAPI\ARBGetSubscriptionRequest();
            $request->setMerchantAuthentication($merchantAuthentication);
            $request->setRefId($refId);
            $request->setSubscriptionId($subscriptionId);
            $request->setIncludeTransactions(true);

            // Controller
            $controller = new AnetController\ARBGetSubscriptionController($request);

            // Getting the response

            $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

//            echo "<pre>";
//            print_r($response);
//            return;
            if ($response != null)
            {
                if($response->getMessages()->getResultCode() == "Ok")
                {


                    $days = $response->getSubscription()->getpaymentSchedule()->getinterval()->getlength();
                    $unit = $response->getSubscription()->getpaymentSchedule()->getinterval()->getunit();


                    $data['payment_period'] = $days." ".$unit;
                    $data['subscription_name'] = $response->getSubscription()->getName();
                    $data['order_no'] = $response->getSubscription()->getorder()->getinvoiceNumber();
                    $data['order_date'] = date('m-d-Y',strtotime($get_payment['created_at']));
                    $data['amount'] = "USD ".$response->getSubscription()->getAmount();
                    $data['status'] = $response->getSubscription()->getStatus();
                    $data['card_number'] = $response->getSubscription()->getProfile()->getPaymentProfile()->getpayment()->getcreditCard()->getcardNumber();
                    $data['card_type'] = $response->getSubscription()->getProfile()->getPaymentProfile()->getpayment()->getcreditCard()->getcardType();
                    $data['subscription_id'] = $get_payment->subscription_id;

                    return view('website.mobile_payment.subscription',$data);
                }
            }
        }
    }


    public function cancel_subscription($subscriptionId){

        /* Create a merchantAuthenticationType object with authentication details
           retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        $refId = 'ref' . time();

        $request = new AnetAPI\ARBCancelSubscriptionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setSubscriptionId($subscriptionId);

        $controller = new AnetController\ARBCancelSubscriptionController($request);

        $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
        {
            $successMessages = $response->getMessages()->getMessage();
            //echo "SUCCESS : " . $successMessages[0]->getCode() . "  " .$successMessages[0]->getText() . "\n";


            /* payment cancel */
            $get_payment = Payment_history::where('subscription_id',$subscriptionId)->first();
            if ($get_payment){
                $inp['subscription_status'] = "Canceled";
                $get_payment->update($inp);
            }

            /* User role change*/
            $get_user = User::where('id',$get_payment->user_id)->first();
            if ($get_user){
                $inp1['role'] = "user";
                $get_user->update($inp1);

                /* clear all session */
                $login_details  = Login_details::where('user_id',$get_user->id)->delete();
            }

            /* salon inactive */
            $remove_owner = saloon_employees::where('user_id',$get_user->id)->where('is_salon_owner','1')->get();
            foreach ($remove_owner as $k => $v){
                $get_salon = saloon::where('is',$v['saloon_id'])->first();
                if ($get_salon){
                    $inp2['status'] = "in-active";
                    $get_salon->update($inp2);

                    /* remove all employee login */
                    $get_employee = User::where('saloon_id',$get_salon->id)->get();
                    foreach ($get_employee as $k=>$v){
                        $login_details  = Login_details::where('user_id',$v['id'])->delete();
                    }
                    /* cancel all booking (serve-now and waiting) */
                    $cancel_booking = Booking::where('saloon_id',$get_salon->id)->where('status','!=','completed')->update(['status' => 'cancel']);
                }
            }

            //$get_salon = saloon::where('user_id',$get_user->id)->first();
            //if ($get_salon){
            //$inp2['status'] = "in-active";
            //$get_salon->update($inp2);

            /* remove all employee login */
            //$get_employee = User::where('saloon_id',$get_salon->id)->get();
            //foreach ($get_employee as $k=>$v){
            //    $login_details  = Login_details::where('user_id',$v['id'])->delete();
            //}
            /* cancel all booking (serve-now and waiting) */
            //$cancel_booking = Booking::where('saloon_id',$get_salon->id)->where('status','!=','completed')->update(['status' => 'cancel']);
            //}
            //return redirect('api/subscription_cancel');
            $data['message'] = "Subscription Cancel!";
            $data['status'] = "0";
            return json_encode($data);
        }
        else {

            $data['message'] = "Subscription Cancel!";
            $data['status'] = "0";
            return json_encode($data);
        }

        $data['message'] = "Subscription Cancel!";
        $data['status'] = "0";
        return json_encode($data);
    }


    function getTransactionDetails($transactionId)
    {
        /* Create a merchantAuthenticationType object with authentication details
           retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('6q836tGbDQn');
        $merchantAuthentication->setTransactionKey('92ZzK8g8qQ96FZeM');

        // Set the transaction's refId
        // The refId is a Merchant-assigned reference ID for the request.
        // If included in the request, this value is included in the response.
        // This feature might be especially useful for multi-threaded applications.
        $refId = 'ref' . time();

        $request = new AnetAPI\GetTransactionDetailsRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setTransId($transactionId);

        $controller = new AnetController\GetTransactionDetailsController($request);

        $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
//        echo  "<pre>";
//        print_r($response);
//            return;

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
        {
            return $response->getTransaction()->getTransactionStatus();
            //echo "                Auth Amount:" . $response->getTransaction()->getAuthAmount() . "\n";
            //echo "                   Trans ID:" . $response->getTransaction()->getTransId() . "\n";
        }
        else
        {
            //echo "ERROR :  Invalid response\n";
            return $errorMessages = $response->getMessages()->getMessage();
            //echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
        }
        return "paymentfailed";

        //capturedPendingSettlement
    }
}
