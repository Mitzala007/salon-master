<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


    /*Check Role Of Login User*/
    protected function authenticated(Request $request, $user)
    {
        $plan = 0;
        if ($user->membership_plan==1 || $user->membership_plan==2 || $user->membership_plan==3){
            $plan = $user->membership_plan;
        }
        if ($user->admin_membership_plan==1 || $user->admin_membership_plan==2 || $user->admin_membership_plan==3){
            $plan = $user->admin_membership_plan;
        }

        if( $user->role == 'admin' || $plan == 1 || $plan == 2 || $plan == 3)
        {
            return redirect('/admin');
        }
        else
        {
            session()->flush();
            return redirect()->back()->withErrors(array('global' => "Sorry, You Have Not Permission For Login."));
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $loginView = 'admin.auth.login';
    protected $redirectTo = '/admin';
    protected $redirectAfterLogout = 'admin/';


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
