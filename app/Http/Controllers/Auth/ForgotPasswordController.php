<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset_password()
    {
        $data['menu'] = "Reset Password";
        return view('admin.auth.passwords.email',$data);
    }

    public function send_password(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $user=User::where('email',$request['email'])->first();

        if(!empty($user)){
            $name=$user['name'];
            $token=$user['remember_token'];

            $to1=$request['email'];
            $from1=config('siteVars.support_email');
            $subject2='Forget Password';
            $mailcontent1 = "Dear <b>" . $name . "</b>, You have requested to reset your password. Please use this link <b><a href=".config('siteVars.siteurl')."admin/generate_password?reset=".$token." target='_blank'>".config('siteVars.siteurl')."admin/generate_password?reset=".$token."</a> </b> to reset your password.";
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: $from1\r\n";
            mail($to1,$subject2,$mailcontent1,$headers);

            $input['date_time']=Carbon::now();
            $user->update($input);

            \Session::flash('success','New Password Information Has Been Send To Your Mail Account Shortly.');
            return redirect()->back();
        }
        else{
            \Session::flash('forgeterror','email');
            return back()->withInput()->withErrors(['femail' => 'Invalid Email Please Try Again!']);
        }
    }

    public function generate_password(Request $request)
    {
        $user = User::where('remember_token',$request['reset'])->first();
        $user_time=$user['date_time'];

        $current_date_time = date('Y-m-d');
        $user_date = date('Y-m-d',strtotime($user_time));

        if ($current_date_time==$user_date)
        {
            $user_time = date('H:i:s',strtotime($user_time));
            $diff_date = Carbon::now()->diffInMinutes($user_time);
            if($diff_date<='30')
            {
                $data['menu'] = "Reset Password";
                return view('admin.auth.passwords.generate_password',$data);
            }
            else
            {
                return view('admin.404');
            }
        }
        else
        {
            return view('admin.404');
        }
    }

    public function generate_password_store(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
        ]);

        $input = $request->all();

        $user = User::where('remember_token',$request['token'])->first();
        $user->update($input);
        \Session::flash('success','Password updated successfully!');
        return redirect('admin/login');
    }
}
