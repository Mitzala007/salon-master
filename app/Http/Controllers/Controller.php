<?php

namespace App\Http\Controllers;

use App\Errorlog;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Image;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $pagination=10;
    protected $service_pagination=100;
    protected $version = "Sal@#2019y31d05m-%";
    protected $version1 = "Sal@#2019mn08d21-%";

    public function language($request){
        $cookie = $request->cookie('language');
        if(empty($cookie) && $cookie==""){
            $cookie = "en";
        }
        return $cookie;
    }

    public function image($photo,$path)
    {
        /* IMAGE UPLOAD VALIDATION */
        $img_ext = $photo->getClientOriginalExtension();
        if ($img_ext=="jpeg" || $img_ext=="jpg" || $img_ext=="png" || $img_ext=="bmp" || $img_ext=="gif" || $img_ext=="JPEG" || $img_ext=="JPG" || $img_ext=="PNG" || $img_ext=="BMP" || $img_ext=="GIF" ) {}
        else{
            return back()->withInput()->withErrors(['image' => 'Invalid file type.']);
        }
        /* ----------------------- */

        $root = base_path() . '/public/resource/'.$path ;
        $name = str_random(20).".".$photo->getClientOriginalExtension();
        $mimetype = $photo->getMimeType();
        $explode = explode("/",$mimetype);
        $type = $explode[0];

        if (!file_exists($root)) {
            mkdir($root, 0777, true);
        }
        $photo->move($root,$name);
        return $path = "public/resource/".$path."/".$name;
    }

    public function Imagethumbnail($image,$folder,$heigth,$width,$prefix=null)
    {
        $imagename=basename($image);
        $savepath = base_path().'/public/resource/'.$folder.'/';
        if (!file_exists($savepath)) {
            mkdir($savepath, 0777, true);
        }
        //$img = Image::make($image)->resize($heigth, $width)->save($savepath.$prefix.$imagename);
        $img = Image::make($image)->resize(null, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save($savepath.$prefix.$imagename);
        return 'public/resource/'.$folder.'/'.$imagename;
    }

    /*WIDTH SET 1024px FOR IMAGE*/
    public function Imagethumbnail_1024($image,$folder,$heigth,$width,$prefix=null)
    {
        $imagename=basename($image);
        $savepath = base_path().'/public/resource/'.$folder.'/1024/';
        if (!file_exists($savepath)) {
            mkdir($savepath, 0777, true);
        }
        //$img = Image::make($image)->resize($heigth, $width)->save($savepath.$prefix.$imagename);
        $img = Image::make($image)->resize(null, 1024, function ($constraint) {
            $constraint->aspectRatio();
        })->save($savepath.$prefix.$imagename);
        return 'public/resource/'.$folder.'/1024/'.$imagename;
    }


    public function errorlog($message,$data,$api)
    {
        $input5['message'] = $message;
        $input5['data'] = $data;
        $input5['api'] = $api;
        $error_log = Errorlog::create($input5);
    }

    public function file($photo,$path)
    {
        /* IMAGE UPLOAD VALIDATION */
        $img_ext = $photo->getClientOriginalExtension();
        $img_name = $photo->getClientOriginalName();

        /* ----------------------- */

        $root = base_path() . '/public/resource/'.$path ;
        $name = $photo->getClientOriginalName();
        $mimetype = $photo->getMimeType();
        $explode = explode("/",$mimetype);
        $type = $explode[0];

        if (!file_exists($root)) {
            mkdir($root, 0777, true);
        }
        $photo->move($root,$name);
        return $path = "public/resource/".$path."/".$name;
    }

    function getTimeDiff($dtime,$atime)
    {
        $nextDay = $dtime>$atime?1:0;
        $dep = explode(':',$dtime);
        $arr = explode(':',$atime);
        $diff = abs(mktime($dep[0],$dep[1],0,date('n'),date('j'),date('y'))-mktime($arr[0],$arr[1],0,date('n'),date('j')+$nextDay,date('y')));
        $hours = floor($diff/(60*60));
        $mins = floor(($diff-($hours*60*60))/(60));
        $secs = floor(($diff-(($hours*60*60)+($mins*60))));
        if(strlen($hours)<2){$hours="0".$hours;}
        if(strlen($mins)<2){$mins="0".$mins;}
        if(strlen($secs)<2){$secs="0".$secs;}
        return $hours.':'.$mins.':'.$secs;
    }

    function firebase_notification($title,$message,$device_token,$type,$send_to,$bid,$salon_id,$salon_title=null){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['type' => $type,'send_to',$send_to,'booking_id'=>$bid,'salon_id'=>$salon_id,'salon_title'=>$salon_title]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $device_token;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

    }

    function get_timezone($latitude,$longitude){
        $time = time();
        $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$latitude,$longitude&timestamp=$time&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $responseJson = curl_exec($ch);
        curl_close($ch);
        $response1 = json_decode($responseJson);
        return $response1->timeZoneId;
    }
}
