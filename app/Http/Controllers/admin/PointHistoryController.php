<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\Point_history;
use App\saloon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;

class PointHistoryController extends Controller
{
    public function point(Request $request, $id)
    {
        $data['menu'] = "User";
        $data['id'] = $id;
        $book_salon = Booking::where('user_id',$id)->pluck('saloon_id');
        $data['salon'] = saloon::whereIn('id',$book_salon)->where('status','active')->pluck('title','id')->prepend('Please Select','');
        if(isset($request['search']) && $request['search'] != '')
        {
            $query = Point_history::where('user_id',$id)->select();

            if (!empty($request->saloon_id))
                $query->where('salon_id',  $request->saloon_id);

            if (!empty($request->start_date))
                $query->where('created_at', '>=', Carbon::parse($request->start_date));

            if (!empty($request->end_date))
                $query->where('created_at', '<', Carbon::parse($request->end_date)->addDay(1));

            if (!empty($request->type))
                $query->where('type', $request->type);

            if (!empty($request->point))
                $query->where('point', $request->point);

            $data['point'] = $query->Paginate($this->pagination);
        }
        else
        {
            $data['point'] = Point_history::where('user_id',$id)->orderBy('id','DESC')->Paginate($this->pagination);
        }



        if ($request->ajax())
        {
            return view('admin.users.point_table',$data);
        }
        return view('admin.users.point_history',$data);
    }

    public function add_point(Request $request)
    {
        $this->validate($request, [
            'salon_id' => 'required',
        ]);

        $get_points = Point_history::where('user_id',$request['user_id'])->where('salon_id',$request['salon_id'])->orderBy('id','DESC')->first();

        $input['user_id'] = $request['user_id'];
        $input['salon_id'] = $request['salon_id'];
        $input['type'] = $request['type'];
        $input['point'] = $request['point'];
        $input['is_last'] = 1;
        if(!empty($get_points))
        {
            $inp['is_last'] = 0;
            $get_points->update($inp);

            $input['point_before'] = $get_points->point_after;
            if($request['type'] == 'CREDIT')
            {
                $input['point_after'] = $request['point'] + $input['point_before'];
            }
            else{
                if($get_points->point_after > $request['point'])
                {
                    $input['point_after'] =  $input['point_before'] - $request['point'];
                }
                else
                {
                    \Session::flash('danger', 'This user have not insufficient points.');
                    return redirect('admin/point_history/'.$request['user_id']);
                }
            }
        }
        else
        {
            $input['point_before'] = 0;
            $input['point_after'] = $request['point'];
        }
        $input['remark'] = $request['point']." ".$request['type'].' by '.config('siteVars.title').' support team';

        $point = Point_history::create($input);

        Activity::log([
            'contentId'   => $point->id,
            'contentType' => 'user management',
            'action'      => 'Insert',
            'description' => 'Point Inserted by '.Auth::user()->role,
            'details'     => 'Point Add',
        ]);

        \Session::flash('success', 'Point has been inserted successfully.');
        return redirect()->back();
    }
}
