<?php

namespace App\Http\Controllers\admin;

use App\Staticpage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;

class StaticpageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        if($id==1){
            $data['mainmenu']="Staticpage";
            $data['menu']="Terms and Conditions";
        }
        if ($id==2){
            $data['mainmenu']="Staticpage";
            $data['menu']="Loyalty Program";
        }

        if ($id==3){
            $data['mainmenu']="Staticpage";
            $data['menu']="User Manual";
        }

        if ($id==4){
            $data['mainmenu']="Staticpage";
            $data['menu']="Salon Manual";
        }

        $data['id'] = $id;
        $data['staticpage'] = Staticpage::findOrFail($id);
        return view('admin.staticpage.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $staticpage = Staticpage::findOrFail($id);
        $input = $request->all();
        $staticpage->update($input);


        if ($id==1) {
            Activity::log([
                'contentId' => $staticpage->id,
                'contentType' => 'Terms and Conditions',
                'action' => 'Update',
                'description' => 'Terms and Conditions updated by ' . Auth::user()->role,
                'details' => 'Terms and Conditions Update',
            ]);

            \Session::flash('success', 'Terms and Conditions has been Updated successfully!');
        }
        if ($id==2){
            Activity::log([
                'contentId' => $staticpage->id,
                'contentType' => 'Loyalty Program',
                'action' => 'Update',
                'description' => 'Loyalty Program updated by ' . Auth::user()->role,
                'details' => 'Loyalty Program Update',
            ]);

            \Session::flash('success', 'Loyalty Program has been Updated successfully!');
        }

        if ($id==3){
            Activity::log([
                'contentId' => $staticpage->id,
                'contentType' => 'User Manual',
                'action' => 'Update',
                'description' => 'User Manual updated by ' . Auth::user()->role,
                'details' => 'User Manual',
            ]);

            \Session::flash('success', 'User Manual has been Updated successfully!');
        }

        if ($id==4){
            Activity::log([
                'contentId' => $staticpage->id,
                'contentType' => 'Salon Manual',
                'action' => 'Update',
                'description' => 'Salon Manual updated by ' . Auth::user()->role,
                'details' => 'Salon Manual',
            ]);

            \Session::flash('success', 'Salon Manual has been Updated successfully!');
        }
        

        return redirect('admin/staticpage/'.$id.'/edit');
    }

    public function destroy($id)
    {
    }
}
