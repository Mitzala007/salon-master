<?php

namespace App\Http\Controllers\admin;

use App\Login_details;
use App\Notification;
use Davibennun\LaravelPushNotification\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;


class NotificationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        $data=[];
        $data['menu']="Notification";
        $data['notification'] = Notification::OrderBy('id','DESC')->Paginate($this->pagination);

        if ($request->ajax())
        {
            return view('admin.notification.table',$data);
        }
        return view('admin.notification.add', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $input = $request->all();
        $message = $request->message;

        $user = Login_details::all();
        foreach ($user as $values){
            $device_char =  strlen($values['device_token']);
            $op['message'] = $request['message'];

            if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
//                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($op['message'],array(
//                    'badge' => 1,
//                    'title' => 'Nailmaster',
//                    'sound' => 'example.aiff',
//                    'actionLocKey' => 'Action button title!',
//                    'locKey' => 'localized key'
//                ));

//                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                    ->to($values['device_token'])
//                    ->send($message);
            }
        }
        $input['notification_type']='admin';
        $input['message']=$request['message'];
        $input['is_read']=0;
        $notification = Notification::create($input);

        Activity::log([
            'contentId'   => $notification->id,
            'contentType' => 'notification management',
            'action'      => 'Insert',
            'description' => 'Notification send by '.Auth::user()->role,
            'details'     => 'Send Notification',
        ]);

        \Session::flash('success', 'Notification send successfully');
        return redirect('admin/notification');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

         Activity::log([
            'contentId'   => $notification->id,
            'contentType' => 'notification management',
            'action'      => 'Delete',
            'description' => 'Notification deleted by '.Auth::user()->role,
            'details'     => 'Delete Notification',
        ]);

        \Session::flash('danger','Notification has been deleted successfully!');
        return ;
    }
}
