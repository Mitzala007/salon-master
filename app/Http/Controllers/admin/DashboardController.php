<?php

namespace App\Http\Controllers\admin;

use App\AppUser;
use App\Booking;
use App\Employees;
use App\Http\Controllers\Controller;
use App\Rating;
use App\saloon;
use App\saloon_employees;
use App\Saloon_services;
use App\Salon_guides;
use App\Service_type;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Regulus\ActivityLog\Models\Activity;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function index(Request $request)
    {
        $data['mainmenu'] = "";
        $data['menu'] = "Dashboard";
        $today = Carbon::now();
        $date=$today->toDateString();

        if(Auth::user()->role == 'admin')
        {
            $data['total_user'] = User::where('role', '!=', 'admin')->where('deleted_at',null)->count();
            $data['normal_user'] = User::where('role','user')->where('deleted_at',null)->count();
            $data['salon_user'] = saloon_employees::where('is_salon_owner',1)->count();
            $data['total_salon'] = saloon::where('deleted_at',null)->count();
            $data['total_service'] = Service_type::where('deleted_at',null)->count();
            $data['total_booking'] = Booking::where('deleted_at',null)->count();
//            $data['locations'] = saloon::where('deleted_at',null)->get();
            $data['booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->orderBy('id','desc')->Paginate($this->pagination);

            /*-------------------MAP GET SALOON LOCATIONS-------------------*/
//            $url = 'https://nailmaster.co/api/get_saloons';
            $url = url('api/get_saloons');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $data['locations'] = json_decode($response,true);

            /*-------------------MONTHLY BOOKING DETAILS FOR CHART-------------------*/

            $today = Carbon::today();
            $chart_labels = [];
            $chart_data = [];
            $from = Carbon::today()->addDay(-30);
            $booking_chart = Booking::where('created_at','>=', $from)->get();

            while($from <= $today)
            {
                $key = $from->format('j/n');
                $chart_labels[] = $key;
                $chart_data[$key] = 0;
                $from->addDay(1);
            }

            foreach($booking_chart as $book)
            {
                $booking_count = Booking::whereDate('created_at',date('Y-m-d', strtotime($book['created_at'])))->count();
                $chart_data[$book->created_at->format('j/n')] = $booking_count;
            }

            $data['booking_chart_lable'] = $chart_labels;
            $data['booking_chart_data'] = array_values($chart_data);

            $get_salon_owner = saloon_employees::where('is_salon_owner',1)->groupBy('user_id')->orderBy('id','DESC')->pluck('user_id');
            $data['user'] = \App\User::where('status','active')->whereIn('id',$get_salon_owner)->latest()->take(8)->get();
        }
        else
        {
            $data['id'] = Auth::user()->id;
            if (session('salon_id')!="")
            {
                $salon_id = session('salon_id');
                $data['salon_id'] = saloon::where('id', $salon_id)->first();

                if($data['salon_id']['timezone'] != '') {
                    $salon_timezone = $data['salon_id']['timezone'];
                } else {
                    $salon_timezone = 'America/Chicago';
                }

            }
            else{
                $get_salon_owner = saloon_employees::where('is_salon_owner',1)->where('user_id',$data['id'])->first();
                $data['salon_id'] = saloon::where('id', $get_salon_owner['saloon_id'])->first();

                if($data['salon_id']['timezone'] != '') {
                    $salon_timezone = $data['salon_id']['timezone'];
                } else {
                    $salon_timezone = 'America/Chicago';
                }
            }

            session(['salon_id' => $data['salon_id']['id']]);
            session(['salon_timezone' => $salon_timezone]);


            /* create salon dropdown in dashboard */
            $my_owner_salon_list = saloon_employees::where('is_salon_owner',1)->where('user_id',$data['id'])->pluck('saloon_id');
            $data['salon_list'] = saloon::select('id','title')->wherein('id',$my_owner_salon_list)->where('status','active')->get();
            /* -------------------------------  */

            $data['total_booking'] = Booking::where('saloon_id',$data['salon_id']['id'])->where('deleted_at',null)->count();
            $data['total_emp'] = saloon_employees::where('saloon_id',$data['salon_id']['id'])->where('is_salon_owner',0)->count();
            $data['total_service'] = Saloon_services::where('saloon_id',$data['salon_id']['id'])->where('deleted_at',null)->count();
            $data['total_rating'] = Rating::with('User')->where('saloon_id', $data['salon_id']['id'])->orderBy('id','desc')->count();
            $data['rating'] = Rating::with('User')->where('saloon_id', $data['salon_id']['id'])->orderBy('id','desc')->get();



            $data['booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->wheredate('date',$date)
                ->where('saloon_id',$data['salon_id']['id'])->orderBy('id','desc')->Paginate($this->pagination);

            $data['pending_booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')
                ->wheredate('date','!=',$date)->where('status','!=','completed')->where('status','!=','cancel')
                ->where('saloon_id',$data['salon_id']['id'])->orderBy('id','desc')->Paginate($this->pagination);

            $data['waiting_list'] = Booking::where('date',date('Y-m-d'))
                ->where('saloon_id',$data['salon_id']['id'])->where('status', 'waiting')->count();
            $data['recent_booking'] = Booking::where('saloon_id',$data['salon_id']['id'])->where('date',date('Y-m-d'))->count();


            $get_booking_users = Booking::where('saloon_id',$data['salon_id']['id'])->groupBy('user_id')->pluck('user_id');
            $data['total_customer'] = User::whereIn('id',$get_booking_users)->where('status','active')->count();

            /*-------------------MONTHLY BOOKING DETAILS FOR CHART-------------------*/

            $today = Carbon::today();
            $chart_labels = [];
            $chart_data = [];
            $from = Carbon::today()->addDay(-30);
            $booking_chart = Booking::where('saloon_id',$data['salon_id']['id'])->where('created_at','>=', $from)->get();

            while($from <= $today)
            {
                $key = $from->format('j/n');
                $chart_labels[] = $key;
                $chart_data[$key] = 0;
                $from->addDay(1);
            }

            foreach($booking_chart as $book)
            {
                $booking_count = Booking::where('saloon_id',$data['salon_id']['id'])->whereDate('created_at',date('Y-m-d', strtotime($book['created_at'])))->count();
                $chart_data[$book->created_at->format('j/n')] = $booking_count;
            }

            $data['booking_chart_lable'] = $chart_labels;
            $data['booking_chart_data'] = array_values($chart_data);
        }
        if ($request->ajax())
        {
            if($request['type']=='latest_booking'){
                return view('admin.latest_booking',$data);
            }
            else{
                return view('admin.pending_booking',$data);
            }
        }

        $data['amount_chart_labels'] ="";
        $data['amount_chart_data'] = "";

        return view('admin.dashboard',$data);
    }

    public function dashboard_salon_session(Request $request){
        if (isset($request['salon_id'])){
            session(['salon_id' => $request['salon_id']]);
        }
        return redirect('admin/dashboard');
    }

    /*-----------------------------------------LOG REPORT-----------------------------------------*/

    public function log_report(Request $request)
    {
        $data['menu'] = 'Log';
        if(isset($request['search']) && $request['search'] != '')
        {
            $filter = (object)[];
            $fileds  = [
                'user_email', 'start_date', 'end_date', 'action', 'section'
            ];

            foreach($fileds as $field)
            {
                $filter->{$field} = $request->input($field);
            }

            $query = Activity::query();

            if ($request->has('user_email'))
                $query->where('user_id',  $filter->{'user_email'});

            if (isset($request['start_date']) && $request['start_date']!="") {
                $start_date = date('Y-m-d', strtotime($request['start_date']));
                $query->where('created_at', '>=', $start_date);
            }

            if (isset($request['end_date']) && $request['end_date']!="") {
                $end_date = date('Y-m-d', strtotime($request['end_date']));
                $query->where('created_at', '<=', $end_date);
            }

            if ($request->has('action'))
                $query->whereIn('action', $filter->action);

            if ($request->has('section'))
                $query->whereIn('content_type', $filter->section);

//            $search=$request['search'];
//            $data['logs'] = Activity::where(function ($query) use ($search) {
//                    $query->orWhere('user_id','like','%'.$search.'%' );
//                    $query->orWhere('content_type', 'like','%'.$search.'%');
//                    $query->orWhere('content_id', 'like','%'.$search.'%');
//                    $query->orWhere('action', 'like','%'.$search.'%');
//                    $query->orWhere('description', 'like','%'.$search.'%');
//                    $query->orWhere('details', 'like','%'.$search.'%');
//                })->OrderBy('id', 'desc')->Paginate($this->pagination);

            $data['logs'] = $query->Paginate($this->pagination);
        }
        else
        {
            $data['logs'] = Activity::orderBy('id','desc')->Paginate($this->pagination);
        }

        if ($request->ajax())
        {
            return view('admin.log_report.table',$data);
        }
        return view('admin.log_report.index',$data);
    }
    
    
     public function salon_guide($id)
    {
        $data['menu'] = 'Salon Management Guide';
        $data['guide'] = Salon_guides::findOrFail($id);
        return view('admin.salon_guide.salon_guide',$data);
    }

    public function salon_guide_update(Request $request, $id)
    {
        $guide = Salon_guides::findOrFail($id);

        $input = $request->all();

        if($photo = $request->file('file'))
        {
            $input['file'] = $this->file($photo,'Salon_Guide');
        }

        $guide->update($input);
        Activity::log([
            'contentId'   => $guide->id,
            'contentType' => 'salon management guide',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => 'Salon Management Guide Update',
        ]);
        \Session::flash('success', 'Salon Management Guide has been updated successfully!');

        return redirect('admin/salon_guide/'.$id);
    }
}
