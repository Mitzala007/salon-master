<?php

namespace App\Http\Controllers\admin;

use App\Rating;
use App\saloon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function index(Request $request)
    {
        $data['menu']="Reviews";
        $id = Auth::user()->id;
        $data['salon_id'] = $id;
        $data['salon'] = saloon::where('user_id', $id)->pluck('title','id')->prepend('Please Select','');
        if(isset($request['search']) && $request['search'] != '')
        {
            $data['rating'] = Rating::with('User')->where('saloon_id', $request['search'])->orderBy('id','desc')->Paginate($this->pagination);
        }
        else{
            $data['rating'] = [];
        }

        if ($request->ajax())
        {
            return view('admin.rating.table',$data);
        }

        return view('admin.rating.index',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
