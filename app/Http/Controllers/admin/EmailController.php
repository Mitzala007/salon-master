<?php

namespace App\Http\Controllers\admin;

use App\Email;
use auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function index()
    {
        $data['menu'] = 'Email';
        $data['emails'] = Email::orderBy('title','ASC')->Paginate($this->pagination);
        return view('admin.emails.index',$data);
    }

    public function create()
    {
        $data['menu'] = 'Email';
        return view('admin.emails.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=> 'required',
            'subject' => 'required',
            'status' => 'required',
            'email' => 'required|email',
        ]);
        $input = $request->all();
        $email = Email::create($input);
        Activity::log([
            'contentId'   => $email->id,
            'contentType' => 'email management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Email Add',
        ]);
        \Session::flash('success', 'Email has been inserted successfully!');
        return redirect('admin/emails');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['menu']="Email";
        $data['emails'] = Email::findOrFail($id);
        return view('admin.emails.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=> 'required',
            'subject' => 'required',
            'status' => 'required',
            'email' => 'required|email',
        ]);

        $input = $request->all();
        $email = Email::findorFail($id);

        $msg = "";
        if ($email->title!=$input['title']){ $msg .= "Title".",";}
        if ($email->subject!=$input['subject']){ $msg .= "Subject".",";}
        if ($email->email!=$input['email']){ $msg .= "Email".",";}
        if ($email->content!=$input['content']){ $msg .= "Content".",";}
        if ($email->status!=$input['status']){ $msg .= "Status".",";}

        $email->update($input);

        Activity::log([
            'contentId'   => $email->id,
            'contentType' => 'email management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);

        \Session::flash('success','Email has been updated successfully!');
        return redirect('admin/emails');
    }

    public function destroy($id)
    {
        $email = Email::findOrFail($id);
        $email->delete();

        Activity::log([
            'contentId'   => $email->id,
            'contentType' => 'email management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Employee Delete',
        ]);

        \Session::flash('danger','Email has been deleted successfully!');
        return;
    }

    public function assign(Request $request)
    {
        $email = Email::findorFail($request['id']);
        $email['status'] = "active";
        $email->update($request->all());
        Activity::log([
            'contentId'   => $email->id,
            'contentType' => 'email management',
            'action'      => 'Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $email = Email::findorFail($request['id']);
        $email['status'] = "in-active";
        $email->update($request->all());

        Activity::log([
            'contentId'   => $email->id,
            'contentType' => 'email management',
            'action'      => 'Update',
            'description' => 'Status change to inactive by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }
}
