<?php

namespace App\Http\Controllers\admin;

use App\Membership_plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;

class MembershipController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }
    public function index(Request $request)
    {
        $query = Membership_plan::select();

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
        {
            if(isset($request['search']) && $request['search'] != '')
            {
                $query->where('title', 'like', '%'.$request['search'].'%');
               $data['search']=$request['search'];
            }
        }

        $data['menu']="Membership";
        $data['membership']=$query->OrderBy('displayorder','ASC')->Paginate($this->pagination);
        if ($request->ajax())
        {
            return view('admin.membership.table',$data);
        }
        return view('admin.membership.index',$data);
    }
    
    public function create()
    {
        $data['menu']="Membership";
        return view('admin.membership.create',$data);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'amount' => 'required|numeric',
            'total_recurring_occurance' => 'required|integer',
            'status'=>'required',
        ]);

        $membership=$request->all();
        $membership['free_trial'] = isset($request['free_trial'])?'1':'0';
        $membership['is_recurring'] = isset($request['is_recurring'])?'1':'0';

        $Membership = Membership_plan::create($membership);

        Activity::log([
            'contentId'   => $Membership->id,
            'contentType' => 'Membership management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Member Add',
        ]);
        \Session::flash('success','Membership plan type has been inserted successfully!');

        return redirect('admin/membership');
    }

    public function edit($id)
    {
        $data['menu']="Membership";
        $data['membership']=Membership_plan::findOrFail($id);
        return view('admin.membership.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
//            'title' => 'required',
//            'amount' => 'required|numeric',
//            'total_recurring_occurance' => 'required|integer',
            'free_trial' => 'required',
            'status'=>'required',
        ]);

        $input = $request->all();
        $membership = Membership_plan::findOrFail($id);

        $input['free_trial'] = isset($request['free_trial'])?'1':'0';
        $input['is_recurring'] = isset($request['is_recurring'])?'1':'0';

        $msg = "";
        if ($membership->id!=$id){ $msg .= "Membership Id".",";}
//        if ($membership->title!=$input['title']){ $msg .= "Title".",";}
//        if ($membership->amount!=$input['amount']){ $msg .= "Amount".",";}
        if ($membership->is_recurring!=$input['is_recurring']){ $msg .= "Is Rcurring".",";}
//        if ($membership->total_recurring_occurance!=$input['total_recurring_occurance']){ $msg .= "Total Recurring Occurance".",";}
        if ($membership->free_trial!=$input['free_trial']){ $msg .= "Free Trial".",";}
        if ($membership->status!=$input['status']){ $msg .= "Status".",";}

        $membership->update($input);

        Activity::log([
            'contentId'   => $membership->id,
            'contentType' => 'Membership management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);
        \Session::flash('success','Membership plan has been updated successfully!');
        return redirect('admin/membership');
    }


    public function destroy($id)
    {
        $membership = Membership_plan::findOrFail($id);
        $membership->delete();

        Activity::log([
            'contentId'   => $membership->id,
            'contentType' => 'Membership management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Member Delete',
        ]);

        \Session::flash('danger','Membership plan has been deleted successfully!');

        return redirect('admin/membership');

    }


    public function reorder(Request $request){
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {

                $membership = Membership_plan::findOrFail($idval);
                $input['displayorder'] = $count;
                $membership->update($input);
                $count ++;
            }
            echo 'Display order change successfully.';
        }
    }

    public function assign(Request $request)
    {
        $membership = Membership_plan::findorFail($request['id']);
        $membership['status'] = "active";
        $membership->update($request->all());
        Activity::log([
            'contentId'   => $membership->id,
            'contentType' => 'membership management',
            'action'      => 'Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $membership = Membership_plan::findorFail($request['id']);
        $membership['status'] = "in-active";
        $membership->update($request->all());

        Activity::log([
            'contentId'   => $membership->id,
            'contentType' => 'membership management',
            'action'      => 'Update',
            'description' => 'Status change to inactive by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function user_membership(Request $request)
    {
        $data['menu']="Membership";

        $membership = Auth::user()->membership_plan;
        $admin_membership = Auth::user()->admin_membership_plan;
        if ($membership !=0)
        {
            $plan = $membership;
        }
        else{
            $plan = $admin_membership;
        }

        $data['membership'] = Membership_plan ::where('id',$plan)->get();
        return view ('admin.users.membership',$data);
    }
}
