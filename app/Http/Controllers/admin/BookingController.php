<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\saloon;
use App\saloon_employees;
use App\Saloon_services;
use App\Service_type;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use auth;
use Illuminate\Support\Facades\DB;
use Regulus\ActivityLog\Models\Activity;
class BookingController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function booking(Request $request)
    {

        $data['menu']="Booking";
        $user = \Illuminate\Support\Facades\Auth::user();
        $plan = 0;
        if ($user['membership_plan']==1 || $user['membership_plan']==2 || $user['membership_plan']==3){
            $plan = $user['membership_plan'];
        }
        if($user['admin_membership_plan']==1 || $user['admin_membership_plan']==2 || $user['admin_membership_plan']==3){
            $plan = $user['admin_membership_plan'];
        }

        $salon_ids = saloon_employees::where('user_id',$user->id)->where('is_salon_owner',1)->pluck('saloon_id');

        $query  = saloon::with('Bookings')->where('status','active')->select();
        if(isset($request['search']) && $request['search'] != '')
        {
            if(Auth::user()->role == 'admin' || $plan==1 || $plan==2 || $plan==3)
            {
                $query->where('title', 'like', '%'.$request['search'].'%');
            }
            else
            {
                $query->wherein('id',$salon_ids)->where('title', 'like', '%'.$request['search'].'%');
            }
            $data['salon'] = $query->Paginate(18);
            $data['search']=$request['search'];
        }
        else
        {
            if(Auth::user()->role == 'admin')
            {
                $data['salon'] = $query->Paginate(18);
            }
            else
            {
                $data['salon'] = $query->wherein('id',$salon_ids)->Paginate(18);
            }
        }
        if ($request->ajax())
        {
            return view('admin.booking.booking_table',$data);
        }


        if(Auth::user()->role == 'admin' || $plan==1 || $plan==2 || $plan==3)
        {
            return view('admin.booking.index', $data);
        }
        else
        {
            return redirect('admin/dashboard');
        }
    }

    public function index(Request $request,$id)
    {
        $data['menu']="Booking";
        $today = Carbon::now();
        $date=$today->toDateString();

        if(Auth::user()->role == 'admin')
        {

            $query = Booking::with('Users')->with('Type')->with('saloon')->where('saloon_id',$id)->select();

            if (isset($request['start_date']) && $request['start_date']!=""){
                $start_date = date('Y-m-d',strtotime($request['start_date']));
                $query->whereDate('date','>=',$start_date);
            }
            if (isset($request['end_date']) && $request['end_date']!=""){
                $end_date = date('Y-m-d',strtotime($request['end_date']));
                $query->whereDate('date','<=',$end_date);
            }
            if(isset($request['search']) && $request['search'] != '')
            {
                //$user = User::Where('name', 'like', '%'.$request['search'].'%')->pluck('id')->all();
                $user = User::Where('name', 'like', '%'.$request['search'].'%')
                    ->orWhere('phone','like',$request['search'].'%')->pluck('id')->all();


                if(!empty($user)){
                    $query->whereIn('user_id', $user);
                }
                else{
                    $query->where('booking_name', 'like','%'.$request['search'].'%');
                }
            }
            if (isset($request['status']) && $request['status']!=""){
                $query->where('status',$request['status']);
            }

            if(isset($request['start_date']) && $request['start_date']!="" && isset($request['end_date']) && $request['end_date']!=""){
                $data['booking'] = $query->orderBy('date','DESC')->orderBy('id', 'DESC')->Paginate($this->pagination);
            }
            else{
                if (isset($request['status']) && $request['status']!=""){
                    $query->where('status',$request['status']);
                }
                else{
                    $query->whereDate('date',$date);
                }
                $data['booking'] = $query->orderBy('date','DESC')->orderBy('id', 'DESC')->Paginate($this->pagination);
            }

        }
        else
        {

            $user_id = Auth::user()->id;
            $user = \Illuminate\Support\Facades\Auth::user();
            $plan = 0;

            if ($user['membership_plan']==1 || $user['membership_plan']==2 || $user['membership_plan']==3){
                $plan = $user['membership_plan'];
            }
            if ($user['admin_membership_plan']==1 || $user['admin_membership_plan']==2 || $user['admin_membership_plan']==3){
                $plan = $user['admin_membership_plan'];
            }

            $data['saloon'] = $input['salon_id'] = session('salon_id');
            date_default_timezone_set(session('salon_timezone'));

            if(isset($data['saloon']))
            {
                $query = Booking::with('Users')->with('Type')->with('saloon')->where('saloon_id',$id)->select();
                if (isset($request['start_date']) && $request['start_date']!=""){
                    $start_date = date('Y-m-d',strtotime($request['start_date']));
                    $query->whereDate('date','>=',$start_date);
                }

                if (isset($request['end_date']) && $request['end_date']!=""){
                    $end_date = date('Y-m-d',strtotime($request['end_date']));
                    $query->whereDate('date','<=',$end_date);
                }

                if(isset($request['search']) && $request['search'] != '')
                {
                    $user = User::Where('name', 'like', '%'.$request['search'].'%')
                        ->orWhere('phone','like',$request['search'].'%')->pluck('id')->all();
                    if(!empty($user)){
                        $query->whereIn('user_id', $user);
                    }
                    else{
                        $query->where('booking_name', 'like','%'.$request['search'].'%');
                    }
                }

                if (isset($request['status']) && $request['status']!=""){
                    $query->where('status',$request['status']);
                }

                if(isset($request['start_date']) && $request['start_date']!="" && isset($request['end_date']) && $request['end_date']!=""){
                    $data['booking'] = $query->orderBy('date','DESC')->orderBy('id', 'DESC')->Paginate($this->pagination);
                }
                else{

                    if (isset($request['status']) && $request['status']!=""){
                        $query->where('status',$request['status']);
                    }
                    else{
                        $query->whereDate('date',$date);
                    }
                    $data['booking'] = $query->orderBy('date','DESC')->orderBy('id', 'DESC')->Paginate($this->pagination);
                }
            }
        }
        if ($request->ajax())
        {
            $data['id'] = $id;
            return view('admin.booking.table',$data);
        }

        $data['id'] = $id;
        return view('admin.booking.saloon_index',$data);
    }

    public function create()
    {
        $data['menu'] = 'Add Booking';
        $id = Auth::user()->id;

        $salon_id = session('salon_id');

        $type_id = Saloon_services::where('saloon_id',$salon_id)->pluck('type_id');
        $data['service_type'] = Service_type::whereIn('id',$type_id)->orderBy('title','ASC')->pluck('title','id')->all();

        $user_id = saloon_employees::where('saloon_id',$salon_id)->where('is_salon_owner','0')->pluck('user_id');
        $data['emp_id'] = User::whereIn('id',$user_id)->pluck('name','id')->all();

        $get_booking_users = Booking::where('saloon_id',$salon_id)->groupBy('user_id')->pluck('user_id');
        $data['customer'] = User::whereIn('id',$get_booking_users)->where('status','active')->pluck('name','id')->all();

        return view('admin.booking.create',$data);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $input['salon_id'] = session('salon_id');
        $input['type'] = "web";
        $input['service_type'] = $request['service_type'];

        if ($input['customer_type'] == 0){
            /* CHECK USER */

            if ($input['email']!=""){
                $user  = User::where('phone',$input['phone'])->orwhere('email',$input['email'])->first();
            }
            else{
                $user  = User::where('phone',$input['phone'])->first();
            }

            if (empty($user)){
                $user = User::create($input);
                $input['user_id'] = $user['id'];
            }
            $input['user_id'] = $user['id'];
        }
        else{
            $user  = User::where('id',$request['user_id'])->first();
            if ($user){  // UPDATE CODE
                if ($user['phone']=="" || $user['email']==""){
                    $ip['phone'] = $request['phone'];
                    $ip['email'] = $request['email'];

                    if ($ip['email']!=""){
                        $check_phone_email = User::where('email',$request['email'])->orWhere('phone',$request['phone'])->first();
                    }
                    else{
                        $check_phone_email = User::Where('phone',$request['phone'])->first();
                    }

                    if ($check_phone_email){
                        $response["Result"] = 0;
                        $response["Message"] = "Email or phone already exists.";
                        return $data['booking'] = $response;
                    }
                    $user->update($ip);
                }
            }
            else{
                $input['user_id'] = $request['user_id'];
            }
        }

        $payload = json_encode($input);

        //print_r($payload);

        /*------------------- WALK IN BOOKING API -------------------*/
        $url = url('api/walkin_book');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $data['booking'] = json_decode($response,true);

    }

//    public function show($id, $vid)
//    {
//        $data['menu']="Booking";
//        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin') {
//            $data['booking'] = Booking::with('Users')->with('Saloon')->with('Booking_details')->with('Booking_details.Service_type')->findorFail($vid);
//        } else {
//            $data['booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->findorFail($vid);
//        }
//        $data['id'] = $id;
//        return view('admin.booking.view', $data);
//    }

    public function show($booking)
    {
        $data['menu']="booking";
        $data['booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')
            ->with('Saloon.Saloon_services')
            ->where('id',$booking)->first();
        return view('admin.booking.view',$data);
    }

    public function edit(Booking $booking)
    {
        //
    }

    public function update(Request $request, Booking $booking)
    {
        //
    }

    public function destroy(Request $request)
    {
        $booking = Booking::findOrFail($request->id);
        $booking->delete();
        Activity::log([
            'contentId'   => $booking->id,
            'contentType' => 'booking management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Booking Delete',
        ]);
        \Session::flash('danger','Booking has been deleted successfully!');
        return ;
    }

    public function update_status($statusval,$id)
    {
        $status = Booking::findOrFail($id);
        $input['status'] = $statusval;
        $status->update($input);
        return 'success';
    }


    public function waiting_list(Request $request)
    {
        $data['menu'] = 'Waiting List';
        $input['type'] = "web";
        $input['salon_id'] = session('salon_id');
        date_default_timezone_set(session('salon_timezone'));
        $input['user_id'] = \Illuminate\Support\Facades\Auth::id();

        $payload = json_encode($input);

        /* CHECK BOOKING TABLE UPDATED */
        $bk = Booking::select('updated_at')->where('saloon_id',$input['salon_id'])->orderBy('updated_at','DESC')->first();
        $data['updated_at'] = date('Y-m-d H:i:s',strtotime($bk['updated_at']));

        //$flag = 1;
        //if (isset($request['updated_at']) && $request['updated_at']==$data['updated_at']){
        //    $flag = 0;
        //    return $flag;
        //}

        //if ($flag==1){
            /*-------------------GET  WAITING LIST -------------------*/
    //        $url = 'https://nailmaster.co/api/do_booking';
            $url = url('api/get_bookings');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $data['booking'] = json_decode($response,true);


            /*Add Booking*/
            $salon_id = session('salon_id');

            $type_id = Saloon_services::where('saloon_id',$salon_id)->pluck('type_id');
            $data['service_type'] = Service_type::whereIn('id',$type_id)->orderBy('title','ASC')->pluck('title','id')->all();

            $user_id = saloon_employees::where('saloon_id',$salon_id)->pluck('user_id');
            $data['emp_id'] = User::whereIn('id',$user_id)->where('available','present')->pluck('name','id')->all();

            $get_booking_users = Booking::where('saloon_id',$salon_id)->groupBy('user_id')->pluck('user_id');
            $data['customer'] = User::whereIn('id',$get_booking_users)->where('status','active')->pluck('name','id')->all();

            if ($request['req_type']=="ajax")
            {
                return view('admin.booking.waiting_list_table',$data);
            }
            else{
                return view('admin.booking.waiting_list', $data);
            }
        //}
    }

    public function serve_now(Request $request){
        $input['booking_id'] = $request['id'];
        $input['type'] = "web";
        if (isset($request['salon_id'])){
            $input['salon_id'] = $request['salon_id'];
        }
        else{
            $input['salon_id'] = session('salon_id');
        }

        $input['user_id'] = \Illuminate\Support\Facades\Auth::id();

        $payload = json_encode($input);

        /*------------------- SERVE NOW API CALLING -------------------*/
            $url = url('api/serve_now');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $data['booking'] = json_decode($response,true);
        //return view('admin.booking.waiting_list', $data);
    }

    public function end_now(Request $request){

        $input['type'] = "web";
        $input['booking_id'] = $request['bid'];
        if (isset($request['salon_id'])){
            $input['salon_id'] = $request['salon_id'];
        }
        else{
            $input['salon_id'] = session('salon_id');
        }
        $input['user_id'] = \Illuminate\Support\Facades\Auth::id();
        $input['discount_amount'] = $request['discount_amount'];
        $input['remarks'] = $request['remarks'];
        $input['final_total'] = $request['final_total'];
        $input['additional_amount'] = $request['additional_amount'];
        $input['point'] = $request['point'];
        $input['payment_mode'] = $request['payment_mode'];
        $input['tax'] = $request['tax'];

        //return  $input['final_total'];
        $payload = json_encode($input);


        /*-------------------SERVE NOW API CALLING -------------------*/
        $url = url('api/end_now');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $data['booking'] = json_decode($response,true);
    }

    public function get_booking_details(Request $request){

        $get_booking = Booking::with('Booking_details')->where('id', $request['id'])->first();
        $sel = "";
        foreach ($get_booking['Booking_details'] as $k => $v){
            $sel .= $v['type_id'].",";
            $employee_id = $v['emp_id'];
        }
        $selected_service = trim($sel,",");

        $type_id = Saloon_services::where('saloon_id',$get_booking['saloon_id'])->pluck('type_id');


        $services = Service_type::whereIn('id',$type_id)->orderBy('title','ASC')->get();
        $selected_services = explode(',',$selected_service);

        $service_dropdown = "";

        foreach($services as $key => $val){
            $selected = "";
            if (in_array($val['id'],$selected_services)){
                $selected = "selected";
            }
            $service_dropdown .= "<option value='".$val['id']."' ".$selected.">".$val['title']."</option>";
        }

        $data['service_dropdown'] = $service_dropdown;


        $employee_list = saloon_employees::where('saloon_id',$get_booking['saloon_id'])->pluck('user_id');
        $employee_list_ids = User::whereIn('id',$employee_list)->where('available','present')->get();


        $employee_dropdown  = "";

        //$employee_dropdown .= "<option value=''>Please Select</option>";
        foreach($employee_list_ids as $key1 => $val1){
            $selected1 = "";
            if ($val1['id']==$employee_id){
                $selected1 = "selected";
            }
            $employee_dropdown .= "<option value='".$val1['id']."' ".$selected1.">".$val1['name']."</option>";
        }

        $data['employee_dropdown'] = $employee_dropdown;

        $user = User::where('id',$get_booking['user_id'])->first();
        $data['user'] = $user;
        $data['booking'] = $get_booking;
        return $data;
    }

    public function edit_booking(Request $request){
        $input = $request->all();
        $input['type'] = "web";
        $input['user_id'] = \Illuminate\Support\Facades\Auth::id();
        $input['service_type'] = $request['service_type'];

        $payload = json_encode($input);

        /*------------------- WALK IN BOOKING API -------------------*/
        $url = url('api/edit_booking');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $data['booking'] = json_decode($response,true);
    }

    public function cancel_booking(Request $request){
        $input = $request->all();
        $input['type'] = "web";
        $input['user_id'] = $request['uid'];
        $input['booking_id'] = $request['bid'];

        $payload = json_encode($input);

        /*------------------- WALK IN BOOKING API -------------------*/
        $url = url('api/cancel_booking');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        return $data['booking'] = json_decode($response,true);
    }
}
