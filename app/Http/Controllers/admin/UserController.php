<?php
namespace App\Http\Controllers\admin;
use App\Booking;
use App\Categories;

use App\cities;
use App\countries;
use App\Employees;
use App\Login_details;
use App\Membership_plan;
use App\Message;
use App\Email;
use App\saloon;
use App\saloon_employees;
use App\Salon_owner_request;
use App\Saloon_services;
use App\Service_type;
use App\states;
use App\User;
use App\work_days;
use Carbon\Carbon;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Regulus\ActivityLog\Models\Activity;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('role');
    }
    
    public function index(Request $request)
    {
        $data = [];
        $data['menu'] = "User";
        $data['membership_plan'] = Membership_plan::OrderBy('displayorder','ASC')->where('status','active')->pluck('title','id');
        $admin_role = Auth::user()->id;
        $id = Auth::user()->id;

        if (isset($request['search']) && $request['search'] != '')
        {
            $country = countries::Where('countryName', 'like', '%'.$request['search'].'%')->pluck('countryID')->all();
            if(!empty($country))
            {
                $data['user'] = User::WhereIn('country',$country)->Paginate($this->pagination);
            }
            else
            {
                if(strtolower($request['search']) == 'super admin')
                {
                    $role = User::Where('role', 'admin')->where('id','!=',Auth::user()->id)->pluck('id')->all();
                }

                if(strtolower($request['search']) == 'salon owner')
                {
                    $check_owner = saloon_employees::where('is_salon_owner',1)->pluck('user_id');
                    $role = User::Wherein('id', $check_owner)->pluck('id')->all();
                }
                else if(strtolower($request['search']) == 'user')
                {
                    $role = User::Where('role', 'user')->pluck('id')->all();
                }
                else if(strtolower($request['search']) == 'employee')
                {
                    $salon_emp = saloon_employees::where('status','accept')->pluck('user_id');
                     $role = User::whereIn('id',$salon_emp)->where('role','user')->pluck('id')->all();
                }
                elseif(strtolower($request['search']) == 'all')
                {
                    if($admin_role == 'admin')
                    {
                        $role = User::where('role', '!=', 'admin')->where('status', 'active')->pluck('id')->all();
                    }
                    else
                    {
                        $role = User::where('role', '!=', 'admin')->where('role','!=','super_admin')->where('status', 'active')->pluck('id')->all();
                    }
                }
                else if($request['search'] == '1' || $request['search'] == '2' || $request['search'] == '3')
                {
                    $role = User::orwhere('membership_plan',$request['search'])->orwhere('admin_membership_plan',$request['search'])->pluck('id');
                }
                else
                {
                    $search = $request['search'];
                    if($request['search'] == 'inactive')
                    {
                        $search = 'in-active';
                    }

                    $data['user'] = User::where(function ($query) use ($search) {
                            $query->orWhere('name','like','%'.$search.'%' );
                            $query->orWhere('email', 'like','%'.$search.'%');
                            $query->orWhere('phone', 'like','%'.$search.'%');
                            $query->orWhere('status', $search);
                        })->Paginate($this->pagination);
//                    }
//                    else
//                    {
//                        $data['user'] = User::where('role', '!=', 'admin')
//                            ->where(function ($query) use ($search) {
//                                $query->orWhere('name','like','%'.$search.'%' );
//                                $query->orWhere('email', 'like','%'.$search.'%');
//                                $query->orWhere('phone', 'like','%'.$search.'%');
//                                $query->orWhere('status', $search);
//                            })->Paginate($this->pagination);
//                    }

                }
                if(!empty($role))
                {
                    $data['user'] = User::WhereIn('id',$role)->Paginate($this->pagination);
                }
            }
            $data['search'] = $request['search'];
        }
        else
        {
            if($admin_role == '1')
            {
                $data['user'] = User::orderBy('id', 'desc')->Paginate($this->pagination);
            }
            else
            {
                $data['user'] = User::orderBy('id', 'desc')->where('id', '!=', $id)->where('role', '!=', 'admin')->where('role', '!=', 'super_admin')->Paginate($this->pagination);
            }
        }

        foreach ($data['user'] as $key => $val) {
            $cc = $val['country'];
            $cdata = countries::select('countryName')->where('countryID', $cc)->first();
            $data['user'][$key]['country'] = $cdata['countryName'];
        }

        if ($request->ajax())
        {
            return view('admin.users.table',$data);
        }

        return view('admin.users.index', $data);
    }

    public function create()
    {
        $data = [];
        $data['menu'] = "User";
        $data['countries'] = countries::pluck('countryName', 'countryID')->all();
        $data['states'] = [];
        $data['cities'] = [];
        $data['membership_plan'] = Membership_plan::OrderBy('displayorder','ASC')->where('status','active')->pluck('title','id')->all();
        //$data['dases'] = '';
        return view("admin.users.create", $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            //'country' => 'required',
            //'state_id' => 'required',
            //'city_id' => 'required',
            'addressline_1' => 'required',
            'zip_code' => 'required',
            //'phone' => 'required',
            'status' => 'required',
            // 'image' => 'required|mimes:jpeg,bmp,png',
        ]);
        
        if (isset($request['phone'])){
            $request['phone'] = $request['countries'].$request['phone'];
            $this->validate($request, [
                'phone' => 'required|unique:users,phone',
            ]);
        }
        $input = $request->all();
        if ($photo = $request->file('image')) {
            $input['image'] = $this->image($photo, 'User');
        }
        if (!empty($request['category'])) {
            $input['cat_id'] = implode(',', $request['category']);
        }

        $token = str_random(30);
        $input['remember_token'] = $token;

        $input['is_email_verified'] = isset($request['is_email_verified'])?'1':'0';

        if(Auth::user()->id == 1)
        {
            $input['role'] = isset($request['role'])?'admin':'user';
        }
        else{
            $input['role'] = 'user';
        }

        if(isset($request['admin_membership_plan'])){
            if($request['admin_plan']=="admin"){
                $input['role'] = 'admin';
                $input['admin_membership_plan'] = 0;
                $input['membership_plan'] = 0;
            }
            else{
                $input['role'] = 'user';
                $input['admin_membership_plan'] = isset($request['admin_membership_plan'])?$request['admin_plan']:'0';
            }
        }
        else{
            $input['role'] = 'user';
            $input['admin_membership_plan'] = 0;
        }


        //$input['phone'] = $request['countries'].$request['phone'];
        $input['phone'] = $request['phone'];
        $user = User::create($input);

        Activity::log([
            'contentId'   => $user->id,
            'contentType' => 'user management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'User Add',
        ]);

        \Session::flash('success', 'User has been inserted successfully!');
        return redirect('admin/users');
    }

    public function show(Request $request, $id)
    {
        $data['menu'] = "User";
        $data['service'] = 'saloon_management';
        $data['user'] = User::findorFail($id);
        $salon_emp = saloon_employees::where('user_id',$data['user']['id'])->first();
        $data['saloon_detail'] = saloon::where('id',$salon_emp['saloon_id'])->first();
        $data['saloon'] = saloon::where('user_id',$data['user']['id'])->first();

        if($data['saloon'])
        {
            $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
            $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
            $data['work_days'] = work_days::select('work_day','start_hour','end_hour')->where('saloon_id', $data['saloon']->id)->get();
            $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $data['saloon']->id)->get();

            foreach($service as $key => $value) {
                $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'] == 1 ? 'Yes' : 'No', 'discount' => $value['discount']);
            }

            $data['booking'] = Booking::with('Users')->with('Type')->with('saloon')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id',$data['saloon']->id)->orderBy('id','DESC')->Paginate($this->pagination);

            $data['employees'] = saloon_employees::where('saloon_id',$data['saloon']->id)->orderBy('id','asc')->Paginate($this->pagination);
        }

        $data['booking'] = collect();
        $data['employees'] = collect();


        $data['id'] = $id;
        return view('admin.users.view', $data);
    }

    public function booking_show(Request $request, $id)
    {
        $data['menu'] = "User";
        $data['service'] = 'saloon_management';
        $data['user'] = User::findorFail($id);
        $data['saloon'] = saloon::where('user_id',$data['user']['id'])->first();
        if(!empty($data['saloon']))
        {
            if (isset($request['search']) && $request['search'] != '') {
                $user = User::Where('name', 'like', '%' . $request['search'] . '%')->pluck('id')->all();
                if (!empty($user)) {
                    $data['booking'] = Booking::with('Users')->with('Type')->with('saloon')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $data['saloon']->id)->WhereIn('user_id', $user)->orderBy('id', 'DESC')->Paginate($this->pagination);
                } else {
                    $search = $request['search'];
                    $data['booking'] = Booking::with('Users')->with('Type')->with('saloon')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $data['saloon']->id)->where(function ($query) use ($search) {
                        $query->orWhere('date', 'like', '%' . $search . '%');
                        $query->orWhere('status', 'like', '%' . $search . '%');
                    })->OrderBy('id', 'DESC')->Paginate($this->pagination);
                }
                $data['search'] = $request['search'];
            } else {
                $data['booking'] = Booking::with('Users')->with('Type')->with('saloon')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $data['saloon']->id)->orderBy('id', 'DESC')->Paginate($this->pagination);
            }
        }
        if ($request->ajax())
        {
            return view('admin.users.booking_table',$data);
        }
    }

    public function employee_show(Request $request, $id)
    {
        $data['menu'] = "User";
        $data['user'] = User::findorFail($id);
        $data['saloon'] = saloon::where('user_id',$data['user']['id'])->first();
        if(!empty($data['saloon']))
        {
            if(isset($request['search_employee']) && $request['search_employee'] != '')
            {
                $search=$request['search_employee'];

                $data['employees'] = Employees::where('saloon_id',$data['saloon']->id)
                    ->where(function ($query) use ($search) {
                        $query->orWhere('first_name','like','%'.$search.'%' );
                        $query->orWhere('last_name', 'like','%'.$search.'%');
                        $query->orWhere('email', 'like','%'.$search.'%');
                        $query->orWhere('phone', 'like','%'.$search.'%');
                        $query->orWhere('available', 'like','%'.$search.'%');
                    })->Paginate($this->pagination);

                $data['search_employee']=$request['search_employee'];
            }
            else
            {
                $data['employees'] = Employees::where('saloon_id',$data['saloon']->id)->orderBy('id','asc')->Paginate($this->pagination);
            }
        }
        if ($request->ajax())
        {
            return view('admin.users.employee_table',$data);
        }
    }

    public function edit($id)
    {
        $data = [];
        $data['menu'] = "User";
        $data['user'] = User::findorFail($id);
        $data['countries'] = countries::pluck('countryName', 'countryID')->all();
        $data['states'] = states::where('countryID', $data['user']->country)
           ->orderBy('stateName', 'ASC')->pluck('stateName', 'stateID')->all();
        $data['cities'] = cities::where('countryID', $data['user']->country)->where('stateID', $data['user']->state_id)
           ->orderBy('cityName', 'ASC')->pluck('cityName', 'cityID')->all();
        $data['membership_plan'] = Membership_plan::OrderBy('displayorder','ASC')->where('status','active')->pluck('title','id')->all();
        return view('admin.users.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id . ',id',
            'status' => 'required',
            //'country' => 'required',
            //'state_id' => 'required',
            //'city_id' => 'required',
            'addressline_1' => 'required',
            'zip_code' => 'required',
            //'phone' => 'required',
        ]);
        
        if (isset($request['phone'])){
            $request['phone'] = $request['countries'].$request['phone'];
            $this->validate($request, [
                'phone' => 'unique:users,phone,'.$id . ',id',
            ]);
        }
        
        if (!empty($request['password'])) {
        } else {
            unset($request['password']);
        }

        $input = $request->all();
        $user = User::findorFail($id);
        
        if (!empty($request['image'] && $request['image'] != "")) {
            if (file_exists($user->image))
                unlink($user->image);
        }
        if ($photo = $request->file('image')) {
            $input['image'] = $this->image($photo, 'User');
        }

        //$input['phone'] = $request['countries'].$request['phone'];
        $input['phone'] = $request['phone'];

        if(Auth::user()->id == 1)
        {
            $input['role'] = isset($request['role'])?'admin':'user';
        }
        else{
            $input['role'] = 'user';
        }

        if ($user->status != $input['status'] || $user->email != $input['email'] || $user->role != $input['role']) {
            $user_session_destroy = Login_details::where('user_id', $id)->delete();

            $logins['remember_token'] = str_random(30);
            $logins['user_id'] = $id;
            $logins['login_time'] = Carbon::now();

            Login_details::create($logins);

        }
        
        
        $input['is_email_verified'] = isset($request['is_email_verified'])?'1':'0';


        //$input['admin_membership_plan'] = isset($request['admin_membership_plan'])?$request['admin_plan']:'0';


        if(isset($request['admin_membership_plan'])){
            if($request['admin_plan']=="admin"){
                $input['role'] = 'admin';
                $input['admin_membership_plan'] = 0;
                $input['membership_plan'] = 0;
            }
            else{
                $input['role'] = 'user';
                $input['admin_membership_plan'] = isset($request['admin_membership_plan'])?$request['admin_plan']:'0';
            }
        }
        else{
            $input['role'] = 'user';
            $input['admin_membership_plan'] = 0;
        }


        if($user['admin_membership_plan'] != $request['admin_membership_plan'])
        {
            $login_user = Login_details::where('user_id',$user->id)->delete();
            $logins['remember_token'] = str_random(30);
            $logins['user_id'] = $id;
            $logins['login_time'] = Carbon::now();
            Login_details::create($logins);
        }

        if ($input['admin_membership_plan']==1 || $input['admin_membership_plan']==2 || $input['admin_membership_plan']==3){
            $update_req = Salon_owner_request::where('status', 0)->where('user_id',$id)->first();
            if ($update_req){
                $ip['status']=1;
                $update_req->update($ip);
            }
        }

        $msg = "";
        if ($user->name!=$input['name']){ $msg .= "Name".",";}
        if ($user->last_name!=$input['last_name']){ $msg .= "Last Name".",";}
        if ($user->birthdate!=$input['birthdate']){ $msg .= "Birthdate".",";}
        if (!empty($request['password'])){ $msg .= "Password".",";}
        if ($request->file('image')){ $msg .= "Profile Pic".",";}
        if ($user->email!=$input['email']){ $msg .= "Email".",";}
        if ($user->role!=$input['role']){ $msg .= "Role".",";}
        if ($user->gender!=$input['gender']){ $msg .= "Gender".",";}
        //if ($user->country!=$input['country']){ $msg .= "Country".",";}
        //if ($user->state_id!=$input['state_id']){ $msg .= "State".",";}
        //if ($user->city_id!=$input['city_id']){ $msg .= "City".",";}
        //if ($user->landmark!=$input['landmark']){ $msg .= "Landmark".",";}
        if ($user->addressline_1!=$input['addressline_1']){ $msg .= "Addressline 1".",";}
        if (isset($user->addressline_2) && $user->addressline_2!=$input['addressline_2']){ $msg .= "Addressline 2".",";}
        if ($user->zip_code!=$input['zip_code']){ $msg .= "Zip code".",";}
        if ($user->phone!=$input['phone']){ $msg .= "Phone".",";}
        //if (isset($user->address) && $user->address!=$input['address']){ $msg .= "Address".",";}
        if ($user->status!=$input['status']){ $msg .= "Status".",";}

        $user->update($input);

         Activity::log([
            'contentId'   => $user->id,
            'contentType' => 'user management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);



        //if ($user->role !='sub_admin'){
//        $saloon = saloon::where('user_id',$id)->first();
//        if ($saloon){
//            $input1['status'] = 'in-active';
//            $saloon->update($input1);
//        }
        //}

        \Session::flash('success', 'User has been updated successfully!');
        return redirect('admin/users');
    }

    public function destroy($id)
    {
        $appuser = User::findOrFail($id);

        /* DELETE MESSAGE*/
        $chat = Message::where(function ($query) use ($appuser, $id) {
            $query->where('sender', $id)
                ->orWhere('receiver',$id);
        })->delete();

        $appuser->delete();

        $user_session_destroy = Login_details::where('user_id', $id)->delete();
        $salon_owner_request = Salon_owner_request::where('user_id',$id)->delete();

        Activity::log([
            'contentId'   => $appuser->id,
            'contentType' => 'users',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'User Delete',
        ]);

        $saloon = saloon::where('user_id',$id)->first();
        if ($saloon){
            $saloon->delete();

           Activity::log([
            'contentId'   => $appuser->id,
            'contentType' => 'user management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'User Delete',
            ]);
        }

        \Session::flash('danger', 'User has been deleted successfully!');
        return;
    }

    public function assign(Request $request)
    {
        $appuser = User::findorFail($request['id']);
        $appuser['status'] = "active";
        $appuser->update($request->all());

        $user_session_destroy = Login_details::where('user_id', $request['id'])->delete();

        Activity::log([
            'contentId'   => $appuser->id,
            'contentType' => 'user management',
            'action'      => 'Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $appuser = User::findorFail($request['id']);
        $appuser['status'] = "in-active";
        $appuser->update($request->all());

        $user_session_destroy = Login_details::where('user_id', $request['id'])->delete();

        Activity::log([
            'contentId'   => $appuser->id,
            'contentType' => 'user management',
            'action'      => 'Update',
            'description' => 'Status change to inactive by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function get_states($cc)
    {
        $states = states::select('stateName', 'stateID')->where('countryID', $cc)
            ->orderBy('stateName', 'ASC')->get();
        $options = '<option value="0">Select State</option>';
        foreach ($states as $state) {
            $options .= '<option value="' . $state['stateID'] . '">' . $state['stateName'] . '</option>';
        }

        echo $options;
        return '';

    }

    public function get_cities($cc, $st)
    {
        $cities = cities::select('cityName', 'cityID')->where('countryID', $cc)->where('stateID', $st)
            ->orderBy('cityName', 'ASC')->get();
        $options = '';

        foreach ($cities as $city) {
            $options .= '<option value="' . $city['cityID'] . '">' . $city['cityName'] . '</option>';
        }

        echo $options;
        return '';

    }

    public function deleted_index(Request $request)
    {
        $data['menu'] = "Deleted User";
        if (isset($request['search']) && $request['search'] != '')
        {
            $country = countries::Where('countryName', 'like', '%'.$request['search'].'%')->pluck('countryID')->all();
            if(!empty($country))
            {
                $data['user'] = User::WhereIn('country',$country)->onlyTrashed()->Paginate($this->pagination);
            }
            else
            {
                $search = $request['search'];
                if($request['search'] == 'inactive')
                {
                    $search = 'in-active';
                }

                $data['user'] = User::where('role', '!=', 'admin')->where('role','!=','super_admin')
                    ->where(function ($query) use ($search) {
                        $query->orWhere('name','like','%'.$search.'%' );
                        $query->orWhere('email', 'like','%'.$search.'%');
                        $query->orWhere('phone', 'like','%'.$search.'%');
                        $query->orWhere('status', $search);
                    })->orderBy('deleted_at','desc')->onlyTrashed()->Paginate($this->pagination);
            }
            $data['search'] = $request['search'];
        }
        else
        {
            $data['user'] = User::onlyTrashed()->orderBy('deleted_at','desc')->Paginate($this->pagination);
        }

        if ($request->ajax())
        {
            return view('admin.deleted_user.table',$data);
        }
        return view('admin.deleted_user.index',$data);
    }

    public function recover_user(Request $request)
    {
        $deleted_user = User::withTrashed()->where('id',$request['id'])->first();

        if($deleted_user->trashed())
        {
            $deleted_user->restore();
        }

        Activity::log([
            'contentId'   => $deleted_user->id,
            'contentType' => 'deleted user management',
            'action'      => 'Update',
            'description' => 'User recover by '.Auth::user()->role,
            'details'     => 'User Recover',
        ]);

        \Session::flash('success', 'User has been recover successfully!');
        return;
    }

    public function force_delete(Request $request)
    {
        $deleted_user = User::withTrashed()->where('id',$request['id'])->first();

        $deleted_user->forceDelete();
        
        $salon_employee  = saloon_employees::where('user_id',$request['id'])->delete();

        Activity::log([
            'contentId'   => $deleted_user->id,
            'contentType' => 'delete user management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'User Delete',
        ]);

        \Session::flash('danger', 'User has been force deleted successfully!');
        return;
    }
    
    public function become_nailmaster(Request $request){
        $data = [];
        $data['menu'] = "Join ".config('siteVars.title');

        if (isset($request['search']) && $request['search'] != '')
        {
            //$get_request = Salon_owner_request::where('status',0)->pluck('user_id');
            $search = $request['search'];
//            $data['user'] = User::where('role', '=', 'user')
//                ->whereIn('id',$get_request)
//                ->where(function ($query) use ($search) {
//                    $query->orWhere('name','like','%'.$search.'%' );
//                    $query->orWhere('email', 'like','%'.$search.'%');
//                    $query->orWhere('phone', 'like','%'.$search.'%');
//                    $query->orWhere('status', $search);
//                    })->Paginate($this->pagination);

            $data['user'] = Salon_owner_request::where('status', 0)
                ->where(function ($query) use ($search) {
                    $query->orWhere('salon_name', $search);
                })->Paginate($this->pagination);
        }
        else
        {
            //$get_request = Salon_owner_request::where('status',0)->pluck('user_id');
            //$data['user'] = User::where('role', '=', 'user')->whereIn('id',$get_request)->orderBy('id', 'desc')->Paginate($this->pagination);
            $data['user'] = Salon_owner_request::where('status',0)->orderBy('id', 'desc')->Paginate($this->pagination);
        }

//        foreach ($data['user'] as $key => $val) {
//            $cc = $val['country'];
//            $cdata = countries::select('countryName')->where('countryID', $cc)->first();
//            $data['user'][$key]['country'] = $cdata['countryName'];
//        }

        if ($request->ajax())
        {
            return view('admin.become_nailmaster.table',$data);
        }
        return view('admin.become_nailmaster.index', $data);
    }

    public function send_email(Request $request){
        $become_nailmaster = Email::findorFail(4);
        $appuser = User::findorFail($request['id']);

        /* EMAIL CODE */
        $subject = $become_nailmaster['subject'];
        $from1 = $become_nailmaster['email'];

        $get_token = Login_details::where('user_id',$request['id'])->first();
        if ($get_token){
            $link = "<a href=".config('siteVars.siteurl')."api/payment/".$get_token['remember_token']." target='_blank'>".config('siteVars.siteurl')."api/payment/".$get_token['remember_token']."</a>";
            $string = ["{link}"];
            $replace_string = [$link];
            $data['content'] = str_replace($string,$replace_string,$become_nailmaster["content"]);
            $em= $appuser['email'];
            $bcc = [];

            $data['title'] = $become_nailmaster['title'];

            Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em, $bcc) {
                $m->from($from1, config('siteVars.title'));
                $m->bcc($bcc);
                $m->to($em)->subject($subject);
            });

            /* SMS CODE */
            $sms_text = strip_tags($data['content']);
            if(substr($appuser->phone, 0, 1) == 1) {
                $clickatell = new \Clickatell\Rest();
                $other_country = 'no';
                $datas = ['to' => [$appuser['phone']], 'from' => '15732073611', 'content' => $sms_text];
            } else {
                $clickatell = new \Clickatell\Rest('india');
                $other_country = 'yes';
                $datas = ['to' => [$appuser['phone']], 'content' => $sms_text];
            }

            $result = $clickatell->sendMessage($datas,$other_country);

        }
        return $request['id'];
    }
    
    public function become_nailmaster_delete(Request $request,$id)
    {
        $request = Salon_owner_request::where('id',$id)->first();
        $request->delete();

        Activity::log([
            'contentId'   => $request->id,
            'contentType' => 'Become '.config('siteVars.siteurl').' management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Request Delete',
        ]);

        \Session::flash('danger', 'Request deleted successfully!');
        return;
    }
}