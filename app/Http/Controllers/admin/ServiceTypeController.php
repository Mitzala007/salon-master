<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\Booking_details;
use App\Category;
use App\Saloon_services;
use App\Service_type;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;

class ServiceTypeController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        $data['menu']="Service Type";
        $data['category'] = Category::where('status','active')->orderBy('title','ASC')->pluck('title','id')->all();

        $query  = Service_type::select();
        if (isset($request['category_id']) && $request['category_id']!=''){
            $query->where('category_id',$request['category_id']);
        }
        if(isset($request['search']) && $request['search'] != '')
        {
            $search = $request['search'];
            if($request['search'] == 'inactive')
            {
                $search = 'in-active';
            }

            $query->where(function ($query) use ($search) {
                $query->orWhere('title','like','%'.$search.'%' );
                $query->orWhere('id','like','%'.$search.'%' );
                $query->orWhere('min_duration', 'like','%'.$search.'%');
                $query->orWhere('status', $search);
            });
            $data['search']=$request['search'];
        }

        $data['service'] = $query->orderBy('title','asc')->Paginate($this->service_pagination);

        if ($request->ajax())
        {
            return view('admin.service.table',$data);
        }

        return view('admin.service.index',$data);
    }

    public function create()
    {
        $data['menu'] = 'Service Type';
        $data['sub_category'] = [];
        $data['category'] = Category::where('status','active')->orderBy('title','ASC')->pluck('title','id')->all();
        return view('admin.service.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'title' => 'required',
            'min_duration' => 'required',
            'status' => 'required',

        ]);
        $input = $request->all();

        /* ADD DISPLAY ORDER */
        $count_display_order = Service_type::count();
        if($count_display_order >= 0)
        {
            $c = stripslashes($count_display_order);
            $c = $c + 1;
        }
        $input['displayorder'] = $c;
        /*---------------------*/

        $service = Service_type::create($input);

        Activity::log([
            'contentId'   => $service->id,
            'contentType' => 'service type management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Service Type Add',
        ]);

        \Session::flash('success', 'Service type has been inserted successfully!');
        return redirect('admin/service');

    }

    public function show(Service_type $service_type)
    {
        //
    }

    public function edit($id)
    {
        $data['menu'] = 'Service Type';
        $data['service'] = Service_type::findOrFail($id);
        $data['category'] = Category::where('status','active')->orderBy('title','ASC')->pluck('title','id')->all();
        $data['sub_category'] = Subcategory::where('category_id', $data['service']->category_id)->orderBy('title', 'ASC')->pluck('title', 'id')->all();
        return view('admin.service.edit',$data);
    }

    public function update(Request $request, Service_type $service_type,$id)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'title' => 'required',
            'min_duration' => 'required',
            'status' => 'required',
        ]);
        $service_type = Service_type::findOrFail($id);
        $input = $request->all();


        $msg = "";
        if ($service_type->title!=$input['title']){ $msg .= "Title".",";}
        if ($service_type->min_duration!=$input['min_duration']){ $msg .= "Min. duration".",";}
        if ($service_type->status!=$input['status']){ $msg .= "Status".",";}

        $service_type->update($input);

        Activity::log([
            'contentId'   => $service_type->id,
            'contentType' => 'service type management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);

        \Session::flash('success','Service type has been updated successfully!');
        return redirect('admin/service');
    }

    public function destroy($id)
    {

        $service_type = Service_type::findOrFail($id);

        $booking_details = Booking_details::where('type_id',$id)->get();
        foreach ($booking_details as $vals){
            $booking = Booking::where('id',$vals['booking_id'])->delete();
            $vals->delete();
        }

        $salon_service = Saloon_services::where('type_id',$id)->delete();

        $service_type->delete();

        Activity::log([
            'contentId'   => $service_type->id,
            'contentType' => 'service type management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Service type Delete',
        ]);

        \Session::flash('danger','Service type has been deleted successfully!');
        return redirect('admin/service');
    }

    public function assign(Request $request)
    {
        $service_type = Service_type::findorFail($request['id']);
        $service_type['status'] = "active";
        $service_type->update($request->all());

        Activity::log([
            'contentId'   => $service_type->id,
            'contentType' => 'service type management',
            'action'      => 'Status Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $service_type = Service_type::findorFail($request['id']);
        $service_type['status'] = "in-active";
        $service_type->update($request->all());

        Activity::log([
            'contentId'   => $service_type->id,
            'contentType' => 'service type management',
            'action'      => 'Status Update',
            'description' => 'Status change to inactive by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function reorder(Request $request)
    {
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {
                $service = Service_type::findOrFail($idval);
                $input['displayorder'] = $count;
                $service->update($input);
                $count ++;
            }
            Activity::log([
                'contentId'   => 0,
                'contentType' => 'service_types',
                'action'      => 'Displayorder Change',
                'description' => 'Displayorder change by '.Auth::user()->role,
                'details'     => 'Status Update',
            ]);

            echo 'Display order change successfully.';
        }
    }

    public function get_sub_category($cc)
    {
        $sub_category = Subcategory::select('title', 'id')->where('category_id', $cc)->orderBy('title', 'ASC')->get();
        $options = '<option value="0">Select State</option>';
        foreach ($sub_category as $cat) {
            $options .= '<option value="' . $cat['id'] . '">' . $cat['title'] . '</option>';
        }
        echo $options;
        return '';

    }

}
