<?php

namespace App\Http\Controllers\admin;

use App\cities;
use App\countries;
use App\states;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Regulus\ActivityLog\Models\Activity;

class ProfileupdateController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        //$this->middleware('user_check:'.$id);
    }

    public function edit($id)
    {
        $data=[];
        $data['menu']="User";
        $data['user'] = User::findorFail($id);
        $data['countries'] = countries::pluck('countryName', 'countryID')->all();
        $data['states'] = states::where('countryID', $data['user']->country)
            ->orderBy('stateName', 'ASC')->pluck('stateName', 'stateID')->all();
        $data['cities'] = cities::where('countryID', $data['user']->country)->where('stateID', $data['user']->state_id)
            ->orderBy('cityName', 'ASC')->pluck('cityName', 'cityID')->all();
        return view('admin.users.profile_edit',$data);
    }


    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id.',id',
            'password' => 'confirmed',
            'country' => 'required',
            'phone' => 'required',
            'image'=>'mimes:jpeg,bmp,png',
        ]);

        if(!empty($request['password'])){
        }
        else{
            unset($request['password']);
        }

        $input = $request->all();
        $user = User::findorFail($id);  
        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'user');
            if(file_exists($user->image))
            {
                unlink($user->image);
            }
        }
        $input['phone'] = $request['countries'].$request['phone'];

        $msg = "";
        if ($user->name!=$input['name']){ $msg .= "Name".",";}
        if ($user->email!=$input['email']){ $msg .= "Email".",";}
        if (!empty($request['password'])){ $msg .= "Password".",";}
        if ($user->phone!=$input['phone']){ $msg .= "Phone".",";}
        if ($user->gender!=$input['gender']){ $msg .= "Gender".",";}
        if ($user->country!=$input['country']){ $msg .= "Country".",";}
        if ($user->state_id!=$input['state_id']){ $msg .= "State".",";}
        if ($user->city_id!=$input['city_id']){ $msg .= "City".",";}
        if ($user->landmark!=$input['landmark']){ $msg .= "Landmark".",";}
        if ($user->addressline_1!=$input['addressline_1']){ $msg .= "Addressline 1".",";}
        if (isset($user->addressline_2) && $user->addressline_2!=$input['addressline_2']){ $msg .= "Addressline 2".",";}
        if ($user->zip_code!=$input['zip_code']){ $msg .= "Zip code".",";}
        if ($request->file('image')){ $msg .= "Profile Pic".",";}

        $user->update($input);

        Activity::log([
            'contentId'   => $user->id,
            'contentType' => 'users',
            'action'      => 'Update',
            'description' => 'Profile Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);

        \Session::flash('success','User has been updated successfully!');
        return redirect('admin/profile_update/'.$id."/edit");
    }
}
