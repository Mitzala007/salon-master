<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Subcategory;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Regulus\ActivityLog\Models\Activity;

class CategoryController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        $data['menu']="Category";

        if(isset($request['search']) && $request['search'] != '')
        {
            $search = $request['search'];
            if($request['search'] == 'inactive')
            {
                $search = 'in-active';
            }
            $data['category'] = Category::where(function ($query) use ($search) {
                $query->orWhere('title','like','%'.$search.'%' );
                $query->orWhere('status', $search);
            })->orderBy('displayorder','asc')->Paginate($this->service_pagination);
            $data['search']=$request['search'];
        }
        else
        {
            $data['category']=Category::orderBy('displayorder','asc')->Paginate($this->service_pagination);

        }
        if ($request->ajax())
        {
            return view('admin.category.table',$data);
        }

        return view('admin.category.index',$data);
    }


    public function create()
    {
        $data['menu'] = 'Category';
        return view('admin.category.create',$data);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png',
        ]);

        $input = $request->all();

        /* ADD DISPLAY ORDER */
        $count_display_order = Category::count();
        if($count_display_order >= 0)
        {
            $c = stripslashes($count_display_order);
            $c = $c + 1;
        }
        $input['displayorder'] = $c;

        /*---------------------*/

        $input = $request->all();
        if ($photo = $request->file('image')) {
            $path1 = $this->image($photo, 'Category');
            $input['image'] = $this->Imagethumbnail($path1,'Category/thumbnail',200,200,null);
        }


        if ($icon = $request->file('icon')) {
            $path1 = $this->image($icon, 'Category/icon');
            $input['icon'] = $this->Imagethumbnail($path1,'Category/thumbnail/icon',200,200,null);
        }

        $category = Category::create($input);

        $arr = count($request->subcategory);
        for ($i = 0; $i < $arr; $i++) {
            $a = $i+1;
            if (!empty($input['subcategory'][$i])) {
                $input1['category_id'] = $category['id'];
                $input1['title'] = $input['subcategory'][$i];
                $input1['status'] ='active';
                Subcategory::create($input1);
            }
        }

        Activity::log([
            'contentId'   => $category->id,
            'contentType' => 'Category management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Category Add',
        ]);

        \Session::flash('success', 'Category has been inserted successfully!');
        return redirect('admin/category');
    }


    public function show($id)
    {
        $data['menu']='Category';
        $data['category_id'] = Category::with('subcategory')->where('id',$id)->first();
        return view('admin.category.view',$data);
    }


    public function edit($id)
    {
        $data['menu'] = 'Category';
        $data['category'] = Category::findOrFail($id);
        $data['sub_category'] = Subcategory::where('category_id', $id)->get();
        return view('admin.category.edit',$data);
    }


    public function update(Request $request, $category_id)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
        ]);
        $category = Category::findOrFail($category_id);
        $input = $request->all();

        $msg = "";
        if ($category->title!=$input['title']){ $msg .= "Title".",";}
        if ($category->status!=$input['status']){ $msg .= "Status".",";}

        if (!empty($request['image'] && $request['image'] != "")) {
            if (file_exists($category->image))
                unlink($category->image);
        }
        if ($photo = $request->file('image')) {
            $path1 = $this->image($photo, 'Category');
            $input['image'] = $this->Imagethumbnail($path1,'Category/thumbnail',200,200,null);
        }

        if ($icon = $request->file('icon')) {
            $path1 = $this->image($icon, 'Category/icon');
            $input['icon'] = $this->Imagethumbnail($path1,'Category/thumbnail/icon',200,200,null);
        }

        $category->update($input);
        if (isset($request['option_array'])) {
            $exp = explode(",", $request['option_array']);
            $opt1 = Subcategory::where('category_id', $category_id)->get();
            foreach ($opt1 as $option) {
                //return $option['id'];
                if (in_array($option['id'], $exp)) {
                    $opt = $request['subcategory_' . $option['id']];

                    if ($opt != "") {
                        $options = Subcategory::where('category_id', $category_id)->where('id', $option['id'])
                            ->update(['title' => $opt]);
                    } else {
                        $this->destroy_option($option['id']);
                    }
                }
            }
        }
        if (isset($request['subcategory'])) {

            $total_count = $request->total_array;
            for ($i = 0; $i <= $total_count; $i++) {
                $a = $i + 1;
                if (!empty($request->subcategory[$i])) {
                    $input1['category_id'] = $category['id'];
                    $input1['title'] = $input['subcategory'][$i];
                    $input1['status'] ='active';
                    Subcategory::create($input1);
                }
            }
        }
        Activity::log([
            'contentId'   => $category->id,
            'contentType' => 'Category management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $category_id,
        ]);

        \Session::flash('success','Category has been updated successfully!');
        return redirect('admin/category');
    }

    public function destroy_option($option_id)
    {
        $options = Subcategory::findorfail($option_id);
        $options->delete();
    }


    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $sub_category  = Subcategory::where('category_id',$id)->delete();

        $category->delete();

        Activity::log([
            'contentId'   => $category->id,
            'contentType' => 'Category management',
            'action'      => 'Delete',
            'description' => 'Deleted by '.Auth::user()->role,
            'details'     => 'Category Delete',
        ]);

        \Session::flash('danger','Category has been deleted successfully!');
        return redirect('admin/category');
    }

    public function assign(Request $request)
    {
        $category = Category::findorFail($request['id']);
        $category['status'] = "active";
        $category->update($request->all());

        Activity::log([
            'contentId'   => $category->id,
            'contentType' => 'Category management',
            'action'      => 'Status Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Category status update',
        ]);

        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $category = Category::findorFail($request['id']);
        $category['status'] = "in-active";
        $category->update($request->all());

        Activity::log([
            'contentId'   => $category->id,
            'contentType' => 'Category management',
            'action'      => 'Status Update',
            'description' => 'Status change to inactive by '.Auth::user()->role,
            'details'     => 'Category status update',
        ]);

        return $request['id'];
    }

    public function reorder(Request $request)
    {
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {
                $category = Category::findOrFail($idval);
                $input['displayorder'] = $count;
                $category->update($input);
                $count ++;
            }
            Activity::log([
                'contentId'   => 0,
                'contentType' => 'Category management',
                'action'      => 'Displayorder Change',
                'description' => 'Displayorder change by '.Auth::user()->role,
                'details'     => 'Category status update',
            ]);

            echo 'Display order change successfully.';
        }
    }
}




