<?php

namespace App\Http\Controllers\admin;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function index(Request $request)
    {
        $data['menu'] = 'Feedback';

        if(isset($request['search']) && $request['search'] != '')
        {
            $search = $request['search'];
            $data['feedback'] = Feedback::where('email','like','%'.$search.'%')->orderBy('id', 'asc')->Paginate($this->pagination);
            $data['search']=$request['search'];
        }
        else
        {
            $data['feedback'] = Feedback::with('User')->orderBy('id','desc')->Paginate($this->pagination);
        }

        if ($request->ajax())
        {
            return view('admin.feedback.table',$data);
        }
        return view('admin.feedback.index',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
