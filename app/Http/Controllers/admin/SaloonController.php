<?php

namespace App\Http\Controllers\admin;

use App\Attendance;
use App\Booking_details;
use App\Login_details;
use App\SalonImages;
use App\saloon;
use App\saloon_employees;
use App\Saloon_services;
use App\Service_type;
use App\User;
use App\work_days;
use auth;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Regulus\ActivityLog\Models\Activity;
use Image;

class SaloonController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
    }

    public function index(Request $request)
    {
        $data['menu']="Salon";
        $user_id = Auth::user()->id;
        $data['user'] = User::where('id',$user_id)->first();
        if(Auth::user()->role == 'admin')
        {
            if(isset($request['search']) && $request['search'] != '')
            {
                $search = $request['search'];
                if($request['search'] == 'inactive')
                {
                    $search = 'in-active';
                }
                $data['saloon'] = saloon::with('User')
                    ->where(function ($query) use ($search) {
                        $query->orWhere('title','like','%'.$search.'%' );
                        $query->orWhere('start_hour', 'like','%'.$search.'%');
                        $query->orWhere('end_hour', 'like','%'.$search.'%');
                        $query->orWhere('status', $search);
                    })->Paginate($this->pagination);
                $data['search']=$request['search'];
            }
            else
            {
                $data['saloon'] = saloon::with('User')->orderBy('displayorder','asc')->Paginate($this->pagination);
            }

            if ($request->ajax())
            {
                return view('admin.saloon.table',$data);
            }
            return view('admin.saloon.index',$data);
        }
        else
        {
            $data['service'] = 'saloon_management';
            $get_salon_owner = saloon_employees::where('is_salon_owner',1)->where('user_id',$user_id)->pluck('saloon_id');
            $data['saloon'] = saloon::with('User')->wherein('id',$get_salon_owner)->orderBy('displayorder','asc')->Paginate($this->pagination);
            return view('admin.saloon.index',$data);
        }
    }

    public function saloon_service()
    {
        $data['menu']="Salon";
        $data['service'] = 'subadmin_service';
        $user_id = Auth::user()->id;

        $data['saloon']=saloon::where('user_id',$user_id)->orderBy('displayorder','asc')->first();

        if(isset($data['saloon'])) {
            if($data['saloon']->id) {
                $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
                $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
                $data['work_days'] = work_days::select('work_day','start_hour','end_hour')->where('saloon_id', $data['saloon']->id)->get();
                $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $data['saloon']->id)->get();
                foreach($service as $key => $value) {
                    $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'] == 1 ? 'Yes' : 'No', 'discount' => $value['discount']);
                }
            }
        } else {
            return redirect('admin/saloon/create');
        }
        return view('admin.saloon.view',$data);
    }

    public function create()
    {
        $data = [];
        $data['menu'] = "Salon";
        $data['service'] = 'saloon_management';
        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();

//        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin') {
//            // $data['saloon_users'] = User::where('role','sub_admin')->pluck('email', 'id')->all();
//            $data['saloon_users'] = User::leftJoin('saloons', function($leftjoin){
//                $leftjoin->on('users.id', '=', 'saloons.user_id')->where('saloons.deleted_at',null);
//            })->where('role','sub_admin')->whereNull('saloons.id')->pluck('users.email', 'users.id')->all();
//        }



        $data['salon_employee'] = saloon_employees::where('is_salon_owner',1)->pluck('user_id');
        $data['saloon_users'] = User::whereNotIn('id',$data['salon_employee'])
            ->where(function($query){
                $query->where('membership_plan',1)
                    ->orwhere('membership_plan',3)
                    ->orwhere('admin_membership_plan',1)
                    ->orwhere('admin_membership_plan',3);
            })
            ->orWhere('membership_plan',2)
            ->pluck('email', 'id')->all();

//        $data['saloon_users'] = "";
        $id = Auth::user()->id;
        $data['saloon'] = saloon::where('user_id',$id)->first();
        return view('admin.saloon.create',$data);
    }

    public function store(Request $request)
    {
        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
        {
            $this->validate($request, [
                'user_id' => 'required',
            ]);
        }
        $this->validate($request, [
            'title' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'location' => 'required',
            'description' => 'required',
            'status'=>'required',
            'image'=>'required|mimes:jpeg,bmp,png',
            'saloon_phone' => 'required|numeric',
//            'user_id' => 'required',
        ]);

        $input = $request->all();

        if($input['global_discount'] == 1)
        {
            $this->validate($request, [
                'promotional_title' => 'max:135',
            ]);
        }

        $role = \Illuminate\Support\Facades\Auth::user();

        $user = User::where('id',$request['user_id'])->first();
        if($role->role == 'admin' || $role->role == 'super_admin')
        {
            $input['user_id'] =  $request['user_id'];
        }
        else{
            $input['user_id'] =  $role->id;
        }

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'saloon');
        }

        if(isset($request['is_promo_display']))
        {
            $input['is_promo_display'] = '1';
        }
        else
        {
            $input['is_promo_display'] = '0';
        }

        if(isset($request['is_online']))
        {
            $input['is_online'] = '1';
        }
        else
        {
            $input['is_online'] = '0';
        }

        if(isset($request['loyalty_program']))
        {
            $input['loyalty_program'] = '1';
        }
        else
        {
            $input['loyalty_program'] = '0';
        }

        $input['saloon_phone'] = $request['countries'].$request['saloon_phone'];
        $input['reg_step'] = 1;
        $input['code'] = rand(100000,999999);

        $saloon = saloon::create($input);

        $s_emp['saloon_id'] = $saloon->id;
        $s_emp['user_id'] = $saloon->user_id;
        $s_emp['status'] = 'accept';
        $s_emp['is_read'] = '1';
        $s_emp['is_salon_owner'] = '1';
        saloon_employees::create($s_emp);

        $inp['saloon_id'] = $saloon->id;
        $user->update($inp);

        Activity::log([
            'contentId'   => $saloon->id,
            'contentType' => 'salon management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Salon Add',
        ]);

        \Session::flash('success', 'Salon has been inserted successfully!');

        if($request['save'] == 'save')
        {
            return redirect('admin/saloon/'.$saloon->id.'/edit');           
        }
        else
        {   
            return redirect('admin/saloon/services/'.$saloon->id);
        }
    }

    public function show($id)
    {
        $data['menu']="Salon";
        $data['service'] = 'saloon_management';

//        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
//        {
            $data['saloon'] = saloon::findorFail($id);

            if($data['saloon']->id) {
                $data['type'] = Service_type::where('status','active')->orderBy('title','ASC')->pluck('title','id')->all();
                $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
                $data['work_days'] = work_days::select('work_day','start_hour','end_hour')->where('saloon_id', $data['saloon']->id)->get();
                $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $data['saloon']->id)->get();

                foreach($service as $key => $value) {
                    $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'] == 1 ? 'Yes' : 'No', 'discount' => $value['discount']);
                }
                $data['s_images'] = SalonImages::where('salon_id',$data['saloon']->id)->get();
            }
            return view('admin.saloon.view',$data);
//        }
    }

    public function edit($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);

        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
        $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();


//        if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin') {
//            $data['saloon_users'] = User::where('role','sub_admin')->pluck('email', 'id')->all();
//        }

        $data['salon_employee'] = saloon_employees::where('is_salon_owner',1)->where('saloon_id',$id)->pluck('user_id');
        if (count($data['salon_employee'])<=0){
            $data['salon_employee1'] = saloon_employees::where('is_salon_owner',1)->pluck('user_id');
//             $data['saloon_users'] = User::where('membership_plan',1)->orwhere('admin_membership_plan',1)->pluck('email', 'id')->all();
            $data['saloon_users'] = User::whereNotIn('id',$data['salon_employee1'])
                ->where(function($query){
                    $query->where('membership_plan',1)
                        ->orwhere('membership_plan',3)
                        ->orwhere('admin_membership_plan',1)
                        ->orwhere('admin_membership_plan',3);
                })
                ->orWhere('membership_plan',2)
                ->pluck('email', 'id')->all();
        }
        else{
            $data['saloon_users'] = User::select('email')->where('id',$data['salon_employee'])->first();
        }

        //$data['saloon_users'];

        $data['service'] = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $id)->get();
        $data['avail_discount'] = array(0 => 'No',  1 => 'Yes');

        foreach($data['service'] as $key => $value) {
            $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount']);
        }
        
        $data['working_days'] = work_days::where('saloon_id', $id)->select('work_day', 'start_hour', 'end_hour')->get()->keyBy('work_day');

        // $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();
        $data['id'] = $id;
//        $data['salon_owner'] = saloon_employees::with('saloon','user')->where('saloon_id',$id)->get();
        return view('admin.saloon.edit',$data);
    }

    /*---------------------------------------NEW SERVICE---------------------------------------*/

    public function services($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);

        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
        $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
        $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $id)->get();
        $data['avail_discount'] = array(0 => 'No',  1 => 'Yes');
        $data['id'] = $id;
         foreach($service as $key => $value) {
            $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'], 'discount' => $value['discount']);
        }

        return view('admin.saloon.services',$data);
    }

    public function save_services(Request $request, $id)
    {
        $input = $request->all();
        $services = Saloon_services::where('saloon_id', $id);
        if(! isset($input['type_id'])) {
            \Session::flash('error', 'You have to select at least one service.');
            return redirect()->back();
        }

        $charge_validate = 0;
        foreach($input['type_id'] as $tid)
        {
            if(empty($input['charges_'.$tid]) && $input['charges_'.$tid] == "")
            {
                $charge_validate = 1;
                $c['charges_'.$tid] = 'required';
            }
        }
        if($charge_validate == 1)
        {
            $this->validate($request, $c);
        }

        if($services) {
            $services->forceDelete();
        }
        $service_log = '';
        foreach($input['type_id'] as $tid) {

            $service['saloon_id']       = $id;
            $service['type_id']         = $tid;
            $service['charges']         = $input['charges_'.$tid];
            $service['duration']        = $input['duration_'.$tid];
            $service['avail_discount']  = !empty($input['avail_discount_'.$tid])?$input['avail_discount_'.$tid]:0;
            $service['discount']        = !empty($input['discount_'.$tid])?$input['discount_'.$tid]:0;
            $service['status']          = 'active';
            $service_log .= $tid.',';
            Saloon_services::create($service);

        }

       Activity::log([
            'contentId'   => $id,
            'contentType' => 'salon management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Salon Service Add',
            'data'        => $service_log,
        ]);

        $saloon = saloon::findOrfail($id);
        $sinput['reg_step'] = 2;
        $saloon->update($sinput);

        \Session::flash('success', 'Service details have been updated successfully!');

        if($request['save'] == 'save')
        {
            return redirect('admin/saloon/services_edit/'.$id);           
        }
        else
        {   
            return redirect('admin/saloon/work_days/'.$id);
        }
    }

    /*---------------------------------------UPDATE SERVICE---------------------------------------*/

    public function services_edit($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);

        $data['type'] = Service_type::where('status','active')->orderBy('title','asc')->pluck('title','id')->all();
        $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
        $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $id)->get();
        $data['avail_discount'] = array(0 => 'No',  1 => 'Yes');
        $data['id'] = $id;
         foreach($service as $key => $value) {
            $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'], 'discount' => $value['discount']);
        }
        $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();
//        $data['salon_owner'] = saloon_employees::with('saloon','user')->where('saloon_id',$id)->get();
        return view('admin.saloon.edit_services',$data);
    }

    public function update_services(Request $request, $id)
    {
        $input = $request->all();
        $services = Saloon_services::where('saloon_id', $id);

        if(! isset($input['type_id'])) {
            \Session::flash('error', 'You have to select at least one service.');
            return redirect()->back();
        }

        $charge_validate = 0;
        foreach($input['type_id'] as $tid)
        {
            if(empty($input['charges_'.$tid]) && $input['charges_'.$tid] == "")
            {
                $charge_validate = 1;
                $c['charges_'.$tid] = 'required';
            }
        }
        if($charge_validate == 1)
        {
            $this->validate($request, $c);
        }

        if($services) {
            $services->forceDelete();
        }
        $service_log = '';
        foreach($input['type_id'] as $tid) {
            $service['saloon_id']       = $id;
            $service['type_id']         = $tid;
            $service['charges']         = $input['charges_'.$tid];
            $service['duration']        = $input['duration_'.$tid];
            $service['avail_discount']  = !empty($input['avail_discount_'.$tid])?$input['avail_discount_'.$tid]:0;
            $service['discount']        = !empty($input['discount_'.$tid])?$input['discount_'.$tid]:0;
            $service['status']          = 'active';
            $service_log .= $tid.',';
            Saloon_services::create($service);
        }

         Activity::log([
            'contentId'   => $id,
            'contentType' => 'salon management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Salon Service Add',
            'data'        => $service_log,
        ]);

        $saloon = saloon::findOrfail($id);
        if ($saloon->reg_step<2){
            $sinput['reg_step'] = 2;
            $saloon->update($sinput);
        }

        \Session::flash('success', 'Service details have been updated successfully!');

        if($request['save'] == 'save')
        {
            return redirect()->back();            
        }
        else
        {
            return redirect('admin/saloon/work_days/'.$id);
        }
    }

    /*---------------------------------------NEW WORK DAYS---------------------------------------*/
    public function work_days($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);
        $data['id'] = $id;
        // $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();
        $data['working_days'] = work_days::where('saloon_id', $id)->select('work_day', 'start_hour', 'end_hour')->get()->keyBy('work_day');
//        $data['salon_owner'] = saloon_employees::with('saloon','user')->where('saloon_id',$id)->get();
        return view('admin.saloon.work_days',$data);
    }

    public function save_days(Request $request, $id)
    {
        $input = $request->all();
        $working_days = work_days::where('saloon_id', $id);

        if(! isset($input['work_days'])) {
            \Session::flash('error', 'You have to select at least one working day.');
            return redirect()->back();
        }

        if($working_days) {
            $working_days->forceDelete();
        }
        $days_log = '';
        foreach($input['work_days'] as $day) {

            $wdays['saloon_id'] = $id;
            $wdays['work_day']  = $day;
            $wdays['start_hour'] = $input['start_hour'][$day];
            $wdays['end_hour']   = $input['end_hour'][$day];
            
            $days_log .= $day.',';

            work_days::create($wdays);
        }

        Activity::log([
            'contentId'   => $id,
            'contentType' => 'salon management',
            'action'      => 'Insert',
            'description' => 'Inserted by '.Auth::user()->role,
            'details'     => 'Work Days Add',
            'data'        => $days_log,
        ]);

        $saloon = saloon::findOrfail($id);

        if ($saloon->reg_step<3){
            $sinput['reg_step'] = 3;
            $saloon->update($sinput);
        }

        \Session::flash('success', 'Salon details have been updated successfully!');

        if($request['save'] == 'save')
        {
            return redirect()->back();            
        }
        else
        {
            return redirect('admin/saloon/images/'.$saloon->id.'/edit');
        }
    }

    public function images($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);
        $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();
        $data['id'] = $id;
        return view('admin.saloon.images',$data);
    }

    /*---------------------------------------UPDATE SALON DETAILS---------------------------------------*/

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'location' => 'required',
            'description' => 'required',
            'status'=>'required',
            'image'=>'mimes:jpeg,bmp,png',
            'saloon_phone' => 'required|numeric',
        ]);

//        $customMessages = [
//            'type_id.required' => 'You have to select at least one service.',
//            'work_days.required' => 'Atlest one working day is required',
//        ];
//
//        $this->validate($request, $rules, $customMessages);

        $input = $request->all();


        if($input['global_discount'] == 1)
        {
            $this->validate($request, [
                'promotional_title' => 'max:135',
            ]);
        }

        $saloondata = saloon::findOrFail($id);

        $saloon_input['title'] = $input['title'];
        $saloon_input['status'] = $input['status'];
        $saloon_input['description'] = $input['description'];
        $saloon_input['location'] = $input['location'];
        $saloon_input['latitude'] = $input['latitude'];
        $saloon_input['longitude'] = $input['longitude'];
        $saloon_input['timezone'] = $input['timezone'];
        $saloon_input['saloon_phone'] = $input['saloon_phone'];
        $saloon_input['global_discount'] = $input['global_discount'];
        $saloon_input['address1'] = $input['address1'];
        $saloon_input['address2'] = $input['address2'];
        if($input['global_discount'] == 1)
        {
            $saloon_input['promotional_title'] = $input['promotional_title'];
        }

        $saloon_input['saloon_phone'] = $request['countries'].$request['saloon_phone'];

        if($photo = $request->file('image'))
        {
            if(file_exists($saloondata->image))
            {
                unlink($saloondata->image);
            }
            $saloon_input['image'] = $this->image($photo,'saloon');
        }

        if (false === strpos($request['website_url'], '://')) {
            $saloon_input['website_url'] = 'http://' . $request['website_url'];
        }

        if(isset($request['is_promo_display']))
        {
            $saloon_input['is_promo_display'] = '1';
        }
        else
        {
            $saloon_input['is_promo_display'] = '0';
        }

        if(isset($request['is_online']))
        {
            $saloon_input['is_online'] = '1';
        }
        else
        {
            $saloon_input['is_online'] = '0';
        }

        if(isset($request['loyalty_program']))
        {
            $saloon_input['loyalty_program'] = '1';
        }
        else
        {
            $saloon_input['loyalty_program'] = '0';
        }

        $msg = "";
        if ($saloondata->title!=$input['title']){ $msg .= "Title".",";}
        if ($saloondata->location!=$input['location']){ $msg .= "Location".",";}
        if ($saloondata->address1!=$input['address1']){ $msg .= "address1".",";}
        if ($saloondata->address2!=$input['address2']){ $msg .= "address2".",";}
        if ($saloondata->latitude!=$input['latitude']){ $msg .= "latitude".",";}
        if ($saloondata->longitude!=$input['longitude']){ $msg .= "Longitude".",";}
        if ($saloondata->timezone!=$input['timezone']){ $msg .= "Timezone".",";}
        if ($saloondata->description!=$input['description']){ $msg .= "Description".",";}
        if ($saloondata->saloon_phone!=$input['saloon_phone']){ $msg .= "Saloon_phone".",";}
        if ($request->file('image')){ $msg .= "Saloon_logo ".",";}
        if ($saloondata->global_discount!=$input['global_discount']){ $msg .= "Global_discount".",";}
        if ($saloondata->status!=$input['status']){ $msg .= "Status".",";}

        $saloondata->update($saloon_input);


        /* ASSIGN OWNER */
        $check_owner = saloon_employees::where('saloon_id',$id)->where('is_salon_owner',1)->first();
        if ($check_owner){
        }
        else{
            $s_emp['saloon_id'] = $id;
            $s_emp['user_id'] = $request['user_id'];
            $s_emp['status'] = 'accept';
            $s_emp['is_read'] = '1';
            $s_emp['is_salon_owner'] = '1';
            saloon_employees::create($s_emp);
        }

        Activity::log([
            'contentId'   => $saloondata->id,
            'contentType' => 'salon management',
            'action'      => 'Update',
            'description' => 'Updated by '.Auth::user()->role,
            'details'     => $msg,
            'updated'     => $id,
        ]);


        \Session::flash('success','Salon details has been updated successfully!');

        if($request['save'] == 'save')
        {
            return redirect()->back();            
        }
        else
        {
            return redirect('admin/saloon/services_edit/'.$id);
        }
    }

    public function destroy($id)
    {
        $saloon = saloon::findOrFail($id);
        $saloon->delete();

         Activity::log([
            'contentId'   => $saloon->id,
            'contentType' => 'salon management',
            'action'      => 'Delete',
            'description' => 'Salon deleted by '.Auth::user()->role,
            'details'     => 'Salon Delete, Salon Service Delete, Workdays Delete, Employee Delete, Salon Image Delete',
        ]);
        
        \Session::flash('danger','Salon has been deleted successfully!');
        return;
    }

    public function assign(Request $request)
    {
        $saloon = saloon::findorFail($request['id']);
        $saloon['status'] = "active";
        $saloon->update($request->all());

        Activity::log([
            'contentId'   => $saloon->id,
            'contentType' => 'salon management',
            'action'      => 'Update',
            'description' => 'Status change to active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);

        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $saloon = saloon::findorFail($request['id']);
        $saloon['status'] = "in-active";
        $saloon->update($request->all());

         Activity::log([
            'contentId'   => $saloon->id,
            'contentType' => 'salon management',
            'action'      => 'Update',
            'description' => 'Status change to in-active by '.Auth::user()->role,
            'details'     => 'Status Update',
        ]);
        return $request['id'];
    }

    public function reorder(Request $request)
    {
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {

                $saloon = saloon::findOrFail($idval);
                $input['displayorder'] = $count;
                $saloon->update($input);
                $count ++;
            }
            echo 'Display order change successfully.';
        }
    }

    /*---------------------------------------ADD OR UPDATE IMAGES OF SALON---------------------------------------*/
    public function images_edit($id)
    {
        $data['menu'] = 'Salon';
        $data['saloon'] = saloon::findOrFail($id);
        $data['salon_media'] = SalonImages::where('salon_id',$id)->orderBy('displayorder','ASC')->get();
        $data['total_media'] = SalonImages::where('salon_id',$id)->count();
        $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();
        $data['id'] = $id;
//        $data['salon_owner'] = saloon_employees::with('saloon','user')->where('saloon_id',$id)->get();
        return view('admin.saloon.edit_images', $data);
    }

    public function images_update(Request $request,$id)
    {
        $saloon = saloon::findOrFail($id);
        if(isset($request['multimage'][0]))
        {
            $multi_count = $request['mcnt'];
            for($i=0;$i<$multi_count;$i++) {

                if($photo = $request->file('multimage')[$i])
                {
                    $salon_images  = SalonImages::where('salon_id',$id)->count();
                    $display_order = $salon_images+1;

                    /* IMAGE */
                    $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

                    /*Image Resize Code*/
                    $img_resize = Image::make($photo->getRealPath());
                    $data = getimagesize($photo);
                    $width = $data[0];
                    if($width > 500){
                        $img_resize->resize(null, 400, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }

                    $img_resize->save(public_path('resource/product/saloon/'.$img_name));
                    /*-------------*/
                    //$input1['image_blob'] = $img_resize;
                    $input1['image'] = "public/resource/product/saloon/".$img_name;

                    /* THUMBNAIL*/

                    $path1 =  $this->image($photo,'product/saloon/thumbnail');
                    if($width > 200){
                        $input1['image_thumbnail'] = $this->Imagethumbnail($path1,'product/saloon/thumbnail',200,200,null);
                    }
                    else{
                        $input1['image_thumbnail'] = $path1;
                    }

//                    $input1['image'] = $image_path;
                    $input1['salon_id'] = $id;
                    $input1['updated_at'] = Carbon::now();
                    $input1['displayorder'] = $display_order;
                    $imageupdate1 = new SalonImages();
                    if (isset($input1['image'])){
                        $imageupdate1->create($input1);
                    }
                }
            }
            return redirect()->back();
        }
        else
        {
            $input = $request->all();
            $prev_ids = $saloon->SalonImages->pluck('id')->all();
            $existing_ids = [];
            $ids = json_decode($request->input('ids'));
            foreach ($ids as $i){
                $media_id = $request->input('media_id' . $i);
                if ($media_id == 'NEW')
                {
                    $imagedata['salon_id'] = $saloon->id;
                    $imagedata['displayorder'] = !empty($input['displayorder'.$i])?$input['displayorder'.$i]: "";
                    if ($photo = $request->file('image' . $i)) {
                        $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

                        /*Image Resize Code*/
                        $img_resize = Image::make($photo->getRealPath());
                        $data = getimagesize($photo);
                        $width = $data[0];
                        if($width > 500){
                            $img_resize->resize(null, 500, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                        }

                        $img_resize->save(public_path('resource/product/saloon/'.$img_name));
                        /*-------------*/
                        //$imagedata['image_blob'] = $img_resize;
                        $imagedata['image'] = "public/resource/product/saloon/".$img_name;

                        /* THUMBNAIL*/

                        $path1 =  $this->image($photo,'product/saloon/thumbnail');
                        if($width > 200){
                            $imagedata['image_thumbnail'] = $this->Imagethumbnail($path1,'product/saloon/thumbnail',200,200,null);
                        }
                        else{
                            $imagedata['image_thumbnail'] = $path1;
                        }

                        $imagedata['updated_at'] = Carbon::now();
                        $imageupdate = new SalonImages();
                        $imageupdate->create($imagedata);
                    }
                }
                else{

                    $existing_ids[] = $media_id;
                    $salon_image =  SalonImages::findOrFail($media_id);
                    if (!empty($salon_image)){
                        $media = [];

                        $media['displayorder'] = $input['displayorder'.$i];
                        if ($photo = $request->file('image' . $i)) {

                            /* IMAGE */
                            $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

                            /*Image Resize Code*/
                            $img_resize = Image::make($photo->getRealPath());
                            $data = getimagesize($photo);
                            $width = $data[0];
                            if($width > 500){
                                $img_resize->resize(null, 500, function ($constraint) {
                                    $constraint->aspectRatio();
                                });
                            }

                            $img_resize->save(public_path('resource/product/saloon/'.$img_name));
                            /*-------------*/
                            //$media['image_blob'] = $img_resize;
                            $imagedata['image'] = "public/resource/product/saloon/".$img_name;

                            /* THUMBNAIL*/

                            $path1 =  $this->image($photo,'product/saloon/thumbnail');
                            if($width > 200){
                                $media['image_thumbnail'] = $this->Imagethumbnail($path1,'product/saloon/thumbnail',200,200,null);
                            }
                            else{
                                $media['image_thumbnail'] = $path1;
                            }
                            $media['updated_at'] = Carbon::now();
                            $salon_image->update($media);
                        }
                    }
                }
            }

            // Delete media
            foreach ($prev_ids as $id)
            {
                if (!in_array($id, $existing_ids))
                {
                    $salon_media = SalonImages::findOrFail($id);
                    $salon_media->forceDelete();
                }
            }
            
            Activity::log([
                'contentId'   => $saloon->id,
                'contentType' => 'salon management',
                'action'      => 'Update',
                'description' => 'Images update by '.Auth::user()->role,
                'details'     => 'Images Update',
            ]);
            
            $input['reg_step'] = 4;
            $saloon->update($input);

            \Session::flash('success', 'Salon image added successfully');

            if($request['save'] == 'save')
            {
                return redirect()->back();
            }
            else
            {
                return redirect('admin/saloon');
            }
        }
    }

    /*---------------------------------------GET USER NAME---------------------------------------*/
    public function get_user(Request $request)
    {
        $keyword = strval($request['query']);
        $search_user = "{$keyword}";
        $locationResult = [];
        $saloon_users = User::whereIn('membership_plan',[1,3])->orWhereIn('admin_membership_plan',[1,3])->where('email','like',$search_user.'%')->pluck('email', 'id')->all();
        $p_search = $saloon_users;
        if (!empty($p_search)) {
            foreach ($p_search as $key => $val){
                $s_emp = saloon_employees::where('user_id',$key)->where('is_salon_owner',1)->get();
                if($s_emp->count()>0){
                    unset($key);
                    continue;
                }
                else{
                    $locationResult[] = $val;
                }

            }

            $result = array_unique($locationResult);
            echo json_encode($result);
        }
    }

    /* -------------------- EMPLOYEE LIST ---------------------------*/

    public function employeelist(Request $request,$id){
        $data['menu']="Employee";
        $user_id = Auth::user()->id;
        $data['salon_id'] = $id;
        $data['user'] = User::where('id',$user_id)->first();
        if(Auth::user()->role == 'admin')
        {
        }
        else
        {
            if(isset($request['search']) && $request['search'] != '') {
                $search = $request['search'];
                $users = User::where('name','like','%'.$request['search'].'%')
                    ->orWhere('email', 'like', '%'.$request['search'].'%')
                    ->where('deleted_at', null)->pluck('id');
                $data['employees'] = saloon_employees::wherein('user_id',$users)->with('saloon','user')
                    ->where('saloon_id',$id)
                    ->where('is_salon_owner','!=',1)
                    ->where('status','accept')
                    ->Paginate($this->service_pagination);
            }
            else{
                $data['employees'] = saloon_employees::with('saloon','user')
                    ->where('saloon_id',$id)
                    ->where('is_salon_owner','!=',1)
                    ->where('status','accept')
                    ->Paginate($this->service_pagination);
            }

            if ($request->ajax())
            {
                return view('admin.employees.table',$data);
            }
            return view('admin.employees.index',$data);
        }

    }

    /*---------------------------------------ASSIGN SALON OWNER---------------------------------------*/

    public function assign_owner(Request $request)
    {
        $user = User::where('email',$request['salon_owner'])->first();
        $input['saloon_id'] = $request['salon_id'];
        $input['user_id'] = $user['id'];
        $input['status'] = 'accept';
        $input['is_read'] = '1';
        $input['is_salon_owner'] = '1';
        saloon_employees::create($input);
        return ;
    }

    public function delete_assign_owner(Request $request,$id,$salon_id)
    {
        $salon_emp = saloon_employees::where('id',$id)->first();

        $date_time = Carbon::now();
        $date_today = $date_time->format('Y-m-d');
        $get_booking = \App\Booking::with('Booking_details')->where('saloon_id', $salon_id)->where('status', 'in-progress')->where('date', $date_today)->get();
        foreach ($get_booking as $bookings){
            if ($bookings['Booking_details'][0]['emp_id'] == $salon_emp['user_id']){
                \Session::flash('warning', 'You can not delete employee in serving mode!');
                return redirect('admin/employee/'.$salon_id.'/list');
            }
            else {

                /* UPDATE EXITING BOOKING IF DELETED EMPLOYEE IS ASSIGN TO SOME BOOKING AND SALON OWNER WILL ASSING AS EMPLOYEE IN THAT BOOKING*/
                $get_salon_owner = saloon_employees::where('saloon_id',$salon_id)->where('is_salon_owner',1)->first();

                $get_booking = \App\Booking::where('saloon_id', $salon_id)->where('status', 'waiting')->where('date', $date_today)->get();
                foreach ($get_booking as $booking){
                    $get_booking_details = Booking_details::where('booking_id',$booking['id'])->get();
                    foreach ($get_booking_details as $details){
                        if($details['emp_id']==$salon_emp['user_id']){
                            $ip['emp_id'] = $get_salon_owner['user_id'];
                            $details->update($ip);
                        }
                    }
                }
                $owner = saloon_employees::where('id', $id)->delete();

                /* REMOVE EMPLOYEE  USER LOGIN */
                $login_details  = Login_details::where('user_id',$salon_emp['user_id'])->delete();

                /* REMOVE ITS ATTENDANCE AND CHANGE STATUS TO ABSENT */

                $user = User::where('id',$salon_emp['user_id'])->first();
                $current_date  = Carbon::now()->format('Y-m-d');
                $start_time  = Carbon::now()->format('H:i');
                $end_time  = Carbon::now()->format('H:i');

                $attendance = Attendance::where('user_id',$salon_emp['user_id'])
                    ->where('date',$current_date)
                    ->where('salon_id',$salon_id)
                    ->where('end_time',null)->first();

                if ($attendance){
                    $interval = $this->getTimeDiff($attendance['start_time'],$end_time);
                    $hours   = date('H',strtotime($interval));
                    $minutes = date('i',strtotime($interval));
                    $total_time = $hours * 60 + $minutes;

                    if ($total_time == 0){
                        $attendance->delete();
                    }

                    $input['start_time'] = date('H:i',strtotime($attendance['start_time']));
                    $input['end_time'] = $end_time;
                    $input['total_time'] = $total_time;
                    $attendance->update($input);

                    $ip['available'] = "absent";
                    $user->update($ip);
                }
            }
        }

        $salon_emp->delete();

        \Session::flash('danger', 'Employee deleted successfully!');
        return redirect('admin/employee/'.$salon_id.'/list');
    }


    public function employee_attendance(Request $request){
        $id = $request['id'];
        $salon_id = $request['salon_id'];
        $user = User::where('id',$id)->first();

        $date_time = Carbon::now();
        $date_today = $date_time->format('Y-m-d');

        /* UPDATE EXITING BOOKING IF EMPLOYEE STATUS IS CHANGE AND SALON OWNER WILL ASSING AS EMPLOYEE IN THAT BOOKING*/

        $get_salon_owner = saloon_employees::where('saloon_id',$salon_id)->where('is_salon_owner',1)->first();
        $get_booking = \App\Booking::where('saloon_id', $salon_id)->where('status', 'waiting')->where('date', $date_today)->get();  // GET ALL CURRENT BOOKING

        $flag=0;
        foreach ($get_booking as $booking){
            $get_booking_details = Booking_details::where('booking_id',$booking['id'])->get();  // GET BOOKING DETAILS
            foreach ($get_booking_details as $details){
                if($details['emp_id']==$id){    // COMPARE WITH BOOKING EMP AND CURRENT STARTUS CHANGE EMP, IF MATCH UPDATE ITS EMPLOYEE ID TO SALON OWNER
                    $ip['emp_id'] = $get_salon_owner['user_id'];
                    $details->update($ip);
                    $flag=1;
                }
            }
        }

        if ($flag==1){
            return $flag;
        }


        $current_date  = Carbon::now()->format('Y-m-d');
        $start_time  = Carbon::now()->format('H:i');
        $end_time  = Carbon::now()->format('H:i');

        $attendance = Attendance::where('user_id',$id)
            ->where('date',$current_date)
            ->where('salon_id',$salon_id)
            ->where('end_time',null)->first();

        if ($attendance){
            $interval = $this->getTimeDiff($attendance['start_time'],$end_time);
            $hours   = date('H',strtotime($interval));
            $minutes = date('i',strtotime($interval));
            $total_time = $hours * 60 + $minutes;

            if ($total_time == 0){
                $attendance->delete();
            }

            $input['start_time'] = date('H:i',strtotime($attendance['start_time']));
            $input['end_time'] = $end_time;
            $input['total_time'] = $total_time;
            $attendance->update($input);

            $ip['available'] = "absent";
            $user->update($ip);
            return $flag;
        }

        $input['user_id'] = $id;
        $input['salon_id'] = $salon_id;
        $input['date'] = $current_date;
        $input['start_time'] = $start_time;
        $attendance = Attendance::create($input);

        $ip['available'] = "present";
        $user->update($ip);

        return $flag;

    }
}
