<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use App\Point_history;
use App\saloon_employees;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        $this->middleware('checkstep');
    }

    public function customer_index(Request $request, $id)
    {
        $data['menu']="Customer";
        $user_id = Auth::user()->id;
        $user = \Illuminate\Support\Facades\Auth::user();
        $plan = 0;
        if ($user['membership_plan']==1 || $user['membership_plan']==2 || $user['membership_plan']==3){
            $plan = $user['membership_plan'];
        }
        if ($user['admin_membership_plan']==1 || $user['admin_membership_plan']==2 || $user['admin_membership_plan']==3){
            $plan = $user['admin_membership_plan'];
        }

        $get_booking_users = Booking::where('saloon_id',$id)->groupBy('user_id')->pluck('user_id');
        $query = User::whereIn('id',$get_booking_users)->where('status','active')->select();

        if(isset($request['search']) && $request['search'] != '')
        {
            $query->where('name','like', '%'.$request['search'].'%')
                ->orWhere('phone','like',$request['search'].'%')
                ->orWhere('email','like',$request['search'].'%')
                ->get();
        }
        else{
            $query->orderBy('id','DESC');
        }

        if ($request->ajax())
        {
            $data['id'] = $id;
            $data['customer'] = $query->Paginate($this->pagination);
            return view('admin.customer.table',$data);
        }

        $data['id'] = $id;
        $data['customer'] = $query->Paginate($this->pagination);
        return view('admin.customer.index',$data);
    }

    public function customer_details(Request $request, $id)
    {
        $data['menu']="Customer";
        $data['user'] = User::findOrFail($id);
        $data['salon_id'] = session('salon_id');

        /*For Bookings*/
        $data['booking'] = Booking::with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id',$data['salon_id'])->where('user_id',$id)->get();
        $data['booking_count'] = Booking::with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id',$data['salon_id'])->where('user_id',$id)->get();
        $data['total_visit'] = Booking::where('saloon_id',$data['salon_id'])->where('user_id',$id)->where('status','completed')->count();

        /*For Points*/
        $data['point'] = Point_history::where('user_id',$id)->orderBy('id','DESC')->Paginate(5);
        $data['point_count'] = Point_history::where('user_id',$id)->orderBy('id','DESC')->first();

        if ($request->ajax())
        {
            return view('admin.users.point_table',$data);
        }

        return view('admin.customer.details',$data);
    }
    public function ajax_get_customer(Request $request){
        return $data['customer'] = User::find($request['id']);

    }
}
