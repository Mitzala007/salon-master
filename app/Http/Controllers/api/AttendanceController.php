<?php

namespace App\Http\Controllers\api;

use App\Attendance;
use App\Login_details;
use App\saloon;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    public function attendance_checkin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_feedback');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                /* ADD ATTENDANCE */
                $appuser = User::findorFail($loginUser->user_id);

                if(isset($request['user_id']) && $request['user_id']!=""){
                    $appuser = User::findorFail($request['user_id']);
                }

                $salon_id = $appuser['saloon_id'];
                $saloon = saloon::where('id',$salon_id)->where('status','active')->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $user_id = $appuser['id'];
                    $current_date  = Carbon::now()->format('Y-m-d');
                    $start_time  = Carbon::now()->format('H:i');
                    $end_time  = Carbon::now()->format('H:i');

                    $attendance = Attendance::where('user_id',$user_id)
                        ->where('date',$current_date)
                        ->where('salon_id',$salon_id)
                        ->where('end_time',null)->first();

                    if ($attendance){

                        $interval = $this->getTimeDiff($attendance['start_time'],$end_time);
                        $hours   = date('H',strtotime($interval));
                        $minutes = date('i',strtotime($interval));
                        $total_time = $hours * 60 + $minutes;

                        if ($total_time == 0){
                            $attendance->delete();
                        }

                        $input['start_time'] = date('H:i',strtotime($attendance['start_time']));
                        $input['end_time'] = $end_time;
                        $input['total_time'] = $total_time;
                        $attendance->update($input);

                        $attendance['total_time'] = $hours.":".$minutes;

                        $user_stat['available']="absent";
                        $appuser->update($user_stat);

                        $response["Result"] = 1;
                        $response["login_status"] = "checkout";
                        $response['User'] = $appuser;
                        $response['Message'] = "Success";
                        $response['Attendance'] = $attendance;
                        return response($response, 200);

                    }

                    $input['user_id'] = $user_id;
                    $input['salon_id'] = $salon_id;
                    $input['date'] = $current_date;
                    $input['start_time'] = $start_time;
                    $attendance = Attendance::create($input);

                    $user_stat['available']="present";
                    $appuser->update($user_stat);
                }

                $response["Result"] = 1;
                $response['login_status'] = "checkin";
                $response['User'] = $appuser;
                $response['Message'] = "Success";
                $response['Attendance'] = $attendance;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function attendance_history(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'date' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_feedback');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                /* GET ATTENDANCE */
                if (isset($request['user_id'])){
                    $appuser = User::findorFail($request['user_id']);
                }
                else{
                    $appuser = User::findorFail($loginUser->user_id);
                }

                $salon_id = $appuser['saloon_id'];
                $saloon = saloon::where('id',$salon_id)->where('status','active')->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else {
                    $user_id = $appuser['id'];
                    $attendance = Attendance::where('user_id', $user_id)
                        ->where('salon_id', $salon_id)
                        ->where('end_time', '!=', null)
                        ->where('date', $request['date'])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                    
                    $todays_total = 0;
                    foreach ($attendance as $k=>$v){

                        $t = $v['total_time'];
                        $h = floor($t/60) ? floor($t/60):'00';
                        $m = $t%60 ? $t%60:'00';
                        
                        $todays_total = $todays_total + $v['total_time'];
                        $v['total_time'] = sprintf("%02d", $h).":".sprintf("%02d", $m);
                        
                        
                    } 
                    
                  
                    /* TODAYS TOTAL */
                    $h = floor($todays_total/60) ? floor($todays_total/60):'00';
                    $m = $todays_total%60 ? $todays_total%60:'00';
                    $todays_total = sprintf("%02d", $h).":".sprintf("%02d", $m);
                    
                    /* MONTHLY TOTAL */
                    $sdate = date('Y-m-01');
                    $edate = date('Y-m-d');
                    $month_total = Attendance::where('date','>=',$sdate)->where('date','<=',$edate)->where('user_id',$user_id)->sum('total_time');
                    $hh = floor($month_total/60) ? floor($month_total/60):'00';
                    $mm = $month_total%60 ? $month_total%60:'00';
                    $monthly_total = sprintf("%02d", $hh).":".sprintf("%02d", $mm);
                               
                    
                    $response["Result"] = 1;
                    $response['Message'] = "Success";
                    $response['Attendance'] = $attendance;
                    $response['Monthly_total'] = $monthly_total;
                    $response['Todays_total'] = $todays_total;
                    return response($response, 200);
                }

                $response["Result"] = 0;
                $response['Message'] = "Success";
                $response['Attendance'] = [];
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

}
