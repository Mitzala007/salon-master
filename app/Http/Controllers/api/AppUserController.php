<?php

namespace App\Http\Controllers\api;

use App\Attendance;
use App\Booking;
use App\Booking_details;
use App\Category;
use App\cities;
use App\countries;
use App\Employees;
use App\Email;
use App\Favourites;
use App\Feedback;
use App\Login_details;
use App\Salon_owner_request;
use App\SalonImages;
use App\saloon;
use App\Salon_reward;
use App\Saloon_services;
use App\saloon_employees;
use App\Point_history;
use App\Service_type;
use App\states;
use App\Message;
use App\Notification;
use App\Rating;
use App\Remainder;
use App\Payment_history;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;
use Clickatell\Rest;
use Clickatell\ClickatellException;
use Davibennun\LaravelPushNotification\PushNotification;

class AppUserController extends Controller
{
    public function login(Request $request)
    {
        try {
            if ($request['version'] != $this->version && $request['version'] != $this->version1) {
                $response["Result"] = 10;  // if ios then redirect to app store and android redirect to playstore
                $response["Message"] = "Please download latest application!";
                return response($response, 200);
            }

            $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where('email', $request['email'])->withTrashed()->first();
            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Email address or password is incorrect. Please try again.";
                return response($response, 200);
            }
            else if($appuser['deleted_at']!=""){
                $response["Result"] = 0;
                $response["Message"] = "Your account is blocked. Contact to nailmaster support team!";
                return response($response, 200);
            }
            else {

                if( Hash::check($request['password'],$appuser['password']) == false)
                {
                    $response["Result"] = 0;
                    $response["Message"] = "Email address or password is incorrect. Please try again.";
                    return response($response, 200);
                }
                else
                {
                    if ($appuser['is_email_verified']!=1){
                        //$token = str_random(30);
                        //$input['remember_token'] = $token;
                        //$appuser->update($input);

                        $em = $request['email'];
                        $bcc = [];
                        $register_email=Email::findorFail(2);
                        $name = $appuser->name;
                        $link = config('siteVars.siteurl')."register_confirmation_email?ids=".$appuser['remember_token']."";
                        $subject = $register_email['subject'];
                        $from1 = $register_email['email'];

                        $string = ["{name}", "{link}"];
                        $replace_string = [$name, $link];

                        $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                        $data['title'] = $register_email['title'];

                        Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                            $m->from($from1, config('siteVars.title'));
                            $m->bcc($bcc);
                            $m->to($em)->subject($subject);
                        });

                        $response["Result"] = 0;
                        $response["Message"] = "Please verify your email address!";
                        return response($response, 200);
                    }

                    if ($appuser->status != 'active') {
                        $response["Result"] = 0;
                        $response["Message"] = "Your account is blocked.";

                        $input = $request->all();
                        $this->errorlog($response["Message"],json_encode($input),'login');
                        return response($response, 200);
                    }

                    if (isset($request['device_type']) && isset($request['device_token'])){
                        $logins['device_type'] = $request['device_type'];
                        $logins['device_token'] = $request['device_token'];
                    }

                    $logins['remember_token'] = str_random(30);
                    $logins['user_id'] = $appuser->id;
                    $logins['login_time'] = Carbon::now();
                    $logins['version_no'] = $request['version_no'];
                    $logins['build_no'] = $request['build_no'];

                    if($appuser['gender'] == 'male') {
                        $img = url('assets/dist/img/male.png');
                    } else if ($appuser['gender'] == 'female') {
                        $img = url('assets/dist/img/female.png');
                    } else {
                        $img = url('assets/dist/img/other.png');
                    }

                    if (isset($request['device_token']) && $request['device_token']!= ""){
                        Login_details::where('device_token',$request['device_token'])->delete();
                    }

                    Login_details::create($logins);

                    $user = User::findorFail($appuser->id);
                    if((isset($request['latitude']) && $request['latitude']!="") && (isset($request['longitude']) && $request['longitude']!="")){
                        /* timezone code */
                        $timezone = $this->get_timezone($request['latitude'],$request['longitude']);

                        $input['latitude'] = $request['latitude'];
                        $input['longitude'] = $request['longitude'];
                        $input['timezone'] = $timezone;
                        $user->update($input);

                        $get_remainder = Remainder::where('user_id',$appuser->id)->first();
                        if ($get_remainder){
                            $remainders['timezone'] = $timezone;
                            $get_remainder->update($remainders);
                        }
                    }

                    if($user->image == '')
                    {
                        $user->image = $img;
                    }

                    unset($user->password);
                    unset($user->deleted_at);

                    if ($user->image != "" && file_exists($user['image'])) {
                        $user->image = url($user->image);
                    }

                    $user['token'] = $logins['remember_token'];

                    $plan = 0;
                    $user['plan_id'] = 0;
                    if ($user['admin_membership_plan'] != null){
                        $plan = $user['admin_membership_plan'];
                    }
                    else{
                        $plan = $user['membership_plan'];
                    }

                    /* MY SALON WITH OWNER SHIP , IM SALON OWNER */
                    if ($plan == 1 || $plan == 2 || $plan == 3){
                        $get_salon_owner = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',1)->get();
                        if ($get_salon_owner){
                            $salon_list = collect();
                            foreach ($get_salon_owner as $k => $v){
                                $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                                if (!empty($get_salon) && $get_salon!=""){
                                    $salon_list->push($get_salon);
                                }
                            }
                        }
                    }

                    /* MY SALON AS EMPLOYEE */

                    $get_salon_employee = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',0)->get();
                    if ($get_salon_employee){
                        $salon_lists = collect();
                        foreach ($get_salon_employee as $k => $v){
                            $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                            if (!empty($get_salon) && $get_salon!=""){
                                $salon_lists->push($get_salon);
                            }
                        }
                    }

                    $user['plan_id'] = $plan;
                    $user['salon'] = !empty($salon_list)?$salon_list:array();
                    $user['my_salon'] = !empty($salon_lists)?$salon_lists:array();

                    array_walk_recursive($user, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    $response["Result"] = 1;
                    $response["User"] = $user;
                    $response['Message'] = "User login Successfully.";
                    return response($response, 200);
                }
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function verify_login(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("email", $request['email'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Email address or password is incorrect. Please try again.";
                return response($response, 200);
            }
            else {

                if( Hash::check($request['password'],$appuser['password']) == false)
                {
                    $response["Result"] = 0;
                    $response["Message"] = "Email address or password is incorrect. Please try again.";
                    return response($response, 200);
                }
                else
                {

                    if ($appuser->status != 'active') {
                        $response["Result"] = 0;
                        $response["Message"] = "Your account is blocked.";

                        $input = $request->all();
                        $this->errorlog($response["Message"],json_encode($input),'verify_login');
                        return response($response, 200);
                    }

                    $response["Result"] = 1;
                    $response['Message'] = "User Verified Successfully.";
                    return response($response, 200);
                }

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function verify_login_otp(Request $request)
    {
        try {
            if ($request['version'] != $this->version && $request['version'] != $this->version1) {
                $response["Result"] = 10;  // if ios then redirect to app store and android redirect to playstore
                $response["Message"] = "Please download latest application!";
                return response($response, 200);
            }

            $validator = Validator::make($request->all(), [
                    'otp' => 'required',
                    'phone' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("otp", $request['otp'])->where('phone',$request['phone'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "OTP is incorrect. Please try again.";
                return response($response, 200);
            }
            else {

                if ($appuser->status != 'active') {
                    $response["Result"] = 0;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'verify_login_otp');
                    return response($response, 200);
                }

                if (isset($request['device_type']) && isset($request['device_token'])){
                    $logins['device_type'] = $request['device_type'];
                    $logins['device_token'] = $request['device_token'];
                }

                $logins['remember_token'] = str_random(30);
                $logins['user_id'] = $appuser->id;
                $logins['login_time'] = Carbon::now();
                $logins['version_no'] = $request['version_no'];
                $logins['build_no'] = $request['build_no'];

                $appuser['otp'] = '';

                if($appuser['gender'] == 'male') {
                    $img = url('assets/dist/img/male.png');
                } else if ($appuser['gender'] == 'female') {
                    $img = url('assets/dist/img/female.png');
                } else {
                    $img = url('assets/dist/img/other.png');
                }

                if (isset($request['device_token']) && $request['device_token']!= ""){
                    Login_details::where('device_token',$request['device_token'])->delete();

                }
                Login_details::create($logins);

                $user = User::findorFail($appuser->id);
                if((isset($request['latitude']) && $request['latitude']!="") && (isset($request['longitude']) && $request['longitude']!="")){
                    /* timezone code */

                    $timezone = $this->get_timezone($request['latitude'],$request['longitude']);
                    $input['latitude'] = $request['latitude'];
                    $input['longitude'] = $request['longitude'];
                    $input['timezone'] = $timezone;
                    $user->update($input);

                    $get_remainder = Remainder::where('user_id',$appuser->id)->first();
                    if ($get_remainder){
                        $remainders['timezone'] = $timezone;
                        $get_remainder->update($remainders);
                    }
                }

                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                if($user->image == '')
                {
                    $user->image = $img;
                }

                unset($user->password);
                unset($user->deleted_at);

                if ($user->image != "" && file_exists($user['image'])) {
                    $user->image = url($user->image);
                }

                $user['token'] = $logins['remember_token'];

                $plan = 0;
                $user['plan_id'] = 0;
                if ($user['admin_membership_plan'] != null){
                    $plan = $user['admin_membership_plan'];
                }
                else{
                    $plan = $user['membership_plan'];
                }

                /* MY SALON WITH OWNER SHIP , IM SALON OWNER */
                //$flag = 0;
                if ($plan == 1 || $plan==2 || $plan==3){
                    $get_salon_owner = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',1)->get();
                    if ($get_salon_owner){
                        $salon_list = collect();
                        foreach ($get_salon_owner as $k => $v){
                            $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                            if (!empty($get_salon) && $get_salon!=""){
                                $salon_list->push($get_salon);
                                //    $flag = 1;
                            }
                        }
                    }
                }

                /* MY SALON AS EMPLOYEE */
                $get_salon_employee = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',0)->get();
                if ($get_salon_employee){
                    $salon_lists = collect();
                    foreach ($get_salon_employee as $k => $v){
                        $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                        if (!empty($get_salon) && $get_salon!=""){
                            $salon_lists->push($get_salon);
                        }
                    }
                }

                $user['plan_id'] = $plan;
                $user['salon'] = !empty($salon_list)?$salon_list:array();
                $user['my_salon'] = !empty($salon_lists)?$salon_lists:array();

//                if ($plan == 1 && $flag==0){
//                    $response["Result"] = 5;
//                    $response["User"] = $user;
//                    $response['Message'] = "Your salon is inactive!";
//                    return response($response, 200);
//                }

                $response["Result"] = 1;
                $response["User"] = $user;
                $response['Message'] = "User login Successfully.";
                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function mobile_login(Request $request)
    {
        try {

            if ($request['version'] != $this->version && $request['version'] != $this->version1) {
                $response["Result"] = 10;  // if ios then redirect to app store and android redirect to playstore
                $response["Message"] = "Please download latest application!";
                return response($response, 200);
            }

            $validator = Validator::make($request->all(), [
                    'phone' => 'required|numeric',
                    //'name'  => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("phone", $request['phone'])->withTrashed()->first();
            if($appuser['deleted_at']!=""){
                $response["Result"] = 0;
                $response["Message"] = "Your account is blocked. Contact to nailmaster support team!";
                return response($response, 200);
            }

            $otp = rand(1000, 9999);
            if (empty($appuser)) {

                $input = $request->all();

                if ($request['name']==""){
                    $input['name'] = "Guest";
                }

                $input['role'] = 'user';
                $input['otp'] = $otp;

                if (config('siteVars.siteurl')=='https://nailmaster.co/' || config('siteVars.siteurl')=='https://salonmaster.co/'){
                    if(substr($request['phone'], 0, 1) == 1) {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'no';
                        $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                    } else {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'yes';
                        $datas = ['to' => [$request['phone']], 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                    }
                    $result = $clickatell->sendMessage($datas,$other_country);
                    $response['Message'] = "OTP Sent to Registered Mobile.";
                }
                else{
                    $response['Message'] = "OTP Sent to Registered Mobile. " . $otp;
                }

                User::create($input);

                $response["Result"] = 1;

                return response($response, 200);

            }
            else {

                $appuser['otp'] = $otp;
                if ($request['name']==""){
                    $appuser['name'] = "Guest";
                }
                else{
                    $appuser['name'] = $request['name'];
                }

                if (config('siteVars.siteurl')=='https://nailmaster.co/' || config('siteVars.siteurl')=='https://salonmaster.co/'){
                    if(substr($request['phone'], 0, 1) == 1) {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'no';
                        $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                    } else {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'yes';
                        $datas = ['to' => [$request['phone']], 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                    }
                    $result = $clickatell->sendMessage($datas,$other_country);
                    $response['Message'] = "OTP Sent to Registered Mobile.";
                }
                else{
                    $response['Message'] = "OTP Sent to Registered Mobile. " . $otp;
                }

                $appuser->save();

                $response["Result"] = 1;
                //$response['Message'] = "OTP Sent to Registered Mobile. " . $otp;
                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_customer(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'phone' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("phone", $request['phone'])->first();
            $otp = rand(1000, 9999);
            $input = $request->all();

            if (isset($request['salon_id'])){
                $saloons = saloon::with('Saloon_services')->with('Saloon_services.services_master')->with('work_days')->where('id',$request['salon_id'])->first();

                if($saloons['timezone'] != '') {
                    date_default_timezone_set($saloons['timezone']);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $today = strtolower($date_time->format('l'));
                $date_today = $date_time->format('Y-m-d');

                $count_images = SalonImages::where('salon_id',$saloons['id'])->count();
                $saloons['image_total'] = $count_images;
                $saloons['long_description'] = url('api/long_description/'.$saloons['id']);
                $status = 'close';
                $saloons['wait_time'] = 0;
                $saloons['act_emp'] = 0;
                $saloons['is_favourite'] = 0;
                $map_pin = 'close.png';
                $saloons['saloon_phone'] = trim($saloons['saloon_phone']);
                $saloons['date_time'] = Carbon::parse($saloons['created_at'])->format('m/d/Y h:i a');
                // $saloons[$key]['c_time'] = $date_time;
                $saloons['is_favourite'] = 1;

                $open_time  = '';
                $close_time = '';

                if(file_exists($saloons['image'])) {
                    $saloons['image'] = url($saloons['image']);
                } else {
                    $saloons['image'] = '';
                }

                if($saloons['is_online'] == 1) {

                    if (count($saloons['work_days'])) {

                        foreach ($saloons['work_days'] as $wd) {
                            if ($wd['work_day'] == $today) {
                                $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                $status = 'open';
                                break;
                            }
                        }
                    }
                }

                if (count($saloons['Saloon_services'])) {

                    foreach ($saloons['Saloon_services'] as $k1 => $v1){
                        $discount_price = ($v1['charges'] * $v1['discount']) / 100;
                        $final_price  = $v1['charges'] - $discount_price;
                        $saloons['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                        $saloons['Saloon_services'][$k1]['service_name'] =  $v1['services_master']['title'];
                    }

                    $sal_services = $saloons['Saloon_services'];
                    unset($saloons->Saloon_services);
//
                    $salon_services =  $sal_services->sortBy('service_name')->toArray();
                    $saloons['saloon_services'] = array_values($salon_services);

                    // array_walk_recursive($saloons['saloon_services'], function (&$item, $key) {
                    //     $item = null === $item ? '' : $item;
                    // });
                }

                // array_walk_recursive($saloons['Saloon_services'], function (&$item, $key) {
                //     $item = null === $item ? '' : $item;
                // });


                if ($status == 'open') {

                    if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                        $status = 'open';
                    } else {
                        $status = 'close';
                    }
                }

                $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $saloons['id'])->get();

                if (count($employees)) {

                    foreach ($employees as &$emp) {

                        if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                            $emp['nick_name'] = $emp['first_name'];
                        }

                        if($emp['status'] == 'active' && $emp['available'] == 'present') {
                            $saloons['act_emp'] += 1;
                        }

                        if(file_exists($emp['image'])) {
                            $emp['image'] = url($emp['image']);
                        } else {
                            $emp['image'] = '';
                        }

                    }
                }

                if ($status == 'open' && $saloons['act_emp'] == 0) {
                    $saloons['act_emp'] = 1;
                }

                $employees->prepend(['id' => 0, 'saloon_id' => 2, 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '']);

                $saloons['employees'] = $employees;

                $saloons['status'] = $status;

                if($status == 'open') {

                    $bookings = Booking::with('Booking_details')->where('saloon_id', $saloons['id'])->where('date', $date_today)->whereNull('end_time')->get();

                    if(! empty($bookings)) {

                        foreach($bookings as $book_key => $book_val) {

                            foreach($book_val['Booking_details'] as $book_details) {
                                $saloons['wait_time'] += ($book_details['duration'] * $book_details['no_of_persons']);
                            }
                        }
                    }

                    $prev = '';
                    $timer_pin = '';

                    foreach(saloon::$duration_pin as $time => $pin) {

                        if($saloons['wait_time'] <= $time) {
                            $timer_pin = $pin;
                            break;
                        } else if($saloons['wait_time'] < $time){
                            $timer_pin = $prev;
                            break;
                        }
                        $prev = $pin;
                    }

                    if($timer_pin == '' && $saloons['wait_time'] >= end(saloon::$duration_pin)) {
                        $timer_pin = end(saloon::$duration_pin);
                    }

                    if($saloons['global_discount'] == 1) {
                        $timer_pin = $timer_pin . '_discount';
                    }

                    $map_pin = $timer_pin.'.png';
                }

                $saloons['map_pin'] = $map_pin;

                array_walk_recursive($saloons, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $response['Salon'] = $saloons;

            if (isset($request['offline_walkin']) && $request['offline_walkin']==1){
                if (!empty($appuser)) {

                    array_walk_recursive($appuser, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    $response["Result"] = 1;
                    $response['User'] = $appuser;
                    $response['Message'] = "Success";
                    return response($response, 200);
                }
                else{
                    $input['role'] = 'user';
                    $get_user  = User::create($input);

                    $appuser = User::where("id", $get_user['id'])->first();

                    array_walk_recursive($appuser, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    $response["Result"] = 1;
                    $response['User'] = $appuser;
                    $response['Message'] = "Success";
                    return response($response, 200);
                }
            }

            if (empty($appuser)) {

                $input['role'] = 'user';
                $input['otp'] = $otp;

                if(substr($request['phone'], 0, 1) == 1) {
                    $clickatell = new \Clickatell\Rest();
                    $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                } else {
                    $clickatell = new \Clickatell\Rest('india');
                    $datas = ['to' => [$request['phone']], 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                }

                $result = $clickatell->sendMessage($datas);

                User::create($input);

                $response["Result"] = 1;
                $response['Message'] = "OTP Sent to Mobile. " . $otp;
                return response($response, 200);

            }
            else {

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'add_customer');
                    return response($response, 200);
                }

                $appuser['otp'] = $otp;

                if(substr($request['phone'], 0, 1) == 1) {
                    $clickatell = new \Clickatell\Rest();
                    $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                } else {
                    $clickatell = new \Clickatell\Rest('india');
                    $datas = ['to' => [$request['phone']], 'content' => config('siteVars.title').' Login Verification code ' . $otp];
                }

                $result = $clickatell->sendMessage($datas);
                $appuser->save();
                $response["Result"] = 1;
                $response['Message'] = "OTP Sent to Registered Mobile. " . $otp;
                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function facebook_login(Request $request)
    {
        try {
            if ($request['version'] != $this->version && $request['version'] != $this->version1) {
                $response["Result"] = 10;  // if ios then redirect to app store and android redirect to playstore
                $response["Message"] = "Please download latest application!";
                return response($response, 200);
            }

            $validator = Validator::make($request->all(), [
                    'facebookid' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where('email', $request['email'])
                ->orwhere(function ($query) use ($request) {
                    $query->where('is_facebook', '1')
                        ->where("facebookid", $request['facebookid']);
                })->withTrashed()->first();

            if($appuser['deleted_at']!=""){
                $response["Result"] = 0;
                $response["Message"] = "Your account is blocked. Contact to nailmaster support team!";
                return response($response, 200);
            }

            /* --------------- LOGIN CODE ------------------- */
            if (!empty($appuser) && $appuser != "" ) {

                if ($appuser->status != 'active') {
                    $response["Result"] = 0;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'facebook_login');
                    return response($response, 200);
                }

                if (isset($request['device_type']) && isset($request['device_token'])){
                    $logins['device_type'] = $request['device_type'];
                    $logins['device_token'] = $request['device_token'];
                }

                $logins['remember_token'] = str_random(30);
                $logins['user_id'] = $appuser->id;
                $logins['login_time'] = Carbon::now();
                $logins['version_no'] = $request['version_no'];
                $logins['build_no'] = $request['build_no'];

                if (isset($request['device_token']) && $request['device_token']!= ""){
                    Login_details::where('device_token',$request['device_token'])->delete();
                }

                Login_details::create($logins);

                $appuser['name'] = $request['name'];
                if (isset($request['image']) && $request['image'] !=""){
                    $appuser['image'] = $request['image'];
                }

                if((isset($request['latitude']) && $request['latitude']!="") && (isset($request['longitude']) && $request['longitude']!="")){
                    /* timezone code */
                    $timezone = $this->get_timezone($request['latitude'],$request['longitude']);
                    $appuser['latitude'] = $request['latitude'];
                    $appuser['longitude'] = $request['longitude'];
                    $appuser['timezone'] = $timezone;
                    //$user->update($input);

                    $get_remainder = Remainder::where('user_id',$appuser->id)->first();
                    if ($get_remainder){
                        $remainders['timezone'] = $timezone;
                        $get_remainder->update($remainders);
                    }
                }

                $appuser->save();

                if (!isset($request['image']) && $request['image'] ==""){
                    if($appuser['gender'] == 'male') {
                        $img = url('assets/dist/img/male.png');
                    } else if ($appuser['gender'] == 'female') {
                        $img = url('assets/dist/img/female.png');
                    } else {
                        $img = url('assets/dist/img/other.png');
                    }


                    if($appuser->image == '')
                    {
                        $appuser->image = $img;
                    }
                }

                $appuser['facebookid'] = $request['facebookid'];
                $appuser['is_facebook'] = 1;

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                unset($appuser->password);
                unset($appuser->deleted_at);

                if ($appuser->image != "" && file_exists($appuser['image'])) {
                    $appuser->image = url($appuser->image);
                }

                $plan = 0;
                $user['plan_id'] = 0;
                if ($appuser['admin_membership_plan'] != null){
                    $plan = $appuser['admin_membership_plan'];
                }
                else{
                    $plan = $appuser['membership_plan'];
                }

                /* MY SALON WITH OWNER SHIP , IM SALON OWNER */
                //$flag = 0;
                if ($plan == 1 || $plan==2 || $plan==3){
                    $get_salon_owner = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',1)->get();
                    if ($get_salon_owner){
                        $salon_list = collect();
                        foreach ($get_salon_owner as $k => $v){
                            $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                            if (!empty($get_salon) && $get_salon!=""){
                                $salon_list->push($get_salon);
                                //$flag = 1;
                            }
                        }
                    }
                }

                /* MY SALON AS EMPLOYEE */

                $get_salon_employee = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner',0)->get();
                if ($get_salon_employee){
                    $salon_lists = collect();
                    foreach ($get_salon_employee as $k => $v){
                        $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                        if (!empty($get_salon) && $get_salon!=""){
                            $salon_lists->push($get_salon);
                        }
                    }
                }

                $appuser['plan_id'] = $plan;
                $appuser['salon'] = !empty($salon_list)?$salon_list:array();
                $appuser['my_salon'] = !empty($salon_lists)?$salon_lists:array();

                $appuser['token'] = $logins['remember_token'];

                $responce["Result"] = 1;
                $responce["User"] = $appuser;
                $responce['Message'] = "Login Successfully.";
                return response($responce, 200);
            }
            /* --------------- REGISTER CODE ------------------- */
            else {

                $user = User::where("email", $request['email'])->first();

                if(!empty($appuser123) && $appuser123 != ""){

                    $input = $request->all();
                    $input['facebookid'] = $request['facebookid'];
                    $input['is_facebook'] = 1;
                    //$input['role'] = "user";
                    if (isset($request['image']) || $request['image'] !=""){
                        $input['image'] = $request['image'];
                    }
                    $user->update($input);
                }
                else{
                    $input = $request->all();
                    $input['facebookid'] = $request['facebookid'];
                    $input['is_facebook'] = 1;
                    $input['role'] = "user";
                    if (isset($request['image']) || $request['image'] !=""){
                        $input['image'] = $request['image'];
                    }
                    $user = User::create($input);
                }


                $logins['remember_token'] = str_random(30);
                $logins['user_id'] = $user->id;
                $logins['login_time'] = Carbon::now();
                $logins['version_no'] = $request['version_no'];
                $logins['build_no'] = $request['build_no'];
                Login_details::create($logins);


                $user = User::findorFail($user->id);

                if((isset($request['latitude']) && $request['latitude']!="") && (isset($request['longitude']) && $request['longitude']!="")){
                    /* timezone code */
                    $latitude = $request['latitude'];
                    $longitude = $request['longitude'];
                    $time = time();
                    $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$latitude,$longitude&timestamp=$time&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $responseJson = curl_exec($ch);
                    curl_close($ch);

                    $response1 = json_decode($responseJson);

                    $input['latitude'] = $request['latitude'];
                    $input['longitude'] = $request['longitude'];
                    $input['timezone'] = $response1->timeZoneId;
                    $user->update($input);
                }

                if (!isset($request['image']) || $request['image'] ==""){
                    if($user['gender'] == 'male') {
                        $img = url('assets/dist/img/male.png');
                    } else if ($user['gender'] == 'female') {
                        $img = url('assets/dist/img/female.png');
                    } else {
                        $img = url('assets/dist/img/other.png');
                    }

                    if($user->image == '')
                    {
                        $user->image = $img;
                    }

                    if ($user->image != "" && file_exists($user['image'])) {
                        $user->image = url($user->image);
                    }
                }


                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });


                unset($user->password);
                unset($user->deleted_at);

                if ($user->image != "" && file_exists($user['image'])) {
                    $user->image = url($user->image);
                }

                $user['plan_id'] = "0";
                $user['salon'] = [];
                $user['token'] = $logins['remember_token'];
                $response["Result"] = 1;
                $response["User"] = $user;
                $response['Message'] = "User Registered Successfully.";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function do_payment(Request $request)   // THIS API IS NOT IN USE //
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                //'amount' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'do_payment');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'do_payment');

                    return response($response, 200);
                }

                if ($appuser['email']!=""){
                    $input1['is_email_verified'] = 1;
                }

                $appuser->update($input1);

                $img = '';
                if($appuser['image'] == '')
                {
                    $appuser['image'] = $img;
                }

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                unset($appuser['deleted_at']);
                unset($appuser['password']);

                $appuser['token'] = $request['remember_token'];

                $appuser['is_salon_assign'] = 0;


                /*$input2['user_id'] = $appuser['id'];
                $input2['amount'] = $request['amount'];
                $input2['payment_status'] = "completed";
                $input2['payment_method'] = "paypal";
                $payment = Payment_history::create($input2);*/


                /* Email to user */

                if ($appuser['email'] != "") {

                    $em = $appuser['email'];
                    $bcc = [];
                    $register_email = Email::findorFail(7);
                    $subject = $register_email['subject'];
                    $from1 = $register_email['email'];

                    $name = $appuser['name'];
                    $amount = 59.99;
                    $string = ["{name}", "{amount}"];
                    $replace_string = [$name, $amount];

                    $data['content'] = str_replace($string, $replace_string, $register_email["content"]);
                    $data['title'] = $register_email['title'];

                    Mail::send('admin.email_template', $data, function ($m) use ($from1, $subject, $em, $bcc) {
                        $m->from($from1, config('siteVars.title'));
                        $m->bcc($bcc);
                        $m->to($em)->subject($subject);
                    });
                }

                $response["Result"] = 1;
                $response['User'] = $appuser;
                $response['Message'] = "Success";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function register(Request $request)
    {
        try {

            if ($request['version'] != $this->version && $request['version'] != $this->version1) {
                $response["Result"] = 10;  // if ios then redirect to app store and android redirect to playstore
                $response["Message"] = "Please download latest application!";
                return response($response, 200);
            }

            $validator = Validator::make($request->all(), [
                    'name'  => 'required',
                    'email' => 'required|email|unique:users,email',
                    'phone' => 'unique:users,phone',
                    'password' => 'required|confirmed|min:6',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }
            else {

                $input = $request->all();
                $input['role'] = 'user';

                $logins['remember_token'] = str_random(30);
                $input['remember_token'] = $logins['remember_token'];

                $img = url('assets/dist/img/other.png');

                if ($photo = $request->file('image')) {
                    $input['image'] = url($this->image($photo, 'user'));
                }

                if (isset($input['birthdate']) && $input['birthdate'] != "") {
                    $input['birthdate'] = Carbon::parse($input['birthdate'])->format('Y-m-d');
                }

                $appuser = User::create($input);

                $logins['user_id'] = $appuser->id;
                $logins['login_time'] = Carbon::now();
                $logins['version_no'] = $request['version_no'];
                $logins['build_no'] = $request['build_no'];


                Login_details::create($logins);

                $user = User::findorFail($appuser->id);

                if((isset($request['latitude']) && $request['latitude']!="") && (isset($request['longitude']) && $request['longitude']!="")){
                    /* timezone code */
                    $timezone = $this->get_timezone($request['latitude'],$request['longitude']);
                    $input['latitude'] = $request['latitude'];
                    $input['longitude'] = $request['longitude'];
                    $input['timezone'] = $timezone;
                    $user->update($input);

                    $get_remainder = Remainder::where('user_id',$appuser->id)->first();
                    if ($get_remainder){
                        $remainders['timezone'] = $timezone;
                        $get_remainder->update($remainders);
                    }
                }

                if ($user->image == '') {
                    $user->image = $img;
                }

                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                unset($user->deleted_at);
                unset($user->password);

                $user['token'] = $logins['remember_token'];
                if ($user->birthdate != null){
                    $user->birthdate = date('m/d/y', strtotime($user->birthdate));
                }
                $em= $request['email'];
                $bcc = [];
                $register_email=Email::findorFail(2);
                $name = $appuser->name;
                $link = config('siteVars.siteurl')."register_confirmation_email?ids=".$logins['remember_token']."";
                $subject = $register_email['subject'];
                $from1 = $register_email['email'];

                $string = ["{name}", "{link}"];
                $replace_string = [$name, $link];

                $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                $data['title'] = $register_email['title'];

                if ($request['is_business']==1){
                    $input['user_id'] = $appuser->id;
                    Salon_owner_request::create($input);
                }

                Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                    $m->from($from1, config('siteVars.title'));
                    $m->bcc($bcc);
                    $m->to($em)->subject($subject);
                });

                $response["Result"] = 1;
                $response["User"] = $user;
                $response['Message'] = "User Registered Successfully.";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function dashboard_user(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'dashboard_user');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'dashboard');
                    return response($response, 200);
                }



                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                unset($appuser->password);
                unset($appuser->deleted_at);

                if ($appuser['gender'] == 'male') {
                    $img = url('assets/dist/img/male.png');
                } else if ($appuser['gender'] == 'female') {
                    $img = url('assets/dist/img/female.png');
                } else {
                    $img = url('assets/dist/img/other.png');
                }

                if ($appuser->image == '') {
                    $appuser->image = $img;
                } else {
                    $appuser->image = url($appuser->image);
                }

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $appuser->countryName = '';
                $appuser->stateName = '';
                $appuser->cityName = '';

                if (!empty($appuser->country)) {
                    $country = countries::where('countryID', $appuser->country)->first();
                    $appuser->countryName = $country->countryName;
                }

                if ($appuser->state_id > 0) {
                    $state = states::where('stateID', $appuser->state_id)->first();
                    $appuser->stateName = $state->stateName;
                }

                if ($appuser->city_id > 0) {
                    $state = cities::where('cityID', $appuser->city_id)->first();
                    $appuser->cityName = $state->cityName;
                }


                $accept_request_counter_qry = saloon_employees::where('user_id',$appuser->id)->where('is_salon_owner','0')->where('is_read','0')->where('status','accept')->count();
                /* CHECK SALON ACTIVE OR EXIST */
                $salon = saloon::where('id',$appuser->saloon_id)->first();
                if($salon){
                    if ($salon['status']=="in-active"){
                        $appuser->saloon_id = 0;
                        $accept_request_counter = 0;
                    }
                    else{
                        $accept_request_counter = $accept_request_counter_qry;
                    }
                }
                else{
                    $accept_request_counter = $accept_request_counter_qry;
                }

                /* UNREAD MESSAGE*/

                $message = Message::where('receiver', $appuser->id)->where('is_read', 0)->count();
                //$w_count = Booking::where('user_id', $appuser->id)->where('status', 'waiting')->count();
                $w_count = Booking::where('user_id', $appuser->id)
                    ->where('date', '>=', $date_today)
                    ->where('status', 'waiting')->orWhereNull('status')->count();


                $pending_request_counter = saloon_employees::where('user_id',$appuser->id)->where('status','pending')->count();

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response["Unread_message"] = $message;
                $response['booking_count'] = $w_count;
                $response['emp_pending_request'] = $pending_request_counter;
                $response['emp_accept_request'] = $accept_request_counter;

                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_profile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_profile');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                if(isset($request['user_id']) && $request['user_id']!=""){
                    $appuser = User::findorFail($request['user_id']);
                }
                else{
                    $appuser = User::findorFail($loginUser->user_id);
                }
                //$appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_profile');

                    return response($response, 200);
                }

                unset($appuser->password);
                unset($appuser->deleted_at);

                if($appuser['gender'] == 'male') {
                    $img = url('assets/dist/img/male.png');
                } else if ($appuser['gender'] == 'female') {
                    $img = url('assets/dist/img/female.png');
                } else {
                    $img = url('assets/dist/img/other.png');
                }

                if($appuser->image == '') {
                    $appuser->image = $img;
                } else {
                    $appuser->image = url($appuser->image);
                }

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $appuser->countryName = '';
                $appuser->stateName = '';
                $appuser->cityName = '';

                if(! empty($appuser->country)) {
                    $country = countries::where('countryID', $appuser->country)->first();
                    $appuser->countryName = $country->countryName;
                }

                if($appuser->state_id > 0) {
                    $state = states::where('stateID', $appuser->state_id)->first();
                    $appuser->stateName = $state->stateName;
                }

                if($appuser->city_id > 0) {
                    $state = cities::where('cityID', $appuser->city_id)->first();
                    $appuser->cityName = $state->cityName;
                }

                if ($appuser->birthdate != null){
                    $appuser->birthdate = date('m/d/y',strtotime($appuser->birthdate));
                }

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'logout');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'logout');

                    return response($response, 200);
                }

                $loginUser->delete();

                $response["Result"] = 1;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function profile_update(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                //'name' => 'required',
                //'email' => 'required|email',
                //'phone' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'profile_update');

                return response($response, 200);
            }

            if (!empty($loginUser)) {


                if (isset($request['device_type']) && isset($request['device_token'])){
                    $update_device_token['device_type'] = $request['device_type'];
                    $update_device_token['device_token'] = $request['device_token'];
                    $loginUser->update($update_device_token);
                }

                if(isset($request['user_id']) && $request['user_id']!=""){
                    $appuser = User::findorFail($request['user_id']);
                }
                else{
                    $appuser = User::findorFail($loginUser->user_id);
                }


                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'profile_update');

                    return response($response, 200);
                }

                if (isset($request['email']) && $request['email'] !="") {
                    $otherUser = User::where("email", $request['email'])->where("id", '!=', $appuser->id)->first();
                    if (!empty($otherUser)) {
                        $response["Result"] = 0;
                        $response["Message"] = "Email address already registered with other user account.";
                        return response($response, 401);
                    }
                }

                $input = $request->all();

                if($photo = $request->file('image'))
                {
                    $input['image'] = $this->image($photo,'user');
                }

                if (isset($input['birthdate']) && $input['birthdate']!="") {
                    $input['birthdate'] = Carbon::parse($input['birthdate'])->format('Y-m-d');
                }

                if((isset($input['latitude']) && $input['latitude']!="") && (isset($input['longitude']) && $input['longitude']!="")){
                    /* timezone code */
                    $timezone = $this->get_timezone($request['latitude'],$request['longitude']);
                    $input['timezone'] = $timezone;

                    /* UPDATE SALON LATITUDE LONGITUDE IF FREELANCER USER IS SALON OWNER */
                    if ($appuser['membership_plan']==2 || $appuser['admin_membership_plan']==2){
                        $salon = saloon::where('id',$appuser['saloon_id'])->first();
                        $fields['latitude'] = $input['latitude'];
                        $fields['longitude'] = $input['longitude'];
                        $fields['timezone'] = $timezone;

                        $salon->update($fields);
                    }
                }

                $appuser->update($input);
                $appuser = User::findorfail($appuser['id']);

                $img = '';
                if (isset($input['gender'])){
                    if($input['gender'] == 'male') {
                        $img = url('assets/dist/img/male.png');
                    } else if ($input['gender'] == 'female') {
                        $img = url('assets/dist/img/female.png');
                    } else {
                        $img = url('assets/dist/img/other.png');
                    }
                }

                if ($appuser->birthdate != null){
                    $appuser->birthdate = date('m/d/y',strtotime($appuser->birthdate));
                }

                if($appuser->image == '') {
                    $appuser->image = $img;
                } else {
                    $appuser->image = url($appuser->image);
                }

                unset($appuser->password);
                unset($appuser->deleted_at);

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response['Message'] = "User Profile Updated Successfully";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function change_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                // 'old_password' => 'required',
                'new_password' => 'required',
                'cnf_password' => 'required',

            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }


            if ($request['new_password'] != $request['cnf_password']) {
                $response["Result"] = 0;
                $response["Message"] = "New Password and confirm password mismatch.";
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'change_password');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'change_password');

                    return response($response, 200);
                }


                $input['password'] = $request['new_password'];
                $appuser->update($input);

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response['Message'] = "User Password Updated Successfully";
                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function forgot_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("email", $request['email'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Invalid email address.";
                return response($response, 200);
            }
            else {
                $pass=str_random(8);
                $appuser->password=$pass;
                $appuser->save();

                $em= $request['email'];
                $bcc = [];

                $forget_email=Email::findorFail(1);
                $name = $appuser->name;
                $password=$pass;
                $subject = $forget_email['subject'];
                $from1 = $forget_email['email'];

                $string = ["{name}", "{password}"];
                $replace_string = [$name, $password];

                $data['content'] = str_replace($string,$replace_string,$forget_email["content"]);
                $data['title'] = $forget_email['title'];

                Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em, $bcc) {
                    $m->from($from1, config('siteVars.title'));
                    $m->bcc($bcc);
                    $m->to($em)->subject($subject);
                });


                $response["Result"]=1;
                $response["Message"] = "A new password has been sent to your registered email address.";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function check_token(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 9;
                $response["Message"] = "Invalid login";
            }
            else{


                $user = User::findorFail($loginUser['user_id']);

                if($user['gender'] == 'male') {
                    $img = url('assets/dist/img/male.png');
                } else if ($user['gender'] == 'female') {
                    $img = url('assets/dist/img/female.png');
                } else {
                    $img = url('assets/dist/img/other.png');
                }
                if($user->image == '')
                {
                    $user->image = $img;
                }

                unset($user->password);
                unset($user->deleted_at);

                if ($user->image != "" && file_exists($user['image'])) {
                    $user->image = url($user->image);
                }

                $user['token'] = $request['remember_token'];

//                $user['is_salon_assign'] = 0;
//                if ($user['role']=='sub_admin'){
//                    $get_salon = saloon::where('user_id',$loginUser['user_id'])->where('status','active')->first();
//                    if (!empty($get_salon) && $get_salon!=""){
//                        $user['is_salon_assign'] = 1;
//                        $user['reg_step'] = $get_salon->reg_step;
//                    }
//                }

                $plan = 0;
                $user['plan_id'] = 0;
                if ($user['admin_membership_plan'] != null){
                    $plan = $user['admin_membership_plan'];
                }
                else{
                    $plan = $user['membership_plan'];
                }

                /* MY SALON WITH OWNER SHIP , IM SALON OWNER */
                if ($plan == 1 || $plan==2 || $plan==3){
                    $get_salon_owner = saloon_employees::where('user_id',$user->id)->where('is_salon_owner',1)->get();
                    if ($get_salon_owner){
                        $salon_list = collect();
                        foreach ($get_salon_owner as $k => $v){
                            $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                            if (!empty($get_salon) && $get_salon!=""){
                                $salon_list->push($get_salon);
                            }
                        }
                    }
                }

                /* MY SALON AS EMPLOYEE */

                $get_salon_employee = saloon_employees::where('user_id',$user->id)->where('is_salon_owner',0)->get();
                if ($get_salon_employee){
                    $salon_lists = collect();
                    foreach ($get_salon_employee as $k => $v){
                        $get_salon = saloon::where('id',$v['saloon_id'])->where('status','active')->first();
                        if (!empty($get_salon) && $get_salon!=""){
                            $salon_lists->push($get_salon);
                        }
                    }
                }

                $user['plan_id'] = $plan;
                $user['salon'] = !empty($salon_list)?$salon_list:array();
                $user['my_salon'] = !empty($salon_lists)?$salon_lists:array();

                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response["User"] = $user;
                $response["Message"] = "Success";
            }

            return response($response, 200);
        }
        catch (Exception $e) {
            return response("", 500);
        }
    }

    public function search_salons(Request $request){

        try {
            $validator = Validator::make($request->all(), [
                'page' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $skip = $request['page'] * 10;
            $skip_next = 10 * ($request['page'] + 1);
            $take = 10;

            if (isset($request['keyword']) && $request['keyword']!=""){
                $saloons = saloon::select('id','user_id','title','location')->where('title', 'like',$request['keyword'].'%')
                    ->orwhere('location', 'like',$request['keyword'].'%')
                    ->orderBy('displayorder', 'asc')->skip($skip)->take($take)->get();

                $saloon_list_next = saloon::where('title', 'like', $request['keyword'].'%')
                    ->orwhere('location', 'like', $request['keyword'].'%')
                    ->orderBy('displayorder', 'asc')->skip($skip_next)->take($take)->get()->count();

                if (!empty($saloon_list_next)) {
                    $has_next_page = 1;
                } else {
                    $has_next_page = 0;
                }

                $response["Result"] = 1;
                $response["has_next_page"] = $has_next_page;
                $response["saloons"] = $saloons;
                $response['Message'] = "Success";
                return response($response, 200);
            }
            else{
                $response["Result"] = 0;
                $response["saloons"] = array();
                $response["has_next_page"] = 0;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        }
        catch (Exception $e) {
            return response("", 500);
        }

    }

    public function get_saloons(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'latitude' => 'required',
                'longitude' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            // date_default_timezone_set('America/Chicago');

            $favourites = array();

//          if($request['remember_token'] != '' && $request['salon_id'] != '') {
            $login_user_id = "";
            if($request['remember_token'] != '') {

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (!empty($loginUser)) {

                    $appuser = User::where('id',$loginUser->user_id)->first();
                    if (empty($appuser)){
                        $response["Result"] = 9;
                        $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                        $input = $request->all();
                        $this->errorlog($response["Message"],json_encode($input),'get_saloons');

                        return response($response, 200);
                    }

                    if ($appuser['latitude']=="" && $appuser['longitude']=="") {
                        if ((isset($request['latitude']) && $request['latitude'] != "") && (isset($request['longitude']) && $request['longitude'] != "")) {
                            /* timezone code */
                            $timezone = $this->get_timezone($request['latitude'], $request['longitude']);

                            $input['latitude'] = $request['latitude'];
                            $input['longitude'] = $request['longitude'];
                            $input['timezone'] = $timezone;
                            $appuser->update($input);

                            $get_remainder = Remainder::where('user_id', $appuser->id)->first();
                            if ($get_remainder) {
                                $remainders['timezone'] = $timezone;
                                $get_remainder->update($remainders);
                            }
                        }
                    }
                    $favourites = Favourites::where('user_id', $appuser->id)->pluck('saloon_id')->toArray();

                    $date_time = Carbon::now();
                    $date_today = $date_time->format('Y-m-d');
                    $message = Message::where('receiver', $appuser->id)->where('is_read', 0)->count();
                    //$w_count = Booking::where('user_id', $appuser->id)->where('status', 'waiting')->count();
                    $w_count = Booking::where('user_id', $appuser->id)
                        ->where('date', '>=', $date_today)
                        ->where('status', 'waiting')->orWhereNull('status')->count();

                    /* SALOON OWNER LOGIN */

                    $saloon = saloon::where('id', $request['id'])->first();
                    $w1_count = 0;
                    $r1_count = 0;
                    if ($saloon){
                        $sid = $saloon->id;
                        $w1_count = Booking::where('saloon_id', $sid)->where('date', $date_today)->where('status', 'waiting')->count();
                        $r1_count = Booking::where('saloon_id', $sid)->where('date', '>', $date_today)->where('status', 'waiting')->count();
                        $response['w_count'] = $w1_count;
                        $response['r_count'] = $r1_count;
                    }
                    else{
                        $response['w_count'] = 0;
                        $response['r_count'] = 0;
                    }

                    $response['w_count'] = $w1_count;
                    $response['r_count'] = $r1_count;
                    $response["Unread_message"] = $message;
                    $response['booking_count'] = $w_count;

                    $login_user_id = $loginUser->user_id;

                }
            }

            $query = saloon::select('saloons.*')
                ->with('Saloon_services')
                ->with('Saloon_services.services_master')
                ->with('work_days')
                ->where('status','active')
                ->select();

            if (isset($request['category_id']) && $request['category_id'] !=''){   // GET SALON LISTING BY CATEGORY
                $salon_ids = Saloon_services::select('saloon_services.*')
                    ->join('service_types', 'service_types.id', '=', 'saloon_services.type_id')
                    ->where('service_types.category_id',$request['category_id'])
                    ->groupBy('saloon_services.saloon_id')
                    ->pluck('saloon_services.saloon_id')->all();
                $query->whereIn('id', $salon_ids);
            }

            if (isset($request['sub_category_id']) && $request['sub_category_id'] !=''){   // GET SALON LISTING BY SUB CATEGORY
                $salon_ids = Saloon_services::select('saloon_services.*')
                    ->join('service_types', 'service_types.id', '=', 'saloon_services.type_id')
                    ->where('service_types.sub_category_id',$request['sub_category_id'])
                    ->groupBy('saloon_services.saloon_id')
                    ->pluck('saloon_services.saloon_id')->all();
                $query->whereIn('id', $salon_ids);
            }

            if (isset($request['service_id']) && $request['service_id'] !=''){   // GET SALON LISTING BY SERVICE
                $salon_ids = Saloon_services::where('type_id',$request['service_id'])
                    ->groupBy('saloon_id')
                    ->pluck('saloon_id')->all();
                $query->whereIn('id', $salon_ids);
            }

            $saloons = $query->where('reg_step',4)->selectRaw('3959 * 2 * ASIN(SQRT( POWER(SIN((' . $request['latitude'] . ' - latitude)*pi()/180/2),2)+COS(' . $request['latitude'] . '*pi()/180 )*COS(latitude*pi()/180)
                  *POWER(SIN((' . $request['longitude'] . '-longitude)*pi()/180/2),2)))  AS distance')->orderBy('displayorder','asc')->HAVING('distance','<=','50')->take(50)->get();


            foreach($saloons as $key => $val) {

                $get_salon_owner = saloon_employees::where('saloon_id',$val['id'])->where('is_salon_owner',1)->count();
                if ($get_salon_owner==0){
                    unset($saloons[$key]);
                    continue;
                }

                /* SET OWNER */

                $salon_owner = saloon_employees::where('saloon_id',$val['id'])->where('is_salon_owner',1)->where('status','accept')->first();
                if($salon_owner){
                    $saloons[$key]['user_id'] = $salon_owner['user_id'];
                }

                $count_images = SalonImages::where('salon_id',$val['id'])->count();
                $saloons[$key]['image_total'] = $count_images;

                if($val['timezone'] != '') {
                    date_default_timezone_set($val['timezone']);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $today = strtolower($date_time->format('l'));
                $date_today = $date_time->format('Y-m-d');

                $status = 'close';
                $saloons[$key]['long_description'] = url('api/long_description/'.$val['id']);
                $saloons[$key]['wait_time'] = 0;
                $saloons[$key]['act_emp'] = 0;
                $saloons[$key]['is_favourite'] = 0;
                $map_pin = 'close.png';
                $saloons[$key]['saloon_phone'] = trim($val['saloon_phone']);
                $saloons[$key]['date_time'] = Carbon::parse($val['created_at'])->format('m/d/Y h:i a');
                // $saloons[$key]['c_time'] = $date_time;

                if(in_array($val['id'], $favourites)) {
                    $saloons[$key]['is_favourite'] = 1;
                }

                // $open_time  = Carbon::createFromTimeString($val['start_hour']);
                // $close_time = Carbon::createFromTimeString($val['end_hour']);

                $open_time  = '';
                $close_time = '';

                // $saloons[$key]['op_time'] = $open_time;
                // $saloons[$key]['cl_time'] = $close_time;

                if(file_exists($val['image'])) {
                    $saloons[$key]['image'] = url($val['image']);
                } else {
                    $saloons[$key]['image'] = '';
                }

                if($saloons[$key]['is_online'] == 1) {

                    if (count($val['work_days'])) {

                        foreach ($val['work_days'] as $wd) {
                            if ($wd['work_day'] == $today) {
                                $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                $status = 'open';
                                break;
                            }
                        }

                    }
                }

                if (count($val['Saloon_services'])) {

                    foreach ($val['Saloon_services'] as $k1=> $v1){
                        $discount_price = floatval(($v1['charges'] * $v1['discount']) / 100);
                        $final_price  = $v1['charges'] - $discount_price;
                        $val['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                        $val['Saloon_services'][$k1]['service_name'] =  $v1['services_master']['title'];
                    }

                    $sal_services = $val['Saloon_services'];
                    unset($val['Saloon_services']);

                    $salon_services =  $sal_services->sortBy('service_name')->toArray();
                    $saloons[$key]['saloon_services'] = array_values($salon_services);
                }

                if ($status == 'open') {

                    if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                        $status = 'open';
                    } else {
                        $status = 'close';
                    }

                }

                $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $val['id'])->get();

                /* GETTING BOOKING IDS FOR EMPLOYEE WAIT TIME  */
                $current_emp_booking = Booking::where('saloon_id', $val['id'])->where('date', $date_today)
                    ->Where(function ($query){
                        $query->where('status', 'waiting')
                            ->orwhere('status', 'in-progress');
                    })->pluck('id');

                /* GETTING WAIT TIME DURATION FOR NO PREFERENCE BOOKING */
                $no_pref_booking_id = Booking_details::whereIn('booking_id',$current_emp_booking)->where('emp_id',0)->pluck('booking_id');
                $no_pre_booking_count = $no_pref_booking_id->count();

                if (count($employees)) {
                    foreach ($employees as &$emp) {
                        if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                            $emp['nick_name'] = $emp['first_name'];
                        }

                        if($emp['status'] == 'active' && $emp['available'] == 'present') {
                            $saloons[$key]['act_emp'] += 1;
                        }

                        if(file_exists($emp['image'])) {
                            $emp['image'] = url($emp['image']);
                        } else {
                            $emp['image'] = '';
                        }

                        /* GETTING EMPLOYEE WAIT TIME DURATION */
                        $getemployee_wait_time = Booking_details::whereIn('booking_id',$current_emp_booking)->where('emp_id',$emp['id'])->sum('duration');
                        if ($getemployee_wait_time >=0){
                            $emp['emp_wait_time'] = $getemployee_wait_time." min";
                        }
                        else{
                            $emp['emp_wait_time'] = "0 min";
                        }

                        /* --------------------------- */

                            array_walk_recursive($emp, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }
                }

                $employees->prepend(['id' => 0, 'saloon_id' => $val['id'], 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '','emp_wait_time'=>'']);

                $saloons[$key]['employees'] = $employees;

                if ($status == 'open' && $saloons[$key]['act_emp'] == 0) {
                    $saloons[$key]['act_emp'] = 1;
                }

                $saloons[$key]['status'] = $status;

                if($status == 'open') {

                    /* ----------------------------------------- WAITING TIME CALCULATION LOGIC ---------------------------------------*/

                    $occupied = Booking::where('saloon_id', $val['id'])->where('date', $date_today)->whereIn('status', array('waiting'))->count();

                    $saloons[$key]['occ_emp'] = $occupied;

                    $date_time = Carbon::now();
                    $date_today = $date_time->format('Y-m-d');

                    $p_employees = User::where('saloon_id', $val['id'])->where('available', 'present')->count();

                    if ($p_employees == 0) {
                        $p_employees = 1;
                    }

                    $s_bookings = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $val['id'])->where('date', $date_today)->where('status', 'in-progress')->orderBy('id', 'asc')->get();

                    $time_frame = 0;
                    $buffer_time = 5; // 10 minutes buffer time.

                    $start_end = Array();

                    foreach ($s_bookings as $book_key => $book_val) {

                        $bid_duration = 0;
                        foreach ($book_val['Booking_details'] as $book_details) {
                            $time_req = ($book_details['duration'] * $book_val['no_of_persons']);
                            $bid_duration += $time_req;
                        }

                        $remaining = $date_time->diffInSeconds(Carbon::parse($book_val['start_time'])->addMinutes($bid_duration), false);

                        if ($remaining <= 0) {
                            $last_service = $book_val['Booking_details']->last();
                            $new_duration = ($last_service->duration) + ceil((abs($remaining) + ($buffer_time * 60)) / 60);
                            $remaining = ($buffer_time * 60);
                            //$last_service->update(array('duration' => $new_duration));
                        }

                        $start_end[] = $remaining;
                        $s_bookings[$book_key]["remaining"] = gmdate('H:i', $remaining);
                        $p_employees -= 1;
                    }

                    $timer = 0;
                    sort($start_end);

                    if (count($start_end)) {
                        $timer = $start_end[0];
                        $time_frame += ceil($timer / 60);
                    }

                    $w_bookings = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $val['id'])->where('date', $date_today)->where('status', 'waiting')->orderBy('id', 'asc')->get();
                    $i = 1;
                    $total_waiting_booking = $w_bookings->count();
                    $additional_service= 0;

                    /* if any employee is free then wait time is 0 */
                    if($p_employees>0){
                        $wait_time = 0;
                    }
                    else{
                        $wait_time = $time_frame;
                    }
                    /* ------------------------------------------- */

                    if ($total_waiting_booking > 0) {
                        foreach ($w_bookings as $book_key => $book_val) {
                            if ($i < $p_employees) {
                                $w_bookings[$book_key]['wait_time'] = '0';
                                $wait_time = 0;
                                $i++;

                                foreach ($book_val['Booking_details'] as $book_details) {
                                    $time_frame += ($book_details['duration'] * $book_val['no_of_persons']);
                                }
                                continue;
                            }


                            $w_bookings[$book_key]['wait_time'] += $time_frame;
                            $w_bookings[$book_key]['wait_time'] = $this->humReadable($w_bookings[$book_key]['wait_time']);

                            $additional_service = 0;
                            foreach ($book_val['Booking_details'] as $book_details) {
                                $time_frame += ($book_details['duration'] * $book_val['no_of_persons']);
                                if ($total_waiting_booking == $i) {
                                    $additional_service += ($book_details['duration'] * $book_val['no_of_persons']);
                                }
                            }

                            $wait_time = $w_bookings[$book_key]['wait_time'];
                            $i++;
                        }
                    }

                    //$w_bookings[$book_key]['wait_time'];

                    //echo $time_frame;
                    $saloons[$key]['wait_time'] = $wait_time+$additional_service;
                    $prev = '';
                    $timer_pin = '';

                    foreach(saloon::$duration_pin as $time => $pin) {

                        if($saloons[$key]['wait_time'] <= $time) {
                            $timer_pin = $pin;
                            break;
                        } else if($saloons[$key]['wait_time'] < $time){
                            $timer_pin = $prev;
                            break;
                        }
                        $prev = $pin;
                    }

                    if($timer_pin == '' && $saloons[$key]['wait_time'] >= end(saloon::$duration_pin)) {
                        $timer_pin = end(saloon::$duration_pin);
                    }

                    if($saloons[$key]['global_discount'] == 1) {
                        $timer_pin = $timer_pin . '_discount';
                    }

                    $map_pin = $timer_pin.'.png';
                }

                $saloons[$key]['map_pin'] = $map_pin;
                $saloons[$key]['booking_id'] = 0;
                $saloons[$key]['booking_status'] = "";
                if ($login_user_id!=""){
                    $login_user_id;
                    $chk_booking = Booking::where('saloon_id', $val['id'])
                        ->where('user_id',$login_user_id)
                        ->where('date', $date_today)
                        ->Where(function ($query){
                            $query->where('status', 'waiting')
                                ->orwhere('status', 'in-progress');
                        })
                        ->first();
                    if ($chk_booking){

                        $saloons[$key]['booking_id'] = $chk_booking['id'];
                        $saloons[$key]['booking_status'] = $chk_booking['status'];
                        $saloons[$key]['map_pin'] = "checkin.png";
                    }
                }

                /* -------------------------------- FREELANCER PIN ------------------------------- */
                $get_salon_owner_plan = User::select('admin_membership_plan','membership_plan')->where('id',$val['user_id'])->first();
                $user_plan = 0;
                if ($get_salon_owner_plan['admin_membership_plan'] != null){
                    $user_plan = $get_salon_owner_plan['admin_membership_plan'];
                }
                else{
                    $user_plan = $get_salon_owner_plan['membership_plan'];
                }
                if($user_plan==2){
                    $saloons[$key]['map_pin'] = "Freelancer_pin.png";
                }
                /*----------------------------------------------------------------------------------*/
                $saloons[$key]['plan_id'] = $user_plan;

                array_walk_recursive($val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $saloons = $saloons->toArray();
            $saloons = array_slice($saloons, 0);

            $response["Result"] = 1;
            $response["saloons"] = $saloons;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function humReadable($wt) {

        if($wt <= 0) return '0';

        $hours = floor($wt / 60);
        $minutes = $wt % 60;

        $wait_string = '';

        if($hours > 0) {
            $wait_string .= $hours . ' hr ';
        }

        $wait_string .= $minutes . ' min';

        return $wt;

    }

    public function get_saloon_info(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $saloons = saloon::with('Saloon_services')->with('Saloon_services.services_master')->with('work_days')->where('id',$request['salon_id'])->first();

            if($saloons['timezone'] != '') {
                date_default_timezone_set($saloons['timezone']);
            } else {
                date_default_timezone_set('America/Chicago');
            }

            $date_time = Carbon::now();
            $today = strtolower($date_time->format('l'));
            $date_today = $date_time->format('Y-m-d');

            $count_images = SalonImages::where('salon_id',$saloons['id'])->count();
            $saloons['image_total'] = $count_images;
            $saloons['long_description'] = url('api/long_description/'.$saloons['id']);
            $status = 'close';
            $saloons['wait_time'] = 0;
            $saloons['act_emp'] = 0;
            $saloons['is_favourite'] = 0;
            $map_pin = 'close.png';
            $saloons['saloon_phone'] = trim($saloons['saloon_phone']);
            $saloons['date_time'] = Carbon::parse($saloons['created_at'])->format('m/d/Y h:i a');
            // $saloons[$key]['c_time'] = $date_time;
            $saloons['is_favourite'] = 1;

            $open_time  = '';
            $close_time = '';

            if(file_exists($saloons['image'])) {
                $saloons['image'] = url($saloons['image']);
            } else {
                $saloons['image'] = '';
            }

            if($saloons['is_online'] == 1) {

                if (count($saloons['work_days'])) {

                    foreach ($saloons['work_days'] as $wd) {
                        if ($wd['work_day'] == $today) {
                            $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                            $close_time = Carbon::createFromTimeString($wd['end_hour']);
                            $status = 'open';
                            break;
                        }
                    }
                }
            }

            if (count($saloons['Saloon_services'])) {

                foreach ($saloons['Saloon_services'] as $k1=> $v1){
                    $discount_price = ($v1['charges'] * $v1['discount']) / 100;
                    $final_price  = $v1['charges'] - $discount_price;
                    $saloons['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                    $saloons['Saloon_services'][$k1]['service_name'] =  $v1['services_master']['title'];
                }

                $sal_services = $saloons['Saloon_services'];
                unset($saloons->Saloon_services);

                $salon_services =  $sal_services->sortBy('service_name')->toArray();
                $saloons['saloon_services'] = array_values($salon_services);

                // array_walk_recursive($saloons['saloon_services'], function (&$item, $key) {
                //     $item = null === $item ? '' : $item;
                // });
            }

            // array_walk_recursive($saloons['Saloon_services'], function (&$item, $key) {
            //     $item = null === $item ? '' : $item;
            // });


            if ($status == 'open') {

                if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                    $status = 'open';
                } else {
                    $status = 'close';
                }
            }

            // $employees = Employees::where('saloon_id', $saloons['id'])->get();

            $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $saloons['id'])->get();

            if (count($employees)) {

                foreach ($employees as &$emp) {

                    if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                        $emp['nick_name'] = $emp['first_name'];
                    }

                    if($emp['status'] == 'active' && $emp['available'] == 'present') {
                        $saloons['act_emp'] += 1;
                    }

                    if(file_exists($emp['image'])) {
                        $emp['image'] = url($emp['image']);
                    } else {
                        $emp['image'] = '';
                    }

                    $emp['emp_wait_time'] = "10 min";

                }
            }

            if ($status == 'open' && $saloons['act_emp'] == 0) {
                $saloons['act_emp'] = 1;
            }

            $employees->prepend(['id' => 0, 'saloon_id' => 2, 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '','emp_wait_time'=>'']);

            $saloons['employees'] = $employees;

            $saloons['status'] = $status;

            if($status == 'open') {

                $bookings = Booking::with('Booking_details')->where('saloon_id', $saloons['id'])->where('date', $date_today)->whereNull('end_time')->get();

                if(! empty($bookings)) {

                    foreach($bookings as $book_key => $book_val) {

                        foreach($book_val['Booking_details'] as $book_details) {
                            $saloons['wait_time'] += ($book_details['duration'] * $book_details['no_of_persons']);
                        }
                    }
                }

                $prev = '';
                $timer_pin = '';

                foreach(saloon::$duration_pin as $time => $pin) {

                    if($saloons['wait_time'] <= $time) {
                        $timer_pin = $pin;
                        break;
                    } else if($saloons['wait_time'] < $time){
                        $timer_pin = $prev;
                        break;
                    }
                    $prev = $pin;
                }

                if($timer_pin == '' && $saloons['wait_time'] >= end(saloon::$duration_pin)) {
                    $timer_pin = end(saloon::$duration_pin);
                }

                if($saloons['global_discount'] == 1) {
                    $timer_pin = $timer_pin . '_discount';
                }

                $map_pin = $timer_pin.'.png';
            }

            $saloons['map_pin'] = $map_pin;
            if(substr($saloons['saloon_phone'], 0, 1) == 1) {
                $str = substr($saloons['saloon_phone'], 1);
                $saloons['formated_salon_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $str);;
            }else{
                $saloons['formated_salon_phone'] = $saloons['saloon_phone'];
            }

            array_walk_recursive($saloons, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });


            /* CATEGORY LISTING */
            $category = Category::where('status','active')->orderBy('displayorder','ASC')->get();
            $salon_services = Saloon_services::where('saloon_id',$request['salon_id'])->pluck('type_id');
            $service_type = Service_type::wherein('id',$salon_services)->groupBy('category_id')->pluck('category_id')->toArray();

            foreach ($category as $val) {
                $val['is_check'] = 0;
                if (in_array($val['id'],$service_type))
                {
                    $val['is_check'] = 1;
                }

                if (file_exists($val['image']) && $val['image']!="") {
                    $val['image'] = url($val['image']);
                } else {
                    $val['image'] = '';
                }

                array_walk_recursive($val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
            /* ---------------- */

            $response["Result"] = 1;
            $response["saloons"] = $saloons;
            $response['Category'] = $category ;
            $response['Message'] = "Success";

            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_saloon_images(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $salon_images = SalonImages::where('salon_id',$request['salon_id'])->orderBy('displayorder','ASC')->get();

            foreach ($salon_images as  $img){
                //$img['image_blob'] = "";
                $img['image'] = url($img['image']);
                $img['image_thumbnail'] = url($img['image_thumbnail']);
            }

            $response["Result"] = 1;
            $response["salon_images"] = $salon_images;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_countries(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_countries');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_countries');

                    return response($response, 200);
                }

                $countries = countries::orderBy('countryName','ASC')->pluck('countryName', 'countryID')->all();

                $countries2 = countries::all();

                $response["Result"] = 1;
                $response["countries"] = $countries;
                $response["countries2"] = $countries2;

                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_states(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'country' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_states');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_states');
                    return response($response, 200);
                }

                $states = states::where('countryID', $request['country'])
                    ->orderBy('stateName', 'ASC')->pluck('stateName', 'stateID')->all();

                $states2 = states::where('countryID', $request['country'])
                    ->orderBy('stateName', 'ASC')->get();

                $response["Result"] = 1;
                $response["states"] = $states;
                $response["states2"] = $states2;
                $response['Message'] = "Success";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_cities(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'country' => 'required',
                'state' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_cities');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_cities');

                    return response($response, 200);
                }

                $cities = cities::where('countryID', $request['country'])->where('stateID', $request['state'])
                    ->orderBy('cityName', 'ASC')->pluck('cityName', 'cityID')->all();

                $cities2 = cities::where('countryID', $request['country'])->where('stateID', $request['state'])
                    ->orderBy('cityName', 'ASC')->get();



                $response["Result"] = 1;
                $response["cities"] = $cities;
                $response["cities2"] = $cities2;
                $response['Message'] = "Success";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function mark_favourite(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'saloon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'mark_favourite');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'mark_favourite');

                    return response($response, 200);
                }

                $saloon = saloon::where('id', $request['saloon_id'])->first();

                if(empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon not found.";
                    return response($response, 200);
                }

                $favourite = Favourites::where('user_id',$appuser->id)->where('saloon_id', $request['saloon_id'])->first();

                if(!empty($favourite)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon already marked as favourite.";
                    return response($response, 200);
                }

                $input = $request->all();
                $input['user_id'] = $appuser->id;

                Favourites::create($input);

                $response["Result"] = 1;
                $response['Message'] = "Salon marked as favourite";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function remove_favourite(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'saloon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'remove_favourite');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'remove_favourite');

                    return response($response, 200);
                }

                $saloon = saloon::where('id', $request['saloon_id'])->first();

                if(empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon not found.";
                    return response($response, 401);
                }

                $favourite = Favourites::where('user_id',$appuser->id)->where('saloon_id', $request['saloon_id'])->first();

                if(empty($favourite)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon not marked as favourite.";
                    return response($response, 401);
                }

                $favourite->delete();

                $response["Result"] = 1;
                $response['Message'] = "Salon removed from favourite";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function favourite_list(Request $request)
    {
        // date_default_timezone_set('America/Chicago');
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'favourite_list');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'favourite_list');

                    return response($response, 200);
                }

                $favourites = Favourites::where('user_id', $appuser->id)->pluck('saloon_id');

                if (empty($favourites)) {
                    $response["Result"] = 0;
                    $response["Message"] = 'No favourite salons';
                    return response($response, 200);
                }

                $saloons = saloon::whereIn('id', $favourites)->with('Saloon_services')->with('Saloon_services.services_master')->with('work_days')->orderBy('displayorder', 'asc')->get();

                // select('title', 'location', 'latitude', 'longitude', 'image')->

                foreach ($saloons as $key => $val) {

                    /* GET SALON OWNER PLAN ID */
                    $get_salon_owner_plan = User::select('admin_membership_plan','membership_plan')->where('id',$val['user_id'])->first();

                    $user_plan = "0";
                    if ($get_salon_owner_plan['membership_plan'] != null || $get_salon_owner_plan['admin_membership_plan'] != null){
                        if($get_salon_owner_plan['membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['membership_plan'];
                        }
                        if($get_salon_owner_plan['admin_membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['admin_membership_plan'];
                        }
                    }
                    $saloons[$key]['owner_plan'] = $user_plan ;
                    /*--------------------------*/


                    if($val['timezone'] != '') {
                        date_default_timezone_set($val['timezone']);
                    } else {
                        date_default_timezone_set('America/Chicago');
                    }

                    $date_time = Carbon::now();
                    $today = strtolower($date_time->format('l'));
                    $date_today = $date_time->format('Y-m-d');

                    $count_images = SalonImages::where('salon_id',$val['id'])->count();
                    $saloons[$key]['image_total'] = $count_images;

                    $saloons[$key]['long_description'] = url('api/long_description/'.$val['id']);
                    $status = 'close';
                    $saloons[$key]['wait_time'] = 0;
                    $saloons[$key]['act_emp'] = 0;
                    $map_pin = 'close.png';
                    $saloons[$key]['is_favourite'] = 1;
                    $saloons[$key]['saloon_phone'] = trim($val['saloon_phone']);
                    $saloons[$key]['date_time'] = Carbon::parse($val['created_at'])->format('m/d/Y h:i a');

                    if (count($val['Saloon_services'])) {

                        foreach ($val['Saloon_services'] as $k1=> $v1){
                            $discount_price = ($v1['charges'] * $v1['discount']) / 100;
                            $final_price  = $v1['charges'] - $discount_price;
                            $val['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                        }
                    }

                    // $open_time  = Carbon::createFromTimeString($val['start_hour']);
                    // $close_time = Carbon::createFromTimeString($val['end_hour']);

                    $open_time  = '';
                    $close_time = '';

                    if (file_exists($val['image'])) {
                        $saloons[$key]['image'] = url($val['image']);
                    } else {
                        $saloons[$key]['image'] = '';
                    }

                    if($saloons[$key]['is_online'] == 1) {

                        if (count($val['work_days'])) {

                            foreach ($val['work_days'] as $wd) {
                                if ($wd['work_day'] == $today) {
                                    $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                    $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                    $status = 'open';
                                    break;
                                }
                            }

                        }

                    }

                    if ($status == 'open') {

                        if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                            $status = 'open';
                        } else {
                            $status = 'close';
                        }

                    }

                    // $employees = Employees::where('saloon_id', $val['id'])->get();

                    $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $val['id'])->get();
                    if (count($employees)) {
                        foreach ($employees as &$emp) {
                            if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                                $emp['nick_name'] = $emp['first_name'];
                            }

                            if($emp['status'] == 'active' && $emp['available'] == 'present') {
                                $saloons[$key]['act_emp'] += 1;
                            }

                            if(file_exists($emp['image'])) {
                                $emp['image'] = url($emp['image']);
                            } else {
                                $emp['image'] = '';
                            }
                            array_walk_recursive($emp, function (&$item, $key) {
                                $item = null === $item ? '' : $item;
                            });
                        }
                    }

                    if ($status == 'open' && $saloons[$key]['act_emp'] == 0) {
                        $saloons[$key]['act_emp'] = 1;
                    }

                    $employees->prepend(['id' => 0, 'saloon_id' => 2, 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '']);

                    $saloons[$key]['employees'] = $employees;

                    $saloons[$key]['status'] = $status;

                    if ($status == 'open') {

                        $bookings = Booking::with('Booking_details')->where('saloon_id', $val['id'])->where('date', $date_today)->whereNull('end_time')->get();

                        if(! empty($bookings)) {

                            foreach($bookings as $book_key => $book_val) {

                                foreach($book_val['Booking_details'] as $book_details) {
                                    $saloons[$key]['wait_time'] += ($book_details['duration'] * $book_details['no_of_persons']);
                                }
                            }
                        }

                        $prev = '';
                        $timer_pin = '';

                        foreach(saloon::$duration_pin as $time => $pin) {

                            if($saloons[$key]['wait_time'] <= $time) {
                                $timer_pin = $pin;
                                break;
                            } else if($saloons[$key]['wait_time'] < $time){
                                $timer_pin = $prev;
                            }
                            $prev = $pin;

                        }

                        if($timer_pin == '' && $saloons[$key]['wait_time'] >= end(saloon::$duration_pin)) {
                            $timer_pin = end(saloon::$duration_pin);
                        }

                        if($saloons[$key]['global_discount'] == 1) {
                            $timer_pin = $timer_pin . '_discount';
                        }

                        $map_pin = $timer_pin.'.png';
                    }

                    $saloons[$key]['map_pin'] = $map_pin;

                    /* if in progress and waiting then map pin change to green */
                    $saloons[$key]['booking_id'] = 0;
                    $saloons[$key]['booking_status'] = "";

                    $chk_booking = Booking::where('saloon_id', $val['id'])
                        ->where('user_id',$appuser['id'])
                        ->where('date', $date_today)
                        ->Where(function ($query){
                            $query->where('status', 'waiting')
                                ->orwhere('status', 'in-progress');
                        })
                        ->first();
                    if ($chk_booking){
                        $saloons[$key]['booking_id'] = $chk_booking['id'];
                        $saloons[$key]['booking_status'] = $chk_booking['status'];
                        $saloons[$key]['map_pin'] = "checkin.png";
                    }

                    /* SET OWNER */

                    $salon_owner = saloon_employees::where('saloon_id',$val['id'])->where('is_salon_owner',1)->where('status','accept')->first();
                    if($salon_owner){
                        $saloons[$key]['user_id'] = $salon_owner['user_id'];
                    }
                }
            }

            $response["Result"] = 1;
            $response["favourites"] = $saloons;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function do_booking(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'saloon_id' => 'required',
                'service_type' => 'required',
                'no_of_persons' => 'required',
                //'name' => 'required',
                //'phone' => 'required',
                //'email' => 'required',
                'date' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'do_booking');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                if (isset($request['manual_booking_user_id']) && $request['manual_booking_user_id'] != ""){
                    $appuser = User::findorFail($request['manual_booking_user_id']);
                }
                else{
                    $appuser = User::findorFail($loginUser->user_id);
                }

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'do_booking');

                    return response($response, 200);
                }

                $saloon = saloon::where('id', $request['saloon_id'])->first();

                if(empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon not found.";
                    return response($response, 401);
                }

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $input = $request->all();
                $types = explode(',', $input['service_type']);

                if(empty($input['date'])) {
                    $date_time = Carbon::now();
                    $date_today = $date_time->format('Y-m-d');
                    $input['date'] = $date_today;
                } else {
                    $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');
                }

                $input['user_id'] = $appuser->id;

                $chk_booking = Booking::where('saloon_id', $input['saloon_id'])
                    ->where('user_id', $input['user_id'])
                    ->where('date', $input['date'])
                    ->Where(function ($query){
                        $query->where('status', 'waiting')
                            ->orwhere('status', 'in-progress');
                    })
                    ->count();

                if($chk_booking) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon already booked for " . $request['date'];
                    return response($response, 200);
                }

                $chk_other = Booking::where('user_id', $input['user_id'])
                    ->where('date', $input['date'])
                    ->Where(function ($query){
                        $query->where('status', 'waiting')
                            ->orwhere('status', 'in-progress');
                    })->count();

                $second_booking = false;
                $second_booking = false;

                if($chk_other) {
                    $second_booking = true;
                }

                $upd_user = 0;
                $upd_arr = array();

//                if(trim($appuser->name) == '') {
//                    $upd_arr['name'] = $input['name'];
//                    $upd_user = 1;
//                }
//
//                if(trim($appuser->email) == '') {
//                    $upd_arr['email'] = $input['email'];
//                    $upd_user = 1;
//                }
//
//                if($upd_user == 1) {
//                    $appuser->update($upd_arr);
//                }

                $input['avail_discount'] = $saloon['global_discount'];
                $input['status'] = 'waiting';

                $total_booking = Booking::count() + 1;
                $input['booking_name'] = config('siteVars.prefix')."-" . Carbon::today()->format("Ymd") . "-" . (10000 + $total_booking);

                $input['checkin_type'] = 1;

                $input['device_type'] = !empty($loginUser['device_type'])?$loginUser['device_type']:"";
                $input['device_token'] = !empty($loginUser['device_token'])?$loginUser['device_token']:"";
                $booking = Booking::create($input);

                $input['booking_id'] = $booking->id;

                /* ---------- ASSIGN EMPLOYEE ID IF NO PREFERENCE IS SELECT THEN CHECK EMPLOYEE EXIST (IF NO EMP THEN OWNER IS ASSING ELSE 0) ---------------  */
                if (isset($request['emp_id']) && $request['emp_id']==0){
                    /* CHECK EMPLOYEE */
                    $get_employee = saloon_employees::where('status','accept')
                        ->where('saloon_id',$saloon['id'])
                        ->where('is_salon_owner',0)
                        ->count();
                    if ($get_employee <= 0){
                        $get_owner = saloon_employees::where('status','accept')
                            ->where('saloon_id',$saloon['id'])
                            ->where('is_salon_owner',1)->first();
                        $input['emp_id'] = $get_owner['user_id'];
                    }
                }


                $sub_total = 0;
                foreach($types as $type) {

                    $saloon_services = Saloon_services::where('saloon_id', $request['saloon_id'])->where('type_id', $type)->first();

                    if (empty($saloon_services)) {
                        continue;
                    }

                    $input['type_id'] = $type;
                    $input['duration'] = $saloon_services['duration'];

                    if($saloon['global_discount'] == 1) {
                        $input['avail_discount'] = $saloon_services['avail_discount'];
                        $input['discount_type'] = 'percent';
                        $input['discount_value'] = $saloon_services['discount'];
                        $input['charges'] = $saloon_services['charges'];

                        $total_charges = ($saloon_services['charges'] * $saloon_services['discount'])/100;
                        $total_charge = $saloon_services['charges']-$total_charges;
                        $input['total_charge'] = $total_charge;
                        $sub_total = $sub_total + $input['total_charge'];
                    }
                    else{
                        $input['charges'] = $saloon_services['charges'];
                        $input['total_charge'] = $saloon_services['charges'];
                        $sub_total = $sub_total + $saloon_services['charges'];
                    }
                    Booking_details::create($input);
                }

                $inp['sub_total'] = $sub_total;
                //$inp['final_total'] = "";
                $booking->update($inp);

                /* PUSH NOTIFICATION */
                $get_salon = saloon::where('id',$request['saloon_id'])->first();
                if (!empty($get_salon)){

                    $get_salon_owner_list = saloon_employees::where('saloon_id',$request['saloon_id'])->where('is_salon_owner',1)->get();
                    foreach ($get_salon_owner_list as $vl){
                        $user_list = Login_details::where('device_token','!=','')->where('user_id',$vl['user_id'])->get();
                        foreach ($user_list as $values){
                            $device_char =  strlen($values['device_token']);

                            $title = 'New Booking';
                            $message = ucfirst($appuser['name'])." successfully Check-in with Booking ID ".$booking['booking_name'];
                            //$op['message'] = $request['name'];
                            if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                                    'badge' => 1,
                                    'title' => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key',
                                    'custom' => array('type' => 'waiting_list_screen','send_to'=>'owner','booking_id'=>'','salon_id'=>'')
                                ));

//                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                    ->to($values['device_token'])
//                                    ->send($message);
                            }

                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                                //$this->firebase_notification($title,$message,$values['device_token']);
//                                $this->firebase_notification($title,$message,$values['device_token'],'waiting_list_screen','owner','','');
                            }

                        }
                    }
                    //$user_list = Login_details::where('device_token','!=','')->where('user_id',$get_salon['user_id'])->get();
                }

                /* NOTIFICATION ADDED TO CRON */
                /*$noti['notification_type']='user';
                $noti['message']=$op['message'];
                $noti['receiver'] = $appuser['id'];
                $noti['is_read']=0;
                Notification::create($noti);*/

                if ($appuser->phone == "" || $appuser->phone == null)
                {
                    $input1['phone'] = $request['phone'];
                    $appuser->update($input1);
                }

                $sms_text = "Thank you for using ".config('siteVars.title').", tap the link to check your waiting time: ".config('siteVars.display_url')."code/".$saloon->id."/".$saloon->code.". Download ".config('siteVars.title')." App ".config('siteVars.siteurl')."app-download";

                if(substr($request['phone'], 0, 1) == 1) {
                    $clickatell = new \Clickatell\Rest();
                    $other_country = 'no';
                    $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => $sms_text];
                } else {
                    $clickatell = new \Clickatell\Rest();
                    $other_country = 'yes';
                    $datas = ['to' => [$request['phone']], 'content' => $sms_text];
                }
//                $result = $clickatell->sendMessage($datas,$other_country);


                /* Email to user */

                $booking_details = Booking_details::where('booking_id', $booking['id'])->get();
                $services = "";
                foreach ($booking_details as $k=>$v){
                    $service_type = Service_type::where('id',$v['type_id'])->first();
                    if ($service_type){
                        $services = $services.$service_type->title.",";
                    }
                }
                $services = trim($services,",");


                $em= $request['email'];
                $bcc = [];
                $register_email=Email::findorFail(6);
                $subject = $register_email['subject'];
                $from1 = $register_email['email'];

                $person = $request['no_of_persons'];
                $name = $appuser['name'];
                $salon = $get_salon['title'];
                $booking_id = $booking['booking_name'];
                $service = $services;
                $date = date('m/d/y',strtotime($request['date']));

                $dlink = config('siteVars.display_url')."code/".$saloon->id."/".$saloon->code;
                $string = ["{booking_id}","{person}","{name}","{salon}","{service}","{date}","{display_link}"];
                $replace_string = [$booking_id,$person,$name,$salon,$service,$date,$dlink];

                $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                $data['title'] = $register_email['title'];

//                Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
//                    $m->from($from1, config('siteVars.title'));
//                    $m->bcc($bcc);
//                    $m->to($em)->subject($subject);
//                });

                $response["Result"] = 1;
                $response['Booking'] = $booking;
                $response['Message'] = "Salon Booked";

                if($second_booking) {
                    return response($response, 202);
                }

                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function edit_booking(Request $request)
    {
        try {

            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;
            }
            else{
                $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'booking_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $response["Result"] = 0;
                    $response["Message"] = implode(',', $validator->errors()->all());
                    return response($response, 200);
                }

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (empty($loginUser) || $request['remember_token'] == "") {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'do_booking');

                    return response($response, 200);
                }
            }

            if (!empty($loginUser)) {

                if (isset($request['manual_booking_user_id']) && $request['manual_booking_user_id'] != ""){
                    $appuser = User::findorFail($request['manual_booking_user_id']);
                }
                else{
                    $appuser = User::findorFail($loginUser['user_id']);
                }

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'do_booking');

                    return response($response, 200);
                }

                $booking = Booking::where('id', $request['booking_id'])->first();

                if(empty($booking)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Booking not found.";
                    return response($response, 401);
                }

                $input = $request->all();
                $types = explode(',', $input['service_type']);

                if(empty($input['date'])) {
                    $date_time = Carbon::now();
                    $date_today = $date_time->format('Y-m-d');
                    $input['date'] = $date_today;
                } else {
                    $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');
                }

                $input['user_id'] = $booking['user_id'];

                $saloon = saloon::where('id', $booking['saloon_id'])->first();

                $chk_booking = Booking::where('saloon_id', $saloon['id'])
                    ->where('user_id', $input['user_id'])
                    ->where('date', $input['date'])
                    ->where('id','!=',$request['booking_id'])
                    ->where('status', 'waiting')->count();

                if($chk_booking) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon already booked for " . $request['date'];
                    return response($response, 200);
                }

                $chk_other = Booking::where('id','!=',$request['booking_id'])->where('user_id', $input['user_id'])->where('date', $input['date'])->where('status', 'waiting')->count();
                $second_booking = false;

                if($chk_other) {
                    $second_booking = true;
                }

                $input['updated_at'] = date('Y-m-d H:i:s');
                $booking->update($input);

                $input['booking_id'] = $booking->id;

                $sub_total = 0;

                /* GET SELECTED EMPLOYEE */

                $get_employee = Booking_details::where('booking_id',$booking['id'])->first();
                $emp_id = $get_employee['emp_id'];

                /*------------------------*/
                $remove_booking_details  = Booking_details::where('booking_id', $booking['id'])->forceDelete();

                foreach($types as $type) {

                    $saloon_services = Saloon_services::where('saloon_id', $saloon['id'])->where('type_id', $type)->first();

                    if (empty($saloon_services)) {
                        continue;
                    }

                    $input['type_id'] = $type;
                    $input['duration'] = $saloon_services['duration'];
                    $input['emp_id'] = isset($input['emp_id'])?$input['emp_id']:$emp_id;

                    if($saloon['global_discount'] == 1) {
                        $input['avail_discount'] = $saloon_services['avail_discount'];
                        $input['discount_type'] = 'percent';
                        $input['discount_value'] = $saloon_services['discount'];
                        $input['charges'] = $saloon_services['charges'];

                        $total_charges = ($saloon_services['charges'] * $saloon_services['discount'])/100;
                        $total_charge = $saloon_services['charges']-$total_charges;
                        $input['total_charge'] = $total_charge;
                        $sub_total = $sub_total + $input['total_charge'];
                    }
                    else{
                        $input['charges'] = $saloon_services['charges'];
                        $input['total_charge'] = $saloon_services['charges'];
                        $sub_total = $sub_total + $saloon_services['charges'];
                    }
                    Booking_details::create($input);
                }

                $inp['sub_total'] = $sub_total;
                $booking->update($inp);

                /* PUSH NOTIFICATION */
                $get_salon = saloon::where('id',$saloon['id'])->first();
//                if (!empty($get_salon)){
//
//                    $get_salon_owner_list = saloon_employees::where('saloon_id',$saloon['id'])->where('is_salon_owner',1)->get();
//                    foreach ($get_salon_owner_list as $vl){
//                        $user_list = Login_details::where('device_token','!=','')->where('user_id',$vl['user_id'])->get();
//                        foreach ($user_list as $values){
//                            $device_char =  strlen($values['device_token']);
//
//                            $title = 'New Booking';
//                            $message = $request['name'];
//                            //$op['message'] = $request['name'];
//                            if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
//                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
//                                    'badge' => 1,
//                                    'title' => $title,
//                                    'sound' => 'example.aiff',
//                                    'actionLocKey' => 'Action button title!',
//                                    'locKey' => 'localized key'
//                                ));
//
//                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                    ->to($values['device_token'])
//                                    ->send($message);
//                            }
//
//                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
//                                $this->firebase_notification($title,$message,$values['device_token']);
//                            }
//                        }
//                    }
//                }

                $user = User::where('id',$booking['user_id'])->first();

                $sms_text = "Thank you for using ".config('siteVars.title').", tap the link to check your waiting time: ".config('siteVars.display_url')."code/".$saloon->id."/".$saloon->code.". Download ".config('siteVars.title')." App ".config('siteVars.siteurl')."app-download";

                if(substr($user['phone'], 0, 1) == 1) {
                    $clickatell = new \Clickatell\Rest();
                    $datas = ['to' => [$user['phone']], 'from' => '15732073611', 'content' => $sms_text];
                } else {
                    $clickatell = new \Clickatell\Rest('india');
                    $datas = ['to' => [$user['phone']], 'content' => $sms_text];
                }
                //$result = $clickatell->sendMessage($datas);


                /* Email to user */

                $booking_details = Booking_details::where('booking_id', $booking['id'])->get();
                $services = "";
                foreach ($booking_details as $k=>$v){
                    $service_type = Service_type::where('id',$v['type_id'])->first();
                    if ($service_type){
                        $services = $services.$service_type->title.",";
                    }
                }
                $services = trim($services,",");

                if ($user['email'] != ""){
                    $em= $user['email'];
                    $bcc = [];
                    $register_email=Email::findorFail(6);
                    $subject = $register_email['subject'];
                    $from1 = $register_email['email'];

                    $person = $request['no_of_persons'];
                    $name = $user['name'];
                    $salon = $get_salon['title'];
                    $booking_id = $booking['booking_name'];
                    $service = $services;
                    $date = date('m/d/y',strtotime($request['date']));

                    $dlink = config('siteVars.display_url')."code/".$saloon->id."/".$saloon->code;
                    $string = ["{booking_id}","{person}","{name}","{salon}","{service}","{date}","{display_link}"];
                    $replace_string = [$booking_id,$person,$name,$salon,$service,$date,$dlink];

                    $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                    $data['title'] = $register_email['title'];

                    Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                        $m->from($from1, config('siteVars.title'));
                        $m->bcc($bcc);
                        $m->to($em)->subject($subject);
                    });
                }

                $response["Result"] = 1;
                $response['Booking'] = $booking;
                $response['Message'] = "Booking Updated";

                if($second_booking) {
                    return response($response, 202);
                }

                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function cancel_booking(Request $request)  // if owner login then pass user_id other wise no need
    {
        try {
            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;
            }
            else{
                $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'booking_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $response["Result"] = 0;
                    $response["Message"] = implode(',', $validator->errors()->all());
                    return response($response, 200);
                }

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (empty($loginUser) || $request['remember_token'] == "") {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'cancel_booking');
                    return response($response, 200);
                }
            }


            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser['user_id']);

                $user_id = $appuser['id'];
                if (isset($request['user_id'])){
                    $user_id = $request['user_id'];    // BOOKING USER ID
                }

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'cancel_booking');

                    return response($response, 200);
                }

                $booking = Booking::where('id', $request['booking_id'])->where('user_id',$user_id)->first();

                if($booking->status == 'completed') {
                    $response["Result"] = 0;
                    $response["Message"] = "Serivce already completed.";
                    return response($response, 200);
                }

                if($booking->status == 'in-progress') {
                    $response["Result"] = 0;
                    $response["Message"] = "Service already in process.";
                    return response($response, 200);
                }

                $columns['status'] = 'cancel';
                if (isset($request['user_id'])) {
                    $columns['is_notify'] = 3; // cancel by admin 3
                }
                else{
                    $columns['is_notify'] = 2; // cancel by user 2
                }

                $booking->update($columns);

                $booking_details = Booking_details::where('booking_id', $booking['id'])->get();
                $services = "";
                foreach ($booking_details as $k=>$v){
                    $service_type = Service_type::where('id',$v['type_id'])->first();
                    if ($service_type){
                        $services = $services.$service_type->title.",";
                    }
                }

                $services = trim($services,",");

                /* CANCEL BOOKING BOOKING BY OWNER */

//                $get_salon = saloon::where('id',$booking['saloon_id'])->first();
//                if (!empty($get_salon)) {
//                    if (isset($request['user_id'])) {
//
//                        $user_list = Login_details::where('device_token','!=','')->where('user_id',$booking['user_id'])->get();
//                        foreach ($user_list as $values){
//                            $device_char =  strlen($values['device_token']);
//                            $title = 'Booking Cancelled';
//                            $message = "Unfortunately " . $get_salon['title'] . " salon rejected you booking";
//                            //$op['message'] = $request['name'];
//                            if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
//                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
//                                    'badge' => 1,
//                                    'title' => $title,
//                                    'sound' => 'example.aiff',
//                                    'actionLocKey' => 'Action button title!',
//                                    'locKey' => 'localized key',
//                                    'custom' => array('type' => 'booking_history_screen','send_to'=>'user','booking_id'=>'','salon_id'=>'')
//                                ));
//
//                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                    ->to($values['device_token'])
//                                    ->send($message);
//                            }
//
//                            if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
//                                //$this->firebase_notification($title, $message, $values['device_token']);
//                                $this->firebase_notification($title,$message,$values['device_token'],'booking_history_screen','user','','');
//                            }
//                        }
//
//
//                    } else {
//
//                        $get_salon_owner_list = saloon_employees::where('saloon_id',$get_salon['id'])->where('is_salon_owner',1)->get();
//                        foreach ($get_salon_owner_list as $vl){
//                            $user_list = Login_details::where('device_token','!=','')->where('user_id',$vl['user_id'])->get();
//                            foreach ($user_list as $values){
//                                $device_char =  strlen($values['device_token']);
//                                $title = 'Booking Cancelled';
//                                $message = "Unfortunately " . $appuser['name'] . " cancelled booking";
//                                //$op['message'] = $request['name'];
//                                if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
//                                    $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
//                                        'badge' => 1,
//                                        'title' => $title,
//                                        'sound' => 'example.aiff',
//                                        'actionLocKey' => 'Action button title!',
//                                        'locKey' => 'localized key',
//                                        'custom' => array('type' => 'waiting_list_screen','send_to'=>'owner','booking_id'=>'','salon_id','')
//                                    ));
//
//                                    \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                        ->to($values['device_token'])
//                                        ->send($message);
//                                }
//
//                                if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
//                                    //$this->firebase_notification($title, $message, $values['device_token']);
//                                    $this->firebase_notification($title,$message,$values['device_token'],'waiting_list_screen','owner','','');
//                                }
//                            }
//                        }
//                    }
//                }


//                if ($appuser['email']!="") {
//                    /* Email to user */
//
//                    $em= $appuser['email'];
//                    $bcc = [];
//                    $register_email=Email::findorFail(5);
//                    $subject = $register_email['subject'];
//                    $from1 = $register_email['email'];
//
//                    $person = $booking['no_of_persons'];
//                    $name = $appuser['name'];
//                    $salon = $get_salon['title'];
//                    $booking_id = $booking['booking_name'];
//                    $date = date('m/d/y',strtotime($booking['date']));
//
//                    $string = ["{booking_id}","{person}","{name}","{salon}","{service}","{date}"];
//                    $replace_string = [$booking_id,$person,$name,$salon,$services,$date];
//
//                    $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
//                    $data['title'] = $register_email['title'];
//
//                    Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
//                        $m->from($from1, config('siteVars.title'));
//                        $m->bcc($bcc);
//                        $m->to($em)->subject($subject);
//                    });
//                }

                $booking->delete();

                Booking_details::where('booking_id', $request['booking_id'])->delete();

                $response["Result"] = 1;
                $response['Message'] = "Booking Cancel";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_booking(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                //'page' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_booking');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_booking');
                    return response($response, 200);
                }

                $favourites = Favourites::where('user_id', $appuser->id)->pluck('saloon_id')->toArray();

                $bookings = array();


                /**/
                if(isset($request['page'])){
                    $skip = $request['page'] * 10;
                    $skip_next = 10 * ($request['page'] + 1);
                    $take = 10;
                }
                else{
                    $skip = 0;
                    $skip_next = 0;
                    $take = 50;
                }

                $c_bookings = Booking::where('user_id', $appuser->id)
                    ->where('status', 'completed')
                    ->with('Saloon')->with('Booking_details')
                    ->with('Booking_details.Service_type')
                    ->orderBy('id','DESC')->skip($skip)->take($take)->get();

                $c_bookings_next = Booking::where('user_id', $appuser->id)
                    ->where('status', 'completed')
                    ->with('Saloon')->with('Booking_details')
                    ->with('Booking_details.Service_type')
                    ->orderBy('id','DESC')->skip($skip_next)->take($take)->get()->count();

                if (!empty($c_bookings_next)) {
                    $has_next_page = 1;
                } else {
                    $has_next_page = 0;
                }

                /**/

                //$c_bookings = Booking::where('user_id', $appuser->id)->where('status', 'completed')->with('Saloon')->with('Booking_details')->with('Booking_details.Service_type')->orderBy('id','DESC')->get();

                foreach($c_bookings as $key => $val) {
                    $c_bookings[$key]['services'] = '';
                    $c_bookings[$key]['date'] = date('m/d/y',strtotime($val['date']));
                    $c_bookings[$key]['date_time'] = Carbon::parse($val['created_at'])->format('m/d/Y h:i a');


                    $c_bookings[$key]['saloon']['is_favourite'] = 0;

                    if(in_array($val['saloon']['id'], $favourites)) {
                        $c_bookings[$key]['saloon']['is_favourite'] = 1;
                    }

                    foreach($val['booking_details'] as $b_key => $b_val) {
                        $c_bookings[$key]['services'] .= $b_val['service_type']['title']. ', ';
                    }

                    $c_bookings[$key]['services'] = trim($c_bookings[$key]['services'], ', ');

                    $c_bookings[$key]['is_rating_given'] = 0;
                    $get_rating  = Rating::where('saloon_id',$val['saloon']['id'])->where('booking_id',$val['id'])->where('user_id',$appuser->id)->first();
                    if ($get_rating){
                        $c_bookings[$key]['is_rating_given'] = 1;
                    }

                    /* SALON OWNER PLAN */
                    $get_salon_owner_plan = User::select('admin_membership_plan','membership_plan')->where('id',$val['saloon']['user_id'])->first();
                    $user_plan = 0;
                    if ($get_salon_owner_plan['membership_plan'] != null || $get_salon_owner_plan['admin_membership_plan'] != null){
                        if($get_salon_owner_plan['membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['membership_plan'];
                        }
                        if($get_salon_owner_plan['admin_membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['admin_membership_plan'];
                        }
                    }
                    $c_bookings[$key]['saloon']['plan_id'] = $user_plan;
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

//                $p_bookings = Booking::where('user_id', $appuser->id)->where('date', '>=', $date_today)->where('status', 'waiting')->orWhereNull('status')->with('Saloon')->with('Booking_details')->with('Booking_details.Service_type')->orderBy('id','DESC')->get();


                $p_bookings = Booking::where('user_id', $appuser->id)
                    ->where('date', '>=', $date_today)
                    ->where(function ($query) use ($request) {
                        $query->where('status', 'waiting')
                            ->orWhere('status', 'in-progress')
                            ->orWhereNull('status')->with('Saloon');
                    })
                    ->with('Booking_details')
                    ->with('Booking_details.Service_type')
                    ->orderBy('id','DESC')->get();

                foreach($p_bookings as $key => $val) {
                    $p_bookings[$key]['services'] = '';
                    $p_bookings[$key]['date'] = date('m/d/y',strtotime($val['date']));
                    $p_bookings[$key]['date_time'] = Carbon::parse($val['created_at'])->format('m/d/Y h:i a');

                    $p_bookings[$key]['saloon']['is_favourite'] = 0;

                    if(in_array($val['saloon']['id'], $favourites)) {
                        $p_bookings[$key]['saloon']['is_favourite'] = 1;
                    }

                    foreach($val['booking_details'] as $b_key => $b_val) {
                        $p_bookings[$key]['services'] .= $b_val['service_type']['title'] . ', ';
                    }

                    $p_bookings[$key]['services'] = trim($p_bookings[$key]['services'], ', ');

                    /* SALON OWNER PLAN */
                    $get_salon_owner_plan = User::select('admin_membership_plan','membership_plan')->where('id',$val['saloon']['user_id'])->first();
                    $user_plan = 0;

                    if ($get_salon_owner_plan['membership_plan'] != null || $get_salon_owner_plan['admin_membership_plan'] != null){
                        if($get_salon_owner_plan['membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['membership_plan'];
                        }
                        if($get_salon_owner_plan['admin_membership_plan'] !=0){
                            $user_plan = $get_salon_owner_plan['admin_membership_plan'];
                        }
                    }
                    $p_bookings[$key]['saloon']['plan_id'] = $user_plan;
                }

                $bookings['completed'] = $c_bookings;
                $bookings['pending'] = $p_bookings;
                $bookings["has_next_page"] = $has_next_page;

                if(empty($bookings)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Booking done.";
                    return response($response, 200);
                }

                $response["Result"] = 1;
                $response['bookings'] = $bookings;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_booking_details(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'booking_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_booking_details');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_booking_details');

                    return response($response, 200);
                }

                $b_details = Booking::where('user_id', $appuser->id)->with('Booking_details')->where('id', $request['booking_id'])->first();

                $b_details['date'] = date('m/d/y',strtotime($b_details['date']));


                if(empty($b_details)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Booking details found.";
                    return response($response, 200);
                }

                $final_total = 0;
                $total = 0;
                foreach($b_details['booking_details'] as $b_key => $b_val) {

                    $b_details['services'] .= $b_val['service_type']['title']. ', ';
                    $get_price  = Saloon_services::where('saloon_id',$b_details['saloon_id'])->where('type_id',$b_val['service_type']['id'])->first();
                    $discount_price = ($get_price['charges'] * $get_price['discount'])/100;
                    $total = $get_price['charges'] - $discount_price;
                    $final_total = $final_total + $total;

                }
                $b_details['services'] = trim($b_details['services'], ', ');
                $b_details['services_total'] = "".$final_total*$b_details['no_of_persons'];

                $favourites = Favourites::where('user_id', $appuser->id)->pluck('saloon_id')->toArray();

                $sid = $b_details->saloon_id;

                $saloons = saloon::where('id', $sid)->with('Saloon_services')->with('Saloon_services.services_master')->with('work_days')->get();

                $date_time = Carbon::now();
                $today = strtolower($date_time->format('l'));
                $date_today = $date_time->format('Y-m-d');

                foreach($saloons as $key => $val) {

                    $status = 'close';
                    $saloons[$key]['wait_time'] = 0;
                    $saloons[$key]['act_emp'] = 0;
                    $saloons[$key]['is_favourite'] = 0;
                    $map_pin = 'close.png';
                    $saloons[$key]['saloon_phone'] = trim($val['saloon_phone']);
                    $saloons[$key]['date_time'] = Carbon::parse($val['created_at'])->format('m/d/Y h:i a');
                    // $saloons[$key]['c_time'] = $date_time;

                    if(in_array($val['id'], $favourites)) {
                        $saloons[$key]['is_favourite'] = 1;
                    }

                    // $open_time = Carbon::createFromTimeString($val['start_hour']);
                    // $close_time = Carbon::createFromTimeString($val['end_hour']);

                    $open_time  = '';
                    $close_time = '';

                    // $saloons[$key]['op_time'] = $open_time;
                    // $saloons[$key]['cl_time'] = $close_time;

                    if(file_exists($val['image'])) {
                        $saloons[$key]['image'] = url($val['image']);
                    } else {
                        $saloons[$key]['image'] = '';
                    }

                    if($saloons[$key]['is_online'] == 1) {

                        if (count($val['work_days'])) {

                            foreach ($val['work_days'] as $wd) {
                                if ($wd['work_day'] == $today) {
                                    $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                    $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                    $status = 'open';
                                    break;
                                }
                            }

                        }

                    }

                    if ($status == 'open') {

                        if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                            $status = 'open';
                        } else {
                            $status = 'close';
                        }

                    }

                    // $employees = Employees::where('saloon_id', $val['id'])->get();

                    $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $val['id'])->get();

                    if (count($employees)) {

                        foreach ($employees as &$emp) {

                            if($emp['status'] == 'active' && $emp['available'] == 'present') {
                                $saloons[$key]['act_emp'] += 1;
                            }

                            if(file_exists($emp['image'])) {
                                $emp['image'] = url($emp['image']);
                            } else {
                                $emp['image'] = '';
                            }

                        }
                    }

                    if ($status == 'open' && $saloons[$key]['act_emp'] == 0) {
                        $saloons[$key]['act_emp'] = 1;
                    }

                    $employees->prepend(['id' => 0, 'saloon_id' => 2, 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '']);

                    $saloons[$key]['employees'] = $employees;

                    $saloons[$key]['status'] = $status;

                    if($saloons[$key]['act_emp'] == 0 && $status == 'open') {
                        $saloons[$key]['act_emp'] = 1;
                    }

                    if($status == 'open') {

                        $bookings = Booking::with('Booking_details')->where('saloon_id', $val['id'])->where('date', $date_today)->whereNull('end_time')->get();

                        if(! empty($bookings)) {

                            foreach($bookings as $book_key => $book_val) {

                                foreach($book_val['Booking_details'] as $book_details) {
                                    $saloons[$key]['wait_time'] += ($book_details['duration'] * $book_details['no_of_persons']);
                                }
                            }
                        }

                        $prev = '';
                        $timer_pin = '';

                        foreach(saloon::$duration_pin as $time => $pin) {

                            if($saloons[$key]['wait_time'] <= $time) {
                                $timer_pin = $pin;
                                break;
                            } else if($saloons[$key]['wait_time'] < $time){
                                $timer_pin = $prev;
                                break;
                            }
                            $prev = $pin;
                        }

                        if($timer_pin == '' && $saloons[$key]['wait_time'] >= end(saloon::$duration_pin)) {
                            $timer_pin = end(saloon::$duration_pin);
                        }

                        if($saloons[$key]['global_discount'] == 1) {
                            $timer_pin = $timer_pin . '_discount';
                        }

                        $map_pin = $timer_pin.'.png';
                    }

                    $saloons[$key]['map_pin'] = $map_pin;


                    /* SET OWNER */

                    $salon_owner = saloon_employees::where('saloon_id',$val['id'])->where('is_salon_owner',1)->where('status','accept')->first();
                    if($salon_owner){
                        $saloons[$key]['user_id'] = $salon_owner['user_id'];
                    }
                }

                $b_details['saloon'] = $saloons[0];
                $response["Result"] = 1;
                $response['bookings'] = $b_details;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_booking_details_owner(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'booking_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_booking_details');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'get_booking_details_owner');

                    return response($response, 200);
                }

                $b_details = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('id', $request['booking_id'])->first();

                $b_details['date'] = date('m/d/y',strtotime($b_details['date']));
                $b_details['checkin_date'] = date('m/d/y h:i A',strtotime($b_details['created_at']));

                if(empty($b_details)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Booking details found.";
                    return response($response, 200);
                }

                $final_total = 0;
                $total = 0;
                foreach($b_details['booking_details'] as $b_key => $b_val) {

                    $b_details['services'] .= $b_val['service_type']['title']. ', ';

                    if(! empty($b_val['emp_id'])) {
                        $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $b_val['emp_id'])->first();
                        $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                        $exp_person_id = $expert['id'];
                        $exp_person_nickname = trim($expert['nick_name']);
                    } else {
                        $exp_person = 'No preference';
                        $exp_person_nickname = 'No preference';
                        $exp_person_id = 0;
                    }

                    $b_val['service_expert'] = $exp_person;
                    $b_val['service_expert_id'] = $exp_person_id;

                    if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                        $b_val['service_expert_nickname'] = $expert['first_name'];
                    }
                    else{
                        $b_val['service_expert_nickname'] = $exp_person_nickname;
                    }
                }
                $b_details['services'] = trim($b_details['services'], ', ');
                $b_details['remaining'] = "";

                /* AVAILABLE POINTS */

                $available_point = Point_history::where('user_id',$b_details['user_id'])->where('salon_id',$b_details['saloon_id'])->where('is_last',1)
                    ->orderBy('id','DESC')->first();

                if (!empty($available_point)){
                    $b_details['available_points'] = $available_point['point_after'];
                }
                else{
                    $b_details['available_points'] = 0;
                }

                $sid = $b_details['saloon_id'];
                $saloons = saloon::where('id', $sid)->first();

                $b_details['tax'] = $saloons['tax'];

                array_walk_recursive($b_details, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                array_walk_recursive($b_details['users'], function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response['bookings'] = $b_details;
                return response($response, 200);

            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function send_message(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'message' => 'required',
                'receiver' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'send_message');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if(empty($request['receiver'])){
                    $response["Result"] = 0;
                    $response["Message"] = "Please Enter receiver";
                    return response($response, 200);
                }


                $input['sender']=$appuser->id;
                if ($request['type']=='image'){
                    if($photo = $request->file('message'))
                    {
                        $input['message'] = $this->image($photo,'message');
                        $path=url('/')."/".$input['message'];
                        $input['image_thumbnail']= $this->Imagethumbnail($path,'message/thumbnail',null,400,null);
                    }
                }
                else {
                    $input['message'] = $request['message'];
                }

                $input['saloon_id']=0;
                $input['type']=$request['type'];

                if(!empty($request['receiver'])){
                    $input['receiver']=$request['receiver'];
                    /* IOS PUSH NOTIFICATION */
                    /*$receiver_lists = Login_details::where('user_id',$request['receiver'])->get();
                    foreach ($receiver_lists as $values){
                        $device_char =  strlen($values['device_token']);
                        $op['message'] = $appuser->name.":".$request['message'];

                        if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                           $a = \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                ->to($values['device_token'])
                                ->send($op['message']);
                        }
                    }
                    */
                }
                else{
                    $input['receiver']=0;
                }

                $input['is_read']=0;
                $input['is_last']=1;


                /* UPDATE LAST MESSAGE BOFORE SENDING */
                $message_check = Message::where(function ($query) use ($appuser, $request) {
                    $query->where('sender', $appuser->id)
                        ->where('receiver', $request['receiver'])
                        ->Where('is_last','1');
                })
                    ->orWhere(function ($query) use ($appuser, $request) {
                        $query->where('receiver', $appuser->id)
                            ->where('sender', $request['receiver'])
                            ->Where('is_last','1');
                    })
                    ->first();
                if ($message_check){
                    $input11['is_last'] = '0';
                    $message_check->update($input11);
                }
                $chat=Message::create($input);

                $get_chat = Message::findorfail($chat['id']);
                if ($get_chat['type']=='image'){
                    if ($get_chat['message'] != "" && file_exists($get_chat['message'])) {
                        $get_chat['message'] = url($get_chat['message']);
                    }
                    if ($get_chat['image_thumbnail'] != "" && file_exists($get_chat['image_thumbnail'])) {
                        $get_chat['image_thumbnail'] = url($get_chat['image_thumbnail']);
                    }
                }

                array_walk_recursive($get_chat, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $get_chat['add_date']= date('m/d/Y, h:i a',strtotime($get_chat['created_at']));
                $get_chat['is_my_message'] = 1;
                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Data']=$get_chat;

                return response($response, 200);
            }
        } catch (\Exception $e) {
            return response("", 500);
        }
    }

    public function get_message(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'receiver_id' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_message');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if(!empty($request['receiver_id'])){
                    if (!empty($request['message_id'])) {
                        $chat = Message::with('user')->where(function ($query) use ($appuser, $request) {
                            $query->where('sender', $appuser->id)
                                ->where('receiver', $request['receiver_id'])
                                ->where('id','>', $request['message_id']);
                        })
                            ->orWhere(function ($query) use ($appuser, $request) {
                                $query->where('receiver', $appuser->id)
                                    ->where('sender', $request['receiver_id'])
                                    ->where('id','>', $request['message_id']);
                            })

                            ->get();
                    }
                    else {
                        $chat = Message::with('user')->where(function ($query) use ($appuser, $request) {
                            $query->where('sender', $appuser->id)
                                ->where('receiver', $request['receiver_id']);
                        })
                            ->orWhere(function ($query) use ($appuser, $request) {
                                $query->where('receiver', $appuser->id)
                                    ->where('sender', $request['receiver_id']);
                            })
                            ->get();
                    }
                }
                else {
                    if (!empty($request['message_id'])) {

                        $query = Message::with('user')->where('id','>',$request['message_id'])->select();
                        $query->where(function ($query) use ($appuser, $request) {
                            $query->orwhere('sender', $appuser->id)
                                ->orwhere('receiver',$appuser->id);
                            $query->where('id','>',$request['message_id']);
                        });
                        $chat = $query->where('id','>',$request['message_id'])->get();

                    }
                    else {
                        $query = Message::with('user');
                        $chat = $query->get();
                    }
                }
                foreach($chat as $key=>$value){

                    array_walk_recursive($value, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    if($value->sender == $appuser->id){
                        $is_my_message=1;
                    }
                    else{
                        $is_my_message=0;
                        $imsg = Message::findOrFail($value['id']);
                        $input['is_read'] = 1;
                        $imsg->update($input);
                    }

                    if ($value['type']=='image'){
                        if ($value['message'] != "" && file_exists($value['message'])) {
                            $value['message'] = url($value['message']);
                        }
                        if ($value['image_thumbnail'] != "" && file_exists($value['image_thumbnail'])) {
                            $value['image_thumbnail'] = url($value['image_thumbnail']);
                        }
                    }

                    $chat[$key]['is_my_message']=$is_my_message;
                    $chat[$key]['add_date']= date('m/d/Y, h:i a',strtotime($value['created_at']));
                }


                $response["Result"] = 1;
                $response['Data']=$chat;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (\Exception $e) {
            return response("", 500);
        }
    }

    public function message_history(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();


            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'message_history');
                return response($response, 200);
            }
            if(!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $chat = Message::where(function ($query) use ($appuser, $request) {
                    $query->where('sender', $appuser->id)
                        ->orWhere('receiver', $appuser->id);
                })
                    ->Where('is_last','1')
                    ->orderBy('id', "DESC")->get();

                foreach ($chat as $k=>$v){
                    if($v['sender'] == $appuser->id){
                        $user=$v['receiver'];
                    }
                    else{
                        $user=$v['sender'];
                    }

                    if ($v['type']=='image'){
                        if ($v['message'] != "" && file_exists($v['message'])) {
                            $v['message'] = url($v['message']);
                        }
                        if ($v['image_thumbnail'] != "" && file_exists($v['image_thumbnail'])) {
                            $v['image_thumbnail'] = url($v['image_thumbnail']);
                        }
                    }

                    $apuser = User::where('id',$user)->first();

                    if(!empty($apuser)) {
                        if ($apuser->image != "") {
                            $apuser->image = url($apuser->image);
                        } elseif ($apuser->fb_image != "") {
                            $apuser->image = $apuser->fb_image;
                        } else {
                            $apuser->image = "";
                        }
                        unset($apuser->password);
                        array_walk_recursive($apuser, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }

                    $unread_message = Message::where('sender',$user)->where('receiver',$appuser->id)->where('is_read',0)->count();

                    $chat[$k]['unread_message'] = $unread_message;
                    $chat[$k]['appuser'] = $apuser;
                    $chat[$k]['add_date']= date('m/d/Y, h:i a',strtotime($v['created_at']));
                }
                $response["Result"] = 1;
                $response['Data']=$chat;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_rating(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'saloon_id' => 'required',
                'booking_id' => 'required',
                'rating' => 'required',
                'review' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();


            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_rating');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $rating=Rating::where('user_id',$appuser->id)->where('saloon_id',$request['saloon_id'])
                    ->where('booking_id',$request['booking_id'])->first();

                if(empty($rating))
                {
                    $input=$request->all();
                    $input['user_id']=$appuser->id;
                    $in=Rating::create($input);
                    $response['Message'] = "Rating Added Successfully";


                    $get_salon = saloon::where('id',$request['saloon_id'])->first();
                    if (!empty($get_salon)){
                        $user_list = Login_details::where('device_token','!=','')->where('user_id',$get_salon['user_id'])->get();
                        foreach ($user_list as $values){
                            $device_char =  strlen($values['device_token']);

                            $title = 'Feedback Provided';
                            $message = $appuser['name']."\n".isset($request['review'])?$request['review']:'';
                            //$op['message'] = $appuser['name']."\n".isset($request['review'])?$request['review']:'';
                            if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                                    'badge' => 1,
                                    'title'     => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key',
                                    'custom' => array('type' => 'review_rating_screen','send_to'=>'owner','booking_id'=>'','salon_id'=>$request['saloon_id']),
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                                //$this->firebase_notification($title,$message,$values['device_token']);
                                $this->firebase_notification($title,$message,$values['device_token'],'review_rating_screen','owner','',$request['saloon_id']);
                            }

                        }
                    }

                    $c_bookings = Booking::where('id', $request['booking_id'])->where('status', 'completed')
                        ->with('Saloon')->with('Booking_details')->with('Booking_details.Service_type')->first();

                    if ($c_bookings){
                        $c_bookings['services'] = '';
                        $c_bookings['date_time'] = Carbon::parse($c_bookings['created_at'])->format('m/d/Y h:i a');

                        $c_bookings['saloon']['is_favourite'] = 0;

                        $favourites = Favourites::where('user_id', $appuser->id)->pluck('saloon_id')->toArray();
                        if(in_array($c_bookings['saloon']['id'], $favourites)) {
                            $c_bookings['saloon']['is_favourite'] = 1;
                        }

                        foreach($c_bookings['booking_details'] as $b_key => $b_val) {
                            $c_bookings['services'] .= $b_val['service_type']['title']. ', ';
                        }

                        $c_bookings['services'] = trim($c_bookings['services'], ', ');


                        $c_bookings['is_rating_given'] = 0;
                        $get_rating  = Rating::where('saloon_id',$c_bookings['saloon']['id'])->where('user_id',$appuser->id)->first();
                        if ($get_rating){
                            $c_bookings['is_rating_given'] = 1;
                        }
                    }

                    $response['Booking'] = $c_bookings;
                }

                else{

                    $response['Message'] = "You have already added your rating to this salon!";
                    $response["Result"] = 0;
                    return response($response, 200);
                }

                $total_rating=0;
                $saloon_rating=Rating::where('saloon_id',$request['saloon_id'])->get();
                $count=0;

                foreach($saloon_rating as $value){
                    $count++;
                    $total_rating = $total_rating + $value->rating;
                }

                $avg=$total_rating/$count;
                $saloon=saloon::findOrFail($request['saloon_id']);
                $input1['rating']=$avg;
                $saloon->update($input1);

                $response["Result"] = 1;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function update_display_code(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'update_display_code');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $get_salon = saloon::where('id',$request['salon_id'])->first();
                if ($get_salon){
                    $generate_code = rand(100000,999999);

                    $input = $request->all();
                    $input['code'] = $generate_code;
                    $get_salon->update($input);
                }

                $response["Result"] = 1;
                $response['Code'] = $generate_code;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function is_display_salon_code(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'is_display' => 'required',
                'salon_id' => 'required'
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'is_display_salon_code');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $get_salon = saloon::where('id',$request['salon_id'])->first();
                if ($get_salon){
                    $input['is_display'] = $request['is_display'];
                    $get_salon->update($input);
                }

                $response["Result"] = 1;
                $response['is_display'] = $request['is_display'];
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function view_rating(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                //'remember_token' => 'required',
                'saloon_id' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $get_salon = saloon::where('id',$request['saloon_id'])->first();
            if ($get_salon) {
                $rating = Rating::with('user', 'saloon')->where('saloon_id', $request['saloon_id'])->orderBy('id', 'DESC')->get();
                foreach ($rating as $keys => $values) {
                    $rating[$keys]['add_date'] = date('m/d/Y, h:i a', strtotime($values['created_at']));
                }
                $response["Result"] = 1;
                $response['Data'] = $rating;
                $response['Message'] = "Success";
                return response($response, 200);
            }
            else{
                $response["Result"] = 0;
                $response['Data'] = [];
                $response['Message'] = "Salon not found!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_remainder(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'title' => 'required',
                'remainder_date' => 'required',
                'remainder_time' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();


            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_remainder');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $get_user_timezone = User::select('timezone')->where('id',$loginUser['user_id'])->first();
                $input['timezone'] = $get_user_timezone['timezone'];

                if (isset($input['remainder_date']) && $input['remainder_date']!="") {
                    $input['remainder_date'] = Carbon::parse($input['remainder_date'])->format('Y-m-d');
                }
                $remainder = Remainder::where('user_id',$loginUser['user_id'])->first();
                if ($remainder){
                    $remainder->update($input);
                    $response['Message'] = "Remainder update successfully.";
                }
                else{
                    $input['user_id'] = $loginUser['user_id'];
                    Remainder::create($input);
                    $response['Message'] = "Remainder set successfully.";
                }

                $response["Result"] = 1;

                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_remainder(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();


            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_remainder');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $remainder = Remainder::where('user_id',$loginUser['user_id'])->first();
                if ($remainder){
                    $response["Result"] = 1;
                    $response["Remainder"] = $remainder;
                    return response($response, 200);
                }
                else{
                    $response["Result"] = 0;
                    $response["Remainder"] = $remainder;
                    return response($response, 200);
                }
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function delete_remainder(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();


            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'delete_remainder');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $remainder = Remainder::where('user_id',$loginUser['user_id'])->first();
                if ($remainder){
                    $remainder->delete();
                }
                $response["Result"] = 1;
                $response["Message"] = "Remainder restored";
                return response($response, 200);

            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function send_message_cronjob(){
        /* IOS PUSH NOTIFICATION */

        $get_unread_message = Message::where('is_read',0)->where('is_notify',0)->get();

        foreach ($get_unread_message as $k => $value){
            $appuser = User::findorfail($value['sender']);
            $receiver_lists = Login_details::where('user_id',$value['receiver'])->get();
            if ($value['type']=='text'){
                $message1 = $value['message'];
            }
            else{
                $message1 = "has send image.";
            }


            foreach ($receiver_lists as $k => $values){
                $device_char =  strlen($values['device_token']);

                //$op['message'] = $message1;

                $title = $appuser->name." says:";
                $message = $message1;

                if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                    $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                        'badge' => 1,
                        'title'     => $title,
                        'sound' => 'example.aiff',
                        'actionLocKey' => 'Action button title!',
                        'locKey' => 'localized key',
                        'custom' => array('type' => 'inbox_screen','send_to'=>'','booking_id'=>'','salon_id'=>'')
                    ));

                    \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                        ->to($values['device_token'])
                        ->send($message);
                }

                if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                    //$this->firebase_notification($title,$message,$values['device_token']);
                    $this->firebase_notification($title,$message,$values['device_token'],'inbox_screen','','','');
                }

                $imsg = Message::findOrFail($value['id']);
                $input['is_notify'] = 1;
                $imsg->update($input);
            }
        }
    }

    public function admin_remainder_cronjob()
    {
        /* IOS PUSH NOTIFICATION */
        /* unread Notification message cron jobs */
        $get_notification = Notification::where('is_read',0)->where('notification_type','admin')->get();
        foreach ($get_notification as $value)
        {
            if ($value['send_to']=='all'){

                if ($value['service_type']=='both' || $value['service_type']=='push'){

                    /* PUSH NOTIFICATION */
                    $user_list = Login_details::where('device_token','!=','')->get();
                    foreach ($user_list as $values){
                        $device_char =  strlen($values['device_token']);
                        //$op['message'] = $value['message'];

                        $title = config('siteVars.title')." Support says";
                        $message = $value['message'];

                        if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                            $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                                'badge' => 1,
                                'title' => $title,
                                'sound' => 'example.aiff',
                                'actionLocKey' => 'Action button title!',
                                'locKey' => 'localized key'
                            ));

                            \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                ->to($values['device_token'])
                                ->send($message);
                        }

                        if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                            $this->firebase_notification($title,$message,$values['device_token']);
                        }

                    }
                }
                if ($value['service_type']=='both' || $value['service_type']=='email'){

                    /* EMAIL */
                    $user_list = User::where('email','!=','')->where('is_email_verified','1')->get();
                    foreach ($user_list as $values){

                        $em= $values['email'];
                        $bcc = [];
                        $register_email=Email::findorFail(3);
                        $subject = $register_email['subject'];
                        $from1 = $register_email['email'];

                        $string = ["{message}"];
                        $replace_string = [$value['message']];

                        $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                        $data['title'] = $register_email['title'];

                        Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                            $m->from($from1, config('siteVars.title'));
                            $m->bcc($bcc);
                            $m->to($em)->subject($subject);
                        });
                    }
                }

            }
            else if($value['send_to']=='sub_admin'){
                $get_owner = User::whereIn('membership_plan',[1,3])->orWhereIn('admin_membership_plan',[1,3])->get();
                foreach ($get_owner as $val){
                    if ($value['service_type']=='both' || $value['service_type']=='push') {
                        $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $val['id'])->get();
                        foreach ($user_list as $values) {
                            $device_char = strlen($values['device_token']);
                            //$op['message'] = $value['message'];

                            $title = config('siteVars.title')." Support says";
                            $message = $value['message'];

                            if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                    'badge' => 1,
                                    'title' => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key'
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                                $this->firebase_notification($title,$message,$values['device_token']);
                            }
                        }
                    }

                    if ($value['service_type']=='both' || $value['service_type']=='email'){
                        /* EMAIL */
                        if($val['email']!="" && $val['is_email_verified']==1){
                            $em= $val['email'];
                            $bcc = [];
                            $register_email=Email::findorFail(3);
                            $subject = $register_email['subject'];
                            $from1 = $register_email['email'];

                            $string = ["{message}"];
                            $replace_string = [$value['message']];

                            $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                            $data['title'] = $register_email['title'];

                            Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                                $m->from($from1, config('siteVars.title'));
                                $m->bcc($bcc);
                                $m->to($em)->subject($subject);
                            });
                        }
                    }
                }
            }
            else if($value['send_to']=='user'){
                $get_users = User::where('role', 'user')->whereNotIn('membership_plan',[1,3])->whereNotIn('admin_membership_plan',[1,3])->where('status', 'active')->get();

                /* NOTIFICATION */
                foreach ($get_users as $users) {
                    if ($value['service_type']=='both' || $value['service_type']=='push') {

                        $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $users['id'])->get();
                        foreach ($user_list as $values) {
                            $device_char = strlen($values['device_token']);
                            // $op['message'] = $value['message'];
                            $title = config('siteVars.title')." Support says";
                            $message = $value['message'];
                            if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                    'badge' => 1,
                                    'title' => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key'
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                                $this->firebase_notification($title,$message,$values['device_token']);
                            }
                        }
                    }

                    if ($value['service_type']=='both' || $value['service_type']=='email'){

                        /* EMAIL */
                        if($users['email']!="" && $users['is_email_verified']==1) {
                            $em = $users['email'];
                            $bcc = [];
                            $register_email = Email::findorFail(3);
                            $subject = $register_email['subject'];
                            $from1 = $register_email['email'];

                            $string = ["{message}"];
                            $replace_string = [$value['message']];

                            $data['content'] = str_replace($string, $replace_string, $register_email["content"]);
                            $data['title'] = $register_email['title'];

                            Mail::send('admin.email_template', $data, function ($m) use ($from1, $subject, $em, $bcc) {
                                $m->from($from1, config('siteVars.title'));
                                $m->bcc($bcc);
                                $m->to($em)->subject($subject);
                            });
                        }
                    }
                }
            }

            $imsg = Notification::findOrFail($value['id']);
            $input['is_read'] = 1;
            $imsg->update($input);
        }
    }

    public function user_remainder_cronjob(){
        /* IOS PUSH NOTIFICATION */

        /* REMAINDER message cron jobs */
        $now = date('Y-m-d');
        $time = date('H:i');
        $get_remainder = Remainder::where('is_notify',1)->where('remainder_date',$now)->get();

        foreach ($get_remainder as $value){

            if($value['timezone'] != '') {
                date_default_timezone_set($value['timezone']);
            } else {
                date_default_timezone_set('America/Chicago');
            }

            $date_time = Carbon::now();
            $today = strtolower($date_time->format('H:i'));
            $date_today = $date_time->format('Y-m-d');

            $remainder_time = date('H:i',strtotime($value['remainder_time']));
            if ($remainder_time == $today){
                $user_list = Login_details::where('user_id',$value['user_id'])->where('device_token','!=','')->get();
                foreach ($user_list as $values){
                    $device_char =  strlen($values['device_token']);

                    $title = 'Remainder';
                    $message = $value['title'];

                    if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                        $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                            'badge' => 1,
                            'title'     => $title,
                            'sound' => 'example.aiff',
                            'actionLocKey' => 'Action button title!',
                            'locKey' => 'localized key',
                            'custom' => array('type' => 'remainder_screen','send_to'=>'user','booking_id'=>'','salon_id'=>'')
                        ));

                        \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                            ->to($values['device_token'])
                            ->send($message);
                    }

                    if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                        $this->firebase_notification($title,$message,$values['device_token'],'remainder_screen','user','','');
                    }
                }
            }
        }
    }

    public function booking_notification(){
        $date_time = Carbon::now();
        $date_today = $date_time->format('Y-m-d');

        $bookings = Booking::where('date',$date_today)
            ->whereIn('is_notify',[1,4])->get();

//        $bookings = Booking::where('date',$date_today)
//            ->where('is_notify',0)
//            ->Where(function ($query){
//                $query->where('status', 'waiting')
//                    ->orwhere('status', 'in-progress');
//            })->get();

        foreach($bookings as $booking){
            /* PUSH NOTIFICATION */
            $saloon = saloon::where('id', $booking['saloon_id'])->where('status', 'active')->first();
            if($booking['status']=='waiting' || $booking['status']=='in-progress') {
                if ($booking['checkin_type'] == 0 || $booking['checkin_type'] == 2) {
                    if (!empty($saloon)) {
                        $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $booking['user_id'])->get();
                        foreach ($user_list as $values) {
                            $device_char = strlen($values['device_token']);

                            $title = 'Booking success';
                            $message = "You have successfully Check-in to " . $saloon['title'] . " with Booking ID " . $booking['booking_name'];
                            //$op['message'] = $request['name'];
                            if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                    'badge' => 1,
                                    'title' => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key',
                                    'custom' => array('type' => 'booking_history_screen', 'send_to' => 'user', 'booking_id' => '', 'salon_id' => '')
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
                                $this->firebase_notification($title, $message, $values['device_token'], 'booking_history_screen', 'user', '', '');
                            }
                        }
                    }

                    $get_user = User::where('id', $booking['user_id'])->first();
                    if ($get_user['phone'] != '') {

                        $sms_text = "Thank you for using " . config('siteVars.title') . ", tap the link to check your waiting time: " . config('siteVars.display_url') . "code/" . $saloon->id . "/" . $saloon->code;

                        if (substr($get_user['phone'], 0, 1) == 1) {
                            $clickatell = new \Clickatell\Rest();
                            $other_country = 'no';
                            $datas = ['to' => [$get_user['phone']], 'from' => '15732073611', 'content' => $sms_text];
                        } else {
                            $clickatell = new \Clickatell\Rest();
                            $other_country = 'yes';
                            $datas = ['to' => [$get_user['phone']], 'content' => $sms_text];
                        }
                        $result = $clickatell->sendMessage($datas, $other_country);
                    }
                }
                if ($booking['checkin_type'] == 1) {
                    /* PUSH NOTIFICATION */
                    $get_user = User::where('id', $booking['user_id'])->first();
                    if (!empty($saloon)) {

                        $get_salon_owner_list = saloon_employees::where('saloon_id', $booking['saloon_id'])->where('is_salon_owner', 1)->get();
                        foreach ($get_salon_owner_list as $vl) {
                            $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $vl['user_id'])->get();
                            foreach ($user_list as $values) {
                                $device_char = strlen($values['device_token']);

                                $title = 'New Booking';
                                $message = ucfirst($get_user['name']) . " successfully Check-in with Booking ID " . $booking['booking_name'];
                                //$op['message'] = $request['name'];
                                if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                    $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                        'badge' => 1,
                                        'title' => $title,
                                        'sound' => 'example.aiff',
                                        'actionLocKey' => 'Action button title!',
                                        'locKey' => 'localized key',
                                        'custom' => array('type' => 'waiting_list_screen', 'send_to' => 'owner', 'booking_id' => '', 'salon_id' => '')
                                    ));

                                    \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                        ->to($values['device_token'])
                                        ->send($message);
                                }

                                if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
                                    $this->firebase_notification($title, $message, $values['device_token'], 'waiting_list_screen', 'owner', '', '');
                                }

                            }
                        }
                    }

                    $sms_text = "Thank you for using " . config('siteVars.title') . ", tap the link to check your waiting time: " . config('siteVars.display_url') . "code/" . $saloon['id'] . "/" . $saloon['code'] . ". Download " . config('siteVars.title') . " App " . config('siteVars.siteurl') . "app-download";

                    if (substr($get_user['phone'], 0, 1) == 1) {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'no';
                        $datas = ['to' => [$get_user['phone']], 'from' => '15732073611', 'content' => $sms_text];
                    } else {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'yes';
                        $datas = ['to' => [$get_user['phone']], 'content' => $sms_text];
                    }
                    $result = $clickatell->sendMessage($datas, $other_country);


                    /* Email to user */

                    $booking_details = Booking_details::where('booking_id', $booking['id'])->get();
                    $services = "";
                    foreach ($booking_details as $k => $v) {
                        $service_type = Service_type::where('id', $v['type_id'])->first();
                        if ($service_type) {
                            $services = $services . $service_type->title . ",";
                        }
                    }
                    $services = trim($services, ",");

                    if ($get_user['email'] != "") {
                        $em = $get_user['email'];
                        $bcc = [];
                        $register_email = Email::findorFail(6);
                        $subject = $register_email['subject'];
                        $from1 = $register_email['email'];

                        $person = $booking['no_of_persons'];
                        $name = $get_user['name'];
                        $salon = $saloon['title'];
                        $booking_id = $booking['booking_name'];
                        $service = $services;
                        $date = date('m/d/y', strtotime($booking['date']));

                        $dlink = config('siteVars.display_url') . "code/" . $saloon->id . "/" . $saloon['code'];
                        $string = ["{booking_id}", "{person}", "{name}", "{salon}", "{service}", "{date}", "{display_link}"];
                        $replace_string = [$booking_id, $person, $name, $salon, $service, $date, $dlink];

                        $data['content'] = str_replace($string, $replace_string, $register_email["content"]);
                        $data['title'] = $register_email['title'];


                        Mail::send('admin.email_template', $data, function ($m) use ($from1, $subject, $em, $bcc) {
                            $m->from($from1, config('siteVars.title'));
                            $m->bcc($bcc);
                            $m->to($em)->subject($subject);
                        });
                    }
                }
            }

            if ($booking['status']=='completed'){

                /* SEND PUSH NOTIFICATION TO USERS */

                $user_list = Login_details::where('device_token', '!=', '')->where('user_id',$booking['user_id'])->get();
                foreach ($user_list as $values) {
                    $device_char = strlen($values['device_token']);

                    $title = "Service completed successfully!";
                    $message = "You service in ".$saloon['title']." has completed successfully, rate now your experience.";

                    if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                        $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                            'badge' => 1,
                            'title'     => $title,
                            'sound' => 'example.aiff',
                            'actionLocKey' => 'Action button title!',
                            'locKey' => 'localized key',
                            'custom' => array('type' => 'add_rating_screen','send_to'=>'user','booking_id'=>"".$booking['id'],'salon_id' => array('salon_id' => "".$saloon['id'], 'salon_title'=>$saloon['title']))
                        ));

                        \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                            ->to($values['device_token'])
                            ->send($message);
                    }

                    if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                        //$this->firebase_notification($title,$message,$values['device_token']);
                        $this->firebase_notification($title,$message,$values['device_token'],'add_rating_screen','user',"".$booking['id'],"".$saloon['id'],$saloon['title']);
                    }
                }
            }

            $inp['is_notify'] = 0;
            $booking->update($inp);
        }

        $bookings = Booking::where('date',$date_today)
            ->whereIn('is_notify',[2,3])->onlyTrashed()->get();
        foreach($bookings as $booking){

            $booking_details = Booking_details::where('booking_id', $booking['id'])->onlyTrashed()->get();
            $services = "";
            foreach ($booking_details as $k=>$v){
                $service_type = Service_type::where('id',$v['type_id'])->first();
                if ($service_type){
                    $services = $services.$service_type->title.",";
                }
            }

            $saloon = saloon::where('id', $booking['saloon_id'])->where('status', 'active')->first();
            if($booking['status']=='cancel'){

                /* CANCEL BOOKING BOOKING BY OWNER */
                $get_salon = saloon::where('id',$booking['saloon_id'])->first();
                if (!empty($get_salon)) {
                    if ($booking['is_notify']==3) {  // cancel_by_salon_owner

                        $user_list = Login_details::where('device_token','!=','')->where('user_id',$booking['user_id'])->get();
                        foreach ($user_list as $values){
                            $device_char =  strlen($values['device_token']);
                            $title = 'Booking Cancelled';
                            $message = "Unfortunately " . $get_salon['title'] . " salon rejected you booking";
                            //$op['message'] = $request['name'];
                            if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                    'badge' => 1,
                                    'title' => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key',
                                    'custom' => array('type' => 'booking_history_screen','send_to'=>'user','booking_id'=>'','salon_id'=>'')
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
                                //$this->firebase_notification($title, $message, $values['device_token']);
                                $this->firebase_notification($title,$message,$values['device_token'],'booking_history_screen','user','','');
                            }
                        }
                    } else { // cancel by user "is_notify" = 2

                        $get_salon_owner_list = saloon_employees::where('saloon_id',$get_salon['id'])->where('is_salon_owner',1)->get();
                        $appuser = User::where('id',$booking['user_id'])->first();
                        foreach ($get_salon_owner_list as $vl){
                            $user_list = Login_details::where('device_token','!=','')->where('user_id',$vl['user_id'])->get();
                            foreach ($user_list as $values){
                                $device_char =  strlen($values['device_token']);
                                $title = 'Booking Cancelled';
                                $message = "Unfortunately " . $appuser['name'] . " cancelled booking";
                                //$op['message'] = $request['name'];
                                if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                                    $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                        'badge' => 1,
                                        'title' => $title,
                                        'sound' => 'example.aiff',
                                        'actionLocKey' => 'Action button title!',
                                        'locKey' => 'localized key',
                                        'custom' => array('type' => 'waiting_list_screen','send_to'=>'owner','booking_id'=>'','salon_id','')
                                    ));

                                    \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                        ->to($values['device_token'])
                                        ->send($message);
                                }

                                if ($values['device_type'] == 'android' && $values['device_token'] != "" && $device_char >= 20) {
                                    //$this->firebase_notification($title, $message, $values['device_token']);
                                    $this->firebase_notification($title,$message,$values['device_token'],'waiting_list_screen','owner','','');
                                }
                            }
                        }
                    }
                }

                $appuser = User::where('id',$booking['user_id'])->first();

                if ($appuser['email']!="") {
                    /* Email to user */

                    $em= $appuser['email'];
                    $bcc = [];
                    $register_email=Email::findorFail(5);
                    $subject = $register_email['subject'];
                    $from1 = $register_email['email'];

                    $person = $booking['no_of_persons'];
                    $name = $appuser['name'];
                    $salon = $get_salon['title'];
                    $booking_id = $booking['booking_name'];
                    $date = date('m/d/y',strtotime($booking['date']));

                    $string = ["{booking_id}","{person}","{name}","{salon}","{service}","{date}"];
                    $replace_string = [$booking_id,$person,$name,$salon,$services,$date];

                    $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                    $data['title'] = $register_email['title'];

                    Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                        $m->from($from1, config('siteVars.title'));
                        $m->bcc($bcc);
                        $m->to($em)->subject($subject);
                    });
                }
            }

            $inp['is_notify'] = 0;
            $booking->update($inp);

        }

    }

    public function add_feedback(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'email' => 'required',
                'comment' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_feedback');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();

                $input['user_id'] = $loginUser['user_id'];
                if(!empty($input['visit_date'])) {
                    $input['visit_date'] = Carbon::parse($input['visit_date'])->format('Y-m-d');
                }
                Feedback::create($input);
                $response['Message'] = "Feedback added successfully!";
                $response["Result"] = 1;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_rewards(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
                'name' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_rewards');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $input['salon_id'] = $saloon['id'];

                if(!empty($input['start_date'])) {
                    $input['start_date'] = Carbon::parse($input['start_date'])->format('Y-m-d');
                }
                if(!empty($input['end_date'])) {
                    $input['end_date'] = Carbon::parse($input['end_date'])->format('Y-m-d');
                }

                Salon_reward::create($input);
                $response['Message'] = "Reward added successfully!";
                $response["Result"] = 1;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function edit_rewards(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'id' => 'required',
                'salon_id' => 'required'
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'edit_rewards');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $get_reward = Salon_reward::where('id',$request['id'])->first();

                if(!empty($input['start_date'])) {
                    $input['start_date'] = Carbon::parse($input['start_date'])->format('Y-m-d');
                }
                if(!empty($input['end_date'])) {
                    $input['end_date'] = Carbon::parse($input['end_date'])->format('Y-m-d');
                }

                $get_reward->update($input);
                $response['Message'] = "Reward update successfully!";
                $response["Result"] = 1;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function delete_rewards(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'id' => 'required',
                'salon_id' => 'required'
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'delete_rewards');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $get_reward = Salon_reward::where('id',$request['id'])->delete();
                $response['Message'] = "Reward deleted successfully!";
                $response["Result"] = 1;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function reward_lists(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'salon_id' => 'required'
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $input = $request->all();
            $saloon = saloon::where('id', $request['salon_id'])->where('status','active')->first();

            if ($saloon){
                $lists = Salon_reward::where('salon_id',$saloon->id)->get();
                foreach ($lists as $k => $val){
                    $lists[$k]['start_date']=Carbon::parse($val['start_date'])->format('m/d/Y');
                    $lists[$k]['end_date']=Carbon::parse($val['end_date'])->format('m/d/Y');
                }

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Rewards'] = $lists;
                $response['loyalty_program'] = $saloon['loyalty_program'];
                return response($response, 200);
            }
            else{
                $response["Result"] = 0;
                $response['Message'] = "Success";
                $response['Rewards'] = [];
                $response['loyalty_program'] = 0;
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* MY rewards list -> list of all salon with final reward total  salon list*/
    public function my_reward_salon_list(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'my_reward_salon_list');

                return response($response, 200);
            }
            if(!empty($loginUser)) {
                //$point_history = Point_history::with('Salon.Salon_rewards')->where('user_id',$loginUser->user_id)->where('is_last','1')->get();

                $point_history = Point_history::select('point_histories.*')->with('Salon.Salon_rewards')
                    ->join('saloons', 'point_histories.salon_id', '=', 'saloons.id')
                    ->where('point_histories.user_id',$loginUser->user_id)
                    ->where('point_histories.is_last','1')
                    ->where('saloons.deleted_at',null)
                    ->get();

                foreach ($point_history as $k => $v){
                    $point_history[$k]['add_date'] = date('d M, Y',strtotime($v['created_at']));
                }
                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Rewards'] = $point_history;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* point history list by salon_id of login user*/
    public function my_reward_list_by_salon(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'my_reward_list_by_salon');

                return response($response, 200);
            }
            if(!empty($loginUser)) {
                $point_history = Point_history::where('salon_id',$request['salon_id'])->where('user_id',$loginUser->user_id)->orderBy('id','DESC')->get();

                foreach ($point_history as $k => $v){
                    $point_history[$k]['add_date'] = date('d M, Y',strtotime($v['created_at']));
                }

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Rewards'] = $point_history;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* salon owner login can view customer point history by passsing customer id*/
    public function customer_point_history(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'customer_point_history');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $point_history = Point_history::where('salon_id',$saloon->id)
                        ->where('user_id',$request['user_id'])
                        ->orderBy('id', 'DESC')->get();
                }

                $response["Result"] = 1;
                $response['Message'] = "success";
                $response["Booking"] = $point_history;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* SEARCH USER / SEARCH EMPLOYEE -> search by phonenumber and email to become his employee and send invitation */
    public function search_user(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'keyword' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'search_user');

                return response($response, 200);
            }
            if(!empty($loginUser)) {
                $keyword = $request['keyword'];
                $user = User::where('id','!=',$loginUser['user_id'])->where('saloon_id',0)
                    ->where(function ($query) use ($keyword) {
                        $query->orWhere('email', $keyword);
                        $query->orWhere('phone', '91'.$keyword);
                        $query->orWhere('phone', '1'.$keyword);
                        $query->orWhere('phone', '61'.$keyword);
                    })->first();

                if (empty($user)){
                    $response["Result"] = 0;
                    $response['Message'] = "No user found!";
                    return response($response, 200);
                }

                if ($user['image'] != "" && file_exists($user['image'])) {
                    $user['image'] = url($user['image']);
                }

                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['User'] = $user;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* SEARCH USER / SEARCH USER  -> search by phonenumber and name */
    public function search_existing_user(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'keyword' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'search_user');

                return response($response, 200);
            }
            if(!empty($loginUser)) {
                $keyword = $request['keyword'];
                $user = User::where('id','!=',$loginUser['user_id'])->where('id','!=')
                    ->where(function ($query) use ($keyword) {
                        $query->orwhere('email', 'like', '%'.$keyword.'%');
                        $query->orWhere('phone', 'like', '%'.$keyword.'%');
                        $query->orWhere('name', 'like', '%'.$keyword.'%');
                    })->take(50)->orderBy('name','ASC')->get();

                if (empty($user)){
                    $response["Result"] = 0;
                    $response['Message'] = "No user found!";
                    return response($response, 200);
                }

                foreach ($user as $val){
                    if ($val['image'] != "" && file_exists($val['image'])) {
                        $val['image'] = url($val['image']);
                    }
                }

                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['User'] = $user;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /* ADD / REMOVE POINT FROM SELLER LOGIC
      Seller can add/ remove customer points  */

    public function reward_point_add_remove(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'point' => 'required',
                'type' => 'required',
                'type' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'reward_point_add_remove');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $get_points = Point_history::where('user_id',$request['user_id'])->where('salon_id',$saloon->id)->orderBy('id','DESC')->first();

                $input['user_id'] = $request['user_id'];
                $input['salon_id'] = $saloon->id;
                $input['type'] = $request['type'];
                $input['point'] = $request['point'];
                $input['is_last'] = 1;
                if(!empty($get_points))
                {
                    $inp['is_last'] = 0;
                    $get_points->update($inp);

                    $input['point_before'] = $get_points->point_after;
                    if($request['type'] == 'CREDIT')
                    {
                        $input['point_after'] = $request['point'] + $input['point_before'];
                    }
                    else{
                        if($get_points->point_after >= $request['point'])
                        {
                            $input['point_after'] =  $input['point_before'] - $request['point'];
                        }
                        else
                        {
                            $response["Result"] = 0;
                            $response['Message'] = "Point can not be less then current point!";
                            return response($response, 200);
                        }
                    }
                }
                else
                {
                    $input['point_before'] = 0;
                    $input['point_after'] = $request['point'];
                }
                $input['remark'] = $request['point']." ".$request['type'].' by salon owner';

                $point = Point_history::create($input);

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Rewards'] = $point;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    /*  GET CUSTOMER CURRENT POINT
        Salon Owner login will get customer current point by passing customer user_id */

    public function get_customer_reward_point(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_customer_reward_point');

                return response($response, 200);
            }
            if(!empty($loginUser)) {

                $input = $request->all();
                $salon_id = $input['salon_id'];
                $saloon = saloon::where('id', $salon_id)->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $get_points = Point_history::where('user_id',$request['user_id'])->where('salon_id',$saloon->id)->orderBy('id','DESC')->first();

                if(!empty($get_points))
                {
                    $current_point = $get_points->point_after;
                }
                else
                {
                    $current_point = 0;
                }

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Available_point'] = $current_point;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function send_invitation(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'send_invitation');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                } else {

                    $check_invitation = saloon_employees::where('user_id',$request['user_id'])->where('saloon_id',$saloon->id)->first();
                    if (!empty($check_invitation)){
                        $response["Result"] = 0;
                        $response['Message'] = "Invitation already sent!";
                        return response($response, 200);
                    }
                    else{
                        $input['saloon_id']  = $saloon->id;
                        $input['user_id']  = $request['user_id'];
                        $input['status']  = "pending";
                        saloon_employees::create($input);

                        $invite_user = User::where('id',$request['user_id'])->first();

                        /*Send Email*/
                        if (!empty($invite_user['email'])){
                            $em= $invite_user['email'];
                            $bcc = [];
                            $register_email=Email::findorFail(8);
                            $name = $invite_user['name'];
                            $link = config('siteVars.siteurl')."send_invitation_email?ids=".$appuser['remember_token']."";
                            $subject = $register_email['subject'];
                            $from1 = $register_email['email'];

                            $string = ["{name}", "{salon_name}"];
                            $replace_string = [$name, $saloon['title']];

                            $data['content'] = str_replace($string,$replace_string,$register_email["content"]);
                            $data['title'] = $register_email['title'];

                            Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em,$bcc) {
                                $m->from($from1, config('siteVars.title'));
                                $m->bcc($bcc);
                                $m->to($em)->subject($subject);
                            });
                        }

                        /*Send Message*/
                        if(!empty($invite_user['phone'])){
                            if(substr($request['phone'], 0, 1) == 1) {
                                $clickatell = new \Clickatell\Rest();
                                $other_country = 'no';
                                $datas = ['to' => [$request['phone']], 'from' => '15732073611', 'content' => $saloon['title'].' has sent invitaion to become his employee'];
                            } else {
                                $clickatell = new \Clickatell\Rest();
                                $other_country = 'yes';
                                $datas = ['to' => [$request['phone']], 'content' => $saloon['title'].' has sent invitaion to become his employee'];
                            }
                            $result = $clickatell->sendMessage($datas,$other_country);
                        }

                        /*Send Push Notification */
                        $user_list = Login_details::where('user_id',$request['user_id'])->get();
                        foreach ($user_list as $values){
                            $device_char =  strlen($values['device_token']);
                            //$op['message'] = $saloon['title']." has sent invitaion to become his employee.\n";

                            $title = "Invitation from ".$saloon['title']."";
                            $message = $saloon['title']." request for become employee";

                            if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                                $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                                    'badge' => 1,
                                    'title'     => $title,
                                    'sound' => 'example.aiff',
                                    'actionLocKey' => 'Action button title!',
                                    'locKey' => 'localized key',
                                    'custom' => array('type' => 'notification_accept_reject_screen','send_to'=>'user','booking_id'=>'','salon_id'=>'')
                                ));

                                \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                    ->to($values['device_token'])
                                    ->send($message);
                            }

                            if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                                //$this->firebase_notification($title,$message,$values['device_token']);
                                $this->firebase_notification($title,$message,$values['device_token'],'notification_accept_reject_screen','user','','');
                            }
                        }
                    }
                }

                $response["Result"] = 1;
                $response["Message"] = "Invitation sent successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function send_invitation_direct(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
                'phone' => 'required',
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'send_invitation');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $input=$request->all();
                unset($input['remember_token']);

                $userphone = User::where('phone',$request['phone'])->first();
                if($userphone)
                {
                    if($userphone['saloon_id'] !=0 || $userphone['saloon_id']!="")
                    {
                        $response["Result"] = 0;
                        $response['Message'] = "Employee already working with other salon";
                        return response($response, 200);
                    }
                    else{
                        $input1['saloon_id'] = $input['salon_id'];
                        $userphone->update($input1);

                        $input['saloon_id'] = $input['salon_id'];
                        $input['user_id'] = $userphone['id'];
                        $input['status'] = "accept";
                        $input['is_read'] = 1;
                        saloon_employees::create($input);
                    }
                }
                else
                {
                    $input['saloon_id']=$request['salon_id'];
                    $userphone = User::create($input);

                    $input['saloon_id'] = $input['salon_id'];
                    $input['user_id'] = $userphone['id'];
                    $input['status'] = "accept";
                    $input['is_read'] = 1;
                    saloon_employees::create($input);
                }


                /* ENTRY IN ATTENDANCE TABLE */

                $user_id = $userphone['id'];
                $current_date  = Carbon::now()->format('Y-m-d');
                $start_time  = Carbon::now()->format('H:i');
                $end_time  = Carbon::now()->format('H:i');

                $attendance = Attendance::where('user_id',$user_id)
                    ->where('date',$current_date)
                    ->where('salon_id',$input['salon_id'])
                    ->where('end_time',null)->first();

                if ($attendance) {
                    $attendance->delete();
                }

                $input1['user_id'] = $user_id;
                $input1['salon_id'] = $input['salon_id'];
                $input1['date'] = $current_date;
                $input1['start_time'] = $start_time;
                $attendance = Attendance::create($input1);

                /*---------------------------*/

                $response["Result"] = 1;
                $response["Message"] = "Employee Added Successfully";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function accept_reject_invitation(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'id' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'accept_reject_invitation');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $get_invitation = saloon_employees::where('id',$request['id'])->where('status','pending')->first();
                if (!empty($get_invitation)){
                    if ($request['status'] == "accept") {
                        $input['status'] = "accept";
                        $get_invitation->update($input);

                        /* ASSIGN SALON TO USER TO BECOME EMPLOYEE */
                        $inp['saloon_id'] = $get_invitation['saloon_id'];
                        $appuser->update($inp);
                        $response["Message"] = "Invitation accepted successfully!";

                        /* REMOVE OTHER INVITATION */
                        $get_invitation = saloon_employees::where('user_id',$appuser->id)->where('status','pending')->delete();

                    }

                    if($request['status'] == "reject") {
                        $get_invitation->delete();
                        $response["Message"] = "Invitation rejected!";
                    }
                }
                else{
                    $response["Message"] = "Invitation Not Found!";
                }

                $get_invitation_list = saloon_employees::with('saloon_master')->where('user_id',$appuser['id'])->where('status','pending')->get();
                foreach($get_invitation_list as $get_in)
                {
                    $get_in['saloon_master']['image'];
                    if ($get_in['saloon_master']['image'] != "" && file_exists($get_in['saloon_master']['image'])) {
                        $get_in['saloon_master']['image'] = url($get_in['saloon_master']['image']);
                    }
                }

                $response["Result"] = 1;
                $response["Invitation"] = $get_invitation_list;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function employee_list(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'employee_list');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)
                    ->where('status','active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                } else {

                    if(isset($request['is_present'])){
                        $get_employee = saloon_employees::select('saloon_employees.*')->with('user_master')
                            ->join('users', 'saloon_employees.user_id', '=', 'users.id')
                            //->where('saloon_employees.user_id','!=',$appuser->id)
                            ->where('saloon_employees.status','accept')
                            ->where('saloon_employees.saloon_id',$saloon['id'])
                            ->where('users.available','present')
                            ->orderBy('users.name','ASC')
                            ->get();
                    }
                    else{
                        $get_employee = saloon_employees::select('saloon_employees.*')->with('user_master')
                            ->join('users', 'saloon_employees.user_id', '=', 'users.id')
                            ->where('saloon_employees.user_id','!=',$appuser->id)
                            ->where('saloon_employees.status','accept')
                            ->where('saloon_employees.saloon_id',$saloon['id'])
                            ->orderBy('users.name','ASC')
                            ->get();
                    }

//                    $get_employee = saloon_employees::with('user_master')->where('saloon_id',$saloon['id'])->where('user_id','!=',$appuser->id)->get();
                    foreach ($get_employee as $k=>$v){
                        if ($v['user_master']['image'] != "" && file_exists($v['user_master']['image'])) {
                            $v['user_master']['image'] = url($v['user_master']['image']);
                        }

                        $current_date  = Carbon::now()->format('Y-m-d');
//                        $get_attendance = Attendance::where('user_id',$v['user_id'])
//                            ->where('salon_id',$saloon['id'])
//                            ->where('date',$current_date)
//                            ->where('end_time',null)
//                            ->where('total_time',null)
//                            ->first();

                        $emp_status = 0;
                        if ($v['user_master']['available']=="present"){
                            $emp_status = 1;
                        }
                        $get_employee[$k]['is_online'] = $emp_status;

                        array_walk_recursive($v['user_master'], function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }
                }

                $response["Result"] = 1;
                $response["Message"] = "Success";
                $response["Employee"] = $get_employee;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function my_invitation_list(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),
                ['remember_token' => 'required',
                ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }
            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();
            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'my_invitation_list');
                return response($response, 200);
            }
            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $get_invitation = saloon_employees::with('saloon_master')->where('user_id',$appuser['id'])->where('status','pending')->get();
                foreach($get_invitation as $get_in)
                {

                    $get_in['saloon_master']['image'];
                    if ($get_in['saloon_master']['image'] != "" && file_exists($get_in['saloon_master']['image'])) {
                        $get_in['saloon_master']['image'] = url($get_in['saloon_master']['image']);
                    }

                }
                $response["Result"] = 1;
                $response["Message"] = "Success";
                $response["Invitation"] = $get_invitation;
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function my_accept_invitation_list(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),
                [
                    'remember_token' => 'required',
                    'salon_id' => 'required',
                ]);
            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }
            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();
            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                $input = $request->all();
                $this->errorlog($response["Message"], json_encode($input), 'my_accept_invitation_list');
                return response($response, 200);
            }
            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $get_salon = saloon::where('id',$request['salon_id'])->first();
                if (!empty($get_salon)){
                    $get_invitation= saloon_employees::with('saloon_master')
                        ->join('saloons', 'saloon_employees.saloon_id', '=', 'saloons.id')
                        ->where('saloon_employees.user_id',$appuser['id'])
                        ->where('saloon_employees.status','accept')
                        ->where('saloon_employees.saloon_id','!=',$get_salon['id'])
                        ->where('saloons.status','active')
                        ->where('saloons.deleted_at',null)
                        ->get();
                }
                else{
                    $get_invitation= saloon_employees::select('saloon_employees.*')
                        ->with('saloon_master')
                        ->join('saloons', 'saloon_employees.saloon_id', '=', 'saloons.id')
                        ->where('saloon_employees.user_id',$appuser['id'])
                        ->where('saloon_employees.status','accept')
                        ->where('saloons.status','active')
                        ->where('saloons.deleted_at',null)
                        ->get();
                }
                foreach($get_invitation as $get_in)
                {
                    $get_in['saloon_master']['image'];
                    if ($get_in['saloon_master']['image'] != "" && file_exists($get_in['saloon_master']['image'])) {
                        $get_in['saloon_master']['image'] = url($get_in['saloon_master']['image']);
                    }

                    $get_inv = saloon_employees::find($get_in['id']);
                    $get_inv->update(['is_read'=>'1']);
                }

                $response["Result"] = 1;
                $response["Message"] = "Success";
                $response["Invitation"] = $get_invitation;
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function remove_employee(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'remove_employee');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $flag = 0;
                    $check_booking = Booking_details::with('Booking')->where('emp_id',$request['user_id'])->get();
                    foreach ($check_booking as $k=>$v){
                        $current_date  = date('Y-m-d');
                        if ($v['booking']['date']>=$current_date && ($v['booking']['status']=="waiting" || $v['booking']['status']=="in-progress"))
                        {
                            $flag = 1;
                        }
                    }

                    if ($flag==0){
                        $get_invitation = saloon_employees::where('user_id',$request['user_id'])->where('saloon_id',$saloon->id)->first();
                        if ($get_invitation){
                            $get_invitation->delete();
                        }

                        $get_user = User::where('id',$request['user_id'])->where('saloon_id',$saloon->id)->first();
                        if ($get_user){
                            $inp['saloon_id'] = 0;
                            $get_user->update($inp);
                        }

                        /* LOGOUT CURRENT REMOVED EMPLOYEE FROM  LIST IN LOGIN DETAIL TABLE */
                        $loginUser = Login_details::where('user_id',$request['user_id'])->delete();
                    }
                    else{
                        $response["Result"] = 0;
                        $response["Message"] = "Employee is under service!";
                        return response($response, 200);
                    }
                }

                $response["Result"] = 1;
                $response["Message"] = "Employee Removed Successfully";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);

        }
    }

    public function financial_report(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'remove_employee');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                $saloon = saloon::where('id', $request['salon_id'])->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $date_time = Carbon::now();
                    $today = strtolower($date_time->format('l'));
                    $date_today = $date_time->format('Y-m-d');
                    $booking = Booking::where('saloon_id', $request['salon_id'])->where('date', $date_today)->where('status','completed')->get();
                    $todays_booking = $booking->count();

                    $todays_sales = "0.00";
                    foreach ($booking as $val){
                        $todays_sales += $val['final_total'];
                    }

                    $new = new Collection();
                    $new->push(['layout' => '1', 'title' => "Today's \n Sales ",'key'=>"$".$todays_sales]);
                    $new->push(['layout' => '1', 'title' => "Today's Booking",'key'=>"".$todays_booking]);
                    $new->push(['layout' => '2', 'title' => "Employee Sales Report",'key'=>config('siteVars.siteurl').'api/salon_report/'.$request['salon_id']]);
                    $new->push(['layout' => '2', 'title' => "Service Sales Report",'key'=>'https://www.google.co.in/']);
                    $new->push(['layout' => '2', 'title' => "Sales \n Report",'key'=>'https://www.google.co.in/']);
                    $new->push(['layout' => '2', 'title' => "Sales by Check-in",'key'=>'https://www.google.co.in/']);

                    $response["Result"] = 1;
                    $response["Message"] = "Success";
                    $response["Financial_report"] = $new;
                    return response($response, 200);
                }
            }

        } catch (Exception $e) {
            return response("", 500);

        }
    }

    public function security_pin(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            $input = $request->all();
            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";


                $this->errorlog($response["Message"],json_encode($input),'profile_update');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'security_pin');

                    return response($response, 200);
                }

                if (isset($input['security_pin']) && $input['security_pin'] !="") {

                    $appuser->update($input);

                    $response["Result"] = 1;
                    $response["Message"] = "Security pin updated successfully!";
                    return response($response, 200);

                }


                if($appuser['security_pin']!="" || $appuser['security_pin']!=null){
                    $response["Result"] = 1;
                    $response["Message"] = "pin set";
                    return response($response, 200);
                }
                else{
                    $response["Result"] = 0;
                    $response["Message"] = "pin not set";
                    return response($response, 200);
                }
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }
}