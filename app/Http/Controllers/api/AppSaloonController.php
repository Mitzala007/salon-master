<?php
namespace App\Http\Controllers\api;
use App\Attendance;
use App\Booking;
use App\Booking_details;
use App\Category;
use App\cities;
use App\countries;
use App\Email;
use App\Employees;
use App\Favourites;
use App\Login_details;
use App\saloon;
use App\Saloon_services;
use App\Salon_owner_request;
use App\SalonImages;
use App\Point_history;
use App\Service_type;
use App\saloon_employees;
use App\states;
use App\Message;
use App\Rating;
use App\User;
use App\work_days;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Clickatell\Rest;
use Regulus\ActivityLog\Models\Activity;
use Clickatell\ClickatellException;
use Vsmoraes\Pdf\Pdf;
use Image;

class AppSaloonController extends Controller
{

    private $pdf;
    public function __construct(Request $request, Pdf $pdf)
    {
        $this->pdf = $pdf;
    }

    public function dashboard(Request $request)
    {
        try {
            //date_default_timezone_set('America/Chicago');
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'dashboard');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'dashboard');
                    return response($response, 200);
                }

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)
                    ->with('Saloon_services')
                    ->with('Saloon_services.services_master')
                    ->with('work_days')
                    //->with('Employees')
                    ->where('status','active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                } else {
                    $sid = $saloon->id;
                }

                if (count($saloon['Saloon_services'])) {

                    foreach ($saloon['Saloon_services'] as $k1=> $v1){
                        $discount_price = ($v1['charges'] * $v1['discount']) / 100;
                        $final_price  = $v1['charges'] - $discount_price;
                        $saloon['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                        $saloon['Saloon_services'][$k1]['service_name'] =  $v1['services_master']['title'];
                    }

                    $sal_services = $saloon['Saloon_services'];
                    unset($saloon->Saloon_services);
//
                    $salon_services =  $sal_services->sortBy('service_name')->toArray();
                    $saloon['saloon_services'] = array_values($salon_services);
                }

                $count_images = SalonImages::where('salon_id',$saloon->id)->count();
                $saloon['image_total'] = $count_images;

                if (file_exists($saloon['image']) && $saloon['image']!="") {
                    $saloon['image'] = url($saloon['image']);
                } else {
                    $saloon['image'] = '';
                }

                $status = 'close';
                $open_time  = '';
                $close_time = '';

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $today = strtolower($date_time->format('l'));
                $date_today = $date_time->format('Y-m-d');
                if($saloon['is_online'] == 1) {

                    if (count($saloon['work_days'])) {
                        foreach ($saloon['work_days'] as $wd) {
                            if ($wd['work_day'] == $today) {
                                $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                $status = 'open';
                                break;
                            }
                        }
                    }
                }

                if ($status == 'open') {
                    if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                        $status = 'open';
                    } else {
                        $status = 'close';
                    }

                }

                $saloon['status'] = $status;

                unset($appuser->password);
                unset($appuser->deleted_at);

                if ($appuser['gender'] == 'male') {
                    $img = url('assets/dist/img/male.png');
                } else if ($appuser['gender'] == 'female') {
                    $img = url('assets/dist/img/female.png');
                } else {
                    $img = url('assets/dist/img/other.png');
                }

                if ($appuser->image == '') {
                    $appuser->image = $img;
                } else {
                    $appuser->image = url($appuser->image);
                }

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $appuser->countryName = '';
                $appuser->stateName = '';
                $appuser->cityName = '';

                if (!empty($appuser->country)) {
                    $country = countries::where('countryID', $appuser->country)->first();
                    $appuser->countryName = $country->countryName;
                }

                if ($appuser->state_id > 0) {
                    $state = states::where('stateID', $appuser->state_id)->first();
                    $appuser->stateName = $state->stateName;
                }

                if ($appuser->city_id > 0) {
                    $state = cities::where('cityID', $appuser->city_id)->first();
                    $appuser->cityName = $state->cityName;
                }

                $plan = 0;
                $appuser['plan_id'] = 0;
                if ($appuser['admin_membership_plan'] != null){
                    $plan = $appuser['admin_membership_plan'];
                }
                else{
                    $plan = $appuser['membership_plan'];
                }
                $user['plan_id'] = $plan;

                /* UNREAD MESSAGE*/
                $message = Message::where('receiver', $appuser->id)->where('is_read', 0)->count();
                $w_count = Booking::where('saloon_id', $sid)->where('date', $date_today)->where('status', 'waiting')->count();
                $r_count = Booking::where('saloon_id', $sid)->where('date', '>', $date_today)->where('status', 'waiting')->count();

                /* CHECK LOGIN STATUS IN SALON*/
                $attendance = Attendance::where('user_id',$appuser->id)->where('salon_id',$saloon->id)->where('date',$date_today)->where('end_time',null)->first();
                /* ------------- */

                /* SALON EMPLOYEE */

                $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $request['salon_id'])->get();
                if (count($employees)) {
                    foreach ($employees as &$emp) {
                        if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                            $emp['nick_name'] = $emp['first_name'];
                        }

                        if(file_exists($emp['image'])) {
                            $emp['image'] = url($emp['image']);
                        } else {
                            $emp['image'] = '';
                        }
                        array_walk_recursive($emp, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }
                }

                $employees->prepend(['id' => 0, 'saloon_id' => $request['salon_id'], 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '']);

                $saloon['employees'] = $employees;
                /*-------------------------*/

                /* CATEGORY LISTING */

                $category = Category::where('status','active')->orderBy('displayorder','ASC')->get();
                $salon_services = Saloon_services::where('saloon_id',$request['salon_id'])->pluck('type_id');
                $service_type = Service_type::wherein('id',$salon_services)->groupBy('category_id')->pluck('category_id')->toArray();

                foreach ($category as $val) {
                    $val['is_check'] = 0;
                    if (in_array($val['id'],$service_type))
                    {
                        $val['is_check'] = 1;
                    }

                    if (file_exists($val['image']) && $val['image']!="") {
                        $val['image'] = url($val['image']);
                    } else {
                        $val['image'] = '';
                    }

                    array_walk_recursive($val, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }

                /* ---------------- */

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response["Salon"] = $saloon;
                $response["Unread_message"] = $message;
                $response['w_count'] = $w_count;
                $response['r_count'] = $r_count;
                $response["Category"] = $category;
                $response["login_time"] = "00:00";
                $response["login_status"] = "checkout";



                if ($attendance){
                    $end_time  = Carbon::now()->format('H:i');
                    $time_diff = $this->getTimeDiff($attendance['start_time'],$end_time);
                    $response["login_time"] = date('H:i',strtotime($time_diff));
                }

                if($appuser['available']=="present") {
                    $response["login_status"] = "checkin";
                }

                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function my_salon_list(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'dashboard');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $get_saon_ids = saloon_employees::where('user_id',$appuser['id'])->orderBy('is_salon_owner','DESC')->get();

                $salon_list = collect();
                foreach ($get_saon_ids as $k=>$v){

                    $salon_id = $request['salon_id'];
                    $saloon = saloon::where('id', $v['saloon_id'])
                        ->with('Saloon_services')
                        ->with('Saloon_services.services_master')
                        ->with('work_days')
                        ->where('status','active')->first();
                    $sid = $saloon->id;

                    $count_images = SalonImages::where('salon_id',$v['saloon_id'])->count();
                    $saloon['image_total'] = $count_images;

                    if (file_exists($saloon['image']) && $saloon['image']!="") {
                        $saloon['image'] = url($saloon['image']);
                    } else {
                        $saloon['image'] = '';
                    }

                    $status = 'close';
                    $open_time  = '';
                    $close_time = '';

                    if($saloon->timezone != '') {
                        date_default_timezone_set($saloon->timezone);
                    } else {
                        date_default_timezone_set('America/Chicago');
                    }

                    $date_time = Carbon::now();
                    $today = strtolower($date_time->format('l'));
                    $date_today = $date_time->format('Y-m-d');

                    if($saloon['is_online'] == 1) {

                        if (count($saloon['work_days'])) {

                            foreach ($saloon['work_days'] as $wd) {
                                if ($wd['work_day'] == $today) {
                                    $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                                    $close_time = Carbon::createFromTimeString($wd['end_hour']);
                                    $status = 'open';
                                    break;
                                }
                            }
                        }
                    }

                    if ($status == 'open') {
                        if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                            $status = 'open';
                        } else {
                            $status = 'close';
                        }
                    }

                    $saloon['status'] = $status;

                    /* is_owner */
                    $saloon['is_salon_owner'] = "0";
                    if ($v['is_salon_owner'] == 1){
                        $saloon['is_salon_owner'] = "1";
                    }

                    $salon_list->push($saloon);
                }


                /* ------------- */
                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response["Salon"] = $salon_list;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function walkin_book(Request $request)
    {
        try {

            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;
            }
            else{
                $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'user_id' => 'required',
                    'service_type' => 'required',
                    'no_of_persons' => 'required',
                    //'name' => 'required',
                    //'phone' => 'required',
                    //'email' => 'required',
                    'date' => 'required',
                    'salon_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $response["Result"] = 0;
                    $response["Message"] = implode(',', $validator->errors()->all());
                    return response($response, 200);
                }

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (empty($loginUser) || $request['remember_token'] == "") {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'walkin_book');
                    return response($response, 200);
                }
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser['user_id']);

                if ($appuser->status != 'active') {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account is blocked.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'walkin_book');

                    return response($response, 200);
                }

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)->where('status','active')->first();

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 401);
                }

                $input = $request->all();
                $types = explode(',', $input['service_type']);

                if (empty($input['date'])) {
                    $date_time = Carbon::now();
                    $date_today = $date_time->format('Y-m-d');
                    $input['date'] = $date_today;
                } else {
                    $input['date'] = Carbon::parse($input['date'])->format('Y-m-d');
                }

                $chk_booking = Booking::where('saloon_id', $saloon['id'])
                    ->where('user_id', $input['user_id'])
                    ->where('date', $input['date'])
                    ->Where(function ($query){
                        $query->where('status', 'waiting')
                            ->orwhere('status', 'in-progress');
                    })->count();

                if($chk_booking) {
                    $response["Result"] = 0;
                    $response['Message'] = "Salon already booked for " . $request['date'];
                    return response($response, 200);
                }

                $upd_user = 0;
                $upd_arr = array();

                $walkin_user = User::findorFail($input['user_id']);

                if (isset($input['email']) && $input['email'] !="") {
                    $otherUser = User::where("email", $input['email'])->where("id", '!=', $walkin_user->id)->first();
                    if (!empty($otherUser)) {
                        $response["Result"] = 0;
                        $response["Message"] = "Email address already registered with other user account.";
                        return response($response, 200);
                    }
                }

//                if (trim($walkin_user['name']) == '') {
//                    $upd_arr['name'] = $input['name'];
//                    $upd_user = 1;
//                }
//
//                if (trim($walkin_user['email']) == '') {
//                    $upd_arr['email'] = $input['email'];
//                    $upd_user = 1;
//                }
//
//                if ($upd_user == 1) {
//                    $walkin_user->update($upd_arr);
//                }

                $input['avail_discount'] = $saloon['global_discount'];
                $input['status'] = 'waiting';
                $input['saloon_id'] = $saloon['id'];

                $total_booking = Booking::count() + 1;
                $input['booking_name'] = config('siteVars.prefix')."-" . Carbon::today()->format("Ymd") . "-" . (10000 + $total_booking);

                $input['device_type'] = !empty($loginUser['device_type'])?$loginUser['device_type']:"";
                $input['device_token'] = !empty($loginUser['device_token'])?$loginUser['device_token']:"";
                $booking = Booking::create($input);

                $input['booking_id'] = $booking->id;

                /* ---------- ASSIGN EMPLOYEE ID IF NO PREFERENCE IS SELECT THEN CHECK EMPLOYEE EXIST (IF NO EMP THEN OWNER IS ASSING ELSE 0) ---------------  */
                if (isset($request['emp_id']) && $request['emp_id']==0){
                    /* CHECK EMPLOYEE */
                    $get_employee = saloon_employees::where('status','accept')
                        ->where('saloon_id',$saloon['id'])
                        ->where('is_salon_owner',0)
                        ->count();
                    if ($get_employee <= 0){
                        $get_owner = saloon_employees::where('status','accept')
                            ->where('saloon_id',$saloon['id'])
                            ->where('is_salon_owner',1)->first();
                        $input['emp_id'] = $get_owner['user_id'];
                    }
                }

                $sub_total = 0;
                foreach ($types as $type) {

                    $saloon_services = Saloon_services::where('saloon_id', $saloon['id'])->where('type_id', $type)->first();

                    if (empty($saloon_services)) {
                        continue;
                    }

                    $input['type_id'] = $type;
                    $input['duration'] = $saloon_services['duration'];

                    if($saloon['global_discount'] == 1) {
                        $input['avail_discount'] = $saloon_services['avail_discount'];
                        $input['discount_type'] = 'percent';
                        $input['discount_value'] = $saloon_services['discount'];
                        $input['charges'] = $saloon_services['charges'];

                        $total_charges = ($saloon_services['charges'] * $saloon_services['discount'])/100;
                        $total_charge = $saloon_services['charges']-$total_charges;
                        $input['total_charge'] = $total_charge;
                        $sub_total = $sub_total + $input['total_charge'];
                    }
                    else{
                        $input['charges'] = $saloon_services['charges'];
                        $input['total_charge'] = $saloon_services['charges'];
                        $sub_total = $sub_total + $saloon_services['charges'];
                    }
                    Booking_details::create($input);
                }
                $inp['sub_total'] = $sub_total;
                $booking->update($inp);

                /* PUSH NOTIFICATION */

                if (!empty($saloon)){
                    $user_list = Login_details::where('device_token','!=','')->where('user_id',$input['user_id'])->get();
                    foreach ($user_list as $values){
                        $device_char =  strlen($values['device_token']);

                        $title = 'Booking success';
                        $message = "You have successfully Check-in to ".$saloon['title']." with Booking ID ".$booking['booking_name'];
                        //$op['message'] = $request['name'];
                        if ($values['device_type']=='ios' && $values['device_token']!="" && $device_char >=20 ) {
                            $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message( $message,array(
                                'badge' => 1,
                                'title' => $title,
                                'sound' => 'example.aiff',
                                'actionLocKey' => 'Action button title!',
                                'locKey' => 'localized key',
                                'custom' => array('type' => 'booking_history_screen','send_to'=>'user','booking_id'=>'','salon_id'=>'')
                            ));

//                            \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                ->to($values['device_token'])
//                                ->send($message);
                        }

                        if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                            //$this->firebase_notification($title,$message,$values['device_token']);
                            //$this->firebase_notification($title,$message,$values['device_token'],'booking_history_screen','user','','');
                        }
                    }
                }

                if($walkin_user['phone'] != '') {

                    $sms_text = "Thank you for using ".config('siteVars.title').", tap the link to check your waiting time: ".config('siteVars.display_url')."code/".$saloon->id."/".$saloon->code;

                    if(substr($walkin_user['phone'], 0, 1) == 1) {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'no';
                        $datas = ['to' => [$walkin_user['phone']], 'from' => '15732073611', 'content' => $sms_text];
                    } else {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'yes';
                        $datas = ['to' => [$walkin_user['phone']], 'content' => $sms_text];
                    }

                    //$result = $clickatell->sendMessage($datas,$other_country);
                }

                $response["Result"] = 1;
                $response['Message'] = "Saloon Booked";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_bookings(Request $request)
    {
        try {

            // date_default_timezone_set('America/Chicago');

            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;

                $appuser = User::findorFail($user_id);
                $sid = $request['salon_id'];
                $saloon = saloon::where('id', $sid)
                    ->with('Saloon_services')
                    ->with('Saloon_services.services_master')
                    ->with('work_days')
                    ->where('status','active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
            }
            else{
                if (isset($request['remember_token']) && isset($request['salon_id'])) {
                    $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();
                    if (empty($loginUser) || $request['remember_token'] == "") {
                        $response["Result"] = 9;
                        $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                        $input = $request->all();
                        $this->errorlog($response["Message"],json_encode($input),'get_bookings');
                        return response($response, 200);
                    }

                    $appuser = User::findorFail($loginUser->user_id);
                    $sid = $request['salon_id'];
                    $saloon = saloon::where('id', $sid)
                        ->with('Saloon_services')
                        ->with('Saloon_services.services_master')
                        ->with('work_days')
                        ->where('status','active')->first();

                    if (empty($saloon)) {
                        $response["Result"] = 0;
                        $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                        return response($response, 200);
                    }

                } else {

                    $validator = Validator::make($request->all(), [
                        'salon_id' => 'required',
                        'code' => 'required',
                    ]);

                    if ($validator->fails()) {
                        $response["Result"] = 0;
                        $response["Message"] = implode(',', $validator->errors()->all());
                        return response($response, 200);
                    }

                    $saloon = saloon::where('id', $request['salon_id'])
                        ->with('Saloon_services')
                        ->with('Saloon_services.services_master')
                        ->with('work_days')
                        ->where('status','active')->first();

                    if (empty($saloon)) {
                        $response["Result"] = 0;
                        $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                        return response($response, 200);
                    }

                    if ($request['code'] != $saloon['code']) {
                        $response["Result"] = 0;
                        $response['Message'] = "Invalid code.";
                        return response($response, 200);
                    }
                    $sid = $saloon->id;

                }


            }


            if (count($saloon['Saloon_services'])) {

                foreach ($saloon['Saloon_services'] as $k1=> $v1){
                    $discount_price = ($v1['charges'] * $v1['discount']) / 100;
                    $final_price  = $v1['charges'] - $discount_price;
                    $saloon['Saloon_services'][$k1]['discount_price'] =  number_format((float)$final_price, 2, '.', '');
                    $saloon['Saloon_services'][$k1]['service_name'] =  $v1['services_master']['title'];
                }

                $sal_services = $saloon['Saloon_services'];
                unset($saloon->Saloon_services);
//
                $salon_services =  $sal_services->sortBy('service_name')->toArray();
                $saloon['saloon_services'] = array_values($salon_services);
            }

            $count_images = SalonImages::where('salon_id',$saloon->id)->count();
            $saloon['image_total'] = $count_images;

            if (file_exists($saloon['image']) && $saloon['image']!="") {
                $saloon['image'] = url($saloon['image']);
            } else {
                $saloon['image'] = '';
            }

            $status = 'close';
            $open_time  = '';
            $close_time = '';

            if($saloon->timezone != '') {
                date_default_timezone_set($saloon->timezone);
            } else {
                date_default_timezone_set('America/Chicago');
            }

            $date_time = Carbon::now();
            $today = strtolower($date_time->format('l'));
            if($saloon['is_online'] == 1) {

                if (count($saloon['work_days'])) {
                    foreach ($saloon['work_days'] as $wd) {
                        if ($wd['work_day'] == $today) {
                            $open_time  = Carbon::createFromTimeString($wd['start_hour']);
                            $close_time = Carbon::createFromTimeString($wd['end_hour']);
                            $status = 'open';
                            break;
                        }
                    }
                }
            }

            if ($status == 'open') {
                if ($date_time->gte($open_time) && $date_time->lte($close_time)) {
                    $status = 'open';
                } else {
                    $status = 'close';
                }

            }

            $saloon['status'] = $status;



            /* SALON EMPLOYEE */

            $employees = User::select('id', 'saloon_id', 'name as first_name', 'nick_name', 'status', 'available', 'image')->where('available','present')->where('saloon_id', $request['salon_id'])->get();
            if (count($employees)) {
                foreach ($employees as &$emp) {
                    if($emp['nick_name'] == NULL || empty($emp['nick_name'])){
                        $emp['nick_name'] = $emp['first_name'];
                    }

                    if(file_exists($emp['image'])) {
                        $emp['image'] = url($emp['image']);
                    } else {
                        $emp['image'] = '';
                    }

                    $emp['emp_wait_time'] = "10 min";

                    array_walk_recursive($emp, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }
            }

            $employees->prepend(['id' => 0, 'saloon_id' => $request['salon_id'], 'first_name' => 'No Preference', 'nick_name' => 'No Preference', 'status' => 'active', 'available' => 'present', 'image' => '','emp_wait_time'=>'']);

            $saloon['employees'] = $employees;
            /*-------------------------*/


            $date_time = Carbon::now();
            $date_today = $date_time->format('Y-m-d');

            $p_employees = User::where('saloon_id', $sid)->where('available', 'present')->count();

            if ($p_employees == 0) {
                $p_employees = 1;
            }

            $s_bookings = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $sid)->where('date', $date_today)->where('status', 'in-progress')->orderBy('id', 'asc')->get();

            $time_frame = 0;
            $buffer_time = 5; // 10 minutes buffer time.

            $start_end = Array();

            foreach ($s_bookings as $book_key => $book_val) {

                $bid_duration = 0;
                $s_bookings[$book_key]['date'] = date('m/d/y',strtotime($book_val['date']));
                $s_bookings[$book_key]['checkin_date'] = date('m/d/y h:i A',strtotime($book_val['created_at']));
                $s_bookings[$book_key]['tax'] = $saloon['tax'];

                foreach ($book_val['Booking_details'] as $book_details) {
                    $time_req = ($book_details['duration'] * $book_val['no_of_persons']);
                    // $time_frame   += $time_req;
                    $bid_duration += $time_req;
                    $s_bookings[$book_key]['services'] .= $book_details['service_type']['title'] . ', ';

                    if(! empty($book_details['emp_id'])) {
                        $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                        $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                        $exp_person_id = $expert['id'];
                        $exp_person_nickname = trim($expert['nick_name']);
                    } else {
                        $exp_person = 'No preference';
                        $exp_person_nickname = 'No preference';
                        $exp_person_id = 0;
                    }

                    $book_details['service_expert'] = $exp_person;
                    $book_details['service_expert_id'] = $exp_person_id;

                    if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                        $book_details['service_expert_nickname'] = $expert['first_name'];
                    }
                    else{
                        $book_details['service_expert_nickname'] = $exp_person_nickname;
                    }
                }

                $remaining = $date_time->diffInSeconds(Carbon::parse($book_val['start_time'])->addMinutes($bid_duration), false);

                if ($remaining <= 0) {
                    // $remaining = abs($remaining) + ($buffer_time * 60);
                    $last_service = $book_val['Booking_details']->last();
                    $new_duration = ($last_service->duration) + ceil((abs($remaining) + ($buffer_time * 60)) / 60);
                    $s_expert = $last_service['service_expert'];

                    $nick_name = $last_service['service_expert_nickname'];
                    unset($last_service['service_expert']);
                    unset($last_service['service_expert_nickname']);
                    unset($last_service['service_expert_id']);

                    $last_service->update(array('duration' => $new_duration));
                    $last_service['service_expert'] = $s_expert;
                    $last_service['service_expert_nickname'] = $nick_name;
                    $remaining = ($buffer_time * 60);
                }

                foreach ($book_val['Booking_details'] as $book_details) {
                    array_walk_recursive($book_details, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }

                $start_end[] = $remaining;
                $s_bookings[$book_key]["remaining"] = gmdate('H:i', $remaining);
                $s_bookings[$book_key]["remaining"] = $this->humReadable($remaining / 60);
                $s_bookings[$book_key]['services'] = trim($s_bookings[$book_key]['services'], ', ');

                /* AVAILABLE POINTS */

                $available_point = Point_history::where('user_id',$book_val['user_id'])->where('salon_id',$saloon->id)->where('is_last',1)
                    ->orderBy('id','DESC')->first();

                if (!empty($available_point)){
                    $s_bookings[$book_key]['available_points'] = $available_point['point_after'];
                }
                else{
                    $s_bookings[$book_key]['available_points'] = 0;
                }

                $p_employees -= 1;



                array_walk_recursive($book_val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $timer = 0;
            sort($start_end);

            if (count($start_end)) {
                $timer = $start_end[0];
                $time_frame += ceil($timer / 60);
            }

            $w_bookings = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $sid)->where('date', $date_today)->where('status', 'waiting')->orderBy('id', 'asc')->get();
            $i = 1;

            foreach ($w_bookings as $book_key => $book_val) {
                $w_bookings[$book_key]['date'] = date('m/d/y',strtotime($book_val['date']));
                $w_bookings[$book_key]['checkin_date'] = date('m/d/y h:i A',strtotime($book_val['created_at']));
                $w_bookings[$book_key]['tax'] = $saloon['tax'];

                if ($i <= $p_employees) {
                    $w_bookings[$book_key]['wait_time'] = '0 min';
                    $i++;

                    foreach ($book_val['Booking_details'] as $book_details) {
                        $time_frame += ($book_details['duration'] * $book_val['no_of_persons']);
                        $w_bookings[$book_key]['services'] .= $book_details['service_type']['title'] . ', ';

                        if(! empty($book_details['emp_id'])) {
                            $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                            $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                            $exp_person_nickname = trim($expert['nick_name']);
                            $exp_person_id = $expert['id'];
                            // if ($saloon['user_id']==$expert['id']){
                            //     $exp_person = trim($expert['first_name']. ' ' . $expert['last_name'])." (Owner)";
                            //     $exp_person_nickname = trim($expert['nick_name'])." (Owner)";
                            // }

                        } else {
                            $exp_person = 'No preference'; // $saloon->title;
                            $exp_person_nickname ='No preference';
                            $exp_person_id = 0;
                        }

                        $book_details['service_expert'] = $exp_person;
                        $book_details['service_expert_id'] = $exp_person_id;

                        if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                            $book_details['service_expert_nickname'] = $expert['first_name'];
                        }
                        else{
                            $book_details['service_expert_nickname'] = $exp_person_nickname;
                        }

                        array_walk_recursive($book_details, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }

                    $w_bookings[$book_key]['services'] = trim($w_bookings[$book_key]['services'], ', ');

                    $book_val['checkin_date'] = date('m/d/y h:i A',strtotime($book_val['created_at']));

                    array_walk_recursive($book_val, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    continue;
                }

                $w_bookings[$book_key]['wait_time'] += $time_frame;
                $w_bookings[$book_key]['wait_time'] = $this->humReadable($w_bookings[$book_key]['wait_time']);

                foreach ($book_val['Booking_details'] as $book_details) {
                    $time_frame += ($book_details['duration'] * $book_val['no_of_persons']);
                    $w_bookings[$book_key]['services'] .= $book_details['service_type']['title'] . ', ';

                    if(! empty($book_details['emp_id'])) {
                        $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                        $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                        $exp_person_nickname = trim($expert['nick_name']);
                        $exp_person_id = $expert['id'];
                        // if ($saloon['user_id']==$expert['id']){
                        //     $exp_person = trim($expert['first_name']. ' ' . $expert['last_name'])." (Owner)";
                        //     $exp_person_nickname = trim($expert['nick_name'])." (Owner)";
                        // }
                    } else {
                        $exp_person = 'No preference'; // $saloon->title;
                        $exp_person_nickname ='No preference';
                        $exp_person_id = 0;
                    }

                    $book_details['service_expert'] = $exp_person;
                    $book_details['service_expert_id'] = $exp_person_id;

                    if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                        $book_details['service_expert_nickname'] = $expert['first_name'];
                    }
                    else{
                        $book_details['service_expert_nickname'] = $exp_person_nickname;
                    }

                    array_walk_recursive($book_details, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }

                $w_bookings[$book_key]['services'] = trim($w_bookings[$book_key]['services'], ', ');
                $i++;

                array_walk_recursive($book_val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }
            
            $message = 0;
            if($request['remember_token'] !="") {
                $message = Message::where('receiver', $appuser->id)->where('is_read', 0)->count();
            }

            $w_count = Booking::where('saloon_id', $sid)->where('date', $date_today)->where('status', 'waiting')->count();
            $r_count = Booking::where('saloon_id', $sid)->where('date', '>', $date_today)->where('status', 'waiting')->count();

            $response["Result"] = 1;
            $response["serving"] = $s_bookings;
            $response["waiting"] = $w_bookings;
            $response["date_time"] = $date_time;
            $response["timer"] = gmdate('H:i', $timer);
            $response["p_emp"] = $p_employees;
            $response["Unread_message"] = $message;
            $response['w_count'] = $w_count;
            $response['r_count'] = $r_count;
            $response['Message'] = "Success";

            $response["salon"] = $saloon;
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function booking_history(Request $request)
    {
        try {

            // date_default_timezone_set('America/Chicago');

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'booking_history');
                return response($response, 200);
            }

            $appuser = User::findorFail($loginUser->user_id);

//            if ($appuser->role != 'sub_admin') {
//                $response["Result"] = 0;
//                $response["Message"] = "Not a saloon owner";
//                return response($response, 200);
//            }

            //$saloon = saloon::where('user_id', $appuser->id)->first();
            $saloon = saloon::where('id', $request['salon_id'])->first();

            if (empty($saloon)) {
                $response["Result"] = 0;
                $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                return response($response, 200);
            }

            $sid = $saloon->id;

            if($saloon->timezone != '') {
                date_default_timezone_set($saloon->timezone);
            } else {
                date_default_timezone_set('America/Chicago');
            }

            $date_time = Carbon::now();
            $date_today = $date_time->format('Y-m-d');

            $p_employees = User::where('saloon_id', $sid)->where('available', 'present')->count();

            if ($p_employees == 0) {
                $p_employees = 1;
            }

            /* SEARCH QUERY */

            $query = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->select();
            $query->where('saloon_id', $sid);
            $query->where('status', 'completed');

            if (isset($request['start_date']) && isset($request['end_date'])) {
                $start_date = date('Y-m-d', strtotime($request['start_date']));
                $end_date = date('Y-m-d', strtotime($request['end_date']));
                $query->where('date','>=',$start_date);
                $query->where('date','<=',$end_date);
            }

            if (isset($request['user_id'])){
                $query->where('user_id',$request['user_id']);
            }

            $s_bookings = $query->orderBy('id', 'DESC')->get();

            /* ----------------------- */

            $time_frame  = 0;
            $buffer_time = 5; // 10 minutes buffer time.

            $start_end = Array();

            foreach ($s_bookings as $book_key => $book_val) {

                $bid_duration = 0;
                $s_bookings[$book_key]['date'] = date('m/d/y',strtotime($book_val['date']));

                foreach ($book_val['Booking_details'] as $book_details) {
                    $time_req = ($book_details['duration'] * $book_val['no_of_persons']);
                    // $time_frame   += $time_req;
                    $bid_duration += $time_req;
                    $s_bookings[$book_key]['services'] .= $book_details['service_type']['title'] . ', ';

                    if(! empty($book_details['emp_id'])) {
                        $expert = User::select('id','name as first_name', 'last_name','nick_name')->where('id', $book_details['emp_id'])->first();
                        $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                        $exp_person_nickname = trim($expert['nick_name']);
                        $exp_person_id = $expert['id'];
                        // if ($saloon['user_id']==$expert['id']){
                        //     $exp_person = trim($expert['first_name']. ' ' . $expert['last_name'])." (Owner)";
                        //     $exp_person_nickname = trim($expert['nick_name'])." (Owner)";
                        // }
                    } else {
                        $exp_person = 'No preference'; // $saloon->title;
                        $exp_person_nickname = 'No preference';
                        $exp_person_id = 0;
                    }

                    $book_details['service_expert'] = $exp_person;
                    $book_details['service_expert_id'] = $exp_person_id;
                    if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                        $book_details['service_expert_nickname'] = $expert['first_name'];
                    }
                    else{
                        $book_details['service_expert_nickname'] = $exp_person_nickname;
                    }

                    if ($book_details['discount_type']==null){
                        $book_details['discount_type'] = "";
                    }
                    if ($book_details['discount_value']==null){
                        $book_details['discount_value'] = "";
                    }
                }

                array_walk_recursive($book_val['Users'], function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $book_val['completed_date'] = date('m/d/Y, h:i a',strtotime($book_val['updated_at']));

                $remaining = $date_time->diffInSeconds(Carbon::parse($book_val['start_time'])->addMinutes($bid_duration), false);

                if ($remaining <= 0) {
                    // $remaining = abs($remaining) + ($buffer_time * 60);
                    $last_service = $book_val['Booking_details']->last();
                    $new_duration = ($last_service->duration) + ceil((abs($remaining) + ($buffer_time * 60)) / 60);
                    $s_expert = $last_service['service_expert'];
                    $nick_name = $last_service['service_expert_nickname'];
                    unset($last_service['service_expert']);
                    unset($last_service['service_expert_nickname']);
                    unset($last_service['service_expert_id']);
                    $last_service->update(array('duration' => $new_duration));
                    $last_service['service_expert'] = $s_expert;
                    $last_service['service_expert_nickname'] = $nick_name;
                    $remaining = ($buffer_time * 60);
                }

                $start_end[] = $remaining;

                $s_bookings[$book_key]["remaining"] = gmdate('H:i', $remaining);
                $s_bookings[$book_key]["remaining"] = $this->humReadable($remaining / 60);
                $s_bookings[$book_key]['services'] = trim($s_bookings[$book_key]['services'], ', ');


                /* AVAILABLE POINTS */

                $available_point = Point_history::where('user_id',$book_val['user_id'])->where('salon_id',$saloon->id)->where('is_last',1)
                    ->orderBy('id','DESC')->first();

                if (!empty($available_point)){
                    $s_bookings[$book_key]['available_points'] = $available_point['point_after'];
                }
                else{
                    $s_bookings[$book_key]['available_points'] = 0;
                }

                $p_employees -= 1;
                array_walk_recursive($book_val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });
            }

            $timer = 0;

            sort($start_end);

            if (count($start_end)) {
                $timer = $start_end[0];
                $time_frame += ceil($timer / 60);
            }

            $response["Result"] = 1;
            $response["history"] = $s_bookings;
            $response["date_time"] = $date_time;
            $response["timer"] = gmdate('H:i', $timer);
            $response["p_emp"] = $p_employees;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function set_technician(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'salon_id' => 'required',
                    'bid' => 'required',
                    'tnew' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }

            $appuser = User::findorFail($loginUser->user_id);

            $salon_id = $request['salon_id'];
            $saloon = saloon::where('id', $salon_id)
                ->where('status','active')->first();

            if (empty($saloon)) {
                $response["Result"] = 0;
                $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                return response($response, 200);
            }

            $sid  = $saloon->id;
            $bid  = $request['bid'];
            $tnew = $request['tnew'];

            if($saloon->timezone != '') {
                date_default_timezone_set($saloon->timezone);
            } else {
                date_default_timezone_set('America/Chicago');
            }

            $date_time = Carbon::now();
            $date_today = $date_time->format('Y-m-d');

            $w_bookings = Booking::with('Booking_details')->where('saloon_id', $sid)->where('id', $bid)->where('status', 'waiting')->where('date', $date_today)->first();

            if (empty($w_bookings)) {
                $response["Result"] = 0;
                $response['Message'] = "Booking not found.";
                return response($response, 200);
            }

            if($tnew > 0) {

                $expert_busy = DB::select("SELECT b.* FROM `bookings` b left JOIN booking_details bd ON bd.booking_id = b.id WHERE b.status = 'in-progress' AND b.date = '".$date_today."' AND saloon_id = '".$sid."' AND bd.emp_id = '".$tnew."'");

                if(! count($expert_busy)) {

                    Booking_details::where('booking_id', $bid)->update(['emp_id' => $tnew]);

                    $ip['updated_at'] = date('Y-m-d H:i:s');
                    $w_bookings->update($ip);

                    if (isset($request['serve_now']) && $request['serve_now'] ==1){
                        $request['booking_id'] = $request['bid'];
                        return $this->serve_now($request);
                    }
                    $response["Result"] = 1;
                    $response['Message'] = "Technician updated successfully!";
                    return response($response, 200);




                } else {

                    $response["Result"] = 0;
                    $response['Message'] = "Service Expert not free at present, select another technician.";
                    return response($response, 200);

                }

            } else {

                $response["Result"] = 0;
                $response['Message'] = "Select new technician.";
                return response($response, 200);

            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function serve_now(Request $request)
    {
        try {
            // date_default_timezone_set('America/Chicago');
            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;
            }
            else{
                $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'booking_id' => 'required',
                    'salon_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $response["Result"] = 0;
                    $response["Message"] = implode(',', $validator->errors()->all());
                    return response($response, 200);
                }

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (empty($loginUser) || $request['remember_token'] == "") {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'serve_now');
                    return response($response, 200);
                }
            }


            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser['user_id']);
                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)
                    ->with('Saloon_services')
                    ->with('Saloon_services.services_master')
                    ->with('work_days')
                    ->with('Employees')
                    ->where('status','active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $sid = $saloon->id;

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                $bid = $request['booking_id'];
                $booking = Booking::where('id', $bid)->with('Booking_details')->where('saloon_id', $sid)->where('status', 'waiting')->where('date', $date_today)->first();

                if (empty($booking)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Booking not found.";
                    return response($response, 200);
                }

                /*--------------- SET TECHNECIAN ----------------*/
//                $tnew = isset($request['tnew'])?$request['tnew']:0;
//                if($tnew > 0) {
//
//                    $expert_busy = DB::select("SELECT b.* FROM `bookings` b left JOIN booking_details bd ON bd.booking_id = b.id WHERE b.status = 'in-progress' AND b.date = '".$date_today."' AND saloon_id = '".$sid."' AND bd.emp_id = '".$tnew."'");
//
//                    if(! count($expert_busy)) {
//
//                        Booking_details::where('booking_id', $bid)->update(['emp_id' => $tnew]);
//
//                        $ip['updated_at'] = date('Y-m-d H:i:s');
//                        $booking->update($ip);
//                    } else {
//
//                        $response["Result"] = 0;
//                        $response['Message'] = "Service Expert not free at present, select another technician.";
//                        return response($response, 200);
//                    }
//                }

                /*-------------------------------*/

                $expert_id = intval($booking->booking_details[0]->emp_id);

                if($expert_id > 0) {

                    $expert_busy  = DB::select("SELECT b.* FROM `bookings` b left JOIN booking_details bd ON bd.booking_id = b.id WHERE b.status = 'in-progress' AND b.date = '".$date_today."' AND saloon_id = '".$sid."' AND bd.emp_id = '".$expert_id."'");
                    if(! count($expert_busy)) {
                        $input['start_time'] = $date_time;
                        $input['status'] = 'in-progress';
                        $booking->update($input);

                        Booking_details::where('booking_id', $request['booking_id'])->update(['emp_id' => $expert_id]);

                    } else {
                        $response["Result"] = 0;
                        $response['Message'] = "Service Expert not free at present, you need to wait further.";
                        return response($response, 200);
                    }
                } else {
                    $free_employee = User::where('saloon_id', $sid)->where('available', 'present')->pluck('id')->toArray();
                    $s_bookings = Booking::select('id')->where('saloon_id', $sid)->where('date', $date_today)->where('status', 'in-progress')->orderBy('id', 'asc')->get();
                    $b_details = Booking_details::select('emp_id')->whereIn('booking_id', $s_bookings)->distinct()->get();

                    foreach ($b_details as $b_detail) {

                        if (($key = array_search($b_detail->emp_id, $free_employee)) !== false) {
                            unset($free_employee[$key]);
                        }
                    }

                    $free_employee = array_values($free_employee);

                    if (empty($free_employee[0])) {
                        $response["Result"] = 0;
                        $response['Message'] = "None of the employees are free to serve.";
                        return response($response, 200);
                    }

                    $input['start_time'] = $date_time;
                    $input['status'] = 'in-progress';

                    $booking->update($input);

                    Booking_details::where('booking_id', $request['booking_id'])->update(['emp_id' => $free_employee[0]]);
                }


                /* SEND PUSH NOTIFICATION IF SALON OWNER IS FREELANCER */
                if($appuser['membership_plan']==2 || $appuser['admin_membership_plan']==2){
                    $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $booking['user_id'])->get();
                    foreach ($user_list as $values) {
                        $device_char = strlen($values['device_token']);

                        $title = "Booking confirmed!";
                        $message = "Freelance on the way!, check live location.";

                        if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
                            $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
                                'badge' => 1,
                                'title'     => $title,
                                'sound' => 'example.aiff',
                                'actionLocKey' => 'Action button title!',
                                'locKey' => 'localized key',
                                'custom' => array('type' => 'check_in_screen','send_to'=>'user','booking_id'=>"".$booking['id'],'salon_id'=>'')
                            ));

                            \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
                                ->to($values['device_token'])
                                ->send($message);
                        }

                        if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
                            //$this->firebase_notification($title,$message,$values['device_token']);
                            $this->firebase_notification($title,$message,$values['device_token'],'check_in_screen','user',"".$booking['id'],'');
                        }
                    }
                }

                /* SERVICE DONE NOTIFICATION */
//                $get_salon = saloon::where('id', $booking['saloon_id'])->first();
//                if (!empty($get_salon)) {
//                    $user_list = Login_details::where('device_token', '!=', '')->where('user_id', $booking['user_id'])->get();
//                    foreach ($user_list as $values) {
//                        $device_char = strlen($values['device_token']);
//
//                        $op['message'] = "";
//                        $title = "Service Done";
//                        $message = "";
//
//                        if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
//                            $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
//                                'badge' => 1,
//                                'title'     => $title,
//                                'sound' => 'example.aiff',
//                                'actionLocKey' => 'Action button title!',
//                                'locKey' => 'localized key'
//                            ));
//
//                            \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                                ->to($values['device_token'])
//                                ->send($message);
//                        }
//
//                        if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
//                            $this->firebase_notification($title,$message,$values['device_token']);
//                        }
//                    }
//                }

                $response["Result"] = 1;
                $response["countries"] = '';
                $response['Message'] = "Technician updated successfully!";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function end_now(Request $request)
    {
        try {

            // date_default_timezone_set('America/Chicago');

            if($request['type']=="web"){
                $user_id = $request['user_id'];
                $loginUser['user_id'] = $user_id;
            }
            else{
                $validator = Validator::make($request->all(), [
                    'remember_token' => 'required',
                    'booking_id' => 'required',
                    'salon_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $response["Result"] = 0;
                    $response["Message"] = implode(',', $validator->errors()->all());
                    return response($response, 200);
                }

                $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

                if (empty($loginUser) || $request['remember_token'] == "") {
                    $response["Result"] = 9;
                    $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                    $input = $request->all();
                    $this->errorlog($response["Message"],json_encode($input),'end_now');

                    return response($response, 200);
                }
            }

            if (!empty($loginUser)) {


                $appuser = User::findorFail($loginUser['user_id']);

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)
                    ->with('Saloon_services')
                    ->with('Saloon_services.services_master')
                    ->with('work_days')
                    ->with('Employees')
                    ->where('status', 'active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact " . config('siteVars.title') . " support.";
                    return response($response, 200);
                } else {
                    $sid = $saloon->id;
                }

                if ($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                $bid = $request['booking_id'];

                $booking = Booking::with('Users')
                    ->with('Booking_details','Saloon')
                    ->with('Booking_details.Service_type')
                    ->where('id', $bid)->where('saloon_id', $sid)->where('date', '<=', $date_today)->first();

                if (empty($booking)) {
                    $response["Result"] = 0;
                    $response['Message'] = "Booking not found.";
                    return response($response, 200);
                }

                if ($booking->status == 'completed') {


                    /* BOOKING BLOCK IN RESPONCE*/

                    $taxval = ($booking['final_total'] * $booking['tax'])/ 100;
                    $booking['taxValue'] = number_format($taxval, 2, '.', '');

                    $bid_duration = 0;
                    foreach ($booking['Booking_details'] as $book_details) {
                        $time_req = ($book_details['duration'] * $booking['no_of_persons']);
                        // $time_frame   += $time_req;
                        $bid_duration += $time_req;

                        if(! empty($book_details['emp_id'])) {
                            $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                            $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                            $exp_person_id = $expert['id'];
                            $exp_person_nickname = trim($expert['nick_name']);
                        } else {
                            $exp_person = 'No preference';
                            $exp_person_nickname = 'No preference';
                            $exp_person_id = 0;
                        }


                        $discount_amount = "00.00";
                        if ($book_details['discount_value'] != 0){
                            $discount_amount = ($book_details['charges'] * $book_details['discount_value']) / 100;
                        }

                        $book_details['discount_amount'] =  number_format($discount_amount, 2, '.', '');
                        $book_details['total_charge'] =  number_format($book_details['total_charge'], 2, '.', '');

                        $booking['service_expert'] = $exp_person;
                        $booking['service_expert_id'] = $exp_person_id;

                        if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                            $booking['service_expert_nickname'] = $expert['first_name'];
                        }
                        else{
                            $booking['service_expert_nickname'] = $exp_person_nickname;
                        }
                        array_walk_recursive($book_details, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }

                    if(substr($booking['saloon']['saloon_phone'], 0, 1) == 1) {
                        $str = substr($booking['saloon']['saloon_phone'], 1);
                        $booking['saloon']['formated_salon_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $str);;
                    }else{
                        $booking['saloon']['formated_salon_phone'] = $booking['saloon']['saloon_phone'];
                    }

					array_walk_recursive($booking['saloon'], function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    $booking['sub_total'] = number_format($booking['sub_total'], 2, '.', '');
                    $booking['discount_amount'] = number_format($booking['discount_amount'], 2, '.', '');
                    $booking['additional_amount'] = number_format($booking['additional_amount'], 2, '.', '');
                    $booking['final_total'] = number_format($booking['final_total'], 2, '.', '');
                    $booking['tax'] = number_format($booking['tax'], 2, '.', '');
                    $booking['order'] = "Order: #".sprintf("%02d", $booking['order_id']);

                    $response["Result"] = 0;
                    $response['Message'] = "Service already completed.";
                    $response['Date_time'] = date('l, M d, Y, h:i A');
                    $response['Booking'] = $booking;

                    /* GENERATE PDF REPORT */
                    $url = 'pdf/'.$booking['booking_name'].'.pdf';
                    $html = view('admin.booking_report',$response)->render();
                    $this->pdf->load($html)->filename(base_path($url))->output();

                    $response["Pdf"] =  url($url);
                    return response($response, 200);

                } else if ($booking->status == 'cancel') {

                    $response["Result"] = 0;
                    $response['Message'] = "Booking already cancelled.";
                    return response($response, 200);

                }


                if (isset($request['point']) && $request['point'] != "" && $request['point'] != 0) {
                    /* POINT USED IN BOOKING */
                    $get_points = Point_history::where('user_id', $booking->user_id)->where('salon_id', $saloon->id)->orderBy('id', 'DESC')->first();

                    if (empty($get_points) || $get_points['point_after'] < $request['point']) {
                        $response["Result"] = 0;
                        $response['Message'] = "No loyalty point available!";
                        return response($response, 200);
                    } else {

                        $inp['is_last'] = 0;
                        $get_points->update($inp);

                        $point = $request['point'];
                        $debit['user_id'] = $booking['user_id'];
                        $debit['salon_id'] = $saloon['id'];
                        $debit['booking_id'] = $booking['id'];
                        $debit['type'] = "DEBIT";
                        $debit['point_before'] = !empty($get_points->point_after) ? $get_points->point_after : 0;
                        $debit['point'] = $point;
                        $debit['point_after'] = $debit['point_before'] - $point;
                        $debit['remark'] = $point . " points debited on booking " . $booking['booking_name'];
                        $debit['is_last'] = 1;
                        Point_history::create($debit);
                    }
                }

                $input['end_time'] = $date_time;
                if (!isset($request['discount_amount']) || $request['discount_amount'] == 0) {
                    $input['discount_amount'] = 0;
                } else {
                    $input['discount_amount'] = $request['discount_amount'];
                }

                $input['point'] = isset($request['point']) ? $request['point'] : 0;
                $input['payment_mode'] = isset($request['payment_mode']) ? $request['payment_mode'] : 1;
                //$input['final_total'] = ($booking['sub_total'] - $input['discount_amount']);
                $input['final_total'] = isset($request['final_total']) ? $request['final_total'] : 00.00;
                $input['tax'] = isset($request['tax']) ? $request['tax'] : 00.00;
                $input['status'] = 'completed';
                $input['additional_amount'] = isset($request['additional_amount']) ? $request['additional_amount'] : 0;
                $input['remarks'] = isset($request['remarks']) ? $request['remarks'] : "";

                /* -------------- GET TODAYS BOOKING OF SELECTED SALON AND SET ORDER ID----------------- */
                $total_today_booking = Booking::where('saloon_id',$request['salon_id'])->where('date',$booking['date'])->where('status','completed')->count();
                $input['order_id'] = $total_today_booking + 1;
                $input['is_notify'] = 4;

                /* ----------------------------------------------------------------------*/

                $booking->update($input);

                //return $booking;

//                /* SEND PUSH NOTIFICATION TO USERS */
//                $user_list = Login_details::where('device_token', '!=', '')->where('user_id',$booking['user_id'])->get();
//                foreach ($user_list as $values) {
//                    $device_char = strlen($values['device_token']);
//
//                    $title = "Service completed successfully!";
//                    $message = "You service in ".$saloon['title']." has completed successfully, rate now your experience.";
//
//                    if ($values['device_type'] == 'ios' && $values['device_token'] != "" && $device_char >= 20) {
//                        $message = \Davibennun\LaravelPushNotification\Facades\PushNotification::Message($message, array(
//                            'badge' => 1,
//                            'title'     => $title,
//                            'sound' => 'example.aiff',
//                            'actionLocKey' => 'Action button title!',
//                            'locKey' => 'localized key',
//                            'custom' => array('type' => 'add_rating_screen','send_to'=>'user','booking_id'=>"".$booking['id'],'salon_id' => array('salon_id' => "".$saloon['id'], 'salon_title'=>$saloon['title']))
//                        ));
//
//                        \Davibennun\LaravelPushNotification\Facades\PushNotification::app('appNameIOS')
//                            ->to($values['device_token'])
//                            ->send($message);
//                    }
//
//                    if ($values['device_type']=='android' && $values['device_token']!="" && $device_char >=20 ) {
//                        //$this->firebase_notification($title,$message,$values['device_token']);
//                        $this->firebase_notification($title,$message,$values['device_token'],'add_rating_screen','user',"".$booking['id'],"".$saloon['id'],$saloon['title']);
//                    }
//                }

                /* IF tax field added then tax update in salon table */
                if ($input['tax'] > 0)
                {
                    $salon_data['tax'] = $input['tax'];
                    $saloon->update($salon_data);
                }

                /* POINT CREDITED BASED ON ORDER PRICE */

                if ($saloon['loyalty_program'] == 1){

                    $get_points = Point_history::where('user_id',$booking->user_id)->where('salon_id',$saloon->id)->orderBy('id','DESC')->first();
                    if (!empty($get_points)){
                        $point_before = $get_points->point_after;
                        $inp['is_last'] = 0;
                        $get_points->update($inp);
                    }
                    else{
                        $point_before = 0;
                    }
                    $point = ceil($input['final_total'])*10;

                    $credited['user_id'] = $booking['user_id'];
                    $credited['salon_id'] = $saloon['id'];
                    $credited['booking_id'] = $request['booking_id'];
                    $credited['type'] = "CREDIT";
                    $credited['point_before'] = $point_before;
                    $credited['point'] = $point;
                    $credited['point_after'] = $point+$credited['point_before'];
                    $credited['remark'] = $point. " points credited on booking ".$booking['booking_name'];
                    $credited['is_last'] = 1;

                    Point_history::create($credited);
                }

                /* BOOKING BLOCK IN RESPONCE*/

                $taxval = ($booking['final_total'] * $booking['tax'])/ 100;
                $booking['taxValue'] = number_format($taxval, 2, '.', '');

                $bid_duration = 0;
                foreach ($booking['Booking_details'] as $book_details) {
                    $time_req = ($book_details['duration'] * $booking['no_of_persons']);
                    // $time_frame   += $time_req;
                    $bid_duration += $time_req;

                    if(! empty($book_details['emp_id'])) {
                        $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                        $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                        $exp_person_id = $expert['id'];
                        $exp_person_nickname = trim($expert['nick_name']);
                    } else {
                        $exp_person = 'No preference';
                        $exp_person_nickname = 'No preference';
                        $exp_person_id = 0;
                    }


                    $discount_amount = "00.00";
                    if ($book_details['discount_value'] != 0){
                        $discount_amount = ($book_details['charges'] * $book_details['discount_value']) / 100;
                    }

                    $book_details['discount_amount'] =  number_format($discount_amount, 2, '.', '');
                    $book_details['total_charge'] =  number_format($book_details['total_charge'], 2, '.', '');

                    $booking['service_expert'] = $exp_person;
                    $booking['service_expert_id'] = $exp_person_id;

                    if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                        $booking['service_expert_nickname'] = $expert['first_name'];
                    }
                    else{
                        $booking['service_expert_nickname'] = $exp_person_nickname;
                    }
                    array_walk_recursive($book_details, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }


                if(substr($booking['saloon']['saloon_phone'], 0, 1) == 1) {
                    $str = substr($booking['saloon']['saloon_phone'], 1);
                    $booking['saloon']['formated_salon_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $str);;
                }else{
                    $booking['saloon']['formated_salon_phone'] = $booking['saloon']['saloon_phone'];
                }

				array_walk_recursive($booking['saloon'], function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                $booking['sub_total'] = number_format($booking['sub_total'], 2, '.', '');
                $booking['discount_amount'] = number_format($booking['discount_amount'], 2, '.', '');
                $booking['additional_amount'] = number_format($booking['additional_amount'], 2, '.', '');
                $booking['final_total'] = number_format($booking['final_total'], 2, '.', '');
                $booking['tax'] = number_format($booking['tax'], 2, '.', '');
                $booking['order'] = "Order: #".sprintf("%02d", $booking['order_id']);

                /*------------------------------------ */

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['Date_time'] = date('l, M d, Y, h:i A');
                $response['Booking'] = $booking;

                /* GENERATE PDF REPORT */
                $url = 'pdf/'.$booking['booking_name'].'.pdf';
                $html = view('admin.booking_report',$response)->render();
                $this->pdf->load($html)->filename(base_path($url))->output();

                $response["Pdf"] =  url($url);

                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function w_count(Request $request)  // booking waiting counter //
    {
        try {

            // date_default_timezone_set('America/Chicago');

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'w_count');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

//                if ($appuser->role != 'sub_admin') {
//                    $response["Result"] = 0;
//                    $response["Message"] = "Not a saloon owner";
//                    return response($response, 200);
//                }

                $sid = $request['salon_id'];
                $saloon = saloon::where('id', $sid)->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                $w_count = Booking::where('saloon_id', $sid)->where('date', $date_today)->where('status', 'waiting')->count();

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['w_count'] = $w_count;

                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function r_count(Request $request)
    {
        try {

            // date_default_timezone_set('America/Chicago');

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'r_count');
                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

//                if ($appuser->role != 'sub_admin') {
//                    $response["Result"] = 0;
//                    $response["Message"] = "Not a saloon owner";
//                    return response($response, 200);
//                }

                $sid = $request['salon_id'];
                $saloon = saloon::where('id', $sid)->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                $r_count = Booking::where('saloon_id', $sid)->where('date', '>', $date_today)->where('status', 'waiting')->count();

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['r_count'] = $r_count;

                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function r_details(Request $request)
    {
        try {

            // date_default_timezone_set('America/Chicago');

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'r_details');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

//                if ($appuser->role == 'sub_admin') {
//                    $saloon = saloon::where('user_id', $appuser->id)
//                        ->where('status','active')->first();
//                }

//                if ($appuser->role  == 'user'){
//                    $salon_id = $request['salon_id'];
//                    $saloon = saloon::where('id', $salon_id)
//                        ->where('status','active')->first();
//                }

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)
                    ->where('status','active')->first();

                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                } else {
                    $sid = $saloon->id;
                }

                if($saloon->timezone != '') {
                    date_default_timezone_set($saloon->timezone);
                } else {
                    date_default_timezone_set('America/Chicago');
                }

                $date_time = Carbon::now();
                $date_today = $date_time->format('Y-m-d');

                $r_details = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->where('saloon_id', $sid)->where('date', '>', $date_today)->where('status', 'waiting')->orderBy('date', 'asc')->get();

                $i = 1;

                foreach ($r_details as $book_key => $book_val) {

                    $r_details[$book_key]['date_time'] = Carbon::parse($book_val['created_at'])->format('m/d/Y h:i a');
                    $r_details[$book_key]['date'] = date('m/d/y',strtotime($book_val['date']));

                    foreach ($book_val['Booking_details'] as $book_details) {

                        $r_details[$book_key]['services'] .= $book_details['service_type']['title'] . ', ';

                        if(! empty($book_details['emp_id'])) {
                            $expert = User::select('id','name as first_name', 'last_name', 'nick_name')->where('id', $book_details['emp_id'])->first();
                            $exp_person = trim($expert['first_name']. ' ' . $expert['last_name']);
                            $exp_person_nickname = trim($expert['nick_name']);
                            $exp_person_id = $expert['id'];
                            // if ($saloon['user_id']==$expert['id']){
                            //     $exp_person = trim($expert['first_name']. ' ' . $expert['last_name'])." (Owner)";
                            //     $exp_person_nickname = trim($expert['nick_name'])." (Owner)";
                            // }
                        } else {
                            $exp_person = 'No preference';
                            $exp_person_nickname = 'No preference';
                            $exp_person_id = 0;
                        }

                        $book_details['service_expert'] = $exp_person;
                        $book_details['service_expert_id'] = $exp_person_id;
                        if($exp_person_nickname == NULL || empty($exp_person_nickname)){
                            $book_details['service_expert_nickname'] = $expert['first_name'];
                        }
                        else{
                            $book_details['service_expert_nickname'] = $exp_person_nickname;
                        }

                        if ($book_details['discount_type']==null){
                            $book_details['discount_type'] = "";
                        }
                        if ($book_details['discount_value']==null){
                            $book_details['discount_value'] = "";
                        }
                    }

                    $r_details[$book_key]['services'] = trim($r_details[$book_key]['services'], ', ');

                    array_walk_recursive($book_val, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });

                    $i++;
                }

                $response["Result"] = 1;
                $response['Message'] = "Success";
                $response['details'] = $r_details;

                return response($response, 200);

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function humReadable($wt) {

        if($wt <= 0) return '0 min';

        $hours = floor($wt / 60);
        $minutes = $wt % 60;

        $wait_string = '';

        if($hours > 0) {
            $wait_string .= $hours . ' hr ';
        }

        $wait_string .= $minutes . ' min';

        return $wait_string;

    }

    public function generate_pdf(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'booking_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'generate_pdf');

                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $data['user'] =$loginUser;
                $data['booking'] = Booking::with('Users')->with('Booking_details')->with('Booking_details.Service_type')->with('Saloon.Saloon_services')->where('id', $request['booking_id'])->where('status', 'completed')->first();
                if(!empty($data['booking']))
                {
                    $url = 'pdf/'.$data['booking']['booking_name'].'.pdf';
                    $html = view('admin.report',$data)->render();
                    $this->pdf->load($html)->filename(base_path($url))->output();

                    $response["Result"] = 1;
                    $response["Pdf"] =  url($url);
                    $response['Message'] = "Success";
                    return response($response, 200);
                }
                else
                {
                    $response["Result"] = 0;
                    $response["Pdf"] =  "";
                    $response['Message'] = "Invalid booking";
                    return response($response, 200);
                }

            }
            else{
                $response["Result"] = 0;
                $response["Pdf"] =  "";
                $response['Message'] = "Invalid booking";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function update_promotion_details(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'update_promotion_details');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $input = $request->all();
                    $saloon->update($input);
                }

                $saloon = saloon::where('id', $salon_id)->first();

                $response["Result"] = 1;
                $response["saloons"] = $saloon;
                $response['Message'] = "Promotion added successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function service_list(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'service_list');
                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)->first();
                $get_total_services = Saloon_services::where('saloon_id',$saloon->id)->count();


                if (isset($request['category_id'])){ // 1,2
                    $category_id = explode(',',$request['category_id']);
                    $type = Service_type::whereIn('category_id',$category_id)->where('status','active')->orderBy('title','ASC')->get();
                }
                else{
                    $type = Service_type::where('status','active')->orderBy('title','ASC')->get();
                }

                foreach ($type as $k => $v){
                    if ($saloon){
                        $get_service  = Saloon_services::where('saloon_id',$saloon->id)->where('type_id',$v['id'])->get();
                        if ($get_service->count()>0){
                            $type[$k]['is_check'] = 1;
                        }
                        else{
                            $type[$k]['is_check'] = 0;
                        }
                        $type[$k]['Salon_service'] = $get_service;

                    }
                }

                $array = collect($type)->sortBy('is_check')->reverse()->toArray();

                $response["Result"] = 1;
                $response["total_check"] = $get_total_services;
                $response["service"] = array_values($array);
                $response['Message'] = "Salon added successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_salon_step1(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'title' => 'required',
                'description' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'location' => 'required',
                //'timezone' => 'required',
                //'website_url' => 'required',
                'global_discount' => 'required',
                //'promotional_title' => 'required',
                //'is_promo_display' => 'required',
                //'image'=>'required|mimes:jpeg,bmp,png',
                'saloon_phone' => 'required|numeric',
            ],
                [
                    'saloon_phone.required' => 'The salon field is required',
                    'saloon_phone.numeric' => 'The salon field must be numeric.'
                ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_salon_step1');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);

                /* CHECK ALREADY HAVE SALON AND CAN ADD SALON IF PLAN IS PURCHASED */
                $plan = 0;
                if ($appuser['admin_membership_plan'] != null){
                    $plan = $appuser['admin_membership_plan'];
                }
                else{
                    $plan = $appuser['membership_plan'];
                }

                if ($plan==0) {
                    $response["Result"] = 9;
                    $response['Message'] = "You can not add salon.";
                    return response($response, 200);
                }

                if ($plan==1 || $plan==2 || $plan==3){
                    $getmysalon = saloon::where('user_id',$appuser->id)->first();
                    if ($getmysalon){
                        $response["Result"] = 9;
                        $response['Message'] = "You can not add more then 1 salon.";
                        return response($response, 200);
                    }
                }

                /*--------------------------------------------------------*/

                $input = $request->all();
                if($input['global_discount'] == 1)
                {
                    $this->validate($request, [
                        'promotional_title' => 'max:135',
                    ]);
                }

                $input['user_id'] = $appuser->id;
                if($photo = $request->file('image'))
                {
                    $input['image'] = $this->image($photo,'saloon');
                }
                if(isset($request['is_promo_display']))
                {
                    $input['is_promo_display'] = '1';
                }
                else
                {
                    $input['is_promo_display'] = '0';
                }
                $input['saloon_phone'] = $request['countries'].$request['saloon_phone'];
                $input['status'] = "active";
                $input['reg_step'] = 1;
                $input['code'] = rand(100000,999999);

                /* timezone code */
                $latitude = $request['latitude'];
                $longitude = $request['longitude'];
                $time = time();
                $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$latitude,$longitude&timestamp=$time&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $responseJson = curl_exec($ch);
                curl_close($ch);

                $response1 = json_decode($responseJson);
                $input['timezone'] = $response1->timeZoneId;

                $saloon = saloon::create($input);


                /* ASSIGN SALON TO SALON OWNER */

                $inp['saloon_id'] = $saloon->id;
                $appuser->update($inp);

                /* ADD SALON EMPLOYEE */

                $inp['user_id'] = $appuser->id;
                $inp['status'] = "accept";
                $inp['is_salon_owner'] = 1;
                saloon_employees::create($inp);

                Activity::log([
                    'contentId'   => $saloon->id,
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Inserted by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon Add',
                ]);

                $get_salon = saloon::where('id',$saloon->id)->first();

                if (file_exists($get_salon['image'])) {
                    $get_salon['image'] = url($get_salon['image']);
                } else {
                    $get_salon['image'] = '';
                }


                /* CATEGORY LISTING */
                $category = Category::where('status','active')->orderBy('displayorder','ASC')->get();
                $salon_services = Saloon_services::where('saloon_id',$get_salon['id'])->pluck('type_id');
                $service_type = Service_type::wherein('id',$salon_services)->groupBy('category_id')->pluck('category_id')->toArray();

                foreach ($category as $val) {
                    $val['is_check'] = 0;
                    if (in_array($val['id'],$service_type))
                    {
                        $val['is_check'] = 1;
                    }

                    if (file_exists($val['image']) && $val['image']!="") {
                        $val['image'] = url($val['image']);
                    } else {
                        $val['image'] = '';
                    }

                    array_walk_recursive($val, function (&$item, $key) {
                        $item = null === $item ? '' : $item;
                    });
                }
                /* ---------------- */

                $response["Result"] = 1;
                $response["saloons"] = $get_salon;
                $response["Category"] = $category;
                $response['Message'] = "Salon added successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_salon_step2(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'service_details' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_salon_step2');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $salon = saloon::where('id',$salon_id)->first();
                if ($salon['reg_step'] < 2){
                    $reg['reg_step'] = 2;
                    $salon->update($reg);
                }

                $service = Saloon_services::where('saloon_id',$salon->id);
                if($service) {
                    $service->forceDelete();
                }

                $serice_json = \GuzzleHttp\json_decode($request['service_details']);
                $salon_service = array();
                foreach($serice_json as $s_json)
                {
                    $input['saloon_id'] = $salon->id;
                    $input['type_id'] = $s_json->type_id;
                    $input['charges'] = $s_json->charges;
                    $input['duration'] = $s_json->duration;
                    $input['avail_discount'] = $s_json->avail_discount;
                    if ($s_json->avail_discount==1){
                        $discount_amount = $s_json->discount;
                    }
                    else{
                        $discount_amount = 0;
                    }
                    $input['discount'] = $discount_amount;
                    $salon_service[] = Saloon_services::create($input);
                }

                Activity::log([
                    'contentId'   => $appuser->id,
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Salon service add by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon service add',
                ]);



                $get_salon = saloon::where('id',$salon->id)->first();
                if (file_exists($get_salon['image'])) {
                    $get_salon['image'] = url($get_salon['image']);
                } else {
                    $get_salon['image'] = '';
                }

                $response["Result"] = 1;
                $response["Salon_service"] = $salon_service;
                $response["Salon"] = $get_salon;
                $response['Message'] = "Salon services add successfully!";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_salon_step3(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'work_day' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_salon_step3');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $salon = saloon::where('id',$salon_id)->first();
                if ($salon['reg_step'] < 3){
                    $reg['reg_step'] = 3;
                    $salon->update($reg);
                }
                $work = work_days::where('saloon_id',$salon->id);
                if($work) {
                    $work->forceDelete();
                }

                $workday_json = \GuzzleHttp\json_decode($request['work_day']);
                $salon_work = array();
                foreach($workday_json as $wd_json)
                {
                    $input['saloon_id'] = $salon->id;
                    $input['work_day'] = $wd_json->work_day;
                    $input['start_hour'] = $wd_json->start_hour;
                    $input['end_hour'] = $wd_json->end_hour;
                    $salon_work[] = work_days::create($input);
                }

                Activity::log([
                    'contentId'   => $appuser->id,
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Salon work days add by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon work days add',
                ]);

                $get_salon = saloon::where('id',$salon->id)->first();
                if (file_exists($get_salon['image'])) {
                    $get_salon['image'] = url($get_salon['image']);
                } else {
                    $get_salon['image'] = '';
                }

                $response["Result"] = 1;
                $response["Salon_workday"] = $salon_work;
                $response["Salon"] = $get_salon;
                $response['Message'] = "Salon work days add successfully!";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_salon_step3(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'get_salon_step3');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $salon = saloon::where('id',$salon_id)->first();

                $day_collection = collect();
                $timestamp = strtotime('next Monday');
                $days = array();
                for ($i = 0; $i < 7; $i++) {
                    $day = strftime('%A', $timestamp);
                    $timestamp = strtotime('+1 day', $timestamp);

                    $work_days_selected = work_days::where('saloon_id',$salon->id)->where('work_day',$day)->first();
                    if (empty($work_days_selected)){
                        $arr = [
                            'work_day'=>strtolower($day),
                            'start_hour'=>'',
                            'end_hour'=>'',
                            'saloon_id'=>$salon->id,
                            'is_check'=>0,
                        ];
                    }
                    else{
                        $arr = [
                            'work_day'=>strtolower($day),
                            'start_hour'=>$work_days_selected->start_hour,
                            'end_hour'=>$work_days_selected->end_hour,
                            'saloon_id'=>$salon->id,
                            'is_check'=>1,
                        ];
                    }

                    $day_collection->push($arr);
                }

                $work_days = $day_collection->all();

                $get_salon = saloon::where('id',$salon->id)->first();
                if (file_exists($get_salon['image'])) {
                    $get_salon['image'] = url($get_salon['image']);
                } else {
                    $get_salon['image'] = '';
                }

                $response["Result"] = 1;
                $response["Salon_workday"] = $work_days;
                $response["Salon"] = $get_salon;
                $response['Message'] = "Salon work days";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_salon_step4(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
                //'counter' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'add_salon_step4');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $salon = saloon::where('id',$salon_id)->first();
                if ($salon['reg_step'] < 4){
                    $reg['reg_step'] = 4;
                    $salon->update($reg);
                }
                $prev_ids = SalonImages::where('salon_id',$salon->id)->pluck('id')->all();
                $existing_ids = [];

                if (isset($request['counter'])){
                    for($i=1; $i<=$request['counter'];$i++)
                    {
                        /*Type Check*/
                        $media_id = $request->input('image_'.$i);
                        $existing_ids[] = $media_id;

                        if($request['type_'.$i] == 1)
                        {
                            if ($photo = $request->file('image_'.$i)) {
                                $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();
                                /*Image Resize Code*/
                                $img_resize = Image::make($photo->getRealPath());
                                $data = getimagesize($photo);
                                $width = $data[0];
                                if($width > 500){
                                    $img_resize->resize(null, 500, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                }

                                $img_resize->save(public_path('resource/product/saloon/'.$img_name));
                                /*-------------*/
                                $input['salon_id'] = $salon->id;
                                //$input['image_blob'] = $img_resize;
                                $input['image'] = "public/resource/product/saloon/".$img_name;

                                /* THUMBNAIL*/
                                $path1 =  $this->image($photo,'product/saloon/thumbnail');
                                if($width > 200){
                                    $input['image_thumbnail'] = $this->Imagethumbnail($path1,'product/saloon/thumbnail',200,200,null);
                                }
                                else{
                                    $input['image_thumbnail'] = $path1;
                                }
                                $count_display_order = SalonImages::where('salon_id',$salon->id)->count();
                                if($count_display_order >= 0)
                                {
                                    $c = stripslashes($count_display_order);
                                    $c = $c + 1;
                                }
                                $input['displayorder'] = $i;
                            }
                            SalonImages::create($input);
                        }
                        else
                        {
                            $get_img = SalonImages::where('id',$request['image_'.$i])->first();
                            $i_disorder['displayorder'] = $i;
                            $get_img->update($i_disorder);
                        }
                    }
                }

                foreach ($prev_ids as $id)
                {
                    if (!in_array($id, $existing_ids))
                    {
                        $salon_media = SalonImages::findOrFail($id);
                        if(file_exists($salon_media->image)){
                            unlink($salon_media->image);
                        }
                        $salon_media->forceDelete();
                    }
                }

                $salon_image = SalonImages::where('salon_id',$salon->id)->get();
                foreach ($salon_image as  $img){
                    //$img['image_blob'] = "".base64_encode($img['image_blob']);
                    $img['image'] = url($img['image']);
                    $img['image_thumbnail'] = url($img['image_thumbnail']);
                }

                Activity::log([
                    'contentId'   => $appuser->id,
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Salon images add by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon images add',
                ]);

                $get_salon = saloon::where('id',$salon->id)->first();
                if (file_exists($get_salon['image'])) {
                    $get_salon['image'] = url($get_salon['image']);
                } else {
                    $get_salon['image'] = '';
                }

                $response["Result"] = 1;
                $response["Salon_images"] = $salon_image;
                $response["Salon"] = $get_salon;
                $response['Message'] = "Salon image add successfully!";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function edit_salon(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
                //'image'=>'mimes:jpeg,bmp,png',
                'saloon_phone' => 'numeric',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'edit_salon');
                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $salon_id = $request['salon_id'];
                $salon = saloon::where('id', $salon_id)->first();
                if (empty($salon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }

                $appuser = User::findorFail($loginUser->user_id);

                $input = $request->all();
                if(isset($input['global_discount']) && $input['global_discount']== 1)
                {
                    $this->validate($request, [
                        'promotional_title' => 'max:135',
                    ]);
                }
                if($photo = $request->file('image'))
                {
                    $input['image'] = $this->image($photo,'saloon');
                }

                if (isset($request['saloon_phone'])){
                    $input['saloon_phone'] = $request['countries'].$request['saloon_phone'];
                }

                if (isset($request['latitude']) && $request['longitude']) {
                    $latitude = $request['latitude'];
                    $longitude = $request['longitude'];
                    $time = time();
                    $url = "https://maps.googleapis.com/maps/api/timezone/json?location=$latitude,$longitude&timestamp=$time&key=AIzaSyAJrk4aqnQXgCBWMMhJnPoXyzTMd6qucjA";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $responseJson = curl_exec($ch);
                    curl_close($ch);

                    $response1 = json_decode($responseJson);
                    $input['timezone'] = $response1->timeZoneId;
                }

                $salon->update($input);

                Activity::log([
                    'contentId'   => $salon->id,
                    'contentType' => 'salon management',
                    'action'      => 'Update',
                    'description' => 'Updated by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon Update',
                ]);


                if (file_exists($salon['image'])) {
                    $salon['image'] = url($salon['image']);
                } else {
                    $salon['image'] = '';
                }

                $response["Result"] = 1;
                $response["saloons"] = $salon;
                $response['Message'] = "Salon updated successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function edit_salon_image(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'image_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'edit_salon_image');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                $get_image = SalonImages::where('id',$request['image_id'])->first();

                if ($photo = $request->file('image')) {

                    /* IMAGE */
                    $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

                    /*Image Resize Code*/
                    $img_resize = Image::make($photo->getRealPath());
                    $data = getimagesize($photo);
                    $width = $data[0];
                    if($width > 500){
                        $img_resize->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }

                    $img_resize->save(public_path('resource/product/saloon/'.$img_name));
                    /*-------------*/
                    //$media['image_blob'] = $img_resize;
                    $imagedata['image'] = "public/resource/product/saloon/".$img_name;

                    /* THUMBNAIL*/

                    $path1 =  $this->image($photo,'product/saloon/thumbnail');
                    if($width > 200){
                        $media['image_thumbnail'] = $this->Imagethumbnail($path1,'product/saloon/thumbnail',200,200,null);
                    }
                    else{
                        $media['image_thumbnail'] = $path1;
                    }
                }



                $get_image['updated_at'] = Carbon::now();
                $get_image->update($media);

                Activity::log([
                    'contentId'   => $request['image_id'],
                    'contentType' => 'salon management',
                    'action'      => 'Update',
                    'description' => 'Updated by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon Image Update',
                ]);

                /* GET SALON DETAILS */
                $response["Result"] = 1;
                $response['Message'] = "Salon image updated successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function delete_salon_image(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'image_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'delete_salon_image');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);
                SalonImages::where('id',$request['image_id'])->delete();

                Activity::log([
                    'contentId'   => $request['image_id'],
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Inserted by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon Add',
                ]);

                /* GET SALON DETAILS */
                $response["Result"] = 1;
                $response['Message'] = "Salon image delete successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function image_displayorder(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'image_displayorder');
                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);

                $img_id = explode(',',$request['image_id']);

                $count = 1;
                foreach ($img_id as $idval) {
                    $advertisement = SalonImages::findOrFail($idval);
                    $input['displayorder'] = $count;
                    $advertisement->update($input);
                    $count ++;
                }

                Activity::log([
                    'contentId'   => $request['image_id'],
                    'contentType' => 'salon management',
                    'action'      => 'Insert',
                    'description' => 'Inserted by '.$appuser->id.'('.$appuser->name.')',
                    'details'     => 'Salon Add',
                ]);

                /* GET SALON DETAILS */
                $response["Result"] = 1;
                $response['Message'] = "Displayorder change successfully!";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function salon_user_list(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'salon_user_list');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $get_booking_users = Booking::where('saloon_id',$saloon->id)->where('status','completed')->groupBy('user_id')->pluck('user_id');
                    $users = User::whereIn('id',$get_booking_users)->where('status','active')->orderBy('name','ASC')->get();
                    foreach ($users as $k => $v){
                        if ($v['image'] == '') {
                            $v['image'] = "";
                        } else {
                            $v['image'] = url($v['image']);
                        }

                        $get_point = Point_history::where('user_id',$v['id'])->where('salon_id',$saloon->id)->orderBy('id','DESC')->first();
                        $users[$k]['total_point'] = !empty($get_point['point_after'])?$get_point['point_after']:0;

                        array_walk_recursive($v, function (&$item, $key) {
                            $item = null === $item ? '' : $item;
                        });
                    }
                }

                array_walk_recursive($users, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response["Users"] = $users;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function user_booking_list(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'user_id' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'user_booking_list');

                return response($response, 200);
            }

            if (!empty($loginUser)) {

                $appuser = User::findorFail($loginUser->user_id);
                $salon_id = $request['salon_id'];
                $saloon = saloon::where('id', $salon_id)->first();
                if (empty($saloon)) {
                    $response["Result"] = 0;
                    $response['Message'] = "No Salon is assigned, contact ".config('siteVars.title')." support.";
                    return response($response, 200);
                }
                else
                {
                    $get_bookings = Booking::with('Booking_details')
                        ->with('Booking_details.Service_type')
                        ->where('saloon_id',$saloon->id)->where('user_id',$request['user_id'])
                        ->orderBy('id', 'DESC')
                        ->get();
                }

                $response["Result"] = 1;
                $response["Booking"] = $get_bookings;
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function become_nailmaster(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'salon_name' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'become_nailmaster');

                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $appuser = User::findorFail($loginUser->user_id);

                $get_request = Salon_owner_request::where('user_id',$appuser['id'])->where('status',0)->first();
                if ($get_request) {
                    $response["Result"] = 0;
                    if (isset($request['is_freelancer']) && $request['is_freelancer'] == 1){
                        $response['Message'] = "You have already sent a request to become a freelancer!";
                    }
                    else{
                        $response['Message'] = "You have already sent a request to become a salon owner!";
                    }

                    return response($response, 200);
                }

                $input = $request->all();
                $input['user_id'] = $appuser['id'];
                Salon_owner_request::create($input);


                /* ------------ SEND PAYMENT LINK TO USER ------------ */
                $become_nailmaster = Email::findorFail(4);

                /* EMAIL CODE */
                $subject = $become_nailmaster['subject'];
                $from1 = $become_nailmaster['email'];

                $link = "<a href=".config('siteVars.siteurl')."api/payment/".$request['remember_token']." target='_blank'>".config('siteVars.siteurl')."api/payment/".$request['remember_token']."</a>";
                $string = ["{link}"];
                $replace_string = [$link];
                $data['content'] = str_replace($string,$replace_string,$become_nailmaster["content"]);
                $em= $appuser['email'];
                $bcc = [];

                $data['title'] = $become_nailmaster['title'];

                if ($appuser['email']!="" || $appuser['email']!=null){
                    Mail::send('admin.email_template',$data, function ($m) use ($from1,$subject,$em, $bcc) {
                        $m->from($from1, config('siteVars.title'));
                        $m->bcc($bcc);
                        $m->to($em)->subject($subject);
                    });
                }

                /* SMS CODE */
                if ($appuser->phone!="" || $appuser->phone!=null){
                    $sms_text = strip_tags($data['content']);
                    if(substr($appuser->phone, 0, 1) == 1) {
                        $clickatell = new \Clickatell\Rest();
                        $other_country = 'no';
                        $datas = ['to' => [$appuser['phone']], 'from' => '15732073611', 'content' => $sms_text];
                    } else {
                        $clickatell = new \Clickatell\Rest('india');
                        $other_country = 'yes';
                        $datas = ['to' => [$appuser['phone']], 'content' => $sms_text];
                    }
                    $result = $clickatell->sendMessage($datas,$other_country);
                }

                $response["Result"] = 1;

                if (isset($request['is_freelancer']) && $request['is_freelancer'] == 1) {
                    $response["Message"] = "Our support staff will contact you via your registered E-mail and/or mobile phone number via SMS message.";
                }else{
                    $response["Message"] = "Thank you for becoming a Nail Master Member, our support staff will contact you via registered Email or SMS for further process.";
                }
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }


    public function category(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                //'remember_token' => 'required',
                'salon_id' => 'required',
            ]);

//            if ($validator->fails()) {
//                $response["Result"] = 0;
//                $response["Message"] = implode(',', $validator->errors()->all());
//                return response($response, 200);
//            }

//            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();
//
//            if (empty($loginUser) || $request['remember_token'] == "") {
//                $response["Result"] = 9;
//                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
//
//                $input = $request->all();
//                $this->errorlog($response["Message"],json_encode($input),'become_nailmaster');
//
//                return response($response, 200);
//            }

            //if (!empty($loginUser)) {

            $category = Category::with('subcategory')->where('status','active')->orderBy('displayorder','ASC')->get();
            $salon_services = Saloon_services::where('saloon_id',$request['salon_id'])->pluck('type_id');
            $service_type = Service_type::wherein('id',$salon_services)->groupBy('category_id')->pluck('category_id')->toArray();

            foreach ($category as $val) {

                $val['is_check'] = 0;
                if (in_array($val['id'],$service_type))
                {
                    $val['is_check'] = 1;
                }

                if ($request['salon_id']==0){
                    $val['is_check'] = 1;
                }

                if (file_exists($val['image']) && $val['image']!="") {
                    $val['image'] = url($val['image']);
                } else {
                    $val['image'] = '';
                }

                if (file_exists($val['icon']) && $val['icon']!="") {
                    $val['icon'] = url($val['icon']);
                } else {
                    $val['icon'] = '';
                }

                array_walk_recursive($val, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

            }


            /* SERVICE LISTING */
            $service_type = Service_type::with('category')->where('status','active')->orderBy('title','ASC')->get();
            foreach ($service_type as $val){
                if (file_exists($val['category']['icon']) && $val['category']['icon']!="") {
                    $val['icon'] = url($val['category']['icon']);
                } else {
                    $val['icon'] = '';
                }
            }

            $response["Result"] = 1;
            $response["Category"] = $category;
            $response["Services"] = $service_type;
            $response["Message"] = "Success";
            return response($response, 200);
            //}

        } catch (Exception $e) {
            return response("", 500);
        }
    }


    public function salon_report(Request $request,$salon_id){


        /*===============total today booking and earning===================== */
        $today = Carbon::now();
        $date=$today->toDateString();
        $booking = Booking::where('saloon_id',$salon_id)->wheredate('date',$date)->where('status','completed')->get();
        $data['today_booking_today'] = Booking::where('saloon_id',$salon_id)->wheredate('date',$date)->where('status','completed')->count(); // TODAY BOOKING

        $total_earning_today = 0;
        foreach ($booking as $book){
            $total_earning_today+=$book['final_total'];
        }
        $data['total_earning_today']=$total_earning_today;    // TODAY EARNING



        /*===============total month booking and earning===================== */
        $current_month = $today->month;
        $data['total_booking_monthly'] = Booking::where('saloon_id',$salon_id)
            ->where('status','completed')
            ->whereMonth('created_at',$current_month)->count();  // MONTHLY BOOKING

        $booking_monthly = Booking::where('saloon_id',$salon_id)->where('status','completed')->whereMonth('created_at',$current_month)->orderBy('date')->get();

        $total_earning_monthly=0;
        $bids = "";

        foreach ($booking_monthly as $book_month){
            $total_earning_monthly+=$book_month['final_total'];
            $bids .= $book_month['id'].",";
        }
        $booking_ids = explode(',',rtrim($bids,','));
        $data['total_earning_monthly']=$total_earning_monthly;   // MONTHLY BOOKING EARNING


        /* ============== Employee LISTING ================================== */

        $employee_list = saloon_employees::where('saloon_id',$salon_id)->where('status','accept')->get();
        foreach ($employee_list as $key =>$val){
            $user = User::where('id',$val['user_id'])->first();
            $employee_list[$key]['employee_name'] = $user['name'];

            /* Employee monthly booking counter */
            $emp_booking_count = Booking_details::select('booking_id')->whereIn('booking_id',$booking_ids)->where('emp_id',$val['user_id'])->distinct()->get()->count();

			if($emp_booking_count > 0){
				$employee_list[$key]['employee_booking_count'] = $emp_booking_count;
    	        $employee_list[$key]['total_percentage'] = number_format((float)($emp_booking_count / $data['total_booking_monthly'])*100, 2, '.', '');	
			}
			else{
	            $employee_list[$key]['employee_booking_count'] = "0";
				$employee_list[$key]['total_percentage'] = "0.00";
			}
			
            $employee_list[$key]['salon_id'] = $salon_id;
        }

        $data['employee_list'] = $employee_list;   // EMPLOYEE LISTING


        /* CHART */

        $today = Carbon::now();
        $chart_labels = [];
        $from = Carbon::now()->startOfMonth();

        $chart_data = [];
        while ($from <= $today)
        {
            $c_date = $from->format('Y-m-d');
            $key = $from->format("j")."-";
            $chart_labels[] = $key;
            $chart_data[$key] = 0;
            $from->addDay(1);

            $booking_total_per_day = Booking::select('final_total')->where('saloon_id',$salon_id)->whereDate('date','=',$c_date)->where('status','completed')->get();
            $book_total =0;
            foreach ($booking_total_per_day as $bs){
                $book_total += $bs['final_total'];
            }
            $chart_data[$key] = number_format((integer)$book_total, 0, '.', '');
        }

        $original_arr = array();
        foreach ($chart_data as $k => $v){
            $new_array = [sprintf("%02d",$k),$v];
            array_push($original_arr, $new_array);
        }

        $data['chart_data'] = $original_arr;

        return view('website.salon_report',$data);
    }

    public function employee_report(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
                'emp_id' => 'required',
                'salon_id' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = Login_details::where("remember_token", $request['remember_token'])->first();

            if (empty($loginUser) || $request['remember_token'] == "") {
                $response["Result"] = 9;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";

                $input = $request->all();
                $this->errorlog($response["Message"],json_encode($input),'user_booking_list');

                return response($response, 200);
            }

            if (!empty($loginUser)) {
                $input = $request->all();
                $employee_id = $input['emp_id'];
                $salon_id = $input['salon_id'];

                if (isset($request['start_date']) && isset($request['end_date'])){
                    $start_date = date('Y-m-d H:i:s',strtotime($request['start_date']));
                    $end_date = date('Y-m-d H:i:s',strtotime($request['end_date']));
                }
                else{
                    $start_date = Carbon::today()->addDay(-30);
                    $end_date = Carbon::now();
                }

                $start_date_formated = date('m/d/Y',strtotime($start_date));
                $end_date_formated = date('m/d/Y',strtotime($end_date));

                $chart_data = [];
                $cnt = 0;
                $total_earning = 0;
                while ($start_date <= $end_date)
                {
                    $c_date = date('Y-m-d',strtotime($start_date));
                    $key = date('j',strtotime($start_date));

                    $start_date = date('Y-m-d H:i:s', strtotime($start_date . ' +1 day'));

                    $book = DB::table('bookings')
                        ->join('booking_details', 'bookings.id', '=', 'booking_details.booking_id')
                        ->select('bookings.*')
                        ->where('bookings.saloon_id',$salon_id)
                        ->whereDate('bookings.date','=',$c_date)
                        ->where('bookings.status','completed')
                        ->sum('bookings.final_total');

                    $chart_data[$cnt]["date"] = sprintf("%02d",$key);
                    $chart_data[$cnt]["day"] = date('l',strtotime($start_date));
                    $chart_data[$cnt]["earning"] = "$".number_format($book, 2);

                    $total_earning +=$book;

                    $cnt++;
                }

                /* ----------- GET EMPLOYEE NAME -------------- */
                $emp_name  = User::select('name')->where('id',$employee_id)->first();

                /* ----------- DISPLAY DATE ----------------*/
                $display_date = date('D, F d, Y',strtotime($request['start_date']))." - ".date('D, F d, Y',strtotime($request['end_date']));

                /* ----------- SALON DETAILS ---------------- */

                $salon = saloon::where('id',$salon_id)->first();

                $response["Result"] = 1;
                $response["Days"] = $chart_data;
                $response["Total_earning"] =  "$".number_format($total_earning, 2);
                $response["Start_date"] = $start_date_formated;
                $response["End_date"] = $end_date_formated;
                $response["Employee_name"] = $emp_name['name'];
                $response["Display_date"] = $display_date;
                $response["Salon"] = $salon;
                $response['Print_date_time'] = date('l, M d, Y, h:i A');
                $response["Message"] = "Success";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }
}
