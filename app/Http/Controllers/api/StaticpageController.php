<?php

namespace App\Http\Controllers\api;

use App\saloon;
use App\Staticpage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticpageController extends Controller
{
    public function terms_and_condition()
    {
        $static=Staticpage::findOrFail(1);
        return response($static->content, 200);
    }

    public function loyalty()
    {
        $static=Staticpage::findOrFail(2);
        return response($static->content, 200);
    }

    public function salon_long_description($id){
        $salon=saloon::findOrFail($id);
        return response($salon->description, 200);
    }

    public function user_manual(){
        $static=Staticpage::findOrFail(3);
        return response($static->content, 200);
    }

    public function salon_manual(){
        $static=Staticpage::findOrFail(4);
        return response($static->content, 200);
    }


}
