<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title','description','image','status','icon','displayorder',
    ];

    protected $hidden = [
        'deleted_at','created_at','updated_at','status',
    ];

        const STATUS_ACTIVE = 'active';
        const STATUS_INACTIVE = 'in-active';
    
        public static $status = [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'In Active',
        ];
    public function Subcategory()
    {
        return $this->hasMany('App\Subcategory','category_id');
    }
}
