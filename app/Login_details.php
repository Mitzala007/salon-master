<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login_details extends Model
{
    protected $fillable = [
        'user_id','remember_token','login_time','login_ip','device_type','device_token','version_no','build_no',
    ];

    const Insert = 'Insert';
    const Update = 'Update';
    const Delete = 'Delete';

    public static $action = [
        self::Insert => 'Insert',
        self::Update => 'Update',
        self::Delete => 'Delete',
    ];

    const User = 'users';
    const Saloon = 'saloons';
    const Booking = 'bookings';
    const Employee = 'employees';
    const Notification = 'notifications';
    const Service_type = 'Service Types';

    public static $section = [
        self::User => 'users',
        self::Saloon => 'saloons',
        self::Booking => 'bookings',
        self::Employee => 'employees',
        self::Notification => 'notifications',
        self::Service_type => 'Service Types',
    ];

    public function saloon_master()
    {
        return $this->belongsTo('App\User', 'user_id');
    }



}
