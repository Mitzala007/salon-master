<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking_details extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'booking_id','type_id','emp_id','duration','discount_type','discount_value','charges','total_charge',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at',
    ];

    public function Booking()
    {
        return $this->belongsTo('App\Booking','booking_id');
    }

    public function Service_type()
    {
        return $this->belongsTo('App\Service_type','type_id');
    }

    public function Service_expert()
    {
        return $this->belongsTo('App\Employees','emp_id');
    }

}
