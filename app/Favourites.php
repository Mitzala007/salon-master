<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourites extends Model
{
    protected $fillable = [
        'saloon_id','user_id',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    public function Saloon()
    {
        return $this->belongsTo('App\saloon', 'saloon_id');
    }


}
