<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class saloon extends Model
{
    use SoftDeletes;
    protected $casts = [
        'is_promo_display' => 'integer',
        'is_online' => 'integer',
        'loyalty_program' => 'integer',
        'tax' => 'string',
    ];
    protected $fillable = [
        'title','status','displayorder','description','location','user_id',
        'latitude','longitude','timezone','image','saloon_phone','address1','address2',
        'global_discount','rating','reg_step','is_display','code','website_url','promotional_title','is_promo_display','is_online','loyalty_program','tax',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    const WORK_MONDAY = 'monday';
    const WORK_TUESDAY = 'tuesday';
    const WORK_WEDNESDAY = 'wednesday';
    const WORK_THURSDAY = 'thursday';
    const WORK_FRIDAY = 'friday';
    const WORK_SATURDAY = 'saturday';
    const WORK_SUNDAY = 'sunday';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public static $global_discount = [
        1 => 'Yes',
        0 => 'No',
    ];

    public static $work_days = [
        self::WORK_MONDAY => 'Monday',
        self::WORK_TUESDAY => 'Tuesday',
        self::WORK_WEDNESDAY => 'Wednesday',
        self::WORK_THURSDAY => 'Thursday',
        self::WORK_FRIDAY => 'Friday',
        self::WORK_SATURDAY => 'Saturday',
        self::WORK_SUNDAY => 'Sunday',
    ];

    public static $duration_pin = [
        0 => 'pin_0',
        5 => 'pin_5',
        10 => 'pin_10',
        15 => 'pin_15',
        20 => 'pin_20',
        30 => 'pin_30',
        45 => 'pin_45',
        60 => 'pin_1h',
        75 => 'pin_1h15',
        90 => 'pin_1h30',
        105 => 'pin_1h45',
        120 => 'pin_2h',
        150 => 'pin_2h30',
        180 => 'pin_3h',
        210 => 'pin_3h30',
        240 => 'pin_4h',
        300 => 'pin_5h',
        360 => 'pin_6h',
        420 => 'pin_7h',
        480 => 'pin_8h',
        540 => 'pin_9h',
        600 => 'pin_10h',
        660 => 'pin_11h',
        720 => 'pin_12h',
    ];

    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function Saloon_services()
    {
        return $this->hasMany('App\Saloon_services','saloon_id');
    }

    public function work_days()
    {
        return $this->hasMany('App\work_days','saloon_id');
    }

    public function Employees()
    {
        return $this->hasMany('App\User','saloon_id')->withTrashed();
    }

    public function Favourites()
    {
        return $this->hasMany('App\Favourites','saloon_id');
    }
    
    public function Rating()
    {
        return $this->hasMany('App\Rating','saloon_id');
    }
	
	public function Bookings()
    {
        return $this->hasMany('App\Booking','saloon_id');
    }
    
     public function SalonImages()
    {
        return $this->hasMany('App\SalonImages', 'salon_id');
    }
    
    public function Salon_rewards()
    {
        return $this->hasMany('App\Salon_reward', 'salon_id');
    }
	
	
	

    public static function boot()
    {
        parent::boot();

        self::deleting(function($model) {

            foreach ($model->Saloon_services as $services)
                $services->delete();

            foreach ($model->work_days as $work_day)
                $work_day->delete();

//            foreach ($model->Employees as $employee)
//                $employee->delete();

            foreach ($model->Favourites as $fav)
                $fav->delete();

            foreach ($model->Rating as $rating)
                $rating->delete();

            foreach ($model->Bookings as $booking)
                $booking->delete();
                
            foreach ($model->SalonImages as $images)
                $images->delete();

        });
    }
}
