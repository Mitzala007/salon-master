<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saloon_employees extends Model
{
    protected $fillable = [
        'saloon_id','user_id','status','is_read','is_salon_owner',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at',
    ];

    public function saloon_master()
    {
        return $this->belongsTo('App\saloon', 'saloon_id')->withTrashed();
    }

    public function user_master()
    {
        return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }


    public function saloon()
    {
        return $this->belongsTo('App\saloon', 'saloon_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


}
