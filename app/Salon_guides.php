<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salon_guides extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'file'
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];
}
