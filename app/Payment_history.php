<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_history extends Model
{
    protected $fillable = [
        'user_id','plan_id','invoice_number','subscription_id','amount','customer_profile_id','customer_payment_id',
        'transaction_id','payment_status','subscription_status','payment_method','payment_number'
    ];
}
