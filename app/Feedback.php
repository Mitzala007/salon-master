<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'first_name','last_name','email','saloon_id','user_id','visit_date','comment',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function Saloon()
    {
        return $this->belongsTo('App\saloon','saloon_id');
    }
}