<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'user_id','salon_id','date','start_time','end_time','total_time'
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];
}
