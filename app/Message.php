<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'saloon_id','sender','receiver','type','message','image_thumbnail','is_read','is_last','is_notify',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];
    
    public function User()
    {
        return $this->belongsTo('App\User','sender');
    }

}
