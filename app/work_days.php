<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class work_days extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'saloon_id','work_day','start_hour','end_hour',
    ];

    public function saloon_master()
    {
        return $this->belongsTo('App\saloon', 'saloon_id');
    }

    protected $hidden = [
        'created_at','updated_at','deleted_at',
    ];


}
