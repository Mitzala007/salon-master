<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon_reward extends Model
{
    protected $fillable = [
        'salon_id','name','description','start_date','end_date',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    public function Salon()
    {
        return $this->belongsTo('App\saloon', 'salon_id');
    }
}
