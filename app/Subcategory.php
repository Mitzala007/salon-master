<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    protected $fillable = [
         'category_id','title','status'
    ];

    protected $hidden = [
        'created_at','updated_at','status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];
    public function Category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
}
