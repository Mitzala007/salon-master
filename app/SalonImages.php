<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalonImages extends Model
{
    protected $fillable = [
        'salon_id','image','image_thumbnail','displayorder',
    ];

    public function Salon()
    {
        return $this->belongsTo('App\saloon', 'salon_id');
    }

    protected $hidden = [
        'created_at','updated_at',
    ];

}
