<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $casts = ['additional_amount'=>'string','payment_mode'=>'string'];

    protected $fillable = [
        'user_id','order_id','booking_name','saloon_id','date','time','avail_discount','start_time','end_time','no_of_persons','status','payment_mode',
        'sub_total','point','final_total','discount_amount','additional_amount','tax','remarks','checkin_type','device_type','device_token','is_notify','updated_at',
    ];

    protected $hidden = [
        'updated_at','deleted_at',
    ];

    const WAITING = 'waiting';
    const IN_PROGRESS = 'in-progress';
    const CANSEL = 'cancel';
    const COMPLETED = 'completed';

    public static $status = [
        self::WAITING => 'waiting',
        self::IN_PROGRESS => 'in-progress',
        self::CANSEL => 'cancel',
        self::COMPLETED => 'completed',
    ];

    public static $customer_type = [
        0 => 'New User',
        1 => 'Existing User',
    ];

    public function Saloon()
    {
        return $this->belongsTo('App\saloon','saloon_id');
    }

    public function Type()
    {
        return $this->belongsTo('App\Service_type', 'type_id');
    }

    public function Users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Employee()
    {
        return $this->belongsTo('App\Employees', 'emp_id');
    }

    public function Booking_details()
    {
        return $this->hasMany('App\Booking_details', 'booking_id');
    }

    public static function boot()
    {
        static::deleted(function($model) {
            foreach ($model->Booking_details as $detail)
                $detail->delete();
        });
        parent::boot();
    }

}
