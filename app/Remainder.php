<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remainder extends Model
{
    protected $fillable = [
        'user_id','title','description','remainder_date','remainder_time','is_notify','timezone',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

}
