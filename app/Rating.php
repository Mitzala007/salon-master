<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable=[
        'user_id','saloon_id','booking_id','rating','review',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    public function Saloon()
    {
        return $this->belongsTo('App\saloon', 'saloon_id');
    }
    
    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
