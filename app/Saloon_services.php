<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saloon_services extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'saloon_id','type_id','charges','duration','avail_discount','discount','status',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at','status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

     public static $durations = [
        5 => 5, 10 => 10, 15 => 15, 20=> 20, 25 => 25, 30 => 30, 35 => 35, 40 => 40, 45 => 45, 60 => 60, 90 => 90, 120 => 120
    ];

    public static $discounts = [
        0 => 0, 5 => 5, 10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50
    ];

    public function saloon()
    {
        return $this->belongsTo('App\saloon', 'id');
    }

    public function services_master()
    {
        return $this->belongsTo('App\Service_type', 'type_id');
    }


}
