<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'id','sender','receiver','notification_type','message','is_read','send_to','service_type',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];
    
    const SEND_ALL = 'all';
    const SEND_SUB_ADMIN = 'sub_admin';
    const SEND_USER = 'user';

    public static $send_type = [
        self::SEND_ALL => 'All',
        self::SEND_SUB_ADMIN => 'Salon Owner',
        self::SEND_USER => 'User',
    ];
    
    const SERVICE_ALL = 'both';
    const SERVICE_EMAIL = 'email';
    const SERVICE_PUSH = 'push';

    public static $service = [
        self::SERVICE_ALL => 'Both',
        self::SERVICE_EMAIL => 'Email',
        self::SERVICE_PUSH => 'Push Notification',
    ];
}
