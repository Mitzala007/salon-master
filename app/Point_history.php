<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point_history extends Model
{
    protected $casts = ['point_before'=>'integer','point_after'=>'integer'];

    protected $fillable = [
        'user_id','salon_id','booking_id','type','point_before','point','point_after','remark','is_last',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at',
    ];

    public function Salon()
    {
        return $this->belongsTo('App\saloon','salon_id');
    }

    public function Booking()
    {
        return $this->belongsTo('App\Booking','booking_id');
    }

    const Add = 'CREDIT';
    const Remove = 'DEBIT';

    public static $type = [
        self::Add=> 'Add',
        self::Remove => 'Remove',

    ];
}
