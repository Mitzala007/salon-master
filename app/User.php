<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $casts = ['admin_membership_plan'=>'string','plan_id'=>'string'];

    protected $fillable = [
        'name','last_name','nick_name','birthdate','email','gender',
        'country','state_id','city_id','landmark','addressline_1','addressline_2','zip_code','phone',
        'password','status','role','otp','image','remember_token','facebookid','is_facebook','is_email_verified','saloon_id',
        'is_notification_sms','is_notification_email','is_notification_push','membership_plan','admin_membership_plan','available',
        'latitude','longitude','timezone','security_pin',
    ];

    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','deleted_at','status',
    ];

    /*For Status*/
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];


    /*FOR MAIN ADMIN*/
    const SUPER_ADMIN   = 'super_admin';
    const ROLE_SALOON = 'sub_admin';
    const ROLE_USER   = 'user';

    public static $user_role = [
        self::SUPER_ADMIN => 'Super Admin',
        self::ROLE_SALOON => 'Salon Owner',
        self::ROLE_USER => 'Normal User',
    ];

    /*FOR SUPER ADMIN*/
    const ROLE_SALOON_1 = 'sub_admin';
    const ROLE_USER_1   = 'user';

    public static $user_role_1 = [
        self::ROLE_SALOON_1 => 'Salon Owner',
        self::ROLE_USER_1 => 'Normal User',
    ];

    const MALE    = 'male';
    const FEMALE  = 'female';

    public static $user_gender = [
        self::MALE => 'Male',
        self::FEMALE => 'Female',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /*For Role*/

    /*For Display in Form*/
    public static $access = [
        self::Access_Admin => 'Admin',
        self::Access_subadmin => 'sub_admin',
        self::Access_user => 'User',
    ];

    /*For Entry in Database*/
    const Access_Admin = 'admin';
    const Access_subadmin = 'sub_admin';
    const Access_user = 'User';

    public function saloons()
    {
        return $this->hasOne('App\saloon','user_id');
    }

}
