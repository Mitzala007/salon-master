<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon_owner_request extends Model
{
    protected $fillable = [
        'user_id','salon_name','status',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];
}
