<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service_type extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id','title','category_id','sub_category_id','min_duration','status','displayorder',
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at','status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];


    const MIN_5    = '5';
    const MIN_10  = '10';
    const MIN_15  = '15';
    const MIN_20  = '20';
    const MIN_25  = '25';
    const MIN_30  = '30';
    const MIN_35  = '35';
    const MIN_40  = '40';
    const MIN_45  = '45';
    const MIN_60  = '60';
    const MIN_90  = '90';
    const MIN_120  = '120';

    public static $minutes = [
        self::MIN_5  => '5 Minutes',
        self::MIN_10  => '10 Minutes',
        self::MIN_15  => '15 Minutes',
        self::MIN_20  => '20 Minutes',
        self::MIN_25  => '25 Minutes',
        self::MIN_30 => '30 Minutes',
        self::MIN_35 => '35 Minutes',
        self::MIN_40 => '40 Minutes',
        self::MIN_45 => '45 Minutes',
        self::MIN_60 => '60 Minutes',
        self::MIN_90 => '90 Minutes',
        self::MIN_120 => '120 Minutes',
    ];


    public function services_types()
    {
        return $this->hasMany('App\Saloon_services','type_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function($model) {
            foreach ($model->services_types as $services)
                $services->delete();

        });
    }


}
