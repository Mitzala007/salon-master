<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorlog extends Model
{
    protected $fillable = [
        'api','data','message',
    ];
}
