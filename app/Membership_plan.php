<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership_plan extends Model
{
    protected $fillable = [
        'title','amount','is_recurring','total_recurring_occurance','free_trial','status','description','short_description','displayorder',
    ];

    protected $hidden = [
        'created_at','updated_at',
    ];

    /*For Status*/
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];
}
